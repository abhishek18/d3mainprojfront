import {Injectable} from "@angular/core";
import { HttpClient,HttpParams} from "@angular/common/http";
import {CommonServiceProvider} from "./common-service";
import { Theme } from "src/app/interface";

@Injectable()
export class ThemeItemProvider {

  constructor(
    public http: HttpClient,
    private services: CommonServiceProvider
  ) {
    console.log('Hello ThemeItem Service Provider');
  }
  saveThemeItem(params) {
    return this.http.post(
      this.services.getServices("saveThemeItem"),
      params, );
  }
  getAllThemeItem() {
    return this.http.get(
        this.services.getServices("getAllThemeItem"), );
  }
  updateThemeItem(params:Theme) {
    return this.http.put(
        this.services.getServices("updateThemeItem"), params);
  }
  getAllItem() {
    return this.http.get(
        this.services.getServices("getAllItem"), );
  }
}
