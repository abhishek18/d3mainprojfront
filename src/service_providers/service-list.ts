import { Injectable } from "@angular/core";


@Injectable()
export class ServiceList{
    public explore(){
        return{
            exportToForm : "exportToForm/csv",
            saveThemeItem : "themeItem/saveThemeItem",
            getAllThemeItem: "themeItem/getThemeItem",
            updateThemeItem: "themeItem/updateTheme",
            getAllItem: "themeItem/getAllItem",
        }
    }
}

