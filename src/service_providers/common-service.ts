import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ServiceList } from "./service-list";




const HOST: string = window.location.hostname;
const PORT: string = window.location.port;

@Injectable()
export class CommonServiceProvider{
    public ROOT_URL = ()=> {
        if (PORT == "8100" || PORT == "8101") {
          return "";
        } else {
          //return "http://localhost:8080/DisplayHibernate/"
          return "https://displayhiber.herokuapp.com/"
        }
      }

      constructor(
        public http: HttpClient,
        private services: ServiceList
      ) {}
      
      public getServices(identifier: string){
        let serviceList = this.services.explore();
        return this.ROOT_URL() + serviceList[identifier];
      }  
}