import { Injectable, Injector } from "@angular/core";

@Injectable()
export class JavaService{

    constructor(
        protected injector: Injector,
        //public modalCtrl: ModalController,
    ){

    }

    status(req) {
        var promise = new Promise((resolve, reject) => {
            console.log(JSON.stringify(req));
            
            switch (req.responseStatus) {
                case "success":
                    resolve(req);
                    break;
                case "failure":
                    reject(req.responseStatus);
                    break;
                default: 
                    console.log("inside service.status default switch case >> ");
                    break;
            }
        });
        return promise;
    } 
}