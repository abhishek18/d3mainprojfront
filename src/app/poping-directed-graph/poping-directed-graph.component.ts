import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked, OnChanges } from '@angular/core';
import * as d3 from 'd3';
import POP_CONFIG from './pop.config';



@Component({
  selector: 'app-poping-directed-graph',
  templateUrl: './poping-directed-graph.component.html',
  styleUrls: ['./poping-directed-graph.component.css']
})
export class PopingDirectedGraphComponent implements OnInit, OnChanges, AfterViewChecked {
  
  @ViewChild('popDirectedChartContainer') private chartContainer: ElementRef;
  
  showAppComponent: boolean=false;
  hostElement: any;
  margin: any;
  width: any;
  height: any;
  svg: any;
  simulation:any;
  nodeElements:any;
  linkForce:any;
  linkElements:any;
  textElements:any;
  dragDrop:any;
  drag_handler:any;
  tooltip:any;
  tip_node:any;
  status: string;
  linearX:any;
  zoom: any;
  zoom_handler: d3.ZoomBehavior<Element, {}>;
  g: any;
  transform: d3.ZoomTransform;
  minZoom:any=1/4;
  maxZoom:any=8;
  linkedByIndex = {};

  constructor(
    private elementRef: ElementRef
  ) { 
  }

  ngOnInit() {
    //-------------//
    //this.createChart();

    this.hostElement = this.chartContainer.nativeElement;
    console.log("this.hostElement >> ",this.hostElement);
    this.margin = { top: 1, right: 200, bottom: 1, left: 1};

    this.width = window.innerWidth - this.margin.left - this.margin.right;
    console.log("this.width >> ",this.width);
    this.height = window.innerHeight - this.margin.top - this.margin.bottom;
    this.transform = d3.zoomIdentity; 
  //----------SVG creation----------------------//

  this.svg = d3.select(this.hostElement)
      .append('svg')
      .attr("class", "svg-graph")
      .attr('width', this.width)
      .attr('height', this.height)
      .attr("style", "background: rgb(74,69,69)")

  //----------Zoom SVG and Element-------------------//
  this.g = this.svg.append("g")
  /*.append('svg:path')
  .attr('d', 'M0,0 L0,10 L10,5 z'); //for arrow */
  this.svg.call(d3.zoom()              
                .on("zoom", ()=>{this.g.attr("transform", d3.event.transform);})
                .scaleExtent([this.minZoom, this.maxZoom]))
//---------------------------------------------//                  
    
    this.linkForce = d3.forceLink(POP_CONFIG.graph.links)
            .id( (link) =>{ return link['id'] })
            .strength(function (link) { return (link.strength*200) })

    this.simulation = d3.forceSimulation()
                      .nodes(POP_CONFIG.graph.nodes)
                      .force('link', this.linkForce)
                      .force('charge', d3.forceManyBody().strength(-8)) 
                      .force('center', d3.forceCenter(this.width/2, this.height/2))

    //-----------Links Creation--------------------//
       this.linkElements = this.g.append("g")
          .attr("class", "links")
          .selectAll("line")
          .data(POP_CONFIG.graph.links)
          .enter().append("line")
            .attr("stroke-width", (l)=>{ return l.strength*6000 })
            .attr("stroke", '#DFE4E7')//"rgba(51,0,0)"
            .attr("stroke-opacity", 0.8) 
            /*.style("opacity", function(o) {
              return o.source === d || o.target === d ? 1 : "green";}) 
            .forEach(function(d) {
                this.linkedByIndex[d.source.index + "," + d.target.index] = 1;
              }); */ 

    //----------NODE Creation------------------//
      this.nodeElements = this.g.append('g')
        .selectAll('circle')
        .data(POP_CONFIG.graph.nodes)
        .enter().append('circle')
        .attr('r', (d)=>{ 
                  let intens = d.viewIntensity;
                  if(intens>=1 && intens<4) return 8;
                  else if(intens>=4 && intens<9) return 11;
                  else if(intens>=9 && intens<12) return 15;
                  else if(intens>=12 && intens<=15) return 20;
                  else if (intens > 16 ) return 27; })
        .attr('fill', this.getNodeColor)
        .style('stroke', function(d) { 
                          return d3.color("#000080"); }) 
        .style('stroke-width',(d)=>{ return d.viewIntensity >20 ? '2px':'0.5px'})  
                                           
        //.attr('class',POP_CONFIG.graph.nodes.id)         

    //----------Add properties in NODE------------------//    
      this.nodeElements  
      //-----------Highlighting the clicked node-----------//
        .on('click', (d)=>{
                    let neighbors = [];
                    neighbors = this.getNeighbors(d)
                    console.log("neighbors >> ",neighbors, " ",);
            // we modify the styles to highlight selected nodes
            this.nodeElements.attr('fill', (d)=>{
                                          if (Array.isArray(neighbors) && neighbors.indexOf(d.id) > -1) 
                                             {return  'rgb(250,250,210)	' ;} //rgb(255,223,0)
                                          else { return 'rgb(74,69,69)'}})
                              .style('opacity', (d)=>{
                                          if (Array.isArray(neighbors) && neighbors.indexOf(d.id) > -1) {return  1 ;} 
                                          else { return 0.2;}})
                            /*.style('z-index', (d)=>{
                                          if (Array.isArray(neighbors) && neighbors.indexOf(d.id) > -1) 
                                              {return  2 ;} 
                                          else { return -1;}})*/                  
            this.linkElements.style("stroke-opacity", function(o) {
                                                      return o.source === d || o.target === d ? 1 : 0.2;})
                            .style("stroke", function(o) {
                              return o.source === d || o.target === d ? 'rgb(0,255,255)' : "#DFE4E7"; });
                            })
        //-------------Get back to previous state by clicking outside-----------------------//                    
        .on('blur',()=>{ console.log("on blur >> ",this.nodeElements );
                          this.nodeElements.attr('fill', this.getNodeColor).style('opacity',1)
                          this.linkElements.style("stroke", '#DFE4E7').style("stroke-opacity", 0.8) }) 
        //-----------Tooltip on mouse over Node---------------//                    
        .on('mouseover', function(d) {
          //console.log("nodeObj >>",JSON.stringify(d)); 
          this.tooltip = d3.select("body")
                        .append("div")
                        .attr("class", "tooltip")
                        .style("opacity", 0);
          this.tooltip.transition()
                      .duration(300)
                      .style("opacity", 0.8);
            this.tooltip.html(" <div style='width: 200px;margin-right: 10px;padding: 0px;display: inline-block;height: 300px;position: absolute;background: #FFFFFF;'> "+
                                  " <div class='left'> <b >Name:</b>" + d.id +"<br><b>Level:</b>" + d.label+
                                  " </div> " +
                                  " <div style='display:table-cell; vertical-align:middle; width:150px; margin:0;height: 250px padding:0;  '>"+
                                  "   <a target='_blank' href="+"./saara-original.jpeg"+">"+
                                  "     <img src='./saara-original.jpeg' alt='5Terre' width='200' height='250'>"+
                                  "   </a>"+
                                  " </div>" +
                              " </div>")
      
            .style("left", (d3.event.pageX ) + "px")
            .style("top", (d3.event.pageY) + "px")
            .style("position","absolute")
          //console.log("this.tooltip >>",this.tooltip);           
        })
        .on('mouseout', function(d){
          //console.log("mouseout.tooltip >>",this.tooltip); 
          this.tooltip.style("visibility", "hidden"); 
        })
        //----------Drag & Drop-----------------//
        .call(d3.drag()
              .on("start", (d)=>{
                if (!d3.event.active) this.simulation.alphaTarget(0.08).restart();
                        d['fx'] = d['x'];d['fy'] = d['y'];})
              .on("drag", (d)=>{
                d['fx'] = d3.event.x;
                d['fy'] = d3.event.y;})
              .on("end", (d)=>{
                          if (!d3.event.active) this.simulation.alphaTarget(0);
                          d['fx'] = null;
                          d['fy'] = null;}))
              /*.style("opacity", function(o) {
                            return this.neighboring(d, o) ? 1 : "green";})   */ 
         
      //----------------------------------------------------//          
    this.textElements = this.g.append('g')
      .selectAll('text')
      .data(POP_CONFIG.graph.nodes)
      .enter().append('text')
        .text(node => node.label)
        .attr('font-size', 10)
        .attr('dx', -1)
        .attr('dy',2)

        this.simulation.nodes(POP_CONFIG.graph.nodes).on('tick', () => {
          this.nodeElements
            .attr('cx', node => node.x)
            .attr('cy', node => node.y)
          this.textElements
            .attr('x', node => node.x)
            .attr('y', node => node.y)
          this.linkElements  
            .attr('x1', link => link.source.x)
            .attr('y1', link => link.source.y)
            .attr('x2', link => link.target.x)
            .attr('y2', link => link.target.y) 
          this.tooltip 

        })
        POP_CONFIG.graph.links.forEach((d) => {
          this.linkedByIndex[d.source['index'] + "," + d.target['index']] = 1;
      });
        this.simulation.force("link").links(POP_CONFIG.graph.links)     
  }
  ngAfterViewChecked() {
   // this.createChart();
   
  }
  ngOnChanges(){
    //this.createChart();
  
  }
  createChart(){
  
  }

  getNodeColor(node,neighbors,flag) {
   
      let intens = node.viewIntensity;
        if(intens>=1 && intens<4) return '#FFE5E7';
        else if(intens>=4 && intens<9) return '#FF9A9E';
        else if(intens>=9 && intens<12) return '#FF4F56';
        else if(intens>=12 && intens<=15) return '#FF030D';
        else if (intens > 16 ) return '#cc0000';

    //return node.level = "rgb(255, 26, 26)";
  }
  selectNode(selectedNode) {
    console.log("selectedNode >> ",JSON.stringify(selectedNode));
    POP_CONFIG.graph.links.forEach(element => {
            //console.log("element id>> ",element.source.id);
            //console.log("selectedNode.id >> ",selectedNode.id);
            
            if (element.source['id'] == selectedNode.id){
              console.log("Inside if >> ",element.source['id']," Inside if node element >> ",this.nodeElements);
              d3.select(selectedNode.id).select("circle").attr('fill', "green").style("stroke-width", 6)

            } else if (element.target['id'] == selectedNode.id){
              console.log("Inside else >> ",element.target['id']);
              d3.select(selectedNode.id).select("circle").attr('fill', "green").style("stroke-width", 6)
            }
      });
  }
  getNeighbors(selectedNode){
    return POP_CONFIG.graph.links.reduce( (neighbors, link) => {
      //method calls the callbackfn function one time for each element in the array
        if (link.target['id'] === selectedNode.id) {
          neighbors.push(link.source['id'])
        } else if (link.source['id'] === selectedNode.id) {
          neighbors.push(link.target['id'])
        }
        return neighbors;
    },[selectedNode.id])//it is used as the initial value to start the accumulation
  }
  isNeighborLink(node, link) {
    console.log("isNeighborLink >>")
    return link.target.id === node.id || link.source.id === node.id
  }
  getTextColor(node, neighbors) {
    console.log("getTextColor >>")
    return Array.isArray(neighbors) && neighbors.indexOf(node.id) > -1 ? 'green' : 'black'
  }
  getLinkColor(node, link) {
    console.log("getLinkColor >>")
    return this.isNeighborLink(node, link) ? 'green' : 'white';//'#E5E5E5'
  }

    isConnected(a, b) {
      console.log("isConnected >>")
        return this.linkedByIndex[a.index + "," + b.index] || this.linkedByIndex[b.index + "," + a.index] || a.index == b.index;
    }
    
}
