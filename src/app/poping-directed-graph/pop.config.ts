const POP_CONFIG = {

    graph:{
      "nodes": [
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-georgette-saree-in-black-sfs592",
          "label": 1
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-khadi-cotton-saree-in-black-sgpn467",
          "label": 2
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-linen-jamdani-saree-in-beige-srga1167",
          "label": 3
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-wine-and-orange-spfa1788",
          "label": 4
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-linen-jamdani-saree-in-off-white-srga1169",
          "label": 5
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "leheriya-printed-kota-silk-saree-in-teal-green-sqta168",
          "label": 6
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-saree-in-peach-sppa32",
          "label": 7
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-multicolor-szma57",
          "label": 8
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-crepe-saree-in-fuchsia-and-off-white-sjra389",
          "label": 9
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-multicolor-sew4351",
          "label": 10
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-cotton-asymmetric-kurta-set-in-dark-purple-mee326",
          "label": 11
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-crepe-saree-in-orange-and-brown-spfa1940",
          "label": 12
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-studded-pair-of-lac-bangle-jkc155",
          "label": 13
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "mysore-crepe-saree-in-light-beige-snga112",
          "label": 14
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-mysore-crepe-saree-in-pink-snga298",
          "label": 15
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "mysore-crepe-saree-in-red-snga120",
          "label": 16
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "mysore-crepe-saree-in-off-white-snga156",
          "label": 17
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "mysore-crepe-saree-in-maroon-snga141",
          "label": 18
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "mysore-crepe-saree-in-fuchsia-snga168",
          "label": 19
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-royal-blue-sew5609",
          "label": 20
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-blue-and-multicolor-szma66",
          "label": 21
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "lehenga-style-lycra-net-and-net-saree-in-maroon-and-peach-sws5283",
          "label": 22
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-crepe-saree-in-red-seh1814",
          "label": 23
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-brocade-sherwani-in-cream-mcd1202",
          "label": 24
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-blue-and-multicolor-spfa2468",
          "label": 25
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-yellow-snba1018",
          "label": 26
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-off-white-and-mustard-snba1014",
          "label": 27
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-brown-snba1017",
          "label": 28
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-off-white-and-multicolor-sew5548",
          "label": 29
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-brown-and-black-szma60",
          "label": 30
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-purple-and-multicolor-snba1012",
          "label": 31
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-art-silk-saree-in-beige-and-brown-syc7018",
          "label": 32
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-cream-ssf3176",
          "label": 33
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-embroidered-art-silk-saree-in-black-and-beige-syla7",
          "label": 34
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-yellow-and-multicolor-spfa2581",
          "label": 35
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-navy-blue-and-multicolor-sew5899",
          "label": 36
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-off-white-and-yellow-spfa2530",
          "label": 37
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-light-pink-smda788",
          "label": 38
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-crepe-jacquard-saree-in-grey-szy736",
          "label": 39
        },
        {
          "viewIntensity": 15,
          "level": "green",
          "id": "digital-printed-satin-saree-in-light-pink-snba1203",
          "label": 40
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bandhani-crepe-saree-in-purple-sjn5758",
          "label": 41
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bandhej-pure-chinon-crepe-saree-in-shaded-beige-and-fuchsia-sjn7086",
          "label": 42
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-jacquard-saree-in-multicolor-and-blue-szy740",
          "label": 43
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-grey-ssx5673",
          "label": 44
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "bangalore-silk-saree-in-maroon-sbra1068",
          "label": 45
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "floral-printed-crepe-saree-in-grey-stu820",
          "label": 46
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-grey-sew4349",
          "label": 47
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-grey-snba1394",
          "label": 48
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-punjabi-suit-in-navy-blue-kch1058",
          "label": 49
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-off-white-mgv335",
          "label": 50
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-sky-blue-and-beige-spfa2604",
          "label": 51
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-pastel-blue-and-multicolor-spfa2619",
          "label": 52
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-light-purple-and-multicolor-spfa2608",
          "label": 53
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-multicolor-spfa2616",
          "label": 54
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-fuchsia-and-light-pink-spfa2605",
          "label": 55
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-beige-spfa2609",
          "label": 56
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-multicolor-spfa2618",
          "label": 57
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-shaded-olive-green-and-blue-spfa2611",
          "label": 58
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-pink-spfa2575",
          "label": 59
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-light-pink-ssf4812",
          "label": 60
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-multicolor-spfa2460",
          "label": 61
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-multicolor-spfa2614",
          "label": 62
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-multicolor-spfa2470",
          "label": 63
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-art-silk-saree-in-multicolor-snba1197",
          "label": 64
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-crepe-jacquard-saree-in-green-ssva124",
          "label": 65
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-jacket-style-lehenga-in-blue-and-pastel-orange-lcc71",
          "label": 66
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-shaded-blue-sgja651",
          "label": 67
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-red-spfa2269",
          "label": 68
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-gown-in-light-olive-green-tch3",
          "label": 69
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-grey-ssf4670",
          "label": 70
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-crepe-jacquard-saree-in-magenta-ssva123",
          "label": 71
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-viscose-saree-in-light-green-ssva134",
          "label": 72
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-beige-spfa2262",
          "label": 73
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-brown-ssf3429",
          "label": 74
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-light-yellow-snba1485",
          "label": 75
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-olive-green-ssf4442",
          "label": 76
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-dark-green-ssf3420",
          "label": 77
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-bhagalpuri-silk-saree-in-light-pastle-green-sew5435",
          "label": 78
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-black-ssf3433",
          "label": 79
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-teal-green-ssf4235",
          "label": 80
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-light-brown-ssf4462",
          "label": 81
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-grey-ssf4447",
          "label": 82
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-handloom-saree-in-off-white-srga680",
          "label": 83
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-satin-saree-in-off-white-and-dark-brown-sew4416",
          "label": 84
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kundan-maang-tikka-jjr13862",
          "label": 85
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-grey-ssf4453",
          "label": 86
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "bandhani-printed-crepe-saree-in-red-sjn3039",
          "label": 87
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "rajasthani-georgette-saree-in-royal-blue-sjra608",
          "label": 88
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-light-dusty-blue-kej1009",
          "label": 89
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "gota-patti-georgette-straight-suit-in-magenta-kch1177",
          "label": 90
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-fuchsia-kpv242",
          "label": 91
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-navy-blue-kch778",
          "label": 92
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-art-silk-pakistani-suit-in-yellow-kch957",
          "label": 93
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-pure-kanchipuram-silk-saree-in-maroon-and-peach-shp775",
          "label": 94
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "kanchipuram-pure-silk-saree-in-black-and-white-stbn87",
          "label": 95
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-pure-silk-saree-in-red-and-white-stbn40",
          "label": 96
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "abstract-printed-cotton-straight-suit-in-fuchsia-kfx2613",
          "label": 97
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroiderd-chanderi-silk-pakistani-suit-in-blue-knsq16",
          "label": 98
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-chanderi-silk-pakistani-suit-in-royal-blue-knsq10",
          "label": 99
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-silk-pakistani-suit-in-royal-blue-kjn2969",
          "label": 100
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-pakistani-suit-in-pink-kgzt263",
          "label": 101
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-pakistani-suit-in-blue-kgzt261",
          "label": 102
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-chiffon-saree-in-shaded-brown-skk22732",
          "label": 103
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "ombre-satin-georgette-saree-in-teal-green-and-teal-blue-ssf3294",
          "label": 104
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "butterfly-pallu-lycra-shimmer-saree-in-magenta-sws5504",
          "label": 105
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "butterfly-pallu-lycra-shimmer-saree-in-lilac-sws5501",
          "label": 106
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-chiffon-saree-in-pink-spfa2147",
          "label": 107
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-green-stu874",
          "label": 108
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-georgette-saree-in-black-sew4784",
          "label": 109
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "embroidered-net-lehenga-in-off-white-lcc151",
          "label": 110
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "printed-cotton-punjabi-suit-in-orange-kfx2510",
          "label": 111
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-off-white-kgb3546",
          "label": 112
        },
        {
          "viewIntensity": 31,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-fuchsia-lyc184",
          "label": 113
        },
        {
          "viewIntensity": 13,
          "level": "green",
          "id": "embroidered-satin-lehenga-in-red-lyc158",
          "label": 114
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-maroon-lcc191",
          "label": 115
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-cotton-punjabi-suit-in-dusty-green-kwy1016",
          "label": 116
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "plain-cotton-kurta-set-in-white-mtx1",
          "label": 117
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "printed-georgette-saree-in-grey-and-beige-spfa1971",
          "label": 118
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-choker-necklace-set-jmy165",
          "label": 119
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-georgette-saree-in-baby-pink-szy716",
          "label": 120
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "printed-crepe-saree-in-baby-pink-ssf3424",
          "label": 121
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-old-rose-ssf4445",
          "label": 122
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-chiffon-saree-in-pink-spfa2095",
          "label": 123
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-print-georgette-saree-in-rose-gold-szy711",
          "label": 124
        },
        {
          "viewIntensity": 8,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-magenta-stya323",
          "label": 125
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-satin-chiffon-saree-in-fuchsia-sew5228",
          "label": 126
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-pink-and-orange-skk22497",
          "label": 127
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embellished-chiffon-saree-in-teal-blue-sjra135",
          "label": 128
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bell-sleeve-georgette-flared-kurti-in-cream-thu1786",
          "label": 129
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "art-silk-nehru-jacket-in-beige-mpe8",
          "label": 130
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "plain-cotton-kurta-set-in-white-mtx95",
          "label": 131
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-net-lehenga-in-dusty-green-lcc148",
          "label": 132
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-and-net-abaya-style-suit-in-baby-pink-kch228",
          "label": 133
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-taffeta-silk-pakistani-suit-in-maroon-kch316",
          "label": 134
        },
        {
          "viewIntensity": 24,
          "level": "green",
          "id": "embroidered-georgette-lehenga-in-old-rose-lcc121",
          "label": 135
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-kurta-churidar-in-beige-mse268",
          "label": 136
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-cotton-linen-pathani-suit-in-white-mrg300",
          "label": 137
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cotton-linen-pathani-suit-in-pink-mrg278",
          "label": 138
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-cotton-pathani-suit-in-white-mtr338",
          "label": 139
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-chiffon-saree-in-black-sjn4102",
          "label": 140
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-chiffon-saree-in-teal-green-sbta162",
          "label": 141
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "lehenga-style-chiffon-saree-in-orange-and-fuchsia-skk22757",
          "label": 142
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-chiffon-saree-in-red-ssf4928",
          "label": 143
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "kanchipuram-saree-in-maroon-snba790",
          "label": 144
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-lehenga-in-beige-lcc149",
          "label": 145
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-cotton-front-slit-anarkali-suit-in-off-white-kjn2365",
          "label": 146
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "embroidered-net-lehenga-in-peach-lqm76",
          "label": 147
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-shaded-pink-and-fuchsia-lat24",
          "label": 148
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-banarasi-silk-jacquard-lehenga-in-fuchsia-luf1334",
          "label": 149
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "half-n-half-embroidered-chiffon-saree-in-beige-and-black-skk22069",
          "label": 150
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-beige-mgv132",
          "label": 151
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "dupion-silk-dhoti-in-beige-mpc568",
          "label": 152
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-border-satin-georgette-saree-in-teal-blue-and-green-ssf4107",
          "label": 153
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embellished-art-silk-saree-in-dark-grey-ssf4488",
          "label": 154
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-pastel-blue-and-navy-blue-skk22486",
          "label": 155
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-beige-kej1054",
          "label": 156
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-rose-gold-kqu945",
          "label": 157
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-art-silk-jacquard-kurta-set-in-orange-mgv110",
          "label": 158
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-snba788",
          "label": 159
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-mustard-ssf4392",
          "label": 160
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "digital-printed-crepe-saree-in-dusty-green-ssf4448",
          "label": 161
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-cotton-linen-pathani-suit-in-white-mrg295",
          "label": 162
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "woven-banarasi-silk-saree-in-pink-seh1659",
          "label": 163
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "banarasi-saree-in-fuchsia-snea920",
          "label": 164
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-tussar-silk-saree-in-blue-seh1663",
          "label": 165
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "banarasi-saree-in-dark-green-snea919",
          "label": 166
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "kanchipuram-saree-in-maroon-snba786",
          "label": 167
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-navy-blue-sqpa177",
          "label": 168
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "banarasi-saree-in-pink-syla52",
          "label": 169
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-kurta-set-in-teal-blue-trb511",
          "label": 170
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-light-green-sfwa247",
          "label": 171
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-light-grey-ssf3609",
          "label": 172
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-dupion-silk-kurta-set-in-off-white-mpw101",
          "label": 173
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-tussar-silk-saree-in-orange-snea940",
          "label": 174
        },
        {
          "viewIntensity": 17,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-szra409",
          "label": 175
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-green-syc6999",
          "label": 176
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "woven-mysore-chiffon-saree-in-maroon-sbra787",
          "label": 177
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-mysore-chiffon-saree-in-green-sbra784",
          "label": 178
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "handloom-silk-saree-in-maroon-sts3420",
          "label": 179
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-art-silk-kurta-set-in-fuchsia-mtx81",
          "label": 180
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-black-sar816",
          "label": 181
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-silk-embroidered-saree-in-fuchsia-snea1057",
          "label": 182
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "plain-cotton-linen-pathani-suit-in-black-mrg292",
          "label": 183
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-art-silk-kurta-churidar-in-white-mse294",
          "label": 184
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-chiffon-saree-in-pink-ssf3652",
          "label": 185
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "gota-patti-chinon-chiffon-saree-in-pastel-green-snya16",
          "label": 186
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-pure-chiffon-saree-in-maroon-szta115",
          "label": 187
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-lilac-kch628",
          "label": 188
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-dark-teal-green-kej1005",
          "label": 189
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-cotton-pathani-suit-in-white-muf654",
          "label": 190
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-georgette-jacket-style-lehenga-in-grey-and-pink-lcc68",
          "label": 191
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-jacquard-and-art-silk-saree-in-yellow-and-blue-smd1443",
          "label": 192
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-lehenga-in-grey-lcc210",
          "label": 193
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-lehenga-in-light-teal-green-lxw170",
          "label": 194
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kanchipuram-saree-in-black-snba787",
          "label": 195
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cotton-silk-churidar-in-off-white-thu1590",
          "label": 196
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-brocade-sherwani-in-dark-blue-and-golden-mpw162",
          "label": 197
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brocade-sherwani-in-black-and-gold-mcd2654",
          "label": 198
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tussar-silk-saree-in-beige-spn3591",
          "label": 199
        },
        {
          "viewIntensity": 15,
          "level": "green",
          "id": "hand-embroidered-net-saree-in-cream-sar885",
          "label": 200
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "hand-embroidered-net-saree-in-white-seh1970",
          "label": 201
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "hand-embroidered-art-silk-saree-in-red-seh1954",
          "label": 202
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-net-saree-in-blue-sar892",
          "label": 203
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-royal-blue-sar865",
          "label": 204
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-royal-blue-sar888",
          "label": 205
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brocade-jacket-in-beige-and-maroon-thu268",
          "label": 206
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "plain-cotton-linen-pathani-suit-in-white-mrg299",
          "label": 207
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "cotton-pathani-suit-in-sky-blue-mse318",
          "label": 208
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "embroidered-art-silk-sherwani-in-white-mkc255",
          "label": 209
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-art-silk-kurta-set-in-black-mgv91",
          "label": 210
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "brocade-sherwani-in-gold-and-dark-blue-mcd2682",
          "label": 211
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-blue-ssf4225",
          "label": 212
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-light-beige-kch649",
          "label": 213
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "printed-georgette-saree-in-navy-blue-ssf3604",
          "label": 214
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-cream-ssf3599",
          "label": 215
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-satin-circular-lehenga-in-red-lqm190",
          "label": 216
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-satin-circular-lehenga-in-shaded-beige-and-pink-lcc142",
          "label": 217
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-velvet-circular-lehenga-in-red-lcc141",
          "label": 218
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-light-beige-lyc235",
          "label": 219
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-a-line-lehenga-in-dark-green-lqm174",
          "label": 220
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-red-lqm194",
          "label": 221
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220",
          "label": 222
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-velvet-a-line-lehenga-in-dark-maroon-lqm169",
          "label": 223
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-red-lyc186",
          "label": 224
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-handloom-art-silk-lehenga-in-fuchsia-lqm152",
          "label": 225
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-beige-lyc178",
          "label": 226
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-beige-ltl89",
          "label": 227
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-beige-ltl84",
          "label": 228
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-circular-lehenga-in-beige-ltl76",
          "label": 229
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "pure-banarasi-silk-saree-in-maroon-snea1007",
          "label": 230
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-cotton-jamdani-saree-in-orange-spn3580",
          "label": 231
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-off-white-and-grey-svra439",
          "label": 232
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "kanchipuram-saree-in-light-pink-smka699",
          "label": 233
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-golden-seh1870",
          "label": 234
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "banarasi-saree-in-turquoise-swz237",
          "label": 235
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kanchipuram-silk-saree-in-off-white-seh1934",
          "label": 236
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-pink-swz199",
          "label": 237
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-satin-saree-in-navy-blue-sew4780",
          "label": 238
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-navy-blue-and-light-green-snma16",
          "label": 239
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "kanchipuram-silk-saree-in-off-white-skra1354",
          "label": 240
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-cotton-angrakha-style-kurta-in-white-and-blue-tja1065",
          "label": 241
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-mangalgiri-cotton-angrakha-style-kurta-in-navy-blue-trb725",
          "label": 242
        },
        {
          "viewIntensity": 8,
          "level": "green",
          "id": "contrast-trim-rayon-kurta-in-red-tdr1036",
          "label": 243
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-yoke-cotton-straight-suit-in-navy-blue-kye789",
          "label": 244
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-handloom-saree-in-yellow-snea667",
          "label": 245
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-chanderi-cotton-saree-in-magenta-sfka778",
          "label": 246
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-mustard-syla228",
          "label": 247
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "banarasi-saree-in-beige-syla255",
          "label": 248
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-kota-doria-saree-in-coral-red-ssf4983",
          "label": 249
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-white-snba1397",
          "label": 250
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-taffeta-silk-punjabi-suit-in-old-rose-kch1326",
          "label": 251
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226",
          "label": 252
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-pakistani-suit-in-light-pink-and-yellow-kxc864",
          "label": 253
        },
        {
          "viewIntensity": 8,
          "level": "green",
          "id": "plain-cowl-style-rayon-kurta-pajama-in-black-mhg696",
          "label": 254
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv424",
          "label": 255
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-red-sud1239",
          "label": 256
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-red-and-pastel-green-sqfa126",
          "label": 257
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-crepe-saree-in-light-pink-sew5367",
          "label": 258
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-satin-butterfly-pallu-saree-in-blue-sppa50",
          "label": 259
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-jacquard-sherwani-in-maroon-mse456",
          "label": 260
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-brocade-sherwani-in-maroon-mrg324",
          "label": 261
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-off-white-mse680",
          "label": 262
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-olive-green-kye830",
          "label": 263
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-brocade-sherwani-in-beige-mpw193",
          "label": 264
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-art-silk-sherwani-with-churidar-in-off-white-mcd2613",
          "label": 265
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421",
          "label": 266
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "velvet-blazer-in-fuchsia-mhg122",
          "label": 267
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-pure-silk-sherwani-in-cream-mcd1497",
          "label": 268
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg504",
          "label": 269
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-yellow-swz86",
          "label": 270
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-pure-tussar-silk-saree-in-red-snea494",
          "label": 271
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "pure-chanderi-silk-woven-saree-in-red-skba239",
          "label": 272
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-peach-swz85",
          "label": 273
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-peach-swz49",
          "label": 274
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-chanderi-silk-woven-saree-in-black-skba251",
          "label": 275
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-peach-swz46",
          "label": 276
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-orange-swz212",
          "label": 277
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-cotton-saree-in-pink-sfka1539",
          "label": 278
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-cotton-saree-in-pink-sfka1364",
          "label": 279
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-chanderi-cotton-saree-in-pink-sfka1369",
          "label": 280
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "embroidered-art-silk-kurta-churidar-in-maroon-mse282",
          "label": 281
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-kurta-churidar-in-rust-mcd1666",
          "label": 282
        },
        {
          "viewIntensity": 12,
          "level": "green",
          "id": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "label": 283
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "jacquard-kurta-churidar-in-blue-mse437",
          "label": 284
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-raw-silk-kurta-churidar-in-black-mcd566",
          "label": 285
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "block-printed-dupion-silk-kurta-pajama-set-in-navy-blue-mmq17",
          "label": 286
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "printed-cotton-rayon-kurta-set-in-black-and-white-mtx3",
          "label": 287
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-churidar-in-black-mcd1189",
          "label": 288
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd1675",
          "label": 289
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-art-silk-kurta-churidar-in-wine-mcd1669",
          "label": 290
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "plain-dupion-silk-kurta-set-in-maroon-mpw240",
          "label": 291
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-jacquard-kurta-churidar-in-teal-blue-mat19",
          "label": 292
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-net-pakistani-style-suit-in-cream-kch320",
          "label": 293
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-raw-silk-kurta-churidar-in-orange-mcd563",
          "label": 294
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "plain-art-silk-sherwani-in-black-mgv139",
          "label": 295
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-raw-silk-kurta-churidar-in-blue-mcd561",
          "label": 296
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-rayon-cotton-combo-of-kurta-in-yellow-and-magenta-tdr792",
          "label": 297
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-art-silk-jacquard-kurta-set-in-olive-green-mgv117",
          "label": 298
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "punjabi-cotton-suit-in-black-ktv198",
          "label": 299
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "digital-printed-georgette-brasso-straight-suit-in-red-kds910",
          "label": 300
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "label": 301
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-red-kch799",
          "label": 302
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-brocade-silk-sherwani-in-beige-mhg729",
          "label": 303
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-churidar-in-beige-mse226",
          "label": 304
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-art-silk-jacquard-sherwani-in-beige-mse763",
          "label": 305
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd2388",
          "label": 306
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-dupion-silk-kurta-set-in-beige-mpw126",
          "label": 307
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-pink-kuf10216",
          "label": 308
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "dupion-silk-dhoti-in-white-mpc642",
          "label": 309
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-cotton-linen-kurta-set-in-pink-mgn53",
          "label": 310
        },
        {
          "viewIntensity": 10,
          "level": "green",
          "id": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940",
          "label": 311
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151",
          "label": 312
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503",
          "label": 313
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "stone-studded-maang-tikka-jjr16701",
          "label": 314
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-cotton-tant-saree-in-light-green-and-white-ssna98",
          "label": 315
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1010",
          "label": 316
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-yoke-art-silk-abaya-style-suit-in-green-and-blue-ombre-kae631",
          "label": 317
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-peach-and-navy-blue-kqu997",
          "label": 318
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-snba660",
          "label": 319
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-anarkali-suit-with-pure-silk-dupatta-in-purple-kjn2075",
          "label": 320
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-beige-kch1145",
          "label": 321
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-georgette-brasso-pakistani-suit-in-off-white-kej952",
          "label": 322
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-peach-kch629",
          "label": 323
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-pink-kch1050",
          "label": 324
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-light-olive-green-kch1156",
          "label": 325
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "banarasi-silk-saree-in-maroon-sew5055",
          "label": 326
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-handloom-saree-in-off-white-and-red-sbta13",
          "label": 327
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-embroidered-net-saree-in-purple-shy30",
          "label": 328
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-chiffon-saree-in-yellow-seh419",
          "label": 329
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-chiffon-jacquard-saree-in-coral-pink-ssf3642",
          "label": 330
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-navy-blue-lzr317",
          "label": 331
        },
        {
          "viewIntensity": 8,
          "level": "green",
          "id": "printed-chiffon-saree-in-red-ssf4758",
          "label": 332
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-georgette-banarasi-saree-in-white-snea1319",
          "label": 333
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-lehenga-in-light-fawn-lcc249",
          "label": 334
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-red-scfa351",
          "label": 335
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-art-silk-anarkali-suit-in-sky-blue-kdfz46",
          "label": 336
        },
        {
          "viewIntensity": 8,
          "level": "green",
          "id": "half-n-half-net-saree-in-beige-and-red-seh1101",
          "label": 337
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-shimmer-saree-in-black-sfs176",
          "label": 338
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "kanchipuram-silk-saree-in-coral-skra1133",
          "label": 339
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-pure-georgette-lehenga-in-maroon-ljn850",
          "label": 340
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-lehenga-style-saree-in-red-and-off-white-spfa2169",
          "label": 341
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "plain-art-silk-dhoti-sherwani-in-black-mhg501",
          "label": 342
        },
        {
          "viewIntensity": 9,
          "level": "green",
          "id": "banarasi-silk-saree-in-peach-sew5779",
          "label": 343
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-stbn110",
          "label": 344
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "bandhej-pure-chinon-crepe-in-red-sjn5487",
          "label": 345
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2685",
          "label": 346
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2683",
          "label": 347
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-light-green-syla42",
          "label": 348
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "brocade-dhoti-jacket-in-off-white-utk382",
          "label": 349
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-jacquard-dhoti-sherwani-in-white-utk189",
          "label": 350
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-art-silk-jacquard-sherwani-in-white-uvx18",
          "label": 351
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-jacket-style-lehenga-in-peach-lcc67",
          "label": 352
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-shimmer-lycra-saree-in-golden-sgpn521",
          "label": 353
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-lycra-shimmer-saree-in-antique-and-maroon-spta264",
          "label": 354
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-saree-in-golden-sppa91",
          "label": 355
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-cotton-jacket-style-kurta-in-multicolor-tvn10",
          "label": 356
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-golden-seh1908",
          "label": 357
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "printed-cotton-jacket-style-kurta-in-multicolor-tvn5",
          "label": 358
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-cotton-jacket-style-kurta-in-multicolor-tvn11",
          "label": 359
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-satin-georgette-dress-in-multicolor-tgw322",
          "label": 360
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-off-white-ssf4529",
          "label": 361
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "art-silk-saree-in-off-white-syc6556",
          "label": 362
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-fuchsia-kch623",
          "label": 363
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-neckline-art-silk-gown-in-fuchsia-and-brown-ttv23",
          "label": 364
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-art-silk-sherwani-in-beige-mgv158",
          "label": 365
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-coral-red-and-beige-sws5349",
          "label": 366
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-dupion-silk-sherwani-in-beige-mkc253",
          "label": 367
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-art-silk-saree-in-beige-ssva333",
          "label": 368
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-beige-ssha1196",
          "label": 369
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroiderd-georgette-saree-in-beige-sxc2555",
          "label": 370
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-crepe-saree-in-light-beige-syc7333",
          "label": 371
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-lycra-shimmer-saree-in-beige-sws5548",
          "label": 372
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-lycra-shimmer-saree-in-light-beige-sws5551",
          "label": 373
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-beige-spfa1964",
          "label": 374
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-georgette-saree-in-light-beige-szy720",
          "label": 375
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-shaded-orange-and-red-spfa2734",
          "label": 376
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chiffon-jacquard-saree-in-light-beige-sqfa366",
          "label": 377
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-lycra-shimmer-saree-in-light-beige-sws5549",
          "label": 378
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-fawn-kch1304",
          "label": 379
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-pakistani-suit-in-beige-kmsg456",
          "label": 380
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-pakistani-suit-in-beige-kexm14",
          "label": 381
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-yellow-syla188",
          "label": 382
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-yellow-sbea138",
          "label": 383
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-yellow-sbea139",
          "label": 384
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "digital-printed-dupion-silk-dhoti-kurta-set-in-light-beige-mgv344",
          "label": 385
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-fuchsia-and-multicolor-lwk2529",
          "label": 386
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-linen-kurta-set-in-light-purple-mpc821",
          "label": 387
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-linen-kurta-set-in-dusty-mustard-mpc823",
          "label": 388
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-black-kjf81",
          "label": 389
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-maroon-kch796",
          "label": 390
        },
        {
          "viewIntensity": 11,
          "level": "green",
          "id": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "label": 391
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-beige-kjf88",
          "label": 392
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-maroon-kch630",
          "label": 393
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch1029",
          "label": 394
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-navy-blue-kch849",
          "label": 395
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-satin-lehenga-in-neon-green-lsh163",
          "label": 396
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-navy-blue-kej1006",
          "label": 397
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-pakistani-suit-in-shaded-dark-beige-kcv1351",
          "label": 398
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-crepe-pakistani-suit-in-sky-blue-kmsg499",
          "label": 399
        },
        {
          "viewIntensity": 8,
          "level": "green",
          "id": "digital-printed-crepe-straight-suit-in-sea-green-kcv1349",
          "label": 400
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-fuchsia-sew5737",
          "label": 401
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-art-silk-saree-in-pink-sew5741",
          "label": 402
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-cream-mhg855",
          "label": 403
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-sky-blue-and-light-green-ssf4528",
          "label": 404
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "printed-georgette-saree-in-yellow-ssf3607",
          "label": 405
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-dark-teal-green-and-beige-sew5412",
          "label": 406
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-brasso-pakistani-suit-in-teal-green-kcv1200",
          "label": 407
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-cotton-saree-in-light-olive-green-shk1496",
          "label": 408
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "contrast-patch-border-rayon-a-line-kurta-in-fuchsia-trq242",
          "label": 409
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-tussar-silk-saree-in-wine-sqta334",
          "label": 410
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-studded-bridal-set-in-maroon-and-off-white-jnc2279",
          "label": 411
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bridal-set-in-maroon-and-green-jnc2304",
          "label": 412
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "plain-art-silk-sherwani-in-blue-mgv147",
          "label": 413
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg699",
          "label": 414
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg703",
          "label": 415
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-khadi-cotton-kurta-set-in-off-white-mve475",
          "label": 416
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-art-silk-kurta-pajama-in-blue-mhg706",
          "label": 417
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-dupion-silk-kurta-churidar-in-off-white-mat28",
          "label": 418
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-churidar-in-turquoise-mcd1183",
          "label": 419
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "contrast-bordered-georgette-saree-in-black-sjra62",
          "label": 420
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-embroidered-lycra-shimmer-saree-in-pink-and-yellow-spfa1292",
          "label": 421
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "plain-art-silk-sherwani-in-teal-blue-mgv146",
          "label": 422
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "foil-printed-rayon-cowl-style-kurta-set-in-blue-and-white-mhg723",
          "label": 423
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-off-white-kbz225",
          "label": 424
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140",
          "label": 425
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-punjabi-suit-in-light-teal-green-kch1055",
          "label": 426
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-silk-saree-in-light-peach-shxa105",
          "label": 427
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-fuchsia-lga292",
          "label": 428
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-navy-blue-kbz231",
          "label": 429
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104",
          "label": 430
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-indowestern-lehenga-in-beige-ljn1417",
          "label": 431
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-poly-cotton-punjabi-suit-in-pastel-orange-kmsg32",
          "label": 432
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-cotton-punjabi-suit-in-white-kxz116",
          "label": 433
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-and-green-seh1790",
          "label": 434
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "handloom-tant-cotton-saree-in-wine-stla302",
          "label": 435
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-jacquard-saree-in-beige-snea803",
          "label": 436
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-earring-in-royal-blue-and-white-jvm2159",
          "label": 437
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "bengal-handloom-cotton-silk-saree-in-offwhite-sswa38",
          "label": 438
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-beige-and-red-sau2526",
          "label": 439
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "bandhani-crepe-saree-in-onion-pink-sjn5755",
          "label": 440
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525",
          "label": 441
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-beige-and-fuchsia-sau2523",
          "label": 442
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-beige-ssx5656",
          "label": 443
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62",
          "label": 444
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-grey-and-white-srp648",
          "label": 445
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "tie-dye-printed-cotton-straight-suit-in-blue-kmsg210",
          "label": 446
        },
        {
          "viewIntensity": 35,
          "level": "green",
          "id": "woven-brocade-silk-sherwani-in-beige-mgv131",
          "label": 447
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "hand-embroidered-brocade-dhoti-sherwani-in-white-mhg458",
          "label": 448
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "oxidised-necklace-jpm2842",
          "label": 449
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "cotton-kurta-pyjama-in-white-mvj18",
          "label": 450
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-lehenga-in-light-orange-and-maroon-lxw139",
          "label": 451
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-light-green-kepd38",
          "label": 452
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "digital-printed-satin-saree-in-grey-ombre-spfa2752",
          "label": 453
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-beige-kej1127",
          "label": 454
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-light-beige-kch1455",
          "label": 455
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454",
          "label": 456
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-lehenga-in-brown-lxe123",
          "label": 457
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-old-rose-kch1009",
          "label": 458
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-purple-and-maroon-kch1123",
          "label": 459
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-navy-blue-kch1038",
          "label": 460
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-wine-kch1042",
          "label": 461
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1044",
          "label": 462
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-dark-green-kch1232",
          "label": 463
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1033",
          "label": 464
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-dark-green-kch1088",
          "label": 465
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-dark-green-kpv243",
          "label": 466
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1509",
          "label": 467
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-sky-blue-kch1052",
          "label": 468
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-teal-green-kch1505",
          "label": 469
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-navy-blue-kej1151",
          "label": 470
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-border-shimmer-lycra-saree-in-light-beige-sgpn480",
          "label": 471
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-abaya-style-suit-in-teal-blue-and-navy-blue-kqu881",
          "label": 472
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-violet-kch875",
          "label": 473
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch574",
          "label": 474
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cotton-rayon-asymmetric-pakistani-suit-in-black-kyt57",
          "label": 475
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-and-net-pakistani-suit-in-maroon-kch244",
          "label": 476
        },
        {
          "viewIntensity": 11,
          "level": "green",
          "id": "embroidered-cotton-linen-kurta-set-in-white-mpw145",
          "label": 477
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-maroon-ktvb78",
          "label": 478
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-saree-in-black-skk22592",
          "label": 479
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-black-kch1295",
          "label": 480
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-armlet-jvk2731",
          "label": 481
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-brocade-sherwani-in-maroon-and-gold-mpe85",
          "label": 482
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-brocade-sherwani-in-maroon-and-golden-mpw161",
          "label": 483
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-light-beige-mhg854",
          "label": 484
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-cotton-linen-kurta-set-in-yellow-mpw158",
          "label": 485
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv149",
          "label": 486
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-khadi-dhoti-sherwani-in-maroon-mhg502",
          "label": 487
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-jacquard-sherwani-in-off-white-mse678",
          "label": 488
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-jacquard-circular-lehenga-in-pink-lxw218",
          "label": 489
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-shaded-teal-green-and-blue-lqm134",
          "label": 490
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-satin-lehenga-in-shaded-pastel-green-and-teal-blue-lcc133",
          "label": 491
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-assymetric-kurta-in-mustard-tjw693",
          "label": 492
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695",
          "label": 493
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-saree-in-coral-sew2932",
          "label": 494
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-tant-saree-in-orange-spn3017",
          "label": 495
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-light-blue-lcc209",
          "label": 496
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-orange-lxw244",
          "label": 497
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-saree-in-peach-sbz3586",
          "label": 498
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-blue-ssf4860",
          "label": 499
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-saree-in-light-blue-smda576",
          "label": 500
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "printed-georgette-saree-in-pastel-orange-snba1318",
          "label": 501
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "printed-georgette-saree-in-pink-snba1316",
          "label": 502
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-wine-syla107",
          "label": 503
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-purple-szy815",
          "label": 504
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-orange-snba1327",
          "label": 505
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-pink-snba1340",
          "label": 506
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "digital-printed-crepe-pakistani-suit-in-rose-gold-kcv1370",
          "label": 507
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-golden-and-royal-blue-spfa1919",
          "label": 508
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-art-silk-saree-in-pink-spfa1916",
          "label": 509
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-pink-sxc2648",
          "label": 510
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-saree-in-sky-blue-spfa1917",
          "label": 511
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "pure-kota-silk-saree-in-fuchsia-snj7098",
          "label": 512
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-georgette-leheriya-saree-in-multicolor-snj7099",
          "label": 513
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-chiffon-saree-in-red-skk22369",
          "label": 514
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-red-stu796",
          "label": 515
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-smka666",
          "label": 516
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-coral-pink-stu793",
          "label": 517
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-cotton-tant-saree-in-pink-srga291",
          "label": 518
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-silk-jamdani-saree-in-pink-skza48",
          "label": 519
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-teal-blue-mmq10",
          "label": 520
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-dark-blue-lqm138",
          "label": 521
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-circular-lehenga-in-dark-blue-lcc135",
          "label": 522
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "dupion-silk-blouse-in-royal-blue-dbu516",
          "label": 523
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "dupion-silk-blouse-in-royal-blue-dbu584",
          "label": 524
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-and-net-saree-in-red-and-off-white-sas992",
          "label": 525
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-ssf4412",
          "label": 526
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "banarasi-saree-in-maroon-and-magenta-sbta519",
          "label": 527
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-kerala-kasavu-cotton-saree-in-cream-spn2299",
          "label": 528
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-jacquard-straight-suit-in-green-kye912",
          "label": 529
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-pink-kye657",
          "label": 530
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-peach-kye661",
          "label": 531
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-navy-blue-kye663",
          "label": 532
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-green-kye658",
          "label": 533
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-chanderi-silk-jacquard-straight-suit-in-light-green-kye884",
          "label": 534
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-green-kye512",
          "label": 535
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-jacquard-straight-suit-in-pink-kye882",
          "label": 536
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-teal-green-kye338",
          "label": 537
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-green-kae737",
          "label": 538
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae744",
          "label": 539
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae732",
          "label": 540
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kae741",
          "label": 541
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kye620",
          "label": 542
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-magenta-kye616",
          "label": 543
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-chanderi-jacquard-straight-suit-in-royal-blue-kwy997",
          "label": 544
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-chanderi-jacquard-straight-suit-in-navy-blue-kwy1004",
          "label": 545
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-jacquard-straight-suit-in-navy-blue-kye724",
          "label": 546
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-jacquard-straight-suit-in-navy-blue-kye910",
          "label": 547
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-navy-blue-and-light-green-kye823",
          "label": 548
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83",
          "label": 549
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-navy-blue-ktvb86",
          "label": 550
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-navy-blue-kry990",
          "label": 551
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-fuchsia-ktvb87",
          "label": 552
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "handloom-chanderi-silk-saree-in-yellow-skba180",
          "label": 553
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-border-crepe-saree-in-dark-green-sew5420",
          "label": 554
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-crepe-saree-in-teal-green-ssf4255",
          "label": 555
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-dupion-silk-kurta-set-in-navy-blue-mpw243",
          "label": 556
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-set-in-maroon-mpc694",
          "label": 557
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "lehenga-style-lycra-shimmer-saree-in-peach-and-beige-sws5629",
          "label": 558
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-lehenga-style-saree-in-peach-sws5588",
          "label": 559
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-lycra-shimmer-saree-in-peach-sjn6843",
          "label": 560
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-net-lehenga-style-saree-in-orange-and-cream-spfa2175",
          "label": 561
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-straight-cut-rayon-suit-in-white-ktv321",
          "label": 562
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "printed-straight-cut-front-slit-suit-in-yellow-ktv249",
          "label": 563
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-punjabi-suit-in-black-ktv238",
          "label": 564
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-punjabi-suit-in-black-ktv19",
          "label": 565
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-brasso-straight-suit-in-peach-kqu1032",
          "label": 566
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-punjabi-suit-in-yellow-ktv302",
          "label": 567
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-kanchipuram-handloom-silk-saree-in-rust-sgta58",
          "label": 568
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-golden-stya262",
          "label": 569
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-pure-kanchipuram-silk-saree-in-maroon-and-golden-shp731",
          "label": 570
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229",
          "label": 571
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-satin-georgette-saree-in-maroon-sew5799",
          "label": 572
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "pure-banarasi-silk-handloom-saree-in-red-sbta30",
          "label": 573
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-silk-nehru-jacket-in-blue-mhg563",
          "label": 574
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-fuchsia-stea297",
          "label": 575
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "pure-banarasi-silk-handloom-saree-in-red-sbta28",
          "label": 576
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "banarasi-saree-in-maroon-stea515",
          "label": 577
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "banarasi-saree-in-green-sew5280",
          "label": 578
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "plain-art-silk-sherwani-in-beige-mmq9",
          "label": 579
        },
        {
          "viewIntensity": 7,
          "level": "green",
          "id": "woven-brocade-silk-sherwani-in-beige-mpw312",
          "label": 580
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-beige-lrf35",
          "label": 581
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-tussar-silk-saree-in-off-white-and-blue-srga481",
          "label": 582
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-tussar-silk-saree-in-beige-snea947",
          "label": 583
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pearl-necklace-set-jmy327",
          "label": 584
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-choker-necklace-set-jmy351",
          "label": 585
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-beige-kch1268",
          "label": 586
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-pakistani-suit-in-red-kuf10123",
          "label": 587
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-asymmetric-pakistani-suit-in-beige-kjr150",
          "label": 588
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-pure-ghicha-silk-saree-in-beige-sts3429",
          "label": 589
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-cotton-kerala-kasavu-saree-in-off-white-spv123",
          "label": 590
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-beige-kch1266",
          "label": 591
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-kurta-set-in-pink-mms841",
          "label": 592
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-cotton-kurta-set-in-light-purple-mpw106",
          "label": 593
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cotton-silk-slub-kurta-set-in-beige-mtx37",
          "label": 594
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "plain-art-silk-kurta-set-in-navy-blue-mms901",
          "label": 595
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-sherwani-in-dark-blue-mpw187",
          "label": 596
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-georgette-saree-in-maroon-sfva37",
          "label": 597
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-kurta-pyjama-sets-in-red-und330",
          "label": 598
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-beige-kgzt173",
          "label": 599
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-kurta-in-mustard-mtr91",
          "label": 600
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-indowestern-lehenga-in-cream-lqu460",
          "label": 601
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-dupion-silk-blouse-in-wine-uam79",
          "label": 602
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "gota-work-pure-georgette-saree-in-pink-ombre-sjn4022",
          "label": 603
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-red-kuf10587",
          "label": 604
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "art-raw-silk-blouse-in-red-dbu576",
          "label": 605
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-kurta-in-brown-and-golden-thu925",
          "label": 606
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-beige-ljn1431",
          "label": 607
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-satin-lehenga-in-beige-and-blue-luf1437",
          "label": 608
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-red-kbz219",
          "label": 609
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-crepe-top-in-old-rose-thu263",
          "label": 610
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-beige-kqu998",
          "label": 611
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-crepe-top-in-BLACK-thu763",
          "label": 612
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-straight-cut-suit-in-blue-kjn1313",
          "label": 613
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "art-silk-and-georgette-blouse-in-beige-and-off-white-ubd496",
          "label": 614
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-art-bhagalpuri-silk-palazzo-in-cream-bnj187",
          "label": 615
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-art-silk-saree-in-pastel-orange-syc7484",
          "label": 616
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-art-silk-saree-in-pastel-green-syc7480",
          "label": 617
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-mustard-syc7476",
          "label": 618
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-art-silk-saree-in-yellow-syc7479",
          "label": 619
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-art-silk-saree-in-black-syc7474",
          "label": 620
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-chiffon-saree-in-orange-and-dark-pink-syc7485",
          "label": 621
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-saree-in-blue-snba1532",
          "label": 622
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-violet-svta135",
          "label": 623
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-satin-chiffon-saree-in-beige-syc7477",
          "label": 624
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-chiffon-saree-in-dark-grey-syc7478",
          "label": 625
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-saree-in-brown-syc7475",
          "label": 626
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-dupion-silk-kurta-set-with-nehru-jacket-in-light-beige-mtr70",
          "label": 627
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-silk-jamdani-saree-in-off-white-srga87",
          "label": 628
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-jamdani-cotton-silk-saree-in-green-srga583",
          "label": 629
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "pure-kanchipuram-handloom-silk-saree-in-olive-green-and-beige-sgta75",
          "label": 630
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-rayon-kurta-in-red-tqz166",
          "label": 631
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-kurta-set-in-maroon-mve281",
          "label": 632
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "plain-art-silk-jacquard-jodhpuri-suit-in-black-mhg851",
          "label": 633
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-jute-silk-sherwani-in-off-white-mgv199",
          "label": 634
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-dress-in-black-and-white-tmw3",
          "label": 635
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-silk-and-art-ghicha-silk-long-dress-in-pink-thu84",
          "label": 636
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "printed-art-silk-circular-lehenga-in-beige-lcc62",
          "label": 637
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "embroidered-lycra-circular-lehenga-in-black-and-beige-lxw366",
          "label": 638
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-light-beige-and-white-kch1150",
          "label": 639
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-jacket-style-lehenga-in-maroon-lbz7",
          "label": 640
        },
        {
          "viewIntensity": 6,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-peach-kch795",
          "label": 641
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bridal-set-jxm499",
          "label": 642
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bridal-set-jxm504",
          "label": 643
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-matha-patti-jdw795",
          "label": 644
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-matha-patti-jrl938",
          "label": 645
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-matha-patti-jrl333",
          "label": 646
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "polki-studded-bridal-necklace-set-jjr14564",
          "label": 647
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "stone-studded-bridal-set-in-maroon-and-golden-jnc2298",
          "label": 648
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-studded-bridal-set-jxm502",
          "label": 649
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-georgette-saree-in-olive-green-sew4716",
          "label": 650
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-tant-saree-in-rust-spn3520",
          "label": 651
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-placket-cotton-pathani-suit-in-light-yellow-meu18",
          "label": 652
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-navy-blue-lyc189",
          "label": 653
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-maroon-lyc250",
          "label": 654
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-dark-maroon-lat23",
          "label": 655
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-mermaid-cut-lehenga-in-cream-lqm173",
          "label": 656
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-shaded-pink-ljf27",
          "label": 657
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-maroon-lyc257",
          "label": 658
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-brocade-jodhpuri-suit-in-cream-mhg856",
          "label": 659
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-art-silk-saree-in-orange-sbm6575",
          "label": 660
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-kota-silk-saree-in-fuchsia-safa121",
          "label": 661
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-maroon-sew5745",
          "label": 662
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-dark-brown-sgpn203",
          "label": 663
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-peach-skk22442",
          "label": 664
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-purple-stea557",
          "label": 665
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "kanchipuram-pure-silk-saree-in-turquoise-and-white-stbn37",
          "label": 666
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "plain-cotton-pathani-suit-in-navy-blue-mtr335",
          "label": 667
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-net-abaya-style-suit-in-peach-kpk9",
          "label": 668
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-fuchsia-kjf79",
          "label": 669
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-chinon-crepe-bandhej-saree-in-maroon-sjn6899",
          "label": 670
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-black-sew3738",
          "label": 671
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-orange-and-peach-sew3732",
          "label": 672
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-off-white-ktvb85",
          "label": 673
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-pure-chanderi-silk-pakistani-suit-in-off-white-kul87",
          "label": 674
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-kurta-in-peach-and-black-tmz201",
          "label": 675
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-pink-kvs1832",
          "label": 676
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-abaya-style-suit-in-light-beige-and-red-kch1351",
          "label": 677
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-beige-kch674",
          "label": 678
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-off-white-kej1022",
          "label": 679
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-kurti-in-orange-tuf1086",
          "label": 680
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-art-silk-anarkali-suit-in-off-white-and-red-kxz9",
          "label": 681
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-light-orange-kch926",
          "label": 682
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-neckline-art-silk-dhoti-kurta-in-beige-mrg207",
          "label": 683
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-jamdani-cotton-silk-saree-in-beige-srga588",
          "label": 684
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-lehenga-in-red-and-beige-lty8",
          "label": 685
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-neckline-art-silk-dhoti-kurta-in-light-beige-mrg206",
          "label": 686
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-pakistani-suit-in-beige-kmsg469",
          "label": 687
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "stone-studded-necklace-set-jxm437",
          "label": 688
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-studded-choker-necklace-set-jxm491",
          "label": 689
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jvk1853",
          "label": 690
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bridal-set-jxm503",
          "label": 691
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jxm234",
          "label": 692
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pearl-necklace-set-jmy315",
          "label": 693
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-studded-necklace-set-jxm239",
          "label": 694
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "floral-printed-georgette-saree-in-old-rose-sew4483",
          "label": 695
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-maang-tikka-jjr14023",
          "label": 696
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-velvet-sherwani-in-black-mgv333",
          "label": 697
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-red-lyc188",
          "label": 698
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "hand-embroidered-brocade-jodhpuri-suit-in-cream-mhg857",
          "label": 699
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-art-silk-jacket-style-kurta-in-light-pink-tsx164",
          "label": 700
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-teal-green-ubg60",
          "label": 701
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-cotton-salwar-set-in-teal-green-uvt10",
          "label": 702
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-maroon-kch1125",
          "label": 703
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "jamdani-woven-cotton-silk-saree-in-black-srga863",
          "label": 704
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "handloom-cotton-silk-jamdani-saree-in-turquoise-stla109",
          "label": 705
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-pure-chanderi-silk-saree-in-orange-skba279",
          "label": 706
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-orange-swz88",
          "label": 707
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-kanchipuram-silk-saree-in-orange-seh1424",
          "label": 708
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-saree-in-red-and-sky-blue-sgka1996",
          "label": 709
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "lehenga-style-chiffon-jacquard-saree-in-pink-and-fuchsia-spfa237",
          "label": 710
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-maroon-snea723",
          "label": 711
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-red-sbta95",
          "label": 712
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-orange-seh864",
          "label": 713
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-pure-silk-handloom-saree-in-maroon-snea401",
          "label": 714
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-sbh1234",
          "label": 715
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-georgette-jacquard-saree-in-pink-sjn6566",
          "label": 716
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-lehenga-net-saree-in-teal-green-sar821",
          "label": 717
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-pure-kanchipuram-saree-in-coral-red-sbra460",
          "label": 718
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-brocade-nehru-jacket-in-grey-mhg369",
          "label": 719
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-saree-in-pink-sjn5646",
          "label": 720
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-handloom-pure-silk-saree-in-orange-stea326",
          "label": 721
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-handloom-pure-silk-saree-in-violet-stea332",
          "label": 722
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-net-saree-in-beige-and-violet-sqfa129",
          "label": 723
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-handloom-silk-saree-in-multi-color-sgta85",
          "label": 724
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-royal-blue-seh1408",
          "label": 725
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-jacquard-saree-in-pink-and-light-orange-stya52",
          "label": 726
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-khadi-dhoti-sherwani-in-brown-mhg505",
          "label": 727
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-and-crepe-saree-in-dark-brown-and-beige-skk21870",
          "label": 728
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-orange-and-beige-stya62",
          "label": 729
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-ombre-orange-and-fuchsia-stl636",
          "label": 730
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-kanchipuram-saree-in-sky-blue-and-maroon-shp620",
          "label": 731
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-tussar-silk-saree-in-green-seh1726",
          "label": 732
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-satin-silk-banarasi-saree-in-teal-blue-snea909",
          "label": 733
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-fuchsia-sbta298",
          "label": 734
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-fuchsia-sbta248",
          "label": 735
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-fuchsia-and-baby-pink-sew3336",
          "label": 736
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-kanchipuram-silk-saree-in-coral-red-sgka2478",
          "label": 737
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-cream-sbra913",
          "label": 738
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-and-net-saree-in-peach-and-pink-sws5166",
          "label": 739
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-mustard-sbta272",
          "label": 740
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-pink-and-yellow-sud1044",
          "label": 741
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-net-saree-in-green-sga7783",
          "label": 742
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-shaded-off-white-and-green-skk22151",
          "label": 743
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "lehenga-style-net-saree-in-teal-green-and-purple-skk13605",
          "label": 744
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-saree-in-light-olive-green-sws5226",
          "label": 745
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-lycra-shimmer-saree-in-rose-gold-and-pink-skga129",
          "label": 746
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-light-green-sau1708",
          "label": 747
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bangle-set-in-royal-blue-and-golden-jda905",
          "label": 748
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-coral-red-seh1476",
          "label": 749
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-handloom-silk-saree-in-light-brown-stea282",
          "label": 750
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-old-rose-sbta261",
          "label": 751
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-beige-sbta308",
          "label": 752
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-sbra887",
          "label": 753
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-lycra-saree-in-antique-and-cream-sfs629",
          "label": 754
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-jacquard-saree-in-wine-and-blue-stya47",
          "label": 755
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "lehenga-style-net-saree-in-beige-and-magenta-sew2988",
          "label": 756
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "lehenga-style-embroidered-art-silk-saree-in-coral-and-green-sud1180",
          "label": 757
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "bengal-handloom-cotton-silk-tant-saree-in-beige-and-maroon-stla91",
          "label": 758
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-beige-and-red-syc7357",
          "label": 759
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-brocade-silk-sherwani-in-beige-mpw310",
          "label": 760
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-brocade-silk-sherwani-in-golden-mxh59",
          "label": 761
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-peach-kch1016",
          "label": 762
        },
        {
          "viewIntensity": 14,
          "level": "green",
          "id": "embroidered-placket-cotton-pathani-suit-in-black-meu14",
          "label": 763
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-viscose-georgette-flared-gown-in-sea-green-tsp178",
          "label": 764
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-sea-green-kmsg305",
          "label": 765
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "abstract-printed-cotton-straight-suit-in-maroon-khbz152",
          "label": 766
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "shibhori-printed-cotton-straight-suit-in-mustard-khbz151",
          "label": 767
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-mysore-chiffon-saree-in-teal-green-sbra780",
          "label": 768
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "embroidered-patch-border-georgette-saree-in-fuchsia-sqh1477",
          "label": 769
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-patch-border-georgette-saree-in-fuchsia-sbz2751",
          "label": 770
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-lehenga-in-cream-lyc255",
          "label": 771
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-red-lyc259",
          "label": 772
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-lehenga-in-maroon-ljn1137",
          "label": 773
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-red-lqm188",
          "label": 774
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-net-a-line-lehenga-in-red-ljn1168",
          "label": 775
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-red-lyc236",
          "label": 776
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-gown-in-dark-green-uku727",
          "label": 777
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-shimmer-net-lehenga-set-in-green-unj279",
          "label": 778
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embellished-lycra-saree-in-royal-blue-spfa1522",
          "label": 779
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "ombre-georgette-saree-in-fuchsia-and-magenta-ssf4494",
          "label": 780
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "banarasi-saree-in-pink-syla41",
          "label": 781
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-saree-in-light-teal-green-syla48",
          "label": 782
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-saree-in-fuchsia-sfka1714",
          "label": 783
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-stbn114",
          "label": 784
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-saree-in-red-snba856",
          "label": 785
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-pink-sfva140",
          "label": 786
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-black-syla47",
          "label": 787
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-brocade-sherwani-in-white-mse370",
          "label": 788
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-maang-tikka-jjr13857",
          "label": 789
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-mysore-crepe-woven-saree-in-old-rose-shu680",
          "label": 790
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-mysore-crepe-woven-saree-in-olive-green-shu674",
          "label": 791
        },
        {
          "viewIntensity": 4,
          "level": "green",
          "id": "block-printed-cotton-maxi-dress-in-indigo-blue-tjw503",
          "label": 792
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chambray-dress-in-blue-thu1529",
          "label": 793
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cotton-flared-tunic-in-red-tyq90",
          "label": 794
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-brown-kch1013",
          "label": 795
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-straight-suit-in-beige-kfx2968",
          "label": 796
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-mysore-silk-saree-in-blue-stbn127",
          "label": 797
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "mysore-silk-saree-in-light-green-sbra388",
          "label": 798
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-mysore-silk-saree-in-yellow-sbra222",
          "label": 799
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-pure-mysore-silk-saree-in-light-beige-sbra201",
          "label": 800
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-mysore-silk-saree-in-pink-stbn126",
          "label": 801
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kanchipuram-saree-in-orange-snba658",
          "label": 802
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-gadwal-silk-saree-in-royal-blue-stga112",
          "label": 803
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-border-jute-cotton-saree-in-sea-green-sjn7083",
          "label": 804
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-chiffon-saree-in-light-beige-sar966",
          "label": 805
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "patch-border-lycra-shimmer-saree-in-pink-smu3164",
          "label": 806
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-silk-embroidered-saree-in-coral-pink-snea1060",
          "label": 807
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-shimmer-chiffon-prestitched-saree-in-pink-suf6834",
          "label": 808
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-chiffon-saree-in-mustard-sjra538",
          "label": 809
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-border-jute-cotton-saree-in-black-sjn7084",
          "label": 810
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "prestitched-maharashtrian-nauvari-saree-in-fuchsia-ssf3010",
          "label": 811
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-dupion-silk-kurta-set-in-off-white-und618",
          "label": 812
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-art-silk-pakistani-suit-in-pink-kmsg367",
          "label": 813
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-fuchsia-sxc1779",
          "label": 814
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-layered-gown-in-fuchsia-uku571",
          "label": 815
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "dupion-silk-dhoti-in-maroon-mpc578",
          "label": 816
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-straight-cut-suit-in-sky-blue-kcv1182",
          "label": 817
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "dupion-silk-dhoti-in-maroon-mpc645",
          "label": 818
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-saree-in-pastel-green-snba1395",
          "label": 819
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-long-skirt-in-navy-blue-thu412",
          "label": 820
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-abaya-style-suit-in-orange-kgzt136",
          "label": 821
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "tant-handloom-cotton-saree-in-off-white-spn1895",
          "label": 822
        },
        {
          "viewIntensity": 5,
          "level": "green",
          "id": "bandhej-crepe-saree-in-black-sjn7073",
          "label": 823
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brocade-flared-dress-in-beige-thu279",
          "label": 824
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-cotton-saree-in-off-white-spn2715",
          "label": 825
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brocade-blouse-in-gold-dbu617",
          "label": 826
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-neckline-cotton-silk-kurta-set-in-blue-mse787",
          "label": 827
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-asymmetric-pakistani-suit-in-mustard-kjr149",
          "label": 828
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-circular-lehenga-in-pink-ltl90",
          "label": 829
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-mustard-kjn3156",
          "label": 830
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bandhani-georgette-saree-in-white-suv123",
          "label": 831
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-cotton-tant-saree-in-off-white-and-sky-blue-shxa130",
          "label": 832
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-net-brasso-saree-in-pink-and-light-green-skra1308",
          "label": 833
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-banarasi-chiffon-saree-in-fuchsia-sau1491",
          "label": 834
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-purple-suf7412",
          "label": 835
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-blouse-in-magenta-utt449",
          "label": 836
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-antique-necklace-set-jdw522",
          "label": 837
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bhagalpuri-silk-saree-in-blue-sts3674",
          "label": 838
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-nehru-jacket-in-beige-mhg559",
          "label": 839
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-purple-sswa612",
          "label": 840
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-double-layered-straight-cut-cotton-chanderi-suit-in-blue-kjn2034",
          "label": 841
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-viscose-georgette-straight-suit-in-red-ktj392",
          "label": 842
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-pure-silk-handloom-saree-in-maroon-snea436",
          "label": 843
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-silk-saree-in-black-shxa12",
          "label": 844
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-mustard-sgpn377",
          "label": 845
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-chanderi-silk-anarkali-suit-in-beige-and-blue-kpk46",
          "label": 846
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-blue-sfwa245",
          "label": 847
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-chanderi-silk-saree-in-magenta-sas1341",
          "label": 848
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-abaya-style-suit-in-wine-kym419",
          "label": 849
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-beige-snea1171",
          "label": 850
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bhagalpuri-silk-saree-in-black-sts3638",
          "label": 851
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-pink-svqa81",
          "label": 852
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-light-orange-spfa1533",
          "label": 853
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-flared-tunic-in-purple-tdr756",
          "label": 854
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-mustard-and-red-sbta231",
          "label": 855
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-crepe-a-line-lehenga-in-fuchsia-luf571",
          "label": 856
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-black-svra379",
          "label": 857
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-handloom-saree-in-red-snea650",
          "label": 858
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-shimmer-georgette-palazzo-in-rose-gold-thu935",
          "label": 859
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-saree-in-mustard-ssl36179",
          "label": 860
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-anarkali-suit-in-black-kvng63",
          "label": 861
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-kurta-set-in-teal-green-msf321",
          "label": 862
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-striaght-cut-suit-in-red-kux122",
          "label": 863
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-net-saree-in-pink-and-green-sqfa159",
          "label": 864
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-leather-mojari-in-pink-duf22",
          "label": 865
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-saree-in-yellow-ssha826",
          "label": 866
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "american-diamond-necklace-set-jvr36",
          "label": 867
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-brown-and-black-szma62",
          "label": 868
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-gown-in-black-tsp176",
          "label": 869
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-mustard-sxc2240",
          "label": 870
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-pink-and-beige-lql46",
          "label": 871
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-tant-saree-in-grey-and-green-srga841",
          "label": 872
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-silk-saree-in-olive-green-snea639",
          "label": 873
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-beige-kch944",
          "label": 874
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-pure-cotton-saree-in-red-suv114",
          "label": 875
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-fuchsia-ukx121",
          "label": 876
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-jamdani-cotton-silk-handloom-saree-in-white-srga709",
          "label": 877
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-art-silk-dhoti-sherwani-in-beige-mhg506",
          "label": 878
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-tussar-silk-saree-in-purple-snea536",
          "label": 879
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-mysore-chiffon-saree-in-maroon-sbra783",
          "label": 880
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-wine-suf6106",
          "label": 881
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pair-of-bangles-jvk2998",
          "label": 882
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-lycra-saree-in-dark-fawn-sgpn207",
          "label": 883
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-organza-saree-in-pink-sfwa144",
          "label": 884
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-banarasi-silk-anarkali-suit-in-fuchsia-kjn2092",
          "label": 885
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-saree-in-dark-green-sfl2711",
          "label": 886
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-crop-top-set-in-rust-ttz75",
          "label": 887
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-anarkali-suit-in-dark-blue-kgb2630",
          "label": 888
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-maroon-kch644",
          "label": 889
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "polki-necklace-set-jjr16090",
          "label": 890
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-pakistani-suit-in-white-and-black-knf369",
          "label": 891
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "printed-jute-hand-bag-in-green-and-multicolor-dlj38",
          "label": 892
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-mysore-silk-saree-in-teal-green-sbra956",
          "label": 893
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "abstract-printed-georgette-saree-in-red-sgja131",
          "label": 894
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-silk-saree-in-purple-sswa612",
          "label": 895
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-malmal-top-in-white-trb291",
          "label": 896
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-taffeta-silk-pakistani-suit-in-black-kjf74",
          "label": 897
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-kurta-pajama-in-royal-blue-mtr31",
          "label": 898
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jjr16537",
          "label": 899
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "beaded-oxidised-earrings-jnj1927",
          "label": 900
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-south-cotton-saree-in-blue-sfl2613",
          "label": 901
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-pure-chanderi-silk-saree-in-black-skba181",
          "label": 902
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "border-satin-georgette-saree-in-shaded-blue-and-teal-green-syc6131",
          "label": 903
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-lehenga-in-pink-lsh70",
          "label": 904
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-pink-and-orange-sfs802",
          "label": 905
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-olive-green-sbra927",
          "label": 906
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-dark-brown-snca361",
          "label": 907
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-light-green-lrf19",
          "label": 908
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-set-in-blue-mpw2",
          "label": 909
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-teal-blue-spfa1886",
          "label": 910
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-layered-mathapatti-jrl909",
          "label": 911
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-saree-in-grey-scda82",
          "label": 912
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-blue-kve129",
          "label": 913
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-tussar-silk-saree-in-maroon-snea971",
          "label": 914
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pacchikari-hoop-jhumka-style-earring-jjr14384",
          "label": 915
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-yellow-sew4571",
          "label": 916
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-maroon-kfx1489",
          "label": 917
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-cream-sgka3089",
          "label": 918
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-saree-in-beige-stla278",
          "label": 919
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-fuchsia-kej941",
          "label": 920
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-abaya-style-suit-in-pink-kxzd38",
          "label": 921
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-viscose-georgette-abaya-style-suit-in-navy-blue-kuf9171",
          "label": 922
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pre-stitched-net-saree-in-purple-suf5371",
          "label": 923
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jjr16523",
          "label": 924
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "patch-border-georgette-saree-in-cream-sjn5924",
          "label": 925
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-pink-sga5415",
          "label": 926
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chikankari-poly-cotton-kurta-set-in-white-mpe163",
          "label": 927
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-beige-skra739",
          "label": 928
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-dark-green-sgqa19",
          "label": 929
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-lycra-net-saree-in-baby-pink-stl737",
          "label": 930
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-front-slit-kurta-in-teal-blue-tqj260",
          "label": 931
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pendant-jts488",
          "label": 932
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-pink-sbra808",
          "label": 933
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-adjustable-pair-of-anklet-jrl703",
          "label": 934
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pendant-set-jvm2500",
          "label": 935
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-silk-saree-in-pink-snea591",
          "label": 936
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-maroon-scfa450",
          "label": 937
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-brown-and-off-white-kfx2290",
          "label": 938
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "net-lehenga-with-embroidered-choli-in-coral-and-maroon-lxw241",
          "label": 939
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-orange-sgka3100",
          "label": 940
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-long-kurta-in-black-trv43",
          "label": 941
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-straight-suit-in-beige-kqu916",
          "label": 942
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-light-olive-green-ssf3783",
          "label": 943
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-shaded-grey-and-yellow-sgja637",
          "label": 944
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-mangalgiri-cotton-saree-in-black-and-turquoise-sbpa48",
          "label": 945
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "tie-n-dye-georgette-long-kurta-in-red-and-black-tjw425",
          "label": 946
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-coral-red-sud1467",
          "label": 947
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-red-kej892",
          "label": 948
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-black-srp611",
          "label": 949
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-asymmetric-kurta-in-black-tnc874",
          "label": 950
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-art-silk-saree-in-beige-and-green-svta91",
          "label": 951
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-turquoise-sud1472",
          "label": 952
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "dori-bangle-set-jdp37",
          "label": 953
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-crepe-asymmetric-tunic-in-red-thu739",
          "label": 954
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-layered-kurta-in-yellow-tbf70",
          "label": 955
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "stone-studded-necklace-set-jmy78",
          "label": 956
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brocade-jacket-style-lehenga-in-yellow-luz2",
          "label": 957
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-beige-uyp19",
          "label": 958
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-saree-in-grey-scda82",
          "label": 959
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-red-lqu201",
          "label": 960
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-black-szma151",
          "label": 961
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-dark-beige-lqm185",
          "label": 962
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "banarasi-silk-saree-in-sky-blue-sbta300",
          "label": 963
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-chiffon-saree-in-yellow-stu626",
          "label": 964
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pacchikari-anklet-jjr14989",
          "label": 965
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-embroidered-saree-in-fuchsia-and-orange-sqfa234",
          "label": 966
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-maroon-scfa445",
          "label": 967
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "contrast-border-chiffon-saree-in-red-szy676",
          "label": 968
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-georgette-saree-in-beige-ssf4325",
          "label": 969
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-net-abaya-style-suit-in-turquoise-kxz119",
          "label": 970
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "abstract-printed-cotton-flared-long-kurta-in-blue-tbk127",
          "label": 971
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-black-skpa706",
          "label": 972
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "gota-work-chiffon-lehenga-in-sky-blue-ljn1380",
          "label": 973
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-shaded-purple-and-cream-sew4469",
          "label": 974
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-black-svqa78",
          "label": 975
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-satin-crepe-saree-in-teal-blue-smda503",
          "label": 976
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-red-and-cream-sbz3393",
          "label": 977
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-patch-border-work-chiffon-saree-in-red-sga5354",
          "label": 978
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-chiffon-saree-in-pink-and-dark-brown-skk22546",
          "label": 979
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-light-old-rose-ssx5447",
          "label": 980
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "teal-green-cotton-dupatta-brj131",
          "label": 981
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-brocade-silk-sherwani-in-golden-mxh55",
          "label": 982
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-teal-green-sgja631",
          "label": 983
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-adjustable-hasli-choker-necklace-set-jvk1692",
          "label": 984
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-south-cotton-long-kurta-set-in-off-white-tja350",
          "label": 985
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-choker-necklace-set-jmy183",
          "label": 986
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-yellow-skpa1054",
          "label": 987
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-jacket-style-lehenga-in-olive-green-and-beige-lqm140",
          "label": 988
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-inshaded-red-and-navy-blue-sqfa245",
          "label": 989
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-cotton-straight-suit-in-green-ombre-kfx2658",
          "label": 990
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-saree-in-sky-blue-safa130",
          "label": 991
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jacquard-nehru-jacket-in-magenta-mse512",
          "label": 992
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-magenta-kym423",
          "label": 993
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-rust-scfa306",
          "label": 994
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-printed-pure-tussar-silk-saree-in-light-beige-sffa25",
          "label": 995
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-tant-saree-in-yellow-spn3522",
          "label": 996
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-jacquard-saree-in-violet-and-cream-sjra174",
          "label": 997
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-brocade-sherwani-in-beige-mhg734",
          "label": 998
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-yellow-sgpn464",
          "label": 999
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-saree-in-peach-sfs621",
          "label": 1000
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-circular-lehenga-in-blue-lqu413",
          "label": 1001
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pendant-set-jvk2097",
          "label": 1002
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-short-kurta-in-aqua-blue-mee289",
          "label": 1003
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "banarasi-silk-saree-in-pink-and-mustard-stma148",
          "label": 1004
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-anarkali-style-lehenga-in-red-luf540",
          "label": 1005
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-raw-silk-kurta-set-in-light-yellow-mxh127",
          "label": 1006
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-black-sbra318",
          "label": 1007
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-georgette-saree-in-black-sjra367",
          "label": 1008
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "tye-n-die-georgette-saree-in-blue-sbh1742",
          "label": 1009
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-royal-blue-kch168",
          "label": 1010
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-red-svra388",
          "label": 1011
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-art-dupion-silk-dhoti-kurta-in-off-white-mpc768",
          "label": 1012
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-art-tussar-silk-dupatta-in-light-grey-bbe61",
          "label": 1013
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-pure-silk-saree-in-purple-stbn49",
          "label": 1014
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-georgette-banarasi-saree-in-royal-blue-snea273",
          "label": 1015
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-beige-scfa459",
          "label": 1016
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "solid-crepe-zouave-pant-in-black-thu851",
          "label": 1017
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-beige-kfx1890",
          "label": 1018
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-mysore-silk-saree-in-dark-green-shu791",
          "label": 1019
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-rayon-straight-kurta-in-pink-and-grey-tbk88",
          "label": 1020
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-rayon-cowl-style-kurta-set-in-beige-and-maroon-mhg725",
          "label": 1021
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-tant-saree-in-orange-spn3017",
          "label": 1022
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-crop-top-in-maroon-trb648",
          "label": 1023
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-lycra-net-saree-in-beige-and-fuchsia-svla323",
          "label": 1024
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-straight-suit-in-light-beige-and-red-kyz201",
          "label": 1025
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-kurta-in-light-purple-tnz15",
          "label": 1026
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-light-beige-and-multicolor-spfa1975",
          "label": 1027
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-chiffon-saree-in-sea-green-sck493",
          "label": 1028
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-beige-sud1026",
          "label": 1029
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-green-sbra330",
          "label": 1030
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-silk-saree-in-light-moss-green-scxa601",
          "label": 1031
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-straight-cut-suit-in-red-and-white-ket30",
          "label": 1032
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-cotton-saree-in-sky-blue-sswa525",
          "label": 1033
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-black-sbh714",
          "label": 1034
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "golden-stones-studded-earrings-jsw124",
          "label": 1035
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-jacquard-saree-in-green-and-teal-green-sgja601",
          "label": 1036
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-saree-in-indigo-blue-stea399",
          "label": 1037
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-brocade-silk-sherwani-in-beige-mhg728",
          "label": 1038
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-printed-cotton-saree-in-olive-green-and-blue-scfa427",
          "label": 1039
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pearl-work-georgette-saree-in-red-skpa150",
          "label": 1040
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-jvk2002",
          "label": 1041
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-net-lehenga-in-off-white-and-indigo-blue-luc19",
          "label": 1042
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "color-block-rayon-kurta-in-blue-and-white-trq266",
          "label": 1043
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-chiffon-saree-in-pink-and-light-blue-skra727",
          "label": 1044
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-kurta-pajama-in-maroon-mtr50",
          "label": 1045
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-super-net-saree-in-turquoise-sfwa169",
          "label": 1046
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-hasli-choker-set-jvk1748",
          "label": 1047
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-anarkali-suit-in-red-kuf8351",
          "label": 1048
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-black-kbz57",
          "label": 1049
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-net-saree-in-red-and-blue-sqh1387",
          "label": 1050
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-banarasi-silk-saree-in-orange-snea1158",
          "label": 1051
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-chanderi-straight-cut-suit-in-light-yellow-kjn2807",
          "label": 1052
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-lycra-combo-leggings-in-blue-yellow-and-green-tsf366",
          "label": 1053
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-turquoise-and-navy-blue-sjra126",
          "label": 1054
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-asymmetric-suit-in-black-kuz270",
          "label": 1055
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-lycra-net-saree-in-beige-and-coral-red-svla324",
          "label": 1056
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-jacquard-kurti-in-peach-tdr647",
          "label": 1057
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-green-ombre-kucd7",
          "label": 1058
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-viscose-pakistani-suit-in-beige-kau131",
          "label": 1059
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-chanderi-silk-saree-in-coral-red-skba171",
          "label": 1060
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-light-orange-utt574",
          "label": 1061
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-necklace-set-jmy337",
          "label": 1062
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-red-sar867",
          "label": 1063
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-kanchipuram-silk-saree-in-purple-seh1462",
          "label": 1064
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-cotton-dupatta-in-peach-orange-bmb6",
          "label": 1065
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-saree-in-beige-shp812",
          "label": 1066
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-satin-georgette-saree-in-red-and-beige-sew4200",
          "label": 1067
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-straight-cut-suit-in-off-white-and-old-rose-kds696",
          "label": 1068
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-dhoti-in-maroon-mpc572",
          "label": 1069
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-pink-lzr175",
          "label": 1070
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-rayon-tunic-in-orange-trq209",
          "label": 1071
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-georgette-silk-banarasi-saree-in-royal-blue-snea273",
          "label": 1072
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-satin-silk-banarasi-saree-in-wine-snea879",
          "label": 1073
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-anarkali-suit-in-royal-blue-kuz110",
          "label": 1074
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-cotton-saree-in-sky-blue-sswa525",
          "label": 1075
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-and-net-saree-in-orange-sas1210",
          "label": 1076
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-pink-kae778",
          "label": 1077
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "studded-bangle-pair-in-magenta-jvk1211",
          "label": 1078
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-maroon-scfa304",
          "label": 1079
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-south-cotton-straight-suit-in-pink-kfx2647",
          "label": 1080
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-top-in-green-tbf38",
          "label": 1081
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "jamdani-cotton-silk-saree-in-red-scxa599",
          "label": 1082
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-linen-kurta-jacket-set-in-white-and-coral-pink-mhg665",
          "label": 1083
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-navy-blue-ombre-sjra133",
          "label": 1084
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-neckline-dupion-silk-kurta-set-in-olive-green-mxh20",
          "label": 1085
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-bhagalpuri-silk-circular-lehenga-in-beige-ljn1272",
          "label": 1086
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-poly-cotton-straight-suit-in-fuchsia-kmsg96",
          "label": 1087
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jacquard-cotton-silk-nehru-jacket-in-light-beige-mjt8",
          "label": 1088
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-silk-saree-in-light-beige-spn2820",
          "label": 1089
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "dupion-silk-dhoti-in-red-mpc657",
          "label": 1090
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-anarkali-suit-in-black-kqb37",
          "label": 1091
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-beige-kgzt236",
          "label": 1092
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-saree-in-off-white-ssf3747",
          "label": 1093
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-dupion-silk-saree-in-beige-svqa189",
          "label": 1094
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-neckline-art-silk-gown-in-brown-ttv15",
          "label": 1095
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-fuchsia-kxc762",
          "label": 1096
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "gota-patti-art-silk-circular-lehenga-in-maroon-ombre-lzl19",
          "label": 1097
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-fuchsia-sbra796",
          "label": 1098
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "gota-patti-chinon-chiffon-saree-in-yellow-snya21",
          "label": 1099
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-linen-kurta-jacket-set-in-white-and-golden-mhg666",
          "label": 1100
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bhagalpuri-silk-saree-in-green-sjn2929",
          "label": 1101
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "mangalgiri-cotton-saree-in-orange-sbpa61",
          "label": 1102
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-sherwani-in-off-white-mrg445",
          "label": 1103
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-silk-straight-cut-suit-in-off-white-kjn2513",
          "label": 1104
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-straight-suit-in-pink-kcu166",
          "label": 1105
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "ombre-chiffon-saree-in-peach-and-orange-szy675",
          "label": 1106
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-a-line-lehenga-in-red-lyn2",
          "label": 1107
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-coral-red-sau2308",
          "label": 1108
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-saree-in-dark-blue-ssja192",
          "label": 1109
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bangle-set-jdp415",
          "label": 1110
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-asymmetric-kurta-in-navy-blue-tgp72",
          "label": 1111
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bangle-pair-in-magenta-jvk1208",
          "label": 1112
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-maroon-sgka1583",
          "label": 1113
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-satin-georgette-saree-in-teal-green-and-sea-green-sew3344",
          "label": 1114
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-blue-kds817",
          "label": 1115
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-gown-in-peach-tbl106",
          "label": 1116
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-block-printed-art-silk-indowestern-lehenga-in-white-lkc97",
          "label": 1117
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "metallic-bangles-in-fuchsia-jvk475",
          "label": 1118
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-punjabi-suit-in-light-beige-and-blue-kyz208",
          "label": 1119
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "contrast-border-chiffon-saree-in-purple-sur11",
          "label": 1120
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-sky-blue-scxa538",
          "label": 1121
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-viscose-georgette-straight-suit-in-peach-krgv27",
          "label": 1122
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "tie-n-dye-georgette-saree-in-pink-sjn6382",
          "label": 1123
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-blue-kqu957",
          "label": 1124
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-necklace-set-jvm2456",
          "label": 1125
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-red-kxc762",
          "label": 1126
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-red-shk1521",
          "label": 1127
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-and-chanderi-silk-jacquard-circular-lehenga-in-pink-ljn1267",
          "label": 1128
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-silk-pant-in-yellow-thu964",
          "label": 1129
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-chiffon-saree-in-coral-red-sqfa341",
          "label": 1130
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-pure-cotton-saree-in-beige-suv113",
          "label": 1131
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-anarkali-suit-in-black-kxh82",
          "label": 1132
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-paithani-silk-saree-in-magenta-stbn102",
          "label": 1133
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-saree-in-dark-green-stla270",
          "label": 1134
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-shaded-orange-sew5495",
          "label": 1135
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-silk-saree-in-pink-snea634",
          "label": 1136
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-flared-long-kurta-in-navy-blue-and-off-white-tkx125",
          "label": 1137
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-top-n-skirt-in-navy-blue-ttz92",
          "label": 1138
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-saree-in-blue-sgya505",
          "label": 1139
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-green-sau1787",
          "label": 1140
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-saree-in-fuchsia-sbea514",
          "label": 1141
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-sjra345",
          "label": 1142
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-kurta-set-in-light-blue-mrg417",
          "label": 1143
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-velvet-circular-lehenga-in-royal-blue-lcc139",
          "label": 1144
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-purple-sqpa251",
          "label": 1145
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-earrings-jjr15202",
          "label": 1146
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-layered-matha-patti-jpm2348",
          "label": 1147
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-choker-necklace-set-jjr16421",
          "label": 1148
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-silk-tant-saree-in-old-rose-shxa396",
          "label": 1149
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-kurta-set-with-nehru-jacket-in-grey-mtr71",
          "label": 1150
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pintucked-cotton-flex-kurta-in-beige-tja681",
          "label": 1151
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-abaya-style-art-silk-suit-in-blue-kch81",
          "label": 1152
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-jacquard-saree-in-yellow-sas1041",
          "label": 1153
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bordered-georgette-saree-in-fuchsia-and-orange-sew3817",
          "label": 1154
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-teal-green-sbra313",
          "label": 1155
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "prestitched-maharashtrian-nauvari-saree-in-purple-ssf3007",
          "label": 1156
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-light-beige-stu761",
          "label": 1157
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "prestitched-maharashtrian-nauvari-saree-in-mustard-ssf3004",
          "label": 1158
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-yellow-sgja640",
          "label": 1159
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-chiffon-saree-in-pink-szta121",
          "label": 1160
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bhagalpuri-silk-saree-in-teal-blue-skra933",
          "label": 1161
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kerala-kasavu-hand-painted-cotton-saree-in-white-sfya30",
          "label": 1162
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-maroon-sqpa67",
          "label": 1163
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-georgette-a-line-suit-in-purple-kjn2539",
          "label": 1164
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-pink-ssf3611",
          "label": 1165
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "jamdani-cotton-saree-in-yellow-shxa243",
          "label": 1166
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kanchipuram-saree-in-pink-seh1808",
          "label": 1167
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bordered-georgette-saree-in-shaded-red-and-pink-sfs680",
          "label": 1168
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-maroon-kch300",
          "label": 1169
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-black-sur162",
          "label": 1170
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-beige-and-dark-green-sgpn305",
          "label": 1171
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-purple-and-beige-suf7414",
          "label": 1172
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-mysore-silk-saree-in-old-rose-and-royal-blue-shu451",
          "label": 1173
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-rayon-kurta-in-turquoise-tyq58",
          "label": 1174
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "foil-printed-chiffon-saree-in-blue-suv90",
          "label": 1175
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-red-suf7713",
          "label": 1176
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-navy-blue-skpa922",
          "label": 1177
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "contrast-border-georgette-saree-in-navy-blue-sjra250",
          "label": 1178
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "tant-cotton-saree-in-orange-shxa288",
          "label": 1179
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-and-net-saree-in-black-and-pink-sqh1394",
          "label": 1180
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-saree-in-teal-green-spfa1897",
          "label": 1181
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-off-white-skra1323",
          "label": 1182
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-dupion-silk-lehenga-in-orange-and-red-lvx95",
          "label": 1183
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-dark-green-sqpa207",
          "label": 1184
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-shimmer-georgette-circular-lehenga-in-off-white-lkc36",
          "label": 1185
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-peach-shka320",
          "label": 1186
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-silk-saree-in-off-white-skra1323",
          "label": 1187
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-yellow-sfwa257",
          "label": 1188
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-wine-snba726",
          "label": 1189
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-chiffon-saree-in-blue-szy670",
          "label": 1190
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ghatchola-crepe-saree-in-indigo-suf7334",
          "label": 1191
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-hasli-choker-set-jvk1765",
          "label": 1192
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-straight-kurta-in-brown-tja594",
          "label": 1193
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-off-white-mse658",
          "label": 1194
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jjr16515",
          "label": 1195
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "american-diamonds-necklace-set-jjr16474",
          "label": 1196
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jmy116",
          "label": 1197
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-saree-in-light-teal-green-sjra101",
          "label": 1198
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2701",
          "label": 1199
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-kurta-in-navy-blue-tmz106",
          "label": 1200
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-satin-saree-in-green-and-teal-green-spfa2146",
          "label": 1201
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-teal-green-and-navy-blue-spta252",
          "label": 1202
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-handloom-saree-in-orange-dual-tone-shp791",
          "label": 1203
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-kanchipuram-saree-in-orange-sbra18",
          "label": 1204
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-cotton-satin-pakistani-suit-in-light-green-kaz177",
          "label": 1205
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-brasso-straight-suit-in-fuchsia-kry925",
          "label": 1206
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-seh1311",
          "label": 1207
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-cut-suit-in-coral-kfx1957",
          "label": 1208
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-lycra-saree-in-orange-sbja51",
          "label": 1209
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-silk-paithani-saree-in-light-purple-stbn98",
          "label": 1210
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kanchipuram-saree-in-cream-seh1906",
          "label": 1211
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-cut-suit-in-red-kux122",
          "label": 1212
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-mysore-silk-saree-in-green-shu638",
          "label": 1213
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-megenta-kfx1967",
          "label": 1214
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "art-silk-blouse-in-red-dbu443",
          "label": 1215
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-linen-cotton-kurta-set-in-white-mhg392",
          "label": 1216
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-kurta-in-navy-blue-and-rust-tbk160",
          "label": 1217
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-chanderi-silk-woven-saree-in-black-skba238",
          "label": 1218
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-anarkali-suit-in-pink-kvng62",
          "label": 1219
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-saree-in-beige-smda704",
          "label": 1220
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-gown-in-red-tsp7",
          "label": 1221
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-jacquard-viscose-saree-in-brown-sas1084",
          "label": 1222
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-dark-olive-sfka337",
          "label": 1223
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-temple-necklace-set-jnj1982",
          "label": 1224
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-bhagalpurisilk-crop-top-with-skirt-in-teal-blue-trb619",
          "label": 1225
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-pathani-suit-in-black-mxh102",
          "label": 1226
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-short-kurta-in-black-mee280",
          "label": 1227
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-saree-in-red-and-black-sgpn391",
          "label": 1228
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-set-in-beige-mpc741",
          "label": 1229
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-shaded-fuchsia-and-peach-stl742",
          "label": 1230
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pacchikari-necklace-set-jjr16060",
          "label": 1231
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-pure-georgette-saree-in-pink-sjn7062",
          "label": 1232
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-orange-ssf4149",
          "label": 1233
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pendent-set-jjr14239",
          "label": 1234
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-adjustable-haathphool-jjr16496",
          "label": 1235
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-satin-pakistani-suit-in-cream-kpv137",
          "label": 1236
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-saree-in-peach-svqa181",
          "label": 1237
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-satin-chiffon-saree-in-orange-and-maroon-sew5414",
          "label": 1238
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-beige-kch890",
          "label": 1239
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-blue-and-light-teal-green-sgja57",
          "label": 1240
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-blue-sgka2808",
          "label": 1241
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-banarasi-silk-saree-in-green-snea1151",
          "label": 1242
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-lycra-and-georgette-saree-in-pink-and-green-ssx5405",
          "label": 1243
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-dusty-green-lcc150",
          "label": 1244
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-art-silk-saree-in-green-sqfa153",
          "label": 1245
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chikankari-poly-cotton-kurta-in-white-mpe126",
          "label": 1246
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-and-net-abaya-suit-in-navy-blue-kym387",
          "label": 1247
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-neckline-art-silk-jacquard-kurta-set-in-navy-blue-mve305",
          "label": 1248
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-waist-chain-in-white-jjr13196",
          "label": 1249
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-georgette-saree-in-green-sfva51",
          "label": 1250
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-poly-cotton-straight-kurta-in-fuchsia-tbk60",
          "label": 1251
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-cotton-silk-saree-in-red-and-black-srga388",
          "label": 1252
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bhagalpuri-silk-saree-in-light-green-sts3673",
          "label": 1253
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-art-silk-nehru-jacket-in-cream-mhg711",
          "label": 1254
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jacquard-art-silk-kurta-set-in-maroon-and-beige-mcd2954",
          "label": 1255
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-handloom-saree-in-beige-shp812",
          "label": 1256
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-rayon-straight-cut-suit-in-green-and-blue-ktp1616",
          "label": 1257
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jute-silk-jodhpuri-jacket-in-black-mcd2959",
          "label": 1258
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-handloom-saree-in-light-beige-and-orange-srga675",
          "label": 1259
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "zari-lace-work-art-silk-short-kurta-in-peach-mnl134",
          "label": 1260
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "chanderi-anarkali-suit-in-orange-with-gota-patti-jacket-kjn2798",
          "label": 1261
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-bhagalpuri-silk-saree-in-black-and-off-white-sbh1596",
          "label": 1262
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-taffeta-silk-blouse-in-maroon-ufa51",
          "label": 1263
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-in-red-and-white-jrl1952",
          "label": 1264
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-top-in-pink-tja569",
          "label": 1265
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-brasso-saree-in-red-sfka1022",
          "label": 1266
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-straight-suit-in-beige-kfx2248",
          "label": 1267
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-mustard-sfka1443",
          "label": 1268
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-armlet-jda1543",
          "label": 1269
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "dupion-silk-top-in-fuchsia-thu770",
          "label": 1270
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-straight-suit-in-light-pink-kcu152",
          "label": 1271
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-navy-blue-sgja450",
          "label": 1272
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-skirt-in-black-bts170",
          "label": 1273
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-armlet-jvk2695",
          "label": 1274
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-maroon-spfa1904",
          "label": 1275
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-black-sma5614",
          "label": 1276
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-banarasi-silk-handloom-saree-in-maroon-snea1235",
          "label": 1277
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kalamkari-cotton-saree-in-light-pink-sswa728",
          "label": 1278
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-indigo-sfva75",
          "label": 1279
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-chiffon-and-net-saree-in-pink-and-yellow-seh1514",
          "label": 1280
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "gota-patti-embroidered-straight-cut-suit-in-shaded-mustard-kjn1311",
          "label": 1281
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-black-kut209",
          "label": 1282
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-art-silk-lehenga-in-peach-lyv11",
          "label": 1283
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-abaya-style-suit-in-black-kqu886",
          "label": 1284
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-rust-seh1577",
          "label": 1285
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-off-white-sul434",
          "label": 1286
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-neckline-art-silk-kurta-set-in-beige-mpd329",
          "label": 1287
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-abaya-style-suit-in-grey-kjn2677",
          "label": 1288
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-muga-silk-saree-in-blue-srp598",
          "label": 1289
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-beige-kfx2018",
          "label": 1290
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-red-lqm187",
          "label": 1291
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-saree-in-mustard-sew2534",
          "label": 1292
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-navy-blue-mgv124",
          "label": 1293
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-cotton-straight-suit-in-beige-kfx1818",
          "label": 1294
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-silk-saree-in-light-green-skra1105",
          "label": 1295
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-front-slit-straight-cut-art-silk-suit-in-navy-blue-kxz76",
          "label": 1296
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-satin-georgette-saree-in-red-and-peach-sgja401",
          "label": 1297
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bandhej-printed-art-silk-a-line-lehenga-in-shaded-royal-blue-luf1285",
          "label": 1298
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-circular-lehenga-in-red-lqm187",
          "label": 1299
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "abstract-printed-georgette-saree-in-light-beige-sgja409",
          "label": 1300
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-yellow-smda727",
          "label": 1301
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-anarkali-long-kurta-in-off-white-tnc562",
          "label": 1302
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-straight-cut-georgette-suit-in-off-white-and-black-kuz235",
          "label": 1303
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-tussar-silk-saree-in-coral-and-peach-seh1557",
          "label": 1304
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-brown-ukx101",
          "label": 1305
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-long-kurta-in-pink-tdr619",
          "label": 1306
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-pure-tissue-saree-in-maroon-sjn6614",
          "label": 1307
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-choker-necklace-set-jjr15671",
          "label": 1308
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-maroon-and-olive-green-scfa326",
          "label": 1309
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-white-kve154",
          "label": 1310
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kerala-kasavu-woven-cotton-saree-in-off-white-spn2941",
          "label": 1311
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-short-kurta-in-brown-mee312",
          "label": 1312
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-banarasi-silk-saree-in-violet-seh1782",
          "label": 1313
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-lycra-net-saree-in-beige-and-orange-svla325",
          "label": 1314
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pearl-layered-kanthimala-mtj31",
          "label": 1315
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bordered-georgette-saree-in-teal-green-sew3819",
          "label": 1316
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-orange-uyp58",
          "label": 1317
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "meenakri-necklace-set-jjr14481",
          "label": 1318
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kundan-bridal-necklace-set-jjr14449",
          "label": 1319
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-cotton-straight-suit-in-orange-scda93",
          "label": 1320
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-green-and-red-spfa2023",
          "label": 1321
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-chiffon-saree-in-beige-and-pink-ssf3466",
          "label": 1322
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-tussar-silk-saree-in-purple-snea486",
          "label": 1323
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "kundan-earrings-jjr15232",
          "label": 1324
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-beige-sga6849",
          "label": 1325
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-circular-lehenga-in-beige-and-red-lxw187",
          "label": 1326
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-dupion-silk-kurta-set-in-navy-blue-mfy246",
          "label": 1327
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bordered-satin-georgette-saree-in-pink-syc6342",
          "label": 1328
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "lehariya-printed-georgette-saree-in-mustard-sjn6087",
          "label": 1329
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-matka-silk-saree-in-green-spn3377",
          "label": 1330
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embossed-velvet-jodhpuri-suit-in-dark-blue-mhg476",
          "label": 1331
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-and-georgette-long-kurta-in-black-and-white-thu1445",
          "label": 1332
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pintucks-kurta-set-in-white-mxh75",
          "label": 1333
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-top-in-maroon-tcz294",
          "label": 1334
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "lehariya-printed-georgette-saree-in-orange-sjn5982",
          "label": 1335
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-front-slit-straight-cut-suit-in-fuchsia-kjn2099",
          "label": 1336
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-dupion-silk-dhoti-kurta-in-red-mty21",
          "label": 1337
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pleated-georgette-palazzo-in-teal-green-byt89",
          "label": 1338
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-a-line-suit-in-off-white-and-brown-kyk74",
          "label": 1339
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "cotton-settu-mundu-in-off-white-spn3001",
          "label": 1340
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-georgette-brasso-saree-in-grey-sgpn577",
          "label": 1341
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-dress-in-red-tpx13",
          "label": 1342
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-mustard-sgka3075",
          "label": 1343
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-peach-sfka593",
          "label": 1344
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-khadi-kurta-set-in-grey-mve192",
          "label": 1345
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-bangle-set-jdp169",
          "label": 1346
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-red-utt603",
          "label": 1347
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-chanderi-cotton-dupatta-in-olive-green-bmb28",
          "label": 1348
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-front-slit-straight-cut-suit-in-beige-kls693",
          "label": 1349
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-tant-saree-in-yellow-spn3289",
          "label": 1350
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-crepe-mysore-saree-in-off-white-and-maroon-shu697",
          "label": 1351
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-maang-tikka-jjr14043",
          "label": 1352
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jmy80",
          "label": 1353
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-jacquard-top-n-skirt-in-beige-and-black-ttz98",
          "label": 1354
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-navy-blue-kej950",
          "label": 1355
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-tussar-silk-saree-in-pastel-orange-and-beige-snua167",
          "label": 1356
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "combo-front-slit-art-silk-straight-suit-in-light-purple-kvng82",
          "label": 1357
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-shaded-light-teal-blue-and-green-sqfa2",
          "label": 1358
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-layered-abaya-style-suit-in-peach-kch575",
          "label": 1359
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-jacquard-saree-in-fuchsia-sas1043",
          "label": 1360
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-kurta-set-in-beige-mpw235",
          "label": 1361
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "leheriya-georgette-abaya-style-suit-in-green-kjn2548",
          "label": 1362
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-pakistani-suit-in-peach-kthb167",
          "label": 1363
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-satin-punjabi-suit-in-coral-kfx2151",
          "label": 1364
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-super-net-jacquard-saree-in-white-sbea518",
          "label": 1365
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-rayon-flared-kurta-in-navy-blue-tqg11",
          "label": 1366
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-poly-cotton-straight-suit-in-teal-blue-kmsg95",
          "label": 1367
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-dupion-silk-saree-in-fuchsia-sgya143",
          "label": 1368
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-silk-saree-in-blue-sswa339",
          "label": 1369
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-banarasi-silk-saree-in-orange-sbta100",
          "label": 1370
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kantha-hand-embroidered-cotton-straight-suit-in-teal-green-kvng16",
          "label": 1371
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chanderi-silk-saree-in-orange-sau2408",
          "label": 1372
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-khadi-cotton-saree-in-brown-sqvd64",
          "label": 1373
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-jacquard-straight-suit-in-black-kmsg6",
          "label": 1374
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-banarasi-silk-saree-in-red-sbta498",
          "label": 1375
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brass-based-necklace-set-jdw452",
          "label": 1376
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-anarkali-suit-in-mustard-kuf8529",
          "label": 1377
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-set-of-bangles-jxm24",
          "label": 1378
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-art-silk-kurta-set-in-maroon-mpd362",
          "label": 1379
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-lycra-net-saree-in-beige-and-orange-stl691",
          "label": 1380
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-art-silk-saree-in-turquoise-sfva20",
          "label": 1381
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-abaya-style-suit-in-light-yellow-kxzd41",
          "label": 1382
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-red-sud1127",
          "label": 1383
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-white-kfx2371",
          "label": 1384
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-anarkali-suit-in-black-and-white-ktv82",
          "label": 1385
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-velvet-lehenga-in-royal-blue-lxw106",
          "label": 1386
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "floral-printed-georgette-saree-in-beige-sgya382",
          "label": 1387
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-green-svqa80",
          "label": 1388
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-teal-green-sew2129",
          "label": 1389
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "linen-nehru-jacket-in-grey-mhg224",
          "label": 1390
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-kurti-in-beige-tmz241",
          "label": 1391
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-violet-kbz228",
          "label": 1392
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-art-silk-saree-in-light-green-sfka1067",
          "label": 1393
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-velvet-and-brocade-sherwani-in-purple-and-multicolor-mpc805",
          "label": 1394
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-blue-sbra412",
          "label": 1395
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "butterfly-pallu-lycra-shimmer-georgette-saree-in-royal-blue-sjn6949",
          "label": 1396
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-art-silk-saree-in-pastel-pink-sfva48",
          "label": 1397
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-velvet-blouse-in-maroon-dbu405",
          "label": 1398
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-art-silk-circular-lehenga-in-shaded-blue-lfu473",
          "label": 1399
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-saree-in-light-blue-and-golden-suf7635",
          "label": 1400
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-bhagalpuri-silk-saree-in-grey-sga6390",
          "label": 1401
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-handloom-saree-in-fuchsia-sgka988",
          "label": 1402
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jjr15567",
          "label": 1403
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-brocade-sharara-lehenga-in-navy-blue-lkc21",
          "label": 1404
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-art-silk-saree-in-green-spta25",
          "label": 1405
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-choker-necklace-set-jmy246",
          "label": 1406
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-chanderi-kurti-in-beige-tuf504",
          "label": 1407
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "tie-dyed-georgette-saree-in-shaded-brown-and-rust-sjra242",
          "label": 1408
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-kurta-in-maroon-and-white-tcz217",
          "label": 1409
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-chiffon-saree-in-red-and-peach-spta280",
          "label": 1410
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pair-of-openable-bangles-jvk2873",
          "label": 1411
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "stone-studded-hasli-choker-set-jvk1754",
          "label": 1412
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-shimmer-georgette-saree-in-olive-green-and-beige-spfa1960",
          "label": 1413
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-jhapta-in-green-jjr13821",
          "label": 1414
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-fuchsia-seh952",
          "label": 1415
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-maroon-skk22112",
          "label": 1416
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-chanderi-saree-in-fuchsia-sfka278",
          "label": 1417
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-muga-silk-kurta-set-in-olive-green-mrg253",
          "label": 1418
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "woven-cotton-saree-in-off-white-spn2715",
          "label": 1419
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "cotton-combo-sets-in-petticoat-uub58",
          "label": 1420
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-teal-green-sgja423",
          "label": 1421
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-red-and-beige-ssja246",
          "label": 1422
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "georgette-readymade-long-kurta-in-light-teal-blue-tbl86",
          "label": 1423
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-border-satin-saree-in-ombre-beige-and-fuchsia-szg655",
          "label": 1424
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-cut-suit-in-yellow-and-white-kfx1972",
          "label": 1425
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-crepe-asymmetric-long-kurta-in-green-thu1453",
          "label": 1426
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-maroon-scfa350",
          "label": 1427
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-pink-seh1815",
          "label": 1428
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "brocade-blouse-in-beige-uyc75",
          "label": 1429
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-dark-pink-scxa794",
          "label": 1430
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-georgette-saree-in-wine-sjra74",
          "label": 1431
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-shrug-in-blue-thu564",
          "label": 1432
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-sherwani-in-dark-blue-mpw174",
          "label": 1433
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embellished-satin-chiffon-saree-in-pastel-green-ssva257",
          "label": 1434
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-chanderi-cotton-silk-saree-in-white-snea533",
          "label": 1435
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-kota-silk-saree-in-red-safa115",
          "label": 1436
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-jacquard-tunic-in-pink-tmw98",
          "label": 1437
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-grey-lsh157",
          "label": 1438
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-olive-green-sau1461",
          "label": 1439
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-matka-silk-saree-in-baby-pink-stla352",
          "label": 1440
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-velvet-sherwani-in-maroon-mrg306",
          "label": 1441
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-brooch-in-white-and-maroon-jjr12719",
          "label": 1442
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-sherwani-in-grey-ombre-mpc803",
          "label": 1443
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-south-cotton-saree-in-cream-stba72",
          "label": 1444
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-art-silk-jodhpuri-suit-in-fuchsia-mhg609",
          "label": 1445
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-cotton-saree-in-off-white-sswa581",
          "label": 1446
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "handloom-kanchipuram-art-silk-saree-in-yellow-skra657",
          "label": 1447
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-linen-saree-in-dark-blue-spn3374",
          "label": 1448
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-silk-jacquard-saree-in-pink-sbea564",
          "label": 1449
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-nehru-jacket-in-beige-and-mustard-mhg606",
          "label": 1450
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-teal-blue-and-pastel-green-sew3335",
          "label": 1451
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jute-silk-nehru-jacket-in-white-mpe21",
          "label": 1452
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "floral-printed-cotton-straight-suit-in-cream-kfx2608",
          "label": 1453
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-spfa2222",
          "label": 1454
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-navy-blue-kvs1709",
          "label": 1455
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-mysore-silk-saree-in-orange-shu620",
          "label": 1456
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-neckline-georgette-anarkali-suit-in-navy-blue-kuz253",
          "label": 1457
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-gown-in-red-taz1",
          "label": 1458
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-beige-kyx1134",
          "label": 1459
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-georgette-saree-in-red-skpa756",
          "label": 1460
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-floral-printed-georgette-saree-in-maroon-and-beige-sew4700",
          "label": 1461
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-straight-suit-in-light-beige-kch822",
          "label": 1462
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-georgette-straight-suit-in-white-kthb142",
          "label": 1463
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-cotton-straight-suit-in-fuchsia-kexm93",
          "label": 1464
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bordered-georgette-saree-in-shaded-turquoise-sfs692",
          "label": 1465
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-linen-jamdani-saree-in-black-skza70",
          "label": 1466
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-jacquard-nehru-jacket-in-grey-mhg587",
          "label": 1467
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pacchikari-necklace-set-jjr16238",
          "label": 1468
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pachhikari-jhumki-style-earring-jjr13408",
          "label": 1469
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "maroon-and-white-artificial-pearl-bangles-jvk1481",
          "label": 1470
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "half-n-half-lycra-saree-in-peach-and-beige-sbja252",
          "label": 1471
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-prestitched-saree-in-teal-blue-and-off-white-suf7700",
          "label": 1472
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "half-n-half-brasso-saree-in-cream-and-wine-suf7454",
          "label": 1473
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-net-and-art-silk-brocade-anarkali-suit-in-white-kthb6",
          "label": 1474
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-net-saree-in-off-white-svx797",
          "label": 1475
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-black-and-purple-scfa257",
          "label": 1476
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-chanderi-silk-saree-in-light-blue-srp689",
          "label": 1477
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-yellow-sgka2962",
          "label": 1478
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-tussar-silk-saree-in-white-seh1415",
          "label": 1479
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-taffeta-silk-punjabi-suit-in-violet-kch975",
          "label": 1480
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-ghicha-silk-sherwani-in-teal-blue-mse632",
          "label": 1481
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-pure-silk-saree-in-green-stbn63",
          "label": 1482
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-shaded-pink-sew5322",
          "label": 1483
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-cotton-saree-in-red-sfka1247",
          "label": 1484
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-punjabi-suit-in-blue-kve121",
          "label": 1485
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-rust-scfa331",
          "label": 1486
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "embroidered-chanderi-silk-saree-in-pink-sas1237",
          "label": 1487
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-half-n-half-crepe-jacquard-and-georgette-saree-in-brown-sga6044",
          "label": 1488
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-lehenga-in-coral-luf1353",
          "label": 1489
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "beaded-necklace-set-jjr16176",
          "label": 1490
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jvk2151",
          "label": 1491
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-kota-silk-saree-in-green-sjn5637",
          "label": 1492
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-crepe-saree-in-orange-ombre-and-fuchsia-sws5257",
          "label": 1493
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-straight-suit-in-coral-pink-kcu162",
          "label": 1494
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pachikari-jhumka-style-long-dangle-earring-jjr13978",
          "label": 1495
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-cotton-dupatta-in-pastel-blue-bmb14",
          "label": 1496
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-saree-in-blue-shxa361",
          "label": 1497
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-dupion-silk-kurta-set-in-maroon-mpc715",
          "label": 1498
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-jacquard-straight-suit-in-beige-kxz150",
          "label": 1499
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-net-brasso-saree-in-teal-blue-and-red-skra1291",
          "label": 1500
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "coimbatore-cotton-silk-saree-in-purple-saha225",
          "label": 1501
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-khadi-cotton-kurta-pajama-in-beige-mms722",
          "label": 1502
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-pair-of-bangles-jvk3102",
          "label": 1503
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-linen-kurta-set-in-white-mhg553",
          "label": 1504
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-anarkali-suit-in-yellow-and-blue-ktj405",
          "label": 1505
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-border-georgette-saree-with-embroidered-blouse-in-orange-sew3700",
          "label": 1506
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-silk-kurti-in-light-purple-tgs60",
          "label": 1507
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-half-n-half-art-silk-and-net-saree-in-fuchsia-and-blue-szg452",
          "label": 1508
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-light-green-sgka2961",
          "label": 1509
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-saree-in-purple-ssk4756a",
          "label": 1510
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-rayon-a-line-suit-in-maroon-kjn3152",
          "label": 1511
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "leheriya-art-silk-lehenga-in-fuchsia-and-pink-ljn1386",
          "label": 1512
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "banarasi-pure-silk-saree-in-fuchsia-snea1309",
          "label": 1513
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-red-suf7624",
          "label": 1514
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-satin-and-chiffon-jacquard-saree-in-fuchsia-and-purple-stl253",
          "label": 1515
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-dark-green-and-beige-stl724",
          "label": 1516
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "prestitched-embroidered-shimmer-net-saree-in-black-suf7169",
          "label": 1517
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-printed-georgette-pakistani-suit-in-dusty-olive-green-kch1004",
          "label": 1518
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-earrings-jjr14373",
          "label": 1519
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-and-net-lehenga-in-red-and-beige-luf1012",
          "label": 1520
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-saree-in-green-syc7070",
          "label": 1521
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-green-sar861",
          "label": 1522
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "solid-color-polyester-palazzo-in-brown-bdz349",
          "label": 1523
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-cotton-silk-straight-kurta-in-grey-tgp44",
          "label": 1524
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-chiffon-saree-in-ombre-yellow-and-purple-svea10",
          "label": 1525
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-silk-saree-in-off-white-and-sky-blue-stla206",
          "label": 1526
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-light-grey-stu493",
          "label": 1527
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-linen-saree-in-off-white-srga788",
          "label": 1528
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-linen-cotton-kurta-set-in-light-blue-mpc898",
          "label": 1529
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-khadi-silk-punjabi-suit-in-off-white-kae643",
          "label": 1530
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-abaya-style-suit-in-fuchsia-kch882",
          "label": 1531
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-viscose-georgette-punjabi-suit-in-yellow-krgv35",
          "label": 1532
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-orange-ssf4215",
          "label": 1533
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-top-in-black-tja585",
          "label": 1534
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-bangalore-silk-saree-in-orange-stbn178",
          "label": 1535
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-velvet-and-brocade-sherwani-in-beige-mpc797",
          "label": 1536
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "printed-cotton-satin-straight-suit-in-white-kfx1904",
          "label": 1537
        },
        {
          "viewIntensity": 3,
          "level": "green",
          "id": "kanchipuram-saree-in-neon-green-skra1060",
          "label": 1538
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-abaya-style-suit-in-blue-and-beige-kqu885",
          "label": 1539
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-straight-cut-georgette-suit-in-fuchsia-kjn1597",
          "label": 1540
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-maroon-sfka343",
          "label": 1541
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-matka-silk-saree-in-green-spn3377",
          "label": 1542
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ornamental-printed-art-silk-saree-in-pastel-green-sew5140",
          "label": 1543
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-shaded-teal-green-kch997",
          "label": 1544
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-blue-sfka637",
          "label": 1545
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stripe-printed-cotton-front-slit-kurta-in-off-white-tja829",
          "label": 1546
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-green-spfa2210",
          "label": 1547
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-georgette-saree-in-green-sew5498",
          "label": 1548
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-linen-saree-in-off-white-srga788",
          "label": 1549
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-cut-suit-in-beige-khbz1",
          "label": 1550
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-mysore-silk-saree-in-charcoal-black-shu515",
          "label": 1551
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-neckline-art-silk-kurta-set-in-off-white-mrg263",
          "label": 1552
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-adjustable-haathphool-jjr16501",
          "label": 1553
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-satin-chiffon-saree-in-sky-blue-sqfa340",
          "label": 1554
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-long-kurta-in-light-yellow-tkh57",
          "label": 1555
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-tussar-silk-saree-in-rust-svqa60",
          "label": 1556
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-gown-in-blue-ufb19",
          "label": 1557
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-silk-saree-in-magenta-sbta172",
          "label": 1558
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-layered-mathapatti-jrl939",
          "label": 1559
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-georgette-crop-top-set-in-rose-gold-tzg28",
          "label": 1560
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bengal-handloom-pure-cotton-saree-in-off-white-sswa198",
          "label": 1561
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-jamdani-saree-in-beige-spn3562",
          "label": 1562
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-cotton-kurta-set-in-beige-mqz16",
          "label": 1563
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-kurta-set-in-white-and-blue-mtr107",
          "label": 1564
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "leheriya-printed-cotton-palazzo-in-sky-blue-bnj307",
          "label": 1565
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-jacquard-kurta-set-in-beige-mse553",
          "label": 1566
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-coral-red-utt570",
          "label": 1567
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-border-satin-chiffon-saree-in-fuchsia-sew3843",
          "label": 1568
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-cotton-saree-in-off-white-sswa581",
          "label": 1569
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-turquoise-sgka1615",
          "label": 1570
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-long-kurta-in-beige-tzq300",
          "label": 1571
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-top-set-in-coral-tjw77",
          "label": 1572
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-saree-in-teal-green-srga321",
          "label": 1573
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embroidered-art-silk-kurta-set-in-navy-blue-mrg403",
          "label": 1574
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-chanderi-silk-layered-kurta-in-copper-thu1473",
          "label": 1575
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-georgette-saree-in-teal-green-and-beige-sbja362",
          "label": 1576
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jjr16393",
          "label": 1577
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "solid-color-rayon-palazzo-in-green-tja965",
          "label": 1578
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-art-silk-dupatta-in-indigo-blue-bbe26",
          "label": 1579
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-midi-dress-in-teal-blue-tyg12",
          "label": 1580
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "golden-and-silver-color-bangle-set-jvm1693",
          "label": 1581
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-grey-sgka1170",
          "label": 1582
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "abstract-printed-rayon-kurta-in-green-tdr1064",
          "label": 1583
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "hand-embroidered-chiffon-saree-in-royal-blue-seh860",
          "label": 1584
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-chanderi-cotton-abaya-style-suit-in-light-yellow-kjn2512",
          "label": 1585
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-kurti-in-mustard-tuf1109",
          "label": 1586
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-tant-saree-in-off-white-spn3039",
          "label": 1587
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-saree-in-beige-svqa87",
          "label": 1588
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "plain-cotton-kurta-set-in-mustard-mve193",
          "label": 1589
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kantha-embroidered-cotton-straight-suit-in-teal-green-kjg46",
          "label": 1590
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-red-scfa332",
          "label": 1591
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-satin-georgette-saree-in-sky-blue-and-fuchisa-sfva56",
          "label": 1592
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-dark-brown-kye817",
          "label": 1593
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-georgette-brasso-saree-in-pink-ombre-sud1406",
          "label": 1594
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-punjabi-suit-in-navy-blue-kbz230",
          "label": 1595
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-kurta-in-light-brown-tnc997",
          "label": 1596
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-jacquard-straight-suit-in-pink-kcu122",
          "label": 1597
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-brown-sgka1186",
          "label": 1598
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-satin-blouse-in-red-dbu498",
          "label": 1599
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-tussar-silk-saree-in-light-beige-snea1166",
          "label": 1600
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "embellished-art-silk-saree-in-peach-ssva275",
          "label": 1601
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-crushed-cotton-skirt-in-off-white-and-red-bnj193",
          "label": 1602
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-in-golden-jdw292",
          "label": 1603
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ombre-georgette-saree-in-green-and-navy-blue-spfa1626",
          "label": 1604
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bandhej-printed-cotton-straight-suit-in-turquoise-kfx2583",
          "label": 1605
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-cotton-saree-in-light-beige-sswa544",
          "label": 1606
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-linen-kurta-jacket-set-in-white-and-fawn-mhg625",
          "label": 1607
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-front-slit-kurti-in-red-tnz58",
          "label": 1608
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-red-sbh1346",
          "label": 1609
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-orange-sbz3262",
          "label": 1610
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-turquoise-sew4345",
          "label": 1611
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-necklace-set-jvk1869",
          "label": 1612
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "woven-banarasi-silk-saree-in-coral-pink-stea494",
          "label": 1613
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-front-slit-kurta-in-violet-tbe269",
          "label": 1614
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-kota-silk-saree-in-teal-green-safa112",
          "label": 1615
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-bordered-crepe-saree-in-sea-green-sws5445",
          "label": 1616
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-saree-in-rust-sfka1152",
          "label": 1617
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-red-and-beige-ssf3481",
          "label": 1618
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-straight-cut-viscose-suit-in-fuchsia-kau148",
          "label": 1619
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-neckline-georgette-jacket-style-kurta-in-beige-thu1722",
          "label": 1620
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-linen-jodhpuri-suit-in-blue-mpc348",
          "label": 1621
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-crepe-mysore-saree-in-off-white-shu700",
          "label": 1622
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-saree-in-maroon-svra165",
          "label": 1623
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-pure-raw-silk-circular-lehenga-in-purple-ljn1178",
          "label": 1624
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-straight-kurta-in-dusty-green-tjw572",
          "label": 1625
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-pure-cotton-saree-in-light-beige-sswa544",
          "label": 1626
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-abaya-style-suit-in-navy-blue-kuf10228",
          "label": 1627
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-saree-in-orange-dual-tone-shp791",
          "label": 1628
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-cotton-silk-kurta-in-fuchsia-trv179",
          "label": 1629
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-straight-cut-suit-in-blue-kfx1140",
          "label": 1630
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-straight-suit-in-red-kpz74",
          "label": 1631
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "satin-petticoat-in-fuchsia-uub76",
          "label": 1632
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-cotton-combo-of-kurta-in-fuchsia-and-beige-tdr763",
          "label": 1633
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-black-szma153",
          "label": 1634
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-punjabi-suit-in-indigo-blue-ktn324",
          "label": 1635
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-long-kurta-in-orange-tqz71",
          "label": 1636
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-abaya-style-art-silk-suit-in-grey-and-pink-kgf5093",
          "label": 1637
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-dupion-silk-blouse-in-black-utt616",
          "label": 1638
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-silk-saree-in-black-shxa25",
          "label": 1639
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-saree-in-olive-green-swz235",
          "label": 1640
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-tunic-in-blue-tnc88",
          "label": 1641
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-adjustable-pair-of-anklet-jrl697",
          "label": 1642
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-kurta-in-black-tcd16",
          "label": 1643
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-crepe-saree-in-beige-ssf4366",
          "label": 1644
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-lehenga-in-sky-blue-lxw348",
          "label": 1645
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-georgette-saree-in-light-green-sqfa30",
          "label": 1646
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-kurta-set-in-yellow-meu6",
          "label": 1647
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-terry-rayon-jacquard-jodhpuri-suit-in-blue-mhg756",
          "label": 1648
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-linen-kurta-set-in-lilac-tzy73",
          "label": 1649
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-linen-kurti-set-in-lilac-tzy73",
          "label": 1650
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pearl-earring-jjr15106",
          "label": 1651
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "velvet-blouse-in-dark-green-dbu400",
          "label": 1652
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-cotton-silk-churidar-in-light-green-thu1584",
          "label": 1653
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-saree-in-coral-red-ssf3782",
          "label": 1654
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pintucked-denim-kurta-in-dark-blue-tcz301",
          "label": 1655
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kanchipuram-saree-in-fuchsia-sgka3095",
          "label": 1656
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-poly-georgette-tunic-in-black-tap23",
          "label": 1657
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-pure-chanderi-silk-saree-in-neon-green-skba274",
          "label": 1658
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-kurta-in-off-white-tnc926",
          "label": 1659
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-chiffon-jacquard-saree-in-red-sgya102",
          "label": 1660
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "two-part-pure-crepe-saree-in-mustard-and-maroon-sjn7063",
          "label": 1661
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-chanderi-cotton-dupatta-in-peach-orange-bmb33",
          "label": 1662
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-beige-and-red-szma114",
          "label": 1663
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "jamdani-cotton-linen-saree-in-white-sgpn331",
          "label": 1664
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-printed-half-n-half-pure-silk-saree-in-black-and-red-sffa45",
          "label": 1665
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-abaya-style-net-suit-in-pink-kvl63",
          "label": 1666
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-yoke-taffeta-abaya-style-suit-in-blue-kes301",
          "label": 1667
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-art-silk-dupatta-in-indigo-blue-bbe32",
          "label": 1668
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-dupion-silk-blouse-in-green-uyp57",
          "label": 1669
        },
        {
          "viewIntensity": 2,
          "level": "green",
          "id": "pure-satin-silk-banarasi-saree-in-maroon-snea892",
          "label": 1670
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-handloom-saree-in-green-snea687",
          "label": 1671
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-mustard-sew4834",
          "label": 1672
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-black-snba1149",
          "label": 1673
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-rayon-top-in-white-tvt101",
          "label": 1674
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kerala-kasavu-hand-painted-cotton-saree-in-white-sfya36",
          "label": 1675
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-viscose-georgette-jacquard-lehenga-in-off-white-luf1155",
          "label": 1676
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-straight-suit-in-maroon-kexm83",
          "label": 1677
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-muslin-lehenga-in-coral-red-ljz83",
          "label": 1678
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-kurti-in-tdr657",
          "label": 1679
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-saree-in-white-sfya9",
          "label": 1680
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "ikkat-printed-cotton-saree-in-black-scfa315",
          "label": 1681
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-lehenga-in-beige-ljn1080",
          "label": 1682
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-dupion-silk-blouse-in-fuchsia-utt619",
          "label": 1683
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-cotton-silk-pakistani-suit-in-light-blue-kyk58",
          "label": 1684
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-half-n-half-art-silk-saree-in-maroon-and-blue-sew3188",
          "label": 1685
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-jacquard-lehenga-in-red-lqu508",
          "label": 1686
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-satin-saree-in-shaded-peach-and-black-sau2018",
          "label": 1687
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-dupion-silk-blouse-in-red-utt705",
          "label": 1688
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "plain-rayon-cotton-a-line-kurta-in-fuchsia-tdr685",
          "label": 1689
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-satin-georgette-abaya-style-suit-in-red-kch1172",
          "label": 1690
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-velvet-sherwani-in-navy-blue-mhg507",
          "label": 1691
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-saree-in-black-skpa889",
          "label": 1692
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-and-art-silk-anarkali-suit-in-peach-kjy899",
          "label": 1693
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-net-lehenga-style-gown-in-orange-and-green-tsp101",
          "label": 1694
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-cotton-straight-suit-in-orange-kye811",
          "label": 1695
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-abaya-style-suit-in-beige-kch543",
          "label": 1696
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-georgette-pakistani-suit-in-red-kvu17",
          "label": 1697
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "georgette-saree-with-embroidered-blouse-in-green-scga35",
          "label": 1698
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "pure-kanchipuram-silk-saree-in-dark-green-sgka2987",
          "label": 1699
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-georgette-kurta-in-off-white-thu1037",
          "label": 1700
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "mysore-chiffon-saree-in-dark-blue-sbra1091",
          "label": 1701
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "woven-art-silk-saree-in-light-green-swz399",
          "label": 1702
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kerala-kasavu-pure-cotton-saree-in-off-white-spn2518",
          "label": 1703
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "kerala-kasavu-pure-cotton-saree-in-off-white-spn2183",
          "label": 1704
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-print-pure-georgette-saree-in-mustard-sjn7119",
          "label": 1705
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-cotton-silk-saree-in-maroon-srga786",
          "label": 1706
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "bandhej-crepe-saree-in-maroon-sjn7093",
          "label": 1707
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-net-saree-in-royal-blue-sar825",
          "label": 1708
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-dusty-blue-spta32",
          "label": 1709
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "block-printed-cotton-viscose-layered-kurta-in-beige-trb553",
          "label": 1710
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-chiffon-saree-in-off-white-saqv5",
          "label": 1711
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-art-silk-anarkali-suit-in-brown-kym373",
          "label": 1712
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "banarasi-saree-in-blue-and-golden-swz193",
          "label": 1713
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "printed-cotton-punjabi-suit-in-royal-blue-khbz205",
          "label": 1714
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "half-n-half-art-silk-saree-in-purple-and-pink-spfa2026",
          "label": 1715
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "digital-print-crepe-saree-in-orange-ssf4275",
          "label": 1716
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-georgette-saree-in-coral-red-sar939",
          "label": 1717
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "stone-studded-earrings-jjr15193",
          "label": 1718
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-taffeta-abaya-style-suit-in-black-kch281",
          "label": 1719
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-rayon-cotton-kurta-in-black-tmw173",
          "label": 1720
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-saree-in-beige-sswa272",
          "label": 1721
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-rayon-kurta-in-green-tdd1542",
          "label": 1722
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "handloom-tant-cotton-saree-in-red-stla295",
          "label": 1723
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "hand-embroidered-satin-saree-in-navy-blue-ssva332",
          "label": 1724
        },
        {
          "viewIntensity": 1,
          "level": "green",
          "id": "embroidered-net-a-line-lehenga-in-red-lyn4",
          "label": 1725
        }
      ],
      "links": [
        {
          "source": "plain-georgette-saree-in-black-sfs592",
          "strength": 0.00046061722708429296,
          "target": "woven-khadi-cotton-saree-in-black-sgpn467"
        },
        {
          "source": "woven-khadi-cotton-saree-in-black-sgpn467",
          "strength": 0.00046061722708429296,
          "target": "handloom-linen-jamdani-saree-in-beige-srga1167"
        },
        {
          "source": "woven-khadi-cotton-saree-in-black-sgpn467",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-wine-and-orange-spfa1788"
        },
        {
          "source": "handloom-linen-jamdani-saree-in-beige-srga1167",
          "strength": 0.00046061722708429296,
          "target": "handloom-linen-jamdani-saree-in-off-white-srga1169"
        },
        {
          "source": "leheriya-printed-kota-silk-saree-in-teal-green-sqta168",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-saree-in-peach-sppa32"
        },
        {
          "source": "printed-crepe-saree-in-multicolor-szma57",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-crepe-saree-in-fuchsia-and-off-white-sjra389"
        },
        {
          "source": "half-n-half-crepe-saree-in-fuchsia-and-off-white-sjra389",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-multicolor-sew4351"
        },
        {
          "source": "half-n-half-crepe-saree-in-fuchsia-and-off-white-sjra389",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-asymmetric-kurta-set-in-dark-purple-mee326"
        },
        {
          "source": "printed-crepe-saree-in-multicolor-sew4351",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-crepe-saree-in-orange-and-brown-spfa1940"
        },
        {
          "source": "printed-crepe-saree-in-multicolor-sew4351",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pair-of-lac-bangle-jkc155"
        },
        {
          "source": "half-n-half-crepe-saree-in-orange-and-brown-spfa1940",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-light-beige-snga112"
        },
        {
          "source": "mysore-crepe-saree-in-light-beige-snga112",
          "strength": 0.00046061722708429296,
          "target": "woven-mysore-crepe-saree-in-pink-snga298"
        },
        {
          "source": "woven-mysore-crepe-saree-in-pink-snga298",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-red-snga120"
        },
        {
          "source": "mysore-crepe-saree-in-red-snga120",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-off-white-snga156"
        },
        {
          "source": "mysore-crepe-saree-in-off-white-snga156",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-maroon-snga141"
        },
        {
          "source": "mysore-crepe-saree-in-off-white-snga156",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-fuchsia-snga168"
        },
        {
          "source": "mysore-crepe-saree-in-maroon-snga141",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-royal-blue-sew5609"
        },
        {
          "source": "printed-crepe-saree-in-royal-blue-sew5609",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-blue-and-multicolor-szma66"
        },
        {
          "source": "printed-crepe-saree-in-royal-blue-sew5609",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-lycra-net-and-net-saree-in-maroon-and-peach-sws5283"
        },
        {
          "source": "printed-crepe-saree-in-blue-and-multicolor-szma66",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-fuchsia-snga168"
        },
        {
          "source": "mysore-crepe-saree-in-fuchsia-snga168",
          "strength": 0.00046061722708429296,
          "target": "woven-crepe-saree-in-red-seh1814"
        },
        {
          "source": "mysore-crepe-saree-in-fuchsia-snga168",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-cream-mcd1202"
        },
        {
          "source": "woven-crepe-saree-in-red-seh1814",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-blue-and-multicolor-spfa2468"
        },
        {
          "source": "digital-printed-crepe-saree-in-yellow-snba1018",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-off-white-and-mustard-snba1014"
        },
        {
          "source": "digital-printed-crepe-saree-in-off-white-and-mustard-snba1014",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-brown-snba1017"
        },
        {
          "source": "digital-printed-crepe-saree-in-brown-snba1017",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-off-white-and-multicolor-sew5548"
        },
        {
          "source": "digital-printed-crepe-saree-in-brown-snba1017",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-brown-and-black-szma60"
        },
        {
          "source": "digital-printed-georgette-saree-in-off-white-and-multicolor-sew5548",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-purple-and-multicolor-snba1012"
        },
        {
          "source": "digital-printed-crepe-saree-in-purple-and-multicolor-snba1012",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-brown-snba1017"
        },
        {
          "source": "printed-crepe-saree-in-brown-and-black-szma60",
          "strength": 0.00046061722708429296,
          "target": "ombre-art-silk-saree-in-beige-and-brown-syc7018"
        },
        {
          "source": "ombre-art-silk-saree-in-beige-and-brown-syc7018",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-cream-ssf3176"
        },
        {
          "source": "printed-georgette-saree-in-cream-ssf3176",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-embroidered-art-silk-saree-in-black-and-beige-syla7"
        },
        {
          "source": "half-n-half-embroidered-art-silk-saree-in-black-and-beige-syla7",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-yellow-and-multicolor-spfa2581"
        },
        {
          "source": "half-n-half-embroidered-art-silk-saree-in-black-and-beige-syla7",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-navy-blue-and-multicolor-sew5899"
        },
        {
          "source": "digital-printed-georgette-saree-in-yellow-and-multicolor-spfa2581",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-off-white-and-yellow-spfa2530"
        },
        {
          "source": "digital-printed-georgette-saree-in-off-white-and-yellow-spfa2530",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-light-pink-smda788"
        },
        {
          "source": "printed-crepe-saree-in-light-pink-smda788",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-jacquard-saree-in-grey-szy736"
        },
        {
          "source": "printed-crepe-saree-in-light-pink-smda788",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "digital-printed-crepe-jacquard-saree-in-grey-szy736",
          "strength": 0.00046061722708429296,
          "target": "bandhani-crepe-saree-in-purple-sjn5758"
        },
        {
          "source": "bandhani-crepe-saree-in-purple-sjn5758",
          "strength": 0.00046061722708429296,
          "target": "bandhej-pure-chinon-crepe-saree-in-shaded-beige-and-fuchsia-sjn7086"
        },
        {
          "source": "bandhej-pure-chinon-crepe-saree-in-shaded-beige-and-fuchsia-sjn7086",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-jacquard-saree-in-multicolor-and-blue-szy740"
        },
        {
          "source": "digital-printed-crepe-jacquard-saree-in-multicolor-and-blue-szy740",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-grey-ssx5673"
        },
        {
          "source": "digital-printed-crepe-jacquard-saree-in-multicolor-and-blue-szy740",
          "strength": 0.00046061722708429296,
          "target": "bangalore-silk-saree-in-maroon-sbra1068"
        },
        {
          "source": "printed-crepe-saree-in-grey-ssx5673",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-crepe-saree-in-grey-stu820"
        },
        {
          "source": "printed-crepe-saree-in-grey-ssx5673",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-grey-sew4349"
        },
        {
          "source": "floral-printed-crepe-saree-in-grey-stu820",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-grey-snba1394"
        },
        {
          "source": "floral-printed-crepe-saree-in-grey-stu820",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-punjabi-suit-in-navy-blue-kch1058"
        },
        {
          "source": "digital-printed-georgette-saree-in-grey-snba1394",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-grey-ssx5673"
        },
        {
          "source": "printed-crepe-saree-in-grey-sew4349",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-embroidered-art-silk-saree-in-black-and-beige-syla7"
        },
        {
          "source": "printed-crepe-saree-in-grey-sew4349",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-off-white-mgv335"
        },
        {
          "source": "digital-printed-art-silk-saree-in-navy-blue-and-multicolor-sew5899",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-sky-blue-and-beige-spfa2604"
        },
        {
          "source": "digital-printed-art-silk-saree-in-sky-blue-and-beige-spfa2604",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-pastel-blue-and-multicolor-spfa2619"
        },
        {
          "source": "digital-printed-art-silk-saree-in-pastel-blue-and-multicolor-spfa2619",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-light-purple-and-multicolor-spfa2608"
        },
        {
          "source": "digital-printed-art-silk-saree-in-light-purple-and-multicolor-spfa2608",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-spfa2616"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-spfa2616",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-fuchsia-and-light-pink-spfa2605"
        },
        {
          "source": "digital-printed-art-silk-saree-in-fuchsia-and-light-pink-spfa2605",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-beige-spfa2609"
        },
        {
          "source": "digital-printed-art-silk-saree-in-beige-spfa2609",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-spfa2618"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-spfa2618",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-shaded-olive-green-and-blue-spfa2611"
        },
        {
          "source": "digital-printed-art-silk-saree-in-shaded-olive-green-and-blue-spfa2611",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-pink-spfa2575"
        },
        {
          "source": "digital-printed-georgette-saree-in-pink-spfa2575",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-light-pink-ssf4812"
        },
        {
          "source": "digital-printed-georgette-saree-in-light-pink-ssf4812",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-spfa2460"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-spfa2460",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-spfa2614"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-spfa2614",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-spfa2470"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-spfa2470",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-snba1197"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-snba1197",
          "strength": 0.00046061722708429296,
          "target": "embroidered-crepe-jacquard-saree-in-green-ssva124"
        },
        {
          "source": "digital-printed-art-silk-saree-in-multicolor-snba1197",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-jacket-style-lehenga-in-blue-and-pastel-orange-lcc71"
        },
        {
          "source": "embroidered-crepe-jacquard-saree-in-green-ssva124",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-shaded-blue-sgja651"
        },
        {
          "source": "digital-printed-crepe-saree-in-shaded-blue-sgja651",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-red-spfa2269"
        },
        {
          "source": "digital-printed-crepe-saree-in-shaded-blue-sgja651",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-gown-in-light-olive-green-tch3"
        },
        {
          "source": "printed-crepe-saree-in-red-spfa2269",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-grey-ssf4670"
        },
        {
          "source": "digital-printed-georgette-saree-in-grey-ssf4670",
          "strength": 0.00046061722708429296,
          "target": "embroidered-crepe-jacquard-saree-in-magenta-ssva123"
        },
        {
          "source": "embroidered-crepe-jacquard-saree-in-magenta-ssva123",
          "strength": 0.00046061722708429296,
          "target": "embroidered-viscose-saree-in-light-green-ssva134"
        },
        {
          "source": "embroidered-viscose-saree-in-light-green-ssva134",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-beige-spfa2262"
        },
        {
          "source": "printed-crepe-saree-in-beige-spfa2262",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-brown-ssf3429"
        },
        {
          "source": "printed-crepe-saree-in-brown-ssf3429",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-light-yellow-snba1485"
        },
        {
          "source": "printed-georgette-saree-in-light-yellow-snba1485",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-olive-green-ssf4442"
        },
        {
          "source": "digital-printed-crepe-saree-in-olive-green-ssf4442",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-dark-green-ssf3420"
        },
        {
          "source": "printed-crepe-saree-in-dark-green-ssf3420",
          "strength": 0.00046061722708429296,
          "target": "printed-bhagalpuri-silk-saree-in-light-pastle-green-sew5435"
        },
        {
          "source": "printed-bhagalpuri-silk-saree-in-light-pastle-green-sew5435",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-black-ssf3433"
        },
        {
          "source": "printed-crepe-saree-in-black-ssf3433",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-teal-green-ssf4235"
        },
        {
          "source": "printed-crepe-saree-in-black-ssf3433",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-light-brown-ssf4462"
        },
        {
          "source": "printed-crepe-saree-in-teal-green-ssf4235",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-grey-ssf4447"
        },
        {
          "source": "printed-crepe-saree-in-teal-green-ssf4235",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-handloom-saree-in-off-white-srga680"
        },
        {
          "source": "digital-printed-crepe-saree-in-grey-ssf4447",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-black-ssf3433"
        },
        {
          "source": "digital-printed-crepe-saree-in-light-brown-ssf4462",
          "strength": 0.00046061722708429296,
          "target": "printed-satin-saree-in-off-white-and-dark-brown-sew4416"
        },
        {
          "source": "digital-printed-crepe-saree-in-light-brown-ssf4462",
          "strength": 0.00046061722708429296,
          "target": "kundan-maang-tikka-jjr13862"
        },
        {
          "source": "printed-satin-saree-in-off-white-and-dark-brown-sew4416",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-grey-ssf4453"
        },
        {
          "source": "digital-printed-crepe-saree-in-grey-ssf4453",
          "strength": 0.00046061722708429296,
          "target": "bandhani-printed-crepe-saree-in-red-sjn3039"
        },
        {
          "source": "bandhani-printed-crepe-saree-in-red-sjn3039",
          "strength": 0.00046061722708429296,
          "target": "rajasthani-georgette-saree-in-royal-blue-sjra608"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-light-dusty-blue-kej1009",
          "strength": 0.00046061722708429296,
          "target": "gota-patti-georgette-straight-suit-in-magenta-kch1177"
        },
        {
          "source": "gota-patti-georgette-straight-suit-in-magenta-kch1177",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-fuchsia-kpv242"
        },
        {
          "source": "gota-patti-georgette-straight-suit-in-magenta-kch1177",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-navy-blue-kch778"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-fuchsia-kpv242",
          "strength": 0.00046061722708429296,
          "target": "gota-patti-georgette-straight-suit-in-magenta-kch1177"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-navy-blue-kch778",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-pakistani-suit-in-yellow-kch957"
        },
        {
          "source": "embroidered-pure-kanchipuram-silk-saree-in-maroon-and-peach-shp775",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-pure-silk-saree-in-black-and-white-stbn87"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-black-and-white-stbn87",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-pure-silk-saree-in-red-and-white-stbn40"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-black-and-white-stbn87",
          "strength": 0.00046061722708429296,
          "target": "abstract-printed-cotton-straight-suit-in-fuchsia-kfx2613"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-red-and-white-stbn40",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-kanchipuram-silk-saree-in-maroon-and-peach-shp775"
        },
        {
          "source": "hand-embroiderd-chanderi-silk-pakistani-suit-in-blue-knsq16",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-chanderi-silk-pakistani-suit-in-royal-blue-knsq10"
        },
        {
          "source": "hand-embroidered-chanderi-silk-pakistani-suit-in-royal-blue-knsq10",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-pakistani-suit-in-royal-blue-kjn2969"
        },
        {
          "source": "digital-printed-art-silk-pakistani-suit-in-pink-kgzt263",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-pakistani-suit-in-blue-kgzt261"
        },
        {
          "source": "printed-chiffon-saree-in-shaded-brown-skk22732",
          "strength": 0.00046061722708429296,
          "target": "ombre-satin-georgette-saree-in-teal-green-and-teal-blue-ssf3294"
        },
        {
          "source": "printed-chiffon-saree-in-shaded-brown-skk22732",
          "strength": 0.00046061722708429296,
          "target": "butterfly-pallu-lycra-shimmer-saree-in-magenta-sws5504"
        },
        {
          "source": "ombre-satin-georgette-saree-in-teal-green-and-teal-blue-ssf3294",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-shaded-brown-skk22732"
        },
        {
          "source": "butterfly-pallu-lycra-shimmer-saree-in-magenta-sws5504",
          "strength": 0.00046061722708429296,
          "target": "butterfly-pallu-lycra-shimmer-saree-in-lilac-sws5501"
        },
        {
          "source": "butterfly-pallu-lycra-shimmer-saree-in-magenta-sws5504",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-saree-in-pink-spfa2147"
        },
        {
          "source": "woven-art-silk-saree-in-green-stu874",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-sew4784"
        },
        {
          "source": "embroidered-net-lehenga-in-off-white-lcc151",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-punjabi-suit-in-orange-kfx2510"
        },
        {
          "source": "embroidered-net-lehenga-in-off-white-lcc151",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-off-white-kgb3546"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-fuchsia-lyc184",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-red-lyc158"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-fuchsia-lyc184",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-maroon-lcc191"
        },
        {
          "source": "embroidered-cotton-punjabi-suit-in-dusty-green-kwy1016",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-white-mtx1"
        },
        {
          "source": "printed-georgette-saree-in-grey-and-beige-spfa1971",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "printed-georgette-saree-in-grey-and-beige-spfa1971",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-choker-necklace-set-jmy165"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-light-pink-smda788"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.0009212344541685859,
          "target": "plain-georgette-saree-in-baby-pink-szy716"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.0013818516812528789,
          "target": "printed-crepe-saree-in-baby-pink-ssf3424"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-old-rose-ssf4445"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-saree-in-pink-spfa2095"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "digital-print-georgette-saree-in-rose-gold-szy711"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-magenta-stya323"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "plain-satin-chiffon-saree-in-fuchsia-sew5228"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-pink-and-orange-skk22497"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "embellished-chiffon-saree-in-teal-blue-sjra135"
        },
        {
          "source": "digital-printed-satin-saree-in-light-pink-snba1203",
          "strength": 0.00046061722708429296,
          "target": "bell-sleeve-georgette-flared-kurti-in-cream-thu1786"
        },
        {
          "source": "plain-georgette-saree-in-baby-pink-szy716",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "plain-georgette-saree-in-baby-pink-szy716",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-magenta-stya323"
        },
        {
          "source": "printed-crepe-saree-in-baby-pink-ssf3424",
          "strength": 0.0009212344541685859,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "printed-crepe-saree-in-baby-pink-ssf3424",
          "strength": 0.00046061722708429296,
          "target": "art-silk-nehru-jacket-in-beige-mpe8"
        },
        {
          "source": "digital-printed-crepe-saree-in-old-rose-ssf4445",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "plain-chiffon-saree-in-pink-spfa2095",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "digital-print-georgette-saree-in-rose-gold-szy711",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "embroidered-art-silk-saree-in-magenta-stya323",
          "strength": 0.0009212344541685859,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "embroidered-art-silk-saree-in-magenta-stya323",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-white-mtx95"
        },
        {
          "source": "plain-satin-chiffon-saree-in-fuchsia-sew5228",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "embroidered-net-lehenga-in-dusty-green-lcc148",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-net-abaya-style-suit-in-baby-pink-kch228"
        },
        {
          "source": "embroidered-net-lehenga-in-dusty-green-lcc148",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-pakistani-suit-in-maroon-kch316"
        },
        {
          "source": "embroidered-georgette-lehenga-in-old-rose-lcc121",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-beige-mse268"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-white-mrg300",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-pink-mrg278"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-pink-mrg278",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-pathani-suit-in-white-mtr338"
        },
        {
          "source": "embroidered-pure-chiffon-saree-in-black-sjn4102",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-chiffon-saree-in-teal-green-sbta162"
        },
        {
          "source": "woven-pure-chiffon-saree-in-teal-green-sbta162",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-chiffon-saree-in-orange-and-fuchsia-skk22757"
        },
        {
          "source": "lehenga-style-chiffon-saree-in-orange-and-fuchsia-skk22757",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-chiffon-saree-in-red-ssf4928"
        },
        {
          "source": "kanchipuram-saree-in-maroon-snba790",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-beige-lcc149"
        },
        {
          "source": "printed-cotton-front-slit-anarkali-suit-in-off-white-kjn2365",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-peach-lqm76"
        },
        {
          "source": "embroidered-net-lehenga-in-peach-lqm76",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-shaded-pink-and-fuchsia-lat24"
        },
        {
          "source": "embroidered-net-lehenga-in-peach-lqm76",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-jacquard-lehenga-in-fuchsia-luf1334"
        },
        {
          "source": "half-n-half-embroidered-chiffon-saree-in-beige-and-black-skk22069",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-beige-mgv132"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-beige-mgv132",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-dhoti-in-beige-mpc568"
        },
        {
          "source": "embroidered-border-satin-georgette-saree-in-teal-blue-and-green-ssf4107",
          "strength": 0.00046061722708429296,
          "target": "embellished-art-silk-saree-in-dark-grey-ssf4488"
        },
        {
          "source": "embellished-art-silk-saree-in-dark-grey-ssf4488",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-pastel-blue-and-navy-blue-skk22486"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-beige-kej1054",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-rose-gold-kqu945"
        },
        {
          "source": "woven-art-silk-jacquard-kurta-set-in-orange-mgv110",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-snba788"
        },
        {
          "source": "printed-crepe-saree-in-mustard-ssf4392",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-dusty-green-ssf4448"
        },
        {
          "source": "printed-crepe-saree-in-mustard-ssf4392",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-white-mrg295"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-fuchsia-snea920"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "woven-tussar-silk-saree-in-blue-seh1663"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-dark-green-snea919"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-maroon-snba786"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-navy-blue-sqpa177"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-pink-syla52"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-kurta-set-in-teal-blue-trb511"
        },
        {
          "source": "woven-banarasi-silk-saree-in-pink-seh1659",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-light-green-sfwa247"
        },
        {
          "source": "banarasi-saree-in-fuchsia-snea920",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-pink-seh1659"
        },
        {
          "source": "banarasi-saree-in-fuchsia-snea920",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-dark-green-snea919"
        },
        {
          "source": "banarasi-saree-in-fuchsia-snea920",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-light-grey-ssf3609"
        },
        {
          "source": "banarasi-saree-in-fuchsia-snea920",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-dupion-silk-kurta-set-in-off-white-mpw101"
        },
        {
          "source": "woven-tussar-silk-saree-in-blue-seh1663",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-pink-seh1659"
        },
        {
          "source": "banarasi-saree-in-dark-green-snea919",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-fuchsia-snea920"
        },
        {
          "source": "banarasi-saree-in-dark-green-snea919",
          "strength": 0.00046061722708429296,
          "target": "pure-tussar-silk-saree-in-orange-snea940"
        },
        {
          "source": "banarasi-saree-in-dark-green-snea919",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "source": "banarasi-saree-in-dark-green-snea919",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-green-syc6999"
        },
        {
          "source": "woven-mysore-chiffon-saree-in-maroon-sbra787",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-maroon-snba786"
        },
        {
          "source": "woven-mysore-chiffon-saree-in-maroon-sbra787",
          "strength": 0.00046061722708429296,
          "target": "woven-mysore-chiffon-saree-in-green-sbra784"
        },
        {
          "source": "kanchipuram-saree-in-maroon-snba786",
          "strength": 0.00046061722708429296,
          "target": "handloom-silk-saree-in-maroon-sts3420"
        },
        {
          "source": "kanchipuram-saree-in-maroon-snba786",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-fuchsia-mtx81"
        },
        {
          "source": "kanchipuram-saree-in-maroon-snba786",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-black-sar816"
        },
        {
          "source": "kanchipuram-saree-in-maroon-snba786",
          "strength": 0.00046061722708429296,
          "target": "pure-silk-embroidered-saree-in-fuchsia-snea1057"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-black-mrg292",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-white-mse294"
        },
        {
          "source": "woven-chiffon-saree-in-pink-ssf3652",
          "strength": 0.00046061722708429296,
          "target": "gota-patti-chinon-chiffon-saree-in-pastel-green-snya16"
        },
        {
          "source": "woven-chiffon-saree-in-pink-ssf3652",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-chiffon-saree-in-maroon-szta115"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-lilac-kch628",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-dark-teal-green-kej1005"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-lilac-kch628",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-pathani-suit-in-white-muf654"
        },
        {
          "source": "embroidered-georgette-jacket-style-lehenga-in-grey-and-pink-lcc68",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-jacquard-and-art-silk-saree-in-yellow-and-blue-smd1443"
        },
        {
          "source": "embroidered-georgette-jacket-style-lehenga-in-grey-and-pink-lcc68",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-lehenga-in-grey-lcc210"
        },
        {
          "source": "embroidered-net-lehenga-in-light-teal-green-lxw170",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-black-snba787"
        },
        {
          "source": "plain-cotton-silk-churidar-in-off-white-thu1590",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-kurta-set-in-orange-mgv110"
        },
        {
          "source": "embroidered-brocade-sherwani-in-dark-blue-and-golden-mpw162",
          "strength": 0.00046061722708429296,
          "target": "brocade-sherwani-in-black-and-gold-mcd2654"
        },
        {
          "source": "handloom-tussar-silk-saree-in-beige-spn3591",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "source": "hand-embroidered-net-saree-in-cream-sar885",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-white-seh1970"
        },
        {
          "source": "hand-embroidered-net-saree-in-cream-sar885",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-saree-in-red-seh1954"
        },
        {
          "source": "hand-embroidered-net-saree-in-cream-sar885",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-blue-sar892"
        },
        {
          "source": "hand-embroidered-net-saree-in-cream-sar885",
          "strength": 0.0013818516812528789,
          "target": "hand-embroidered-georgette-saree-in-royal-blue-sar865"
        },
        {
          "source": "hand-embroidered-net-saree-in-cream-sar885",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-royal-blue-sar888"
        },
        {
          "source": "hand-embroidered-net-saree-in-cream-sar885",
          "strength": 0.00046061722708429296,
          "target": "brocade-jacket-in-beige-and-maroon-thu268"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-white-mrg295",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-white-mrg299"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-white-mrg299",
          "strength": 0.00046061722708429296,
          "target": "cotton-pathani-suit-in-sky-blue-mse318"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-white-mrg299",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-sherwani-in-white-mkc255"
        },
        {
          "source": "plain-cotton-linen-pathani-suit-in-white-mrg299",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-black-mgv91"
        },
        {
          "source": "cotton-pathani-suit-in-sky-blue-mse318",
          "strength": 0.00046061722708429296,
          "target": "brocade-sherwani-in-gold-and-dark-blue-mcd2682"
        },
        {
          "source": "cotton-pathani-suit-in-sky-blue-mse318",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-blue-ssf4225"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-light-beige-kch649",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-light-brown-ssf4462"
        },
        {
          "source": "printed-georgette-saree-in-navy-blue-ssf3604",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-cream-ssf3599"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-navy-blue-ssf3604"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-circular-lehenga-in-red-lqm190"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-maroon-lcc191"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-circular-lehenga-in-shaded-beige-and-pink-lcc142"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-circular-lehenga-in-red-lcc141"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-light-beige-lyc235"
        },
        {
          "source": "embroidered-satin-lehenga-in-red-lyc158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-a-line-lehenga-in-dark-green-lqm174"
        },
        {
          "source": "embroidered-satin-circular-lehenga-in-red-lqm190",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-red-lqm194"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-red-lqm194",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-maroon-lcc191"
        },
        {
          "source": "embroidered-velvet-lehenga-in-maroon-lcc191",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-red-lyc158"
        },
        {
          "source": "embroidered-velvet-lehenga-in-maroon-lcc191",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220"
        },
        {
          "source": "embroidered-velvet-lehenga-in-maroon-lcc191",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-a-line-lehenga-in-dark-maroon-lqm169"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-maroon-lcc191"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-red-lyc186"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220",
          "strength": 0.00046061722708429296,
          "target": "embroidered-handloom-art-silk-lehenga-in-fuchsia-lqm152"
        },
        {
          "source": "embroidered-velvet-lehenga-in-red-lyc186",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220"
        },
        {
          "source": "embroidered-velvet-lehenga-in-red-lyc186",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-beige-lyc178"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-beige-lyc178",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-red-lyc186"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-beige-lyc178",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-beige-ltl89"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-beige-ltl89",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-beige-lyc178"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-beige-ltl89",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-beige-ltl84"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-beige-ltl84",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-beige-ltl89"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-beige-ltl84",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-circular-lehenga-in-beige-ltl76"
        },
        {
          "source": "embroidered-net-circular-lehenga-in-beige-ltl76",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-beige-ltl84"
        },
        {
          "source": "pure-banarasi-silk-saree-in-maroon-snea1007",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-jamdani-saree-in-orange-spn3580"
        },
        {
          "source": "pure-banarasi-silk-saree-in-maroon-snea1007",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-off-white-and-grey-svra439"
        },
        {
          "source": "kanchipuram-saree-in-light-pink-smka699",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-golden-seh1870"
        },
        {
          "source": "kanchipuram-saree-in-golden-seh1870",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-turquoise-swz237"
        },
        {
          "source": "banarasi-saree-in-turquoise-swz237",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-silk-saree-in-off-white-seh1934"
        },
        {
          "source": "banarasi-saree-in-turquoise-swz237",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-pink-swz199"
        },
        {
          "source": "banarasi-saree-in-turquoise-swz237",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-saree-in-navy-blue-sew4780"
        },
        {
          "source": "banarasi-saree-in-turquoise-swz237",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-navy-blue-and-light-green-snma16"
        },
        {
          "source": "kanchipuram-silk-saree-in-off-white-seh1934",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-silk-saree-in-off-white-skra1354"
        },
        {
          "source": "floral-printed-cotton-angrakha-style-kurta-in-white-and-blue-tja1065",
          "strength": 0.00046061722708429296,
          "target": "plain-mangalgiri-cotton-angrakha-style-kurta-in-navy-blue-trb725"
        },
        {
          "source": "plain-mangalgiri-cotton-angrakha-style-kurta-in-navy-blue-trb725",
          "strength": 0.00046061722708429296,
          "target": "contrast-trim-rayon-kurta-in-red-tdr1036"
        },
        {
          "source": "contrast-trim-rayon-kurta-in-red-tdr1036",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-yoke-cotton-straight-suit-in-navy-blue-kye789"
        },
        {
          "source": "contrast-trim-rayon-kurta-in-red-tdr1036",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-handloom-saree-in-yellow-snea667"
        },
        {
          "source": "contrast-trim-rayon-kurta-in-red-tdr1036",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-magenta-sfka778"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-mustard-syla228",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-beige-syla255"
        },
        {
          "source": "banarasi-saree-in-beige-syla255",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-saree-in-multicolor-snba1197"
        },
        {
          "source": "banarasi-saree-in-beige-syla255",
          "strength": 0.00046061722708429296,
          "target": "printed-kota-doria-saree-in-coral-red-ssf4983"
        },
        {
          "source": "printed-kota-doria-saree-in-coral-red-ssf4983",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-white-snba1397"
        },
        {
          "source": "embroidered-taffeta-silk-punjabi-suit-in-old-rose-kch1326",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-punjabi-suit-in-old-rose-kch1326"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-off-white-kgb3546"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-pakistani-suit-in-light-pink-and-yellow-kxc864"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-black-mhg696",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv424"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-black-mhg696",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-red-sud1239"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-black-mhg696",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-red-and-pastel-green-sqfa126"
        },
        {
          "source": "butterfly-pallu-lycra-shimmer-saree-in-lilac-sws5501",
          "strength": 0.00046061722708429296,
          "target": "butterfly-pallu-lycra-shimmer-saree-in-magenta-sws5504"
        },
        {
          "source": "plain-chiffon-saree-in-pink-spfa2147",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-crepe-saree-in-light-pink-sew5367"
        },
        {
          "source": "plain-chiffon-saree-in-pink-spfa2147",
          "strength": 0.00046061722708429296,
          "target": "plain-satin-butterfly-pallu-saree-in-blue-sppa50"
        },
        {
          "source": "embroidered-border-crepe-saree-in-light-pink-sew5367",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-saree-in-pink-spfa2147"
        },
        {
          "source": "embroidered-jacquard-sherwani-in-maroon-mse456",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-sherwani-in-maroon-mrg324"
        },
        {
          "source": "embroidered-jacquard-sherwani-in-maroon-mse456",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-off-white-mse680"
        },
        {
          "source": "embroidered-jacquard-sherwani-in-maroon-mse456",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-olive-green-kye830"
        },
        {
          "source": "hand-embroidered-brocade-sherwani-in-maroon-mrg324",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-sherwani-in-maroon-mse456"
        },
        {
          "source": "hand-embroidered-brocade-sherwani-in-maroon-mrg324",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-beige-mpw193"
        },
        {
          "source": "embroidered-art-silk-sherwani-with-churidar-in-off-white-mcd2613",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421"
        },
        {
          "source": "velvet-blazer-in-fuchsia-mhg122",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-silk-sherwani-in-cream-mcd1497"
        },
        {
          "source": "velvet-blazer-in-fuchsia-mhg122",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg504"
        },
        {
          "source": "woven-chanderi-silk-saree-in-yellow-swz86",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-tussar-silk-saree-in-red-snea494"
        },
        {
          "source": "woven-pure-tussar-silk-saree-in-red-snea494",
          "strength": 0.0009212344541685859,
          "target": "pure-chanderi-silk-woven-saree-in-red-skba239"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-red-skba239",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-tussar-silk-saree-in-red-snea494"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-red-skba239",
          "strength": 0.0009212344541685859,
          "target": "woven-chanderi-silk-saree-in-peach-swz85"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-red-skba239",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-peach-swz49"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-red-skba239",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-black-skba251"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz85",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-red-skba239"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz85",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-peach-swz46"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz85",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-orange-swz212"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz85",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-pink-sfka1539"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz46",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-peach-swz85"
        },
        {
          "source": "woven-cotton-silk-saree-in-orange-swz212",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-peach-swz85"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz49",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-red-skba239"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-swz49",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-pink-sfka1364"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-black-skba251",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-red-skba239"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-pink-sfka1539",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-pink-sfka1369"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-pink-sfka1369",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-red-skba239"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-pink-sfka1369",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-peach-swz49"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-pink-sfka1364",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-pink-sfka1369"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-maroon-mse282",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-rust-mcd1666"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-maroon-mse282",
          "strength": 0.0009212344541685859,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-maroon-mse282",
          "strength": 0.00046061722708429296,
          "target": "jacquard-kurta-churidar-in-blue-mse437"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-rust-mcd1666",
          "strength": 0.0009212344541685859,
          "target": "embroidered-raw-silk-kurta-churidar-in-black-mcd566"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-black-mcd566",
          "strength": 0.00046061722708429296,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-black-mcd566",
          "strength": 0.00046061722708429296,
          "target": "block-printed-dupion-silk-kurta-pajama-set-in-navy-blue-mmq17"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-black-mcd566",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-rayon-kurta-set-in-black-and-white-mtx3"
        },
        {
          "source": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "strength": 0.0009212344541685859,
          "target": "embroidered-art-silk-kurta-churidar-in-maroon-mse282"
        },
        {
          "source": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "strength": 0.00046061722708429296,
          "target": "jacquard-kurta-churidar-in-blue-mse437"
        },
        {
          "source": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-black-mcd1189"
        },
        {
          "source": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "strength": 0.0009212344541685859,
          "target": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd1675"
        },
        {
          "source": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "strength": 0.0009212344541685859,
          "target": "embroidered-art-silk-kurta-churidar-in-wine-mcd1669"
        },
        {
          "source": "brocade-art-silk-kurta-set-in-maroon-mcd2947",
          "strength": 0.0009212344541685859,
          "target": "plain-dupion-silk-kurta-set-in-maroon-mpw240"
        },
        {
          "source": "jacquard-kurta-churidar-in-blue-mse437",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-kurta-churidar-in-teal-blue-mat19"
        },
        {
          "source": "jacquard-kurta-churidar-in-blue-mse437",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-pakistani-style-suit-in-cream-kch320"
        },
        {
          "source": "embroidered-jacquard-kurta-churidar-in-teal-blue-mat19",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-dusty-green-lcc148"
        },
        {
          "source": "embroidered-jacquard-kurta-churidar-in-teal-blue-mat19",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-black-mcd1189"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-black-mcd1189",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-multicolor-sew4351"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-black-mcd1189",
          "strength": 0.00046061722708429296,
          "target": "embroidered-raw-silk-kurta-churidar-in-orange-mcd563"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-orange-mcd563",
          "strength": 0.00046061722708429296,
          "target": "embroidered-raw-silk-kurta-churidar-in-black-mcd566"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-orange-mcd563",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-black-mgv139"
        },
        {
          "source": "block-printed-dupion-silk-kurta-pajama-set-in-navy-blue-mmq17",
          "strength": 0.00046061722708429296,
          "target": "embroidered-raw-silk-kurta-churidar-in-blue-mcd561"
        },
        {
          "source": "block-printed-dupion-silk-kurta-pajama-set-in-navy-blue-mmq17",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-cotton-combo-of-kurta-in-yellow-and-magenta-tdr792"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-blue-mcd561",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-beige-syla255"
        },
        {
          "source": "embroidered-raw-silk-kurta-churidar-in-blue-mcd561",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-kurta-set-in-olive-green-mgv117"
        },
        {
          "source": "woven-art-silk-jacquard-kurta-set-in-olive-green-mgv117",
          "strength": 0.0009212344541685859,
          "target": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd1675"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd1675",
          "strength": 0.0009212344541685859,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd1675",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-beige-mse268"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd1675",
          "strength": 0.00046061722708429296,
          "target": "punjabi-cotton-suit-in-black-ktv198"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-wine-mcd1669",
          "strength": 0.0009212344541685859,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-wine-mcd1669",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-brasso-straight-suit-in-red-kds910"
        },
        {
          "source": "plain-dupion-silk-kurta-set-in-maroon-mpw240",
          "strength": 0.00046061722708429296,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "plain-dupion-silk-kurta-set-in-maroon-mpw240",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "plain-dupion-silk-kurta-set-in-maroon-mpw240",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-red-kch799"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "cotton-pathani-suit-in-sky-blue-mse318"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mhg729"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-beige-mse226"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-jacquard-sherwani-in-beige-mse763"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd2388"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-dupion-silk-kurta-set-in-beige-mpw126"
        },
        {
          "source": "embroidered-beige-art-silk-kurta-set-mcd2852",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-pink-kuf10216"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mhg729",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mhg729",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-dhoti-in-white-mpc642"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-beige-mse226",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-beige-mse226",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-kurta-set-in-pink-mgn53"
        },
        {
          "source": "embroidered-art-silk-jacquard-sherwani-in-beige-mse763",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-maroon-mse282"
        },
        {
          "source": "embroidered-art-silk-jacquard-sherwani-in-beige-mse763",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd2388",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-magenta-stya323"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd2388",
          "strength": 0.00046061722708429296,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-beige-mse268",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-beige-mse268",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151"
        },
        {
          "source": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940",
          "strength": 0.0009212344541685859,
          "target": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421"
        },
        {
          "source": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503"
        },
        {
          "source": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-maang-tikka-jjr16701"
        },
        {
          "source": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940"
        },
        {
          "source": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-white-mse294"
        },
        {
          "source": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-saree-in-red-seh1954"
        },
        {
          "source": "embroidered-art-dupion-silk-kurta-set-in-beige-mpw126",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-tant-saree-in-light-green-and-white-ssna98"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1010",
          "strength": 0.00046061722708429296,
          "target": "woven-yoke-art-silk-abaya-style-suit-in-green-and-blue-ombre-kae631"
        },
        {
          "source": "woven-yoke-art-silk-abaya-style-suit-in-green-and-blue-ombre-kae631",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-peach-and-navy-blue-kqu997"
        },
        {
          "source": "woven-yoke-art-silk-abaya-style-suit-in-green-and-blue-ombre-kae631",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-snba660"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-peach-and-navy-blue-kqu997",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-anarkali-suit-with-pure-silk-dupatta-in-purple-kjn2075"
        },
        {
          "source": "banarasi-silk-anarkali-suit-with-pure-silk-dupatta-in-purple-kjn2075",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-beige-kch1145"
        },
        {
          "source": "woven-georgette-brasso-pakistani-suit-in-off-white-kej952",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-peach-kch629"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-peach-kch629",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-dark-blue-and-golden-mpw162"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-pink-kch1050",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg504"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-pink-kch1050",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-light-olive-green-kch1156"
        },
        {
          "source": "banarasi-silk-saree-in-maroon-sew5055",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-handloom-saree-in-off-white-and-red-sbta13"
        },
        {
          "source": "plain-art-silk-sherwani-in-black-mgv139",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-beige-mgv132"
        },
        {
          "source": "plain-art-silk-sherwani-in-black-mgv139",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-silk-sherwani-in-cream-mcd1497"
        },
        {
          "source": "embroidered-pure-silk-sherwani-in-cream-mcd1497",
          "strength": 0.00046061722708429296,
          "target": "stone-embroidered-net-saree-in-purple-shy30"
        },
        {
          "source": "hand-embroidered-chiffon-saree-in-yellow-seh419",
          "strength": 0.00046061722708429296,
          "target": "woven-chiffon-jacquard-saree-in-coral-pink-ssf3642"
        },
        {
          "source": "embroidered-net-lehenga-in-navy-blue-lzr317",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-red-ssf4758"
        },
        {
          "source": "printed-chiffon-saree-in-red-ssf4758",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-dark-green-snea919"
        },
        {
          "source": "printed-chiffon-saree-in-red-ssf4758",
          "strength": 0.00046061722708429296,
          "target": "pure-georgette-banarasi-saree-in-white-snea1319"
        },
        {
          "source": "printed-chiffon-saree-in-red-ssf4758",
          "strength": 0.0009212344541685859,
          "target": "embroidered-georgette-lehenga-in-light-fawn-lcc249"
        },
        {
          "source": "printed-chiffon-saree-in-red-ssf4758",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-red-scfa351"
        },
        {
          "source": "embroidered-georgette-saree-in-red-szra409",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-anarkali-suit-in-sky-blue-kdfz46"
        },
        {
          "source": "embroidered-georgette-saree-in-red-szra409",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-saree-in-beige-and-red-seh1101"
        },
        {
          "source": "embroidered-georgette-saree-in-red-szra409",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-shimmer-saree-in-black-sfs176"
        },
        {
          "source": "embroidered-georgette-saree-in-red-szra409",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-silk-saree-in-coral-skra1133"
        },
        {
          "source": "hand-embroidered-art-silk-anarkali-suit-in-sky-blue-kdfz46",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-red-ssf4758"
        },
        {
          "source": "pure-georgette-banarasi-saree-in-white-snea1319",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-red-ssf4758"
        },
        {
          "source": "embroidered-georgette-lehenga-in-light-fawn-lcc249",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-red-ssf4758"
        },
        {
          "source": "hand-embroidered-pure-georgette-lehenga-in-maroon-ljn850",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-turquoise-swz237"
        },
        {
          "source": "lehenga-style-lycra-net-and-net-saree-in-maroon-and-peach-sws5283",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-kurta-set-in-olive-green-mgv117"
        },
        {
          "source": "lehenga-style-lycra-net-and-net-saree-in-maroon-and-peach-sws5283",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-style-saree-in-red-and-off-white-spfa2169"
        },
        {
          "source": "plain-cotton-pathani-suit-in-white-mtr338",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-dhoti-sherwani-in-black-mhg501"
        },
        {
          "source": "half-n-half-art-silk-jacquard-and-art-silk-saree-in-yellow-and-blue-smd1443",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-lilac-kch628"
        },
        {
          "source": "banarasi-silk-saree-in-peach-sew5779",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-stbn110"
        },
        {
          "source": "banarasi-silk-saree-in-peach-sew5779",
          "strength": 0.00046061722708429296,
          "target": "bandhej-pure-chinon-crepe-in-red-sjn5487"
        },
        {
          "source": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2685",
          "strength": 0.00046061722708429296,
          "target": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2683"
        },
        {
          "source": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2683",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-light-green-syla42"
        },
        {
          "source": "brocade-dhoti-jacket-in-off-white-utk382",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-dhoti-sherwani-in-white-utk189"
        },
        {
          "source": "woven-art-silk-jacquard-dhoti-sherwani-in-white-utk189",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-jacquard-sherwani-in-white-uvx18"
        },
        {
          "source": "hand-embroidered-art-silk-jacquard-sherwani-in-white-uvx18",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-dhoti-sherwani-in-white-utk189"
        },
        {
          "source": "embroidered-georgette-jacket-style-lehenga-in-peach-lcc67",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-kurta-churidar-in-teal-blue-mat19"
        },
        {
          "source": "embroidered-shimmer-lycra-saree-in-golden-sgpn521",
          "strength": 0.00046061722708429296,
          "target": "plain-lycra-shimmer-saree-in-antique-and-maroon-spta264"
        },
        {
          "source": "embroidered-shimmer-lycra-saree-in-golden-sgpn521",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-saree-in-golden-sppa91"
        },
        {
          "source": "embroidered-shimmer-lycra-saree-in-golden-sgpn521",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-jacket-style-kurta-in-multicolor-tvn10"
        },
        {
          "source": "plain-lycra-shimmer-saree-in-antique-and-maroon-spta264",
          "strength": 0.00046061722708429296,
          "target": "embroidered-shimmer-lycra-saree-in-golden-sgpn521"
        },
        {
          "source": "plain-lycra-shimmer-saree-in-antique-and-maroon-spta264",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-golden-seh1908"
        },
        {
          "source": "kanchipuram-saree-in-golden-seh1908",
          "strength": 0.00046061722708429296,
          "target": "plain-lycra-shimmer-saree-in-antique-and-maroon-spta264"
        },
        {
          "source": "embroidered-satin-saree-in-golden-sppa91",
          "strength": 0.00046061722708429296,
          "target": "embroidered-shimmer-lycra-saree-in-golden-sgpn521"
        },
        {
          "source": "printed-cotton-jacket-style-kurta-in-multicolor-tvn10",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-jacket-style-kurta-in-multicolor-tvn5"
        },
        {
          "source": "printed-cotton-jacket-style-kurta-in-multicolor-tvn5",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-jacket-style-kurta-in-multicolor-tvn10"
        },
        {
          "source": "printed-cotton-jacket-style-kurta-in-multicolor-tvn5",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-jacket-style-kurta-in-multicolor-tvn11"
        },
        {
          "source": "printed-cotton-jacket-style-kurta-in-multicolor-tvn11",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-jacket-style-kurta-in-multicolor-tvn5"
        },
        {
          "source": "printed-cotton-jacket-style-kurta-in-multicolor-tvn11",
          "strength": 0.00046061722708429296,
          "target": "printed-satin-georgette-dress-in-multicolor-tgw322"
        },
        {
          "source": "printed-satin-georgette-dress-in-multicolor-tgw322",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-jacket-style-kurta-in-multicolor-tvn11"
        },
        {
          "source": "digital-printed-georgette-saree-in-off-white-ssf4529",
          "strength": 0.00046061722708429296,
          "target": "art-silk-saree-in-off-white-syc6556"
        },
        {
          "source": "digital-printed-georgette-saree-in-off-white-ssf4529",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-fuchsia-kch623"
        },
        {
          "source": "art-silk-saree-in-off-white-syc6556",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-off-white-ssf4529"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-off-white-mse680",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-light-teal-green-lxw170"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-off-white-mse680",
          "strength": 0.00046061722708429296,
          "target": "brocade-sherwani-in-gold-and-dark-blue-mcd2682"
        },
        {
          "source": "brocade-sherwani-in-gold-and-dark-blue-mcd2682",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-beige-mpw193"
        },
        {
          "source": "brocade-sherwani-in-gold-and-dark-blue-mcd2682",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-neckline-art-silk-gown-in-fuchsia-and-brown-ttv23"
        },
        {
          "source": "embroidered-brocade-sherwani-in-beige-mpw193",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-white-mrg300"
        },
        {
          "source": "embroidered-brocade-sherwani-in-beige-mpw193",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151",
          "strength": 0.00046061722708429296,
          "target": "velvet-blazer-in-fuchsia-mhg122"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-sherwani-in-beige-mgv158"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-coral-red-and-beige-sws5349"
        },
        {
          "source": "hand-embroidered-art-silk-sherwani-in-beige-mgv158",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv151"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg504",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg504",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-sherwani-in-beige-mkc253"
        },
        {
          "source": "hand-embroidered-art-silk-saree-in-beige-ssva333",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-beige-ssha1196"
        },
        {
          "source": "embroidered-georgette-saree-in-beige-ssha1196",
          "strength": 0.00046061722708429296,
          "target": "embroiderd-georgette-saree-in-beige-sxc2555"
        },
        {
          "source": "embroiderd-georgette-saree-in-beige-sxc2555",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-crepe-saree-in-light-beige-syc7333"
        },
        {
          "source": "embroidered-border-crepe-saree-in-light-beige-syc7333",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-shimmer-saree-in-beige-sws5548"
        },
        {
          "source": "embroidered-lycra-shimmer-saree-in-beige-sws5548",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-shimmer-saree-in-light-beige-sws5551"
        },
        {
          "source": "embroidered-lycra-shimmer-saree-in-beige-sws5548",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-beige-spfa1964"
        },
        {
          "source": "embroidered-lycra-shimmer-saree-in-light-beige-sws5551",
          "strength": 0.00046061722708429296,
          "target": "plain-georgette-saree-in-light-beige-szy720"
        },
        {
          "source": "embroidered-lycra-shimmer-saree-in-light-beige-sws5551",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-shaded-orange-and-red-spfa2734"
        },
        {
          "source": "plain-georgette-saree-in-light-beige-szy720",
          "strength": 0.00046061722708429296,
          "target": "woven-chiffon-jacquard-saree-in-light-beige-sqfa366"
        },
        {
          "source": "woven-chiffon-jacquard-saree-in-light-beige-sqfa366",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-shimmer-saree-in-light-beige-sws5549"
        },
        {
          "source": "embroidered-lycra-shimmer-saree-in-light-beige-sws5549",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-shimmer-saree-in-beige-sws5548"
        },
        {
          "source": "embroidered-art-silk-saree-in-beige-spfa1964",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-shimmer-saree-in-light-beige-sws5551"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-fawn-kch1304",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-pakistani-suit-in-beige-kmsg456"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-fawn-kch1304",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-pakistani-suit-in-beige-kexm14"
        },
        {
          "source": "banarasi-silk-saree-in-yellow-syla188",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-yellow-sbea138"
        },
        {
          "source": "woven-cotton-silk-saree-in-yellow-sbea138",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-yellow-sbea139"
        },
        {
          "source": "woven-cotton-silk-saree-in-yellow-sbea138",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-dupion-silk-dhoti-kurta-set-in-light-beige-mgv344"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-fuchsia-and-multicolor-lwk2529",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-kurta-set-in-light-purple-mpc821"
        },
        {
          "source": "plain-cotton-linen-kurta-set-in-light-purple-mpc821",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-kurta-set-in-dusty-mustard-mpc823"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-black-kjf81",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-maroon-kch796"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-maroon-kch796",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-lilac-kch628"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-maroon-kch796",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-maroon-kch796"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-beige-kjf88"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-maroon-kch630"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch1029"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-navy-blue-kch849"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-neon-green-lsh163"
        },
        {
          "source": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-pakistani-suit-in-maroon-kch316"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-dark-teal-green-kej1005",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-off-white-kgb3546"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-dark-teal-green-kej1005",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-navy-blue-kej1006"
        },
        {
          "source": "digital-printed-crepe-pakistani-suit-in-shaded-dark-beige-kcv1351",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-pakistani-suit-in-sky-blue-kmsg499"
        },
        {
          "source": "digital-printed-crepe-pakistani-suit-in-sky-blue-kmsg499",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-straight-suit-in-sea-green-kcv1349"
        },
        {
          "source": "embroidered-art-silk-saree-in-fuchsia-sew5737",
          "strength": 0.00046061722708429296,
          "target": "ombre-art-silk-saree-in-pink-sew5741"
        },
        {
          "source": "stone-studded-pair-of-lac-bangle-jkc155",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-cream-mhg855"
        },
        {
          "source": "digital-printed-georgette-saree-in-sky-blue-and-light-green-ssf4528",
          "strength": 0.00046061722708429296,
          "target": "handloom-silk-saree-in-maroon-sts3420"
        },
        {
          "source": "handloom-silk-saree-in-maroon-sts3420",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-yellow-ssf3607"
        },
        {
          "source": "handloom-silk-saree-in-maroon-sts3420",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-dark-teal-green-and-beige-sew5412"
        },
        {
          "source": "handloom-silk-saree-in-maroon-sts3420",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-brasso-pakistani-suit-in-teal-green-kcv1200"
        },
        {
          "source": "handloom-silk-saree-in-maroon-sts3420",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-light-olive-green-shk1496"
        },
        {
          "source": "printed-georgette-saree-in-yellow-ssf3607",
          "strength": 0.00046061722708429296,
          "target": "woven-chiffon-saree-in-pink-ssf3652"
        },
        {
          "source": "printed-georgette-saree-in-yellow-ssf3607",
          "strength": 0.00046061722708429296,
          "target": "contrast-patch-border-rayon-a-line-kurta-in-fuchsia-trq242"
        },
        {
          "source": "handloom-pure-chiffon-saree-in-maroon-szta115",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-tussar-silk-saree-in-wine-sqta334"
        },
        {
          "source": "stone-studded-bridal-set-in-maroon-and-off-white-jnc2279",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bridal-set-in-maroon-and-green-jnc2304"
        },
        {
          "source": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv424",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-beige-mse226"
        },
        {
          "source": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv424",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-pyjama-in-off-white-mgv421"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg504"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-yellow-sbea138"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-blue-mgv147"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-white-mse294",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg699"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-white-mse294",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg703"
        },
        {
          "source": "embroidered-art-silk-kurta-churidar-in-white-mse294",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-cotton-kurta-set-in-off-white-mve475"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg699",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-pajama-in-blue-mhg706"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg699",
          "strength": 0.00046061722708429296,
          "target": "woven-dupion-silk-kurta-churidar-in-off-white-mat28"
        },
        {
          "source": "plain-art-silk-kurta-pajama-in-blue-mhg706",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-turquoise-mcd1183"
        },
        {
          "source": "contrast-bordered-georgette-saree-in-black-sjra62",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-dupion-silk-dhoti-kurta-set-in-light-beige-mgv344"
        },
        {
          "source": "embroidered-georgette-and-net-abaya-style-suit-in-baby-pink-kch228",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-pink-kch1050"
        },
        {
          "source": "embroidered-georgette-and-net-abaya-style-suit-in-baby-pink-kch228",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-embroidered-lycra-shimmer-saree-in-pink-and-yellow-spfa1292"
        },
        {
          "source": "plain-art-silk-sherwani-in-teal-blue-mgv146",
          "strength": 0.00046061722708429296,
          "target": "foil-printed-rayon-cowl-style-kurta-set-in-blue-and-white-mhg723"
        },
        {
          "source": "plain-art-silk-sherwani-in-teal-blue-mgv146",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-turquoise-mcd1183"
        },
        {
          "source": "punjabi-cotton-suit-in-black-ktv198",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-teal-blue-mgv146"
        },
        {
          "source": "punjabi-cotton-suit-in-black-ktv198",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-off-white-kbz225"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-off-white-kbz225",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140"
        },
        {
          "source": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-punjabi-suit-in-light-teal-green-kch1055"
        },
        {
          "source": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-light-peach-shxa105"
        },
        {
          "source": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-fuchsia-lga292"
        },
        {
          "source": "embroidered-chanderi-silk-punjabi-suit-in-light-teal-green-kch1055",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-navy-blue-kbz231"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-navy-blue-kbz231",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104"
        },
        {
          "source": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-punjabi-suit-in-orange-kfx2510"
        },
        {
          "source": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-navy-blue-and-light-green-snma16"
        },
        {
          "source": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-indowestern-lehenga-in-beige-ljn1417"
        },
        {
          "source": "printed-cotton-punjabi-suit-in-orange-kfx2510",
          "strength": 0.00046061722708429296,
          "target": "embroidered-poly-cotton-punjabi-suit-in-pastel-orange-kmsg32"
        },
        {
          "source": "embroidered-poly-cotton-punjabi-suit-in-pastel-orange-kmsg32",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-punjabi-suit-in-white-kxz116"
        },
        {
          "source": "embroidered-cotton-punjabi-suit-in-white-kxz116",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-dupion-silk-kurta-set-in-beige-mpw126"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-snba660",
          "strength": 0.00046061722708429296,
          "target": "embroidered-raw-silk-kurta-churidar-in-orange-mcd563"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-snba660",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-stbn110"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-stbn110",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-peach-sew5779"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-stbn110",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-and-green-seh1790"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-stbn110",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-snba788"
        },
        {
          "source": "handloom-tant-cotton-saree-in-wine-stla302",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-jacquard-saree-in-beige-snea803"
        },
        {
          "source": "handloom-tant-cotton-saree-in-wine-stla302",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-earring-in-royal-blue-and-white-jvm2159"
        },
        {
          "source": "bengal-handloom-cotton-silk-saree-in-offwhite-sswa38",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-beige-and-red-sau2526"
        },
        {
          "source": "bengal-handloom-cotton-silk-saree-in-offwhite-sswa38",
          "strength": 0.00046061722708429296,
          "target": "bandhani-crepe-saree-in-onion-pink-sjn5755"
        },
        {
          "source": "woven-chanderi-silk-saree-in-beige-and-red-sau2526",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525"
        },
        {
          "source": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-beige-and-fuchsia-sau2523"
        },
        {
          "source": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-beige-ssx5656"
        },
        {
          "source": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62"
        },
        {
          "source": "woven-chanderi-silk-saree-in-beige-and-fuchsia-sau2523",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525"
        },
        {
          "source": "woven-cotton-silk-saree-in-beige-ssx5656",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525"
        },
        {
          "source": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-red-lyc158"
        },
        {
          "source": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-dark-teal-green-kej1005"
        },
        {
          "source": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62",
          "strength": 0.00046061722708429296,
          "target": "bengal-handloom-cotton-silk-saree-in-offwhite-sswa38"
        },
        {
          "source": "bandhani-crepe-saree-in-onion-pink-sjn5755",
          "strength": 0.00046061722708429296,
          "target": "handloom-silk-saree-in-maroon-sts3420"
        },
        {
          "source": "bandhani-crepe-saree-in-onion-pink-sjn5755",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104"
        },
        {
          "source": "bandhani-crepe-saree-in-onion-pink-sjn5755",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-beige-and-indigo-blue-sau2525"
        },
        {
          "source": "bandhani-crepe-saree-in-onion-pink-sjn5755",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-grey-and-white-srp648"
        },
        {
          "source": "bandhani-crepe-saree-in-onion-pink-sjn5755",
          "strength": 0.00046061722708429296,
          "target": "tie-dye-printed-cotton-straight-suit-in-blue-kmsg210"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mgv131",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-teal-blue-mgv146"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mgv131",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-dhoti-sherwani-in-white-mhg458"
        },
        {
          "source": "oxidised-necklace-jpm2842",
          "strength": 0.00046061722708429296,
          "target": "cotton-kurta-pyjama-in-white-mvj18"
        },
        {
          "source": "embroidered-net-lehenga-in-light-orange-and-maroon-lxw139",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-light-green-kepd38"
        },
        {
          "source": "plain-cotton-linen-kurta-set-in-pink-mgn53",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-jacquard-saree-in-multicolor-and-blue-szy740"
        },
        {
          "source": "digital-printed-satin-saree-in-grey-ombre-spfa2752",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-navy-blue-mhg503"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-beige-kjf88",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-beige-kej1127"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-beige-kej1127",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-light-beige-kch1455"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-light-beige-kch1455",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-light-beige-kch1455",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-lehenga-in-brown-lxe123"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-light-beige-kch1455"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-old-rose-kch1009"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-purple-and-maroon-kch1123"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-old-rose-kch1009",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-maroon-kch630",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch1029",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-navy-blue-kch1038"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-navy-blue-kch1038",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-wine-kch1042"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-navy-blue-kch1038",
          "strength": 0.0009212344541685859,
          "target": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1044"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-wine-kch1042",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-navy-blue-kch1038"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1044",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-dark-green-kch1232"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1044",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-pakistani-style-suit-in-cream-kch320"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-dark-green-kch1232",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1033"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-dark-green-kch1033",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-dark-green-kch1088"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-dark-green-kch1088",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-dark-green-kpv243"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-dark-green-kpv243",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1509"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1509",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-sky-blue-kch1052"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1509",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-teal-green-kch1505"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-sky-blue-kch1052",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-pastel-blue-kch1509"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-teal-green-kch1505",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-navy-blue-kej1151"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-navy-blue-kej1151",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-navy-blue-kch1038"
        },
        {
          "source": "embroidered-net-pakistani-style-suit-in-cream-kch320",
          "strength": 0.0009212344541685859,
          "target": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158"
        },
        {
          "source": "embroidered-net-pakistani-style-suit-in-cream-kch320",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-shimmer-lycra-saree-in-light-beige-sgpn480"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-navy-blue-kch849",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-abaya-style-suit-in-teal-blue-and-navy-blue-kqu881"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-navy-blue-kch849",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-violet-kch875"
        },
        {
          "source": "printed-cotton-abaya-style-suit-in-teal-blue-and-navy-blue-kqu881",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-navy-blue-kch849"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-violet-kch875",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-purple-and-maroon-kch1123",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-violet-kch1454"
        },
        {
          "source": "embroidered-georgette-lehenga-in-brown-lxe123",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch574"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch574",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-rayon-asymmetric-pakistani-suit-in-black-kyt57"
        },
        {
          "source": "plain-cotton-rayon-asymmetric-pakistani-suit-in-black-kyt57",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-net-pakistani-suit-in-maroon-kch244"
        },
        {
          "source": "embroidered-georgette-and-net-pakistani-suit-in-maroon-kch244",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-fuchsia-kch1029"
        },
        {
          "source": "embroidered-cotton-linen-kurta-set-in-white-mpw145",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-white-mse294"
        },
        {
          "source": "embroidered-cotton-linen-kurta-set-in-white-mpw145",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-maroon-ktvb78"
        },
        {
          "source": "embroidered-cotton-linen-kurta-set-in-white-mpw145",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-skk22592"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg703",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-black-kch1295"
        },
        {
          "source": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg703",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-armlet-jvk2731"
        },
        {
          "source": "hand-embroidered-net-saree-in-white-seh1970",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "source": "hand-embroidered-net-saree-in-white-seh1970",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-saree-in-red-seh1954"
        },
        {
          "source": "hand-embroidered-art-silk-saree-in-red-seh1954",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-sherwani-in-beige-mgv158"
        },
        {
          "source": "hand-embroidered-art-silk-saree-in-red-seh1954",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-white-seh1970"
        },
        {
          "source": "hand-embroidered-art-silk-saree-in-red-seh1954",
          "strength": 0.0009212344541685859,
          "target": "half-n-half-net-saree-in-beige-and-red-seh1101"
        },
        {
          "source": "half-n-half-net-saree-in-beige-and-red-seh1101",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "source": "half-n-half-net-saree-in-beige-and-red-seh1101",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-saree-in-red-seh1954"
        },
        {
          "source": "half-n-half-net-saree-in-beige-and-red-seh1101",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-coral-red-and-beige-sws5349"
        },
        {
          "source": "half-n-half-net-saree-in-beige-and-red-seh1101",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-shimmer-saree-in-black-sfs176"
        },
        {
          "source": "half-n-half-georgette-saree-in-coral-red-and-beige-sws5349",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-saree-in-beige-and-red-seh1101"
        },
        {
          "source": "embroidered-net-shimmer-saree-in-black-sfs176",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-blue-sar892"
        },
        {
          "source": "embroidered-net-shimmer-saree-in-black-sfs176",
          "strength": 0.00046061722708429296,
          "target": "stone-embroidered-net-saree-in-purple-shy30"
        },
        {
          "source": "hand-embroidered-net-saree-in-blue-sar892",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "source": "hand-embroidered-net-saree-in-blue-sar892",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "source": "stone-embroidered-net-saree-in-purple-shy30",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-saree-in-beige-and-red-seh1101"
        },
        {
          "source": "embroidered-brocade-sherwani-in-maroon-and-gold-mpe85",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-jacket-style-lehenga-in-grey-and-pink-lcc68"
        },
        {
          "source": "embroidered-brocade-sherwani-in-maroon-and-gold-mpe85",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-sherwani-in-beige-mkc253"
        },
        {
          "source": "embroidered-dupion-silk-sherwani-in-beige-mkc253",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-sherwani-in-maroon-and-golden-mpw161"
        },
        {
          "source": "embroidered-dupion-silk-sherwani-in-beige-mkc253",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-light-beige-mhg854"
        },
        {
          "source": "embroidered-dupion-silk-sherwani-in-beige-mkc253",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-linen-kurta-set-in-yellow-mpw158"
        },
        {
          "source": "hand-embroidered-brocade-sherwani-in-maroon-and-golden-mpw161",
          "strength": 0.00046061722708429296,
          "target": "embroidered-raw-silk-kurta-churidar-in-blue-mcd561"
        },
        {
          "source": "hand-embroidered-brocade-sherwani-in-maroon-and-golden-mpw161",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv149"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv149",
          "strength": 0.00046061722708429296,
          "target": "block-printed-dupion-silk-kurta-pajama-set-in-navy-blue-mmq17"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv149",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-maroon-mhg502"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-maroon-mhg502",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-cream-mcd1202"
        },
        {
          "source": "embroidered-brocade-sherwani-in-cream-mcd1202",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-jacquard-sherwani-in-off-white-mse678"
        },
        {
          "source": "embroidered-brocade-sherwani-in-cream-mcd1202",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-circular-lehenga-in-pink-lxw218"
        },
        {
          "source": "embroidered-art-silk-jacquard-sherwani-in-off-white-mse678",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-dark-blue-and-golden-mpw162"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-shaded-teal-green-and-blue-lqm134",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-fuchsia-lyc184"
        },
        {
          "source": "embroidered-satin-circular-lehenga-in-shaded-beige-and-pink-lcc142",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-shaded-pastel-green-and-teal-blue-lcc133"
        },
        {
          "source": "embroidered-satin-circular-lehenga-in-shaded-beige-and-pink-lcc142",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-dupion-silk-dhoti-kurta-set-in-light-beige-mgv344"
        },
        {
          "source": "embroidered-satin-lehenga-in-shaded-pastel-green-and-teal-blue-lcc133",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-assymetric-kurta-in-mustard-tjw693"
        },
        {
          "source": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "source": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-coral-sew2932"
        },
        {
          "source": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-tant-saree-in-orange-spn3017"
        },
        {
          "source": "woven-banarasi-silk-jacquard-lehenga-in-fuchsia-luf1334",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-light-blue-lcc209"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-light-blue-lcc209",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-orange-lxw244"
        },
        {
          "source": "printed-georgette-saree-in-peach-sbz3586",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg703"
        },
        {
          "source": "printed-georgette-saree-in-peach-sbz3586",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-blue-ssf4860"
        },
        {
          "source": "digital-printed-georgette-saree-in-blue-ssf4860",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-light-blue-smda576"
        },
        {
          "source": "printed-georgette-saree-in-light-blue-smda576",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pastel-orange-snba1318"
        },
        {
          "source": "printed-georgette-saree-in-pastel-orange-snba1318",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pink-snba1316"
        },
        {
          "source": "printed-georgette-saree-in-pastel-orange-snba1318",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-wine-syla107"
        },
        {
          "source": "printed-georgette-saree-in-pink-snba1316",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pastel-orange-snba1318"
        },
        {
          "source": "printed-georgette-saree-in-pink-snba1316",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-purple-szy815"
        },
        {
          "source": "printed-georgette-saree-in-pink-snba1316",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-orange-snba1327"
        },
        {
          "source": "printed-georgette-saree-in-purple-szy815",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pink-snba1316"
        },
        {
          "source": "printed-georgette-saree-in-orange-snba1327",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pink-snba1316"
        },
        {
          "source": "printed-georgette-saree-in-wine-syla107",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pink-snba1340"
        },
        {
          "source": "digital-printed-crepe-pakistani-suit-in-rose-gold-kcv1370",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-pakistani-style-suit-in-cream-kch320"
        },
        {
          "source": "woven-art-silk-saree-in-golden-and-royal-blue-spfa1919",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-pink-spfa1916"
        },
        {
          "source": "woven-art-silk-saree-in-pink-spfa1916",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-crepe-saree-in-shaded-blue-sgja651"
        },
        {
          "source": "woven-art-silk-saree-in-pink-spfa1916",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-pink-sxc2648"
        },
        {
          "source": "woven-art-silk-saree-in-pink-spfa1916",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-sky-blue-spfa1917"
        },
        {
          "source": "woven-art-silk-saree-in-pink-sxc2648",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-pink-spfa1916"
        },
        {
          "source": "pure-kota-silk-saree-in-fuchsia-snj7098",
          "strength": 0.0009212344541685859,
          "target": "pure-georgette-leheriya-saree-in-multicolor-snj7099"
        },
        {
          "source": "pure-kota-silk-saree-in-fuchsia-snj7098",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-saree-in-red-skk22369"
        },
        {
          "source": "pure-georgette-leheriya-saree-in-multicolor-snj7099",
          "strength": 0.00046061722708429296,
          "target": "pure-kota-silk-saree-in-fuchsia-snj7098"
        },
        {
          "source": "plain-chiffon-saree-in-red-skk22369",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-red-stu796"
        },
        {
          "source": "plain-chiffon-saree-in-red-skk22369",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-smka666"
        },
        {
          "source": "embroidered-chiffon-saree-in-red-stu796",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-saree-in-red-skk22369"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-smka666",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-coral-pink-stu793"
        },
        {
          "source": "embroidered-chiffon-saree-in-coral-pink-stu793",
          "strength": 0.00046061722708429296,
          "target": "pure-kota-silk-saree-in-fuchsia-snj7098"
        },
        {
          "source": "embroidered-chiffon-saree-in-coral-pink-stu793",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-tant-saree-in-pink-srga291"
        },
        {
          "source": "handloom-cotton-tant-saree-in-pink-srga291",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-coral-pink-stu793"
        },
        {
          "source": "handloom-cotton-tant-saree-in-pink-srga291",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-silk-jamdani-saree-in-pink-skza48"
        },
        {
          "source": "handloom-cotton-silk-jamdani-saree-in-pink-skza48",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-tant-saree-in-pink-srga291"
        },
        {
          "source": "embroidered-georgette-saree-in-coral-sew2932",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695"
        },
        {
          "source": "embroidered-georgette-saree-in-coral-sew2932",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-teal-blue-mmq10"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-dark-blue-lqm138",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-circular-lehenga-in-dark-blue-lcc135"
        },
        {
          "source": "dupion-silk-blouse-in-royal-blue-dbu516",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-blouse-in-royal-blue-dbu584"
        },
        {
          "source": "dupion-silk-blouse-in-royal-blue-dbu584",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-blouse-in-royal-blue-dbu516"
        },
        {
          "source": "half-n-half-georgette-and-net-saree-in-red-and-off-white-sas992",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-ssf4412"
        },
        {
          "source": "embroidered-georgette-saree-in-red-ssf4412",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-maroon-and-magenta-sbta519"
        },
        {
          "source": "pure-kerala-kasavu-cotton-saree-in-cream-spn2299",
          "strength": 0.00046061722708429296,
          "target": "bengal-handloom-cotton-silk-saree-in-offwhite-sswa38"
        },
        {
          "source": "embroidered-chanderi-silk-jacquard-straight-suit-in-green-kye912",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-pink-kye657"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-pink-kye657",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-peach-kye661"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-pink-kye657",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-navy-blue-kye663"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-peach-kye661",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-pink-kye657"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-peach-kye661",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-green-kye658"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-green-kye658",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-peach-kye661"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-green-kye658",
          "strength": 0.0009212344541685859,
          "target": "embroidered-chanderi-silk-jacquard-straight-suit-in-light-green-kye884"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-green-kye658",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-navy-blue-kye663"
        },
        {
          "source": "embroidered-chanderi-silk-jacquard-straight-suit-in-light-green-kye884",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-green-kye658"
        },
        {
          "source": "embroidered-chanderi-silk-jacquard-straight-suit-in-light-green-kye884",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-green-kye512"
        },
        {
          "source": "embroidered-chanderi-silk-jacquard-straight-suit-in-light-green-kye884",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-jacquard-straight-suit-in-pink-kye882"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-green-kye512",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-teal-green-kye338"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-teal-green-kye338",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-green-kae737"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-green-kae737",
          "strength": 0.0009212344541685859,
          "target": "embroidered-chanderi-silk-straight-suit-in-green-kye658"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-green-kae737",
          "strength": 0.0009212344541685859,
          "target": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae744"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae744",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-jacquard-sherwani-in-beige-mse763"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae744",
          "strength": 0.0013818516812528789,
          "target": "embroidered-chanderi-silk-straight-suit-in-green-kae737"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae744",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae732"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-navy-blue-kye663",
          "strength": 0.0009212344541685859,
          "target": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kae741"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-navy-blue-kye663",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae732"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kae741",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-navy-blue-kye663"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kae741",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kye620"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae732",
          "strength": 0.0009212344541685859,
          "target": "embroidered-chanderi-silk-straight-suit-in-dark-brown-kae744"
        },
        {
          "source": "embroidered-chanderi-silk-jacquard-straight-suit-in-pink-kye882",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-jacquard-straight-suit-in-light-green-kye884"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kye620",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-magenta-kye616"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kye620",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-chanderi-jacquard-straight-suit-in-royal-blue-kwy997"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-magenta-kye616",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-royal-blue-kye620"
        },
        {
          "source": "embroidered-cotton-chanderi-jacquard-straight-suit-in-royal-blue-kwy997",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-chanderi-jacquard-straight-suit-in-navy-blue-kwy1004"
        },
        {
          "source": "embroidered-cotton-chanderi-jacquard-straight-suit-in-navy-blue-kwy1004",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-jacquard-straight-suit-in-navy-blue-kye724"
        },
        {
          "source": "woven-cotton-jacquard-straight-suit-in-navy-blue-kye724",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-jacquard-straight-suit-in-navy-blue-kye910"
        },
        {
          "source": "embroidered-chanderi-silk-jacquard-straight-suit-in-navy-blue-kye910",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-navy-blue-and-light-green-kye823"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-navy-blue-and-light-green-kye823",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-navy-blue-ktvb86"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-navy-blue-kry990"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-fuchsia-ktvb87"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-navy-blue-ktvb86",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-navy-blue-kry990",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-sky-blue-ktvb83"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-navy-blue-kry990",
          "strength": 0.00046061722708429296,
          "target": "handloom-chanderi-silk-saree-in-yellow-skba180"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-fuchsia-ktvb87",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-maroon-ktvb78"
        },
        {
          "source": "embroidered-border-crepe-saree-in-dark-green-sew5420",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-teal-green-ssf4255"
        },
        {
          "source": "plain-dupion-silk-kurta-set-in-navy-blue-mpw243",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-set-in-maroon-mpc694"
        },
        {
          "source": "lehenga-style-lycra-shimmer-saree-in-peach-and-beige-sws5629",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-style-saree-in-peach-sws5588"
        },
        {
          "source": "embroidered-net-lehenga-style-saree-in-peach-sws5588",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-lycra-shimmer-saree-in-peach-and-beige-sws5629"
        },
        {
          "source": "embroidered-net-lehenga-style-saree-in-peach-sws5588",
          "strength": 0.00046061722708429296,
          "target": "plain-lycra-shimmer-saree-in-peach-sjn6843"
        },
        {
          "source": "plain-lycra-shimmer-saree-in-peach-sjn6843",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-style-saree-in-peach-sws5588"
        },
        {
          "source": "plain-lycra-shimmer-saree-in-peach-sjn6843",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-style-saree-in-orange-and-cream-spfa2175"
        },
        {
          "source": "embroidered-net-lehenga-style-saree-in-orange-and-cream-spfa2175",
          "strength": 0.00046061722708429296,
          "target": "plain-lycra-shimmer-saree-in-peach-sjn6843"
        },
        {
          "source": "printed-straight-cut-rayon-suit-in-white-ktv321",
          "strength": 0.00046061722708429296,
          "target": "printed-straight-cut-front-slit-suit-in-yellow-ktv249"
        },
        {
          "source": "printed-straight-cut-front-slit-suit-in-yellow-ktv249",
          "strength": 0.00046061722708429296,
          "target": "printed-punjabi-suit-in-black-ktv238"
        },
        {
          "source": "printed-straight-cut-front-slit-suit-in-yellow-ktv249",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-brasso-straight-suit-in-red-kds910"
        },
        {
          "source": "printed-punjabi-suit-in-black-ktv238",
          "strength": 0.00046061722708429296,
          "target": "printed-straight-cut-front-slit-suit-in-yellow-ktv249"
        },
        {
          "source": "printed-punjabi-suit-in-black-ktv238",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-punjabi-suit-in-black-ktv19"
        },
        {
          "source": "printed-cotton-punjabi-suit-in-black-ktv19",
          "strength": 0.00046061722708429296,
          "target": "printed-punjabi-suit-in-black-ktv238"
        },
        {
          "source": "digital-printed-georgette-brasso-straight-suit-in-red-kds910",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-crepe-saree-in-grey-stu820"
        },
        {
          "source": "digital-printed-georgette-brasso-straight-suit-in-red-kds910",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-brasso-straight-suit-in-peach-kqu1032"
        },
        {
          "source": "embroidered-georgette-brasso-straight-suit-in-peach-kqu1032",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-punjabi-suit-in-yellow-ktv302"
        },
        {
          "source": "foil-printed-rayon-cowl-style-kurta-set-in-blue-and-white-mhg723",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-lehenga-in-old-rose-lcc121"
        },
        {
          "source": "pure-kanchipuram-handloom-silk-saree-in-rust-sgta58",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-golden-stya262"
        },
        {
          "source": "embroidered-art-silk-saree-in-golden-stya262",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-shimmer-lycra-saree-in-light-beige-sgpn480"
        },
        {
          "source": "embroidered-border-shimmer-lycra-saree-in-light-beige-sgpn480",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-peach-sew5779"
        },
        {
          "source": "embroidered-border-shimmer-lycra-saree-in-light-beige-sgpn480",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-off-white-ssf4529"
        },
        {
          "source": "bandhej-pure-chinon-crepe-in-red-sjn5487",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-maroon-sew5055"
        },
        {
          "source": "banarasi-silk-handloom-saree-in-off-white-and-red-sbta13",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-kanchipuram-silk-saree-in-maroon-and-golden-shp731"
        },
        {
          "source": "embroidered-pure-kanchipuram-silk-saree-in-maroon-and-golden-shp731",
          "strength": 0.00046061722708429296,
          "target": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229"
        },
        {
          "source": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229",
          "strength": 0.00046061722708429296,
          "target": "plain-satin-georgette-saree-in-maroon-sew5799"
        },
        {
          "source": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229",
          "strength": 0.00046061722708429296,
          "target": "pure-banarasi-silk-handloom-saree-in-red-sbta30"
        },
        {
          "source": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-nehru-jacket-in-blue-mhg563"
        },
        {
          "source": "plain-satin-georgette-saree-in-maroon-sew5799",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-fuchsia-stea297"
        },
        {
          "source": "banarasi-silk-saree-in-fuchsia-stea297",
          "strength": 0.00046061722708429296,
          "target": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229"
        },
        {
          "source": "pure-banarasi-silk-handloom-saree-in-red-sbta30",
          "strength": 0.0009212344541685859,
          "target": "pure-banarasi-silk-handloom-saree-in-red-sbta28"
        },
        {
          "source": "pure-banarasi-silk-handloom-saree-in-red-sbta30",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-maroon-stea515"
        },
        {
          "source": "pure-banarasi-silk-handloom-saree-in-red-sbta28",
          "strength": 0.0009212344541685859,
          "target": "pure-banarasi-silk-handloom-saree-in-red-sbta30"
        },
        {
          "source": "pure-banarasi-silk-handloom-saree-in-red-sbta28",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-green-sew5280"
        },
        {
          "source": "plain-art-silk-sherwani-in-blue-mgv147",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-peach-kch629"
        },
        {
          "source": "plain-art-silk-sherwani-in-blue-mgv147",
          "strength": 0.0009212344541685859,
          "target": "woven-art-silk-jacquard-sherwani-in-teal-blue-mmq10"
        },
        {
          "source": "plain-art-silk-sherwani-in-blue-mgv147",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-beige-mmq9"
        },
        {
          "source": "plain-art-silk-sherwani-in-blue-mgv147",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-beige-mpw312"
        },
        {
          "source": "embroidered-net-lehenga-in-beige-lcc149",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-sherwani-with-churidar-in-off-white-mcd2613"
        },
        {
          "source": "embroidered-net-lehenga-in-beige-lcc149",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-beige-lrf35"
        },
        {
          "source": "handloom-pure-tussar-silk-saree-in-off-white-and-blue-srga481",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-tussar-silk-saree-in-beige-snea947"
        },
        {
          "source": "pearl-necklace-set-jmy327",
          "strength": 0.00046061722708429296,
          "target": "kundan-choker-necklace-set-jmy351"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-beige-kch1268",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-pakistani-suit-in-red-kuf10123"
        },
        {
          "source": "embroidered-net-pakistani-suit-in-red-kuf10123",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-asymmetric-pakistani-suit-in-beige-kjr150"
        },
        {
          "source": "handloom-pure-ghicha-silk-saree-in-beige-sts3429",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-kerala-kasavu-saree-in-off-white-spv123"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-beige-kch1266",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-art-silk-pakistani-suit-in-wine-kch158"
        },
        {
          "source": "plain-cotton-kurta-set-in-pink-mms841",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-set-in-light-purple-mpw106"
        },
        {
          "source": "embroidered-cotton-kurta-set-in-light-purple-mpw106",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-royal-blue-sew5609"
        },
        {
          "source": "embroidered-cotton-kurta-set-in-light-purple-mpw106",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-slub-kurta-set-in-beige-mtx37"
        },
        {
          "source": "plain-cotton-silk-slub-kurta-set-in-beige-mtx37",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-punjabi-suit-in-beige-kxz104"
        },
        {
          "source": "plain-cotton-silk-slub-kurta-set-in-beige-mtx37",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-navy-blue-mms901"
        },
        {
          "source": "plain-art-silk-kurta-set-in-navy-blue-mms901",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-sherwani-in-dark-blue-mpw187"
        },
        {
          "source": "plain-georgette-saree-in-maroon-sfva37",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-pyjama-sets-in-red-und330"
        },
        {
          "source": "embroidered-art-silk-kurta-pyjama-sets-in-red-und330",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-beige-kgzt173"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-beige-kgzt173",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-off-white-kgb3546",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-dark-teal-green-kej1005"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-off-white-kgb3546",
          "strength": 0.0009212344541685859,
          "target": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-navy-blue-kej1006",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-off-white-lcc151"
        },
        {
          "source": "printed-georgette-saree-in-cream-ssf3599",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-in-mustard-mtr91"
        },
        {
          "source": "plain-cotton-kurta-in-mustard-mtr91",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-indowestern-lehenga-in-cream-lqu460"
        },
        {
          "source": "embroidered-art-silk-indowestern-lehenga-in-cream-lqu460",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-neckline-art-silk-gown-in-fuchsia-and-brown-ttv23"
        },
        {
          "source": "hand-embroidered-neckline-art-silk-gown-in-fuchsia-and-brown-ttv23",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-red-lyc158"
        },
        {
          "source": "embroidered-art-dupion-silk-blouse-in-wine-uam79",
          "strength": 0.00046061722708429296,
          "target": "gota-work-pure-georgette-saree-in-pink-ombre-sjn4022"
        },
        {
          "source": "gota-work-pure-georgette-saree-in-pink-ombre-sjn4022",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-red-kuf10587"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-red-kuf10587",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-fuchsia-kbz226"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-red-kuf10587",
          "strength": 0.00046061722708429296,
          "target": "art-raw-silk-blouse-in-red-dbu576"
        },
        {
          "source": "printed-cotton-pakistani-suit-in-light-pink-and-yellow-kxc864",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-red-kuf10587"
        },
        {
          "source": "art-raw-silk-blouse-in-red-dbu576",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-kurta-in-brown-and-golden-thu925"
        },
        {
          "source": "art-raw-silk-blouse-in-red-dbu576",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-beige-ljn1431"
        },
        {
          "source": "embroidered-georgette-kurta-in-brown-and-golden-thu925",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-lehenga-in-beige-and-blue-luf1437"
        },
        {
          "source": "digital-printed-satin-lehenga-in-beige-and-blue-luf1437",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-red-kbz219"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-red-kbz219",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-punjabi-suit-in-old-rose-kch1326"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-red-kbz219",
          "strength": 0.00046061722708429296,
          "target": "plain-crepe-top-in-old-rose-thu263"
        },
        {
          "source": "plain-crepe-top-in-old-rose-thu263",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-grey-ombre-spfa2752"
        },
        {
          "source": "plain-crepe-top-in-old-rose-thu263",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-beige-kqu998"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-beige-kqu998",
          "strength": 0.00046061722708429296,
          "target": "plain-crepe-top-in-BLACK-thu763"
        },
        {
          "source": "plain-crepe-top-in-BLACK-thu763",
          "strength": 0.00046061722708429296,
          "target": "embroidered-straight-cut-suit-in-blue-kjn1313"
        },
        {
          "source": "embroidered-straight-cut-suit-in-blue-kjn1313",
          "strength": 0.00046061722708429296,
          "target": "art-silk-and-georgette-blouse-in-beige-and-off-white-ubd496"
        },
        {
          "source": "art-silk-and-georgette-blouse-in-beige-and-off-white-ubd496",
          "strength": 0.00046061722708429296,
          "target": "art-raw-silk-blouse-in-red-dbu576"
        },
        {
          "source": "embroidered-net-lehenga-in-beige-ljn1431",
          "strength": 0.00046061722708429296,
          "target": "printed-art-bhagalpuri-silk-palazzo-in-cream-bnj187"
        },
        {
          "source": "woven-border-art-silk-saree-in-pastel-orange-syc7484",
          "strength": 0.00046061722708429296,
          "target": "woven-border-art-silk-saree-in-pastel-green-syc7480"
        },
        {
          "source": "woven-border-art-silk-saree-in-pastel-green-syc7480",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-mustard-syc7476"
        },
        {
          "source": "embroidered-georgette-saree-in-mustard-syc7476",
          "strength": 0.00046061722708429296,
          "target": "woven-border-art-silk-saree-in-yellow-syc7479"
        },
        {
          "source": "woven-border-art-silk-saree-in-yellow-syc7479",
          "strength": 0.00046061722708429296,
          "target": "woven-border-art-silk-saree-in-black-syc7474"
        },
        {
          "source": "woven-border-art-silk-saree-in-black-syc7474",
          "strength": 0.00046061722708429296,
          "target": "ombre-chiffon-saree-in-orange-and-dark-pink-syc7485"
        },
        {
          "source": "ombre-chiffon-saree-in-orange-and-dark-pink-syc7485",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-blue-snba1532"
        },
        {
          "source": "woven-art-silk-saree-in-blue-snba1532",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-blue-mgv147"
        },
        {
          "source": "woven-art-silk-saree-in-blue-snba1532",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-violet-svta135"
        },
        {
          "source": "embroidered-art-silk-saree-in-violet-svta135",
          "strength": 0.00046061722708429296,
          "target": "woven-border-satin-chiffon-saree-in-beige-syc7477"
        },
        {
          "source": "woven-border-satin-chiffon-saree-in-beige-syc7477",
          "strength": 0.00046061722708429296,
          "target": "woven-border-chiffon-saree-in-dark-grey-syc7478"
        },
        {
          "source": "woven-border-chiffon-saree-in-dark-grey-syc7478",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-brown-syc7475"
        },
        {
          "source": "embroidered-chanderi-silk-punjabi-suit-in-navy-blue-kch1058",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-red-kbz219"
        },
        {
          "source": "embroidered-chanderi-silk-punjabi-suit-in-navy-blue-kch1058",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-set-with-nehru-jacket-in-light-beige-mtr70"
        },
        {
          "source": "printed-cotton-rayon-kurta-set-in-black-and-white-mtx3",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-light-orange-and-maroon-lxw139"
        },
        {
          "source": "handloom-pure-silk-jamdani-saree-in-off-white-srga87",
          "strength": 0.00046061722708429296,
          "target": "handloom-jamdani-cotton-silk-saree-in-green-srga583"
        },
        {
          "source": "handloom-jamdani-cotton-silk-saree-in-green-srga583",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-handloom-silk-saree-in-olive-green-and-beige-sgta75"
        },
        {
          "source": "pure-kanchipuram-handloom-silk-saree-in-olive-green-and-beige-sgta75",
          "strength": 0.00046061722708429296,
          "target": "printed-rayon-kurta-in-red-tqz166"
        },
        {
          "source": "pure-kanchipuram-handloom-silk-saree-in-olive-green-and-beige-sgta75",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-kurta-set-in-maroon-mve281"
        },
        {
          "source": "plain-art-silk-jacquard-jodhpuri-suit-in-black-mhg851",
          "strength": 0.00046061722708429296,
          "target": "brocade-art-silk-kurta-set-in-maroon-mcd2947"
        },
        {
          "source": "plain-art-silk-jacquard-jodhpuri-suit-in-black-mhg851",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-sherwani-in-beige-mkc253"
        },
        {
          "source": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-light-beige-mhg854",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-cream-mhg855"
        },
        {
          "source": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-cream-mhg855",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mgv149"
        },
        {
          "source": "plain-art-silk-kurta-set-in-fuchsia-mtx81",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-set-in-light-purple-mpw106"
        },
        {
          "source": "plain-art-silk-kurta-set-in-fuchsia-mtx81",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-white-mtx95"
        },
        {
          "source": "plain-cotton-kurta-set-in-white-mtx95",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-off-white-lcc151"
        },
        {
          "source": "plain-cotton-kurta-set-in-white-mtx95",
          "strength": 0.0013818516812528789,
          "target": "plain-cotton-kurta-set-in-white-mtx1"
        },
        {
          "source": "plain-cotton-kurta-set-in-white-mtx95",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-cotton-kurta-set-in-off-white-mve475"
        },
        {
          "source": "plain-cotton-kurta-set-in-white-mtx1",
          "strength": 0.0013818516812528789,
          "target": "plain-cotton-kurta-set-in-white-mtx95"
        },
        {
          "source": "plain-cotton-kurta-set-in-white-mtx1",
          "strength": 0.00046061722708429296,
          "target": "plain-jute-silk-sherwani-in-off-white-mgv199"
        },
        {
          "source": "plain-khadi-cotton-kurta-set-in-off-white-mve475",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-punjabi-suit-in-white-kxz116"
        },
        {
          "source": "plain-khadi-cotton-kurta-set-in-off-white-mve475",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-white-mtx95"
        },
        {
          "source": "printed-cotton-dress-in-black-and-white-tmw3",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-silk-and-art-ghicha-silk-long-dress-in-pink-thu84"
        },
        {
          "source": "printed-art-silk-circular-lehenga-in-beige-lcc62",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-navy-blue-mms901"
        },
        {
          "source": "printed-art-silk-circular-lehenga-in-beige-lcc62",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-circular-lehenga-in-black-and-beige-lxw366"
        },
        {
          "source": "printed-art-silk-circular-lehenga-in-beige-lcc62",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-light-beige-and-white-kch1150"
        },
        {
          "source": "embroidered-lycra-circular-lehenga-in-black-and-beige-lxw366",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-jacket-style-lehenga-in-blue-and-pastel-orange-lcc71"
        },
        {
          "source": "embroidered-lycra-circular-lehenga-in-black-and-beige-lxw366",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-jacket-style-lehenga-in-maroon-lbz7"
        },
        {
          "source": "embroidered-net-jacket-style-lehenga-in-blue-and-pastel-orange-lcc71",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-light-beige-and-white-kch1150"
        },
        {
          "source": "embroidered-net-jacket-style-lehenga-in-blue-and-pastel-orange-lcc71",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-peach-kch795"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-light-beige-and-white-kch1150",
          "strength": 0.0009212344541685859,
          "target": "embroidered-net-abaya-style-suit-in-peach-kch795"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-peach-kch795",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-black-mrg292"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-peach-kch795",
          "strength": 0.00046061722708429296,
          "target": "printed-art-silk-circular-lehenga-in-beige-lcc62"
        },
        {
          "source": "dupion-silk-dhoti-in-white-mpc642",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-pink-spfa1916"
        },
        {
          "source": "stone-studded-bridal-set-jxm499",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bridal-set-jxm504"
        },
        {
          "source": "stone-studded-bridal-set-jxm504",
          "strength": 0.00046061722708429296,
          "target": "kundan-matha-patti-jdw795"
        },
        {
          "source": "kundan-matha-patti-jdw795",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-matha-patti-jrl938"
        },
        {
          "source": "stone-studded-matha-patti-jrl938",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-matha-patti-jrl333"
        },
        {
          "source": "stone-studded-matha-patti-jrl333",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-circular-lehenga-in-red-lcc141"
        },
        {
          "source": "embroidered-velvet-circular-lehenga-in-red-lcc141",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-fuchsia-lyc184"
        },
        {
          "source": "polki-studded-bridal-necklace-set-jjr14564",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bridal-set-in-maroon-and-golden-jnc2298"
        },
        {
          "source": "stone-studded-bridal-set-in-maroon-and-golden-jnc2298",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bridal-set-jxm502"
        },
        {
          "source": "stone-studded-bridal-set-in-maroon-and-golden-jnc2298",
          "strength": 0.00046061722708429296,
          "target": "kundan-maang-tikka-jjr13862"
        },
        {
          "source": "floral-printed-georgette-saree-in-olive-green-sew4716",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-dark-teal-green-and-beige-sew5412"
        },
        {
          "source": "half-n-half-art-silk-saree-in-dark-teal-green-and-beige-sew5412",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-pink-seh1659"
        },
        {
          "source": "handloom-cotton-tant-saree-in-rust-spn3520",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-jamdani-saree-in-orange-spn3580"
        },
        {
          "source": "handloom-cotton-jamdani-saree-in-orange-spn3580",
          "strength": 0.00046061722708429296,
          "target": "embroidered-placket-cotton-pathani-suit-in-light-yellow-meu18"
        },
        {
          "source": "embroidered-velvet-lehenga-in-navy-blue-lyc189",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-maroon-lyc250"
        },
        {
          "source": "embroidered-velvet-lehenga-in-maroon-lyc250",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-dark-maroon-lat23"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-dark-maroon-lat23",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-mermaid-cut-lehenga-in-cream-lqm173"
        },
        {
          "source": "embroidered-art-silk-mermaid-cut-lehenga-in-cream-lqm173",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-shaded-pink-ljf27"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-shaded-pink-ljf27",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-maroon-lyc257"
        },
        {
          "source": "embroidered-velvet-lehenga-in-maroon-lyc257",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-light-beige-lyc235"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-light-beige-lyc235",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-red-lyc158"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-light-beige-lyc235",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-jodhpuri-suit-in-cream-mhg856"
        },
        {
          "source": "embroidered-velvet-a-line-lehenga-in-dark-maroon-lqm169",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-mermaid-cut-lehenga-in-cream-lqm173"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-red-kch799",
          "strength": 0.00046061722708429296,
          "target": "printed-art-silk-saree-in-orange-sbm6575"
        },
        {
          "source": "embroidered-kota-silk-saree-in-fuchsia-safa121",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-maroon-snba786"
        },
        {
          "source": "embroidered-kota-silk-saree-in-fuchsia-safa121",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-maroon-sew5745"
        },
        {
          "source": "embroidered-art-silk-saree-in-maroon-sew5745",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-dark-brown-sgpn203"
        },
        {
          "source": "embroidered-chiffon-saree-in-dark-brown-sgpn203",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-peach-skk22442"
        },
        {
          "source": "banarasi-silk-saree-in-purple-stea557",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-peach-sew5779"
        },
        {
          "source": "banarasi-saree-in-green-sew5280",
          "strength": 0.00046061722708429296,
          "target": "pure-banarasi-silk-handloom-saree-in-red-sbta30"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-turquoise-and-white-stbn37",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-handloom-silk-saree-in-rust-sgta58"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-turquoise-and-white-stbn37",
          "strength": 0.00046061722708429296,
          "target": "tie-dye-printed-cotton-straight-suit-in-blue-kmsg210"
        },
        {
          "source": "stone-studded-maang-tikka-jjr16701",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-pathani-suit-in-navy-blue-mtr335"
        },
        {
          "source": "hand-embroidered-net-abaya-style-suit-in-peach-kpk9",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-fuchsia-kjf79"
        },
        {
          "source": "rajasthani-georgette-saree-in-royal-blue-sjra608",
          "strength": 0.00046061722708429296,
          "target": "pure-chinon-crepe-bandhej-saree-in-maroon-sjn6899"
        },
        {
          "source": "embroidered-georgette-saree-in-black-skk22592",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-beige-mgv131"
        },
        {
          "source": "embroidered-georgette-saree-in-black-skk22592",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-sew3738"
        },
        {
          "source": "embroidered-georgette-saree-in-black-skk22592",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-orange-and-peach-sew3732"
        },
        {
          "source": "embroidered-georgette-saree-in-black-sew3738",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-orange-and-peach-sew3732"
        },
        {
          "source": "half-n-half-art-silk-saree-in-orange-and-peach-sew3732",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-dark-green-snea919"
        },
        {
          "source": "half-n-half-art-silk-saree-in-orange-and-peach-sew3732",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-skk22592"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-black-kch1295",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-slub-kurta-set-in-beige-mtx37"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-off-white-ktvb85",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-pure-chanderi-silk-pakistani-suit-in-off-white-kul87"
        },
        {
          "source": "hand-embroidered-pure-chanderi-silk-pakistani-suit-in-off-white-kul87",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-kurta-in-peach-and-black-tmz201"
        },
        {
          "source": "printed-crepe-kurta-in-peach-and-black-tmz201",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-pink-kvs1832"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-pink-kvs1832",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-abaya-style-suit-in-light-beige-and-red-kch1351"
        },
        {
          "source": "printed-georgette-abaya-style-suit-in-light-beige-and-red-kch1351",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-blue-snba1532"
        },
        {
          "source": "printed-georgette-abaya-style-suit-in-light-beige-and-red-kch1351",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-beige-kch674"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-beige-kch674",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-off-white-kej1022"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-beige-kch674",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurti-in-orange-tuf1086"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-off-white-kej1022",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-anarkali-suit-in-off-white-and-red-kxz9"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-light-orange-kch926",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-peach-kch795"
        },
        {
          "source": "woven-art-silk-jacquard-circular-lehenga-in-pink-lxw218",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-neon-green-lsh163"
        },
        {
          "source": "woven-art-silk-jacquard-circular-lehenga-in-pink-lxw218",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-dhoti-kurta-in-beige-mrg207"
        },
        {
          "source": "embroidered-satin-lehenga-in-neon-green-lsh163",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-pajama-in-blue-mhg706"
        },
        {
          "source": "handloom-jamdani-cotton-silk-saree-in-beige-srga588",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-lehenga-in-red-and-beige-lty8"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-fuchsia-kch623",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-light-beige-kch649"
        },
        {
          "source": "embroidered-neckline-art-silk-dhoti-kurta-in-light-beige-mrg206",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-dhoti-kurta-in-beige-mrg207"
        },
        {
          "source": "embroidered-neckline-art-silk-dhoti-kurta-in-beige-mrg207",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-churidar-in-off-white-thu1590"
        },
        {
          "source": "embroidered-neckline-art-silk-dhoti-kurta-in-beige-mrg207",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-dhoti-kurta-in-light-beige-mrg206"
        },
        {
          "source": "printed-cotton-pakistani-suit-in-beige-kmsg469",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-fawn-kch1304"
        },
        {
          "source": "stone-studded-necklace-set-jxm437",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-choker-necklace-set-jxm491"
        },
        {
          "source": "stone-studded-necklace-set-jxm437",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jvk1853"
        },
        {
          "source": "stone-studded-choker-necklace-set-jxm491",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bridal-set-jxm503"
        },
        {
          "source": "stone-studded-bridal-set-jxm503",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jxm234"
        },
        {
          "source": "stone-studded-necklace-set-jxm234",
          "strength": 0.00046061722708429296,
          "target": "pearl-necklace-set-jmy315"
        },
        {
          "source": "pearl-necklace-set-jmy315",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jxm437"
        },
        {
          "source": "stone-studded-necklace-set-jvk1853",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jxm239"
        },
        {
          "source": "stone-studded-necklace-set-jxm239",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-choker-necklace-set-jxm491"
        },
        {
          "source": "floral-printed-georgette-saree-in-old-rose-sew4483",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "source": "embroidered-georgette-lehenga-in-grey-lcc210",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-jacket-style-lehenga-in-grey-and-pink-lcc68"
        },
        {
          "source": "kundan-maang-tikka-jjr13862",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-punjabi-suit-in-dusty-green-kwy1016"
        },
        {
          "source": "kundan-maang-tikka-jjr13862",
          "strength": 0.00046061722708429296,
          "target": "beaded-maang-tikka-jjr14023"
        },
        {
          "source": "plain-jute-silk-sherwani-in-off-white-mgv199",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-navy-blue-mcd1940"
        },
        {
          "source": "plain-jute-silk-sherwani-in-off-white-mgv199",
          "strength": 0.00046061722708429296,
          "target": "plain-velvet-sherwani-in-black-mgv333"
        },
        {
          "source": "plain-cotton-pathani-suit-in-navy-blue-mtr335",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-abaya-style-suit-in-light-beige-and-red-kch1351"
        },
        {
          "source": "half-n-half-georgette-saree-in-navy-blue-and-light-green-snma16",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-beige-kch674"
        },
        {
          "source": "plain-cotton-kurti-in-orange-tuf1086",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-orange-lxw244"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-turquoise-mcd1183",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-teal-blue-mgv146"
        },
        {
          "source": "embroidered-dupion-silk-kurta-churidar-in-turquoise-mcd1183",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-jodhpuri-suit-in-cream-mhg856"
        },
        {
          "source": "embroidered-brocade-jodhpuri-suit-in-cream-mhg856",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-pink-and-orange-skk22497"
        },
        {
          "source": "embroidered-brocade-jodhpuri-suit-in-cream-mhg856",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-red-lyc188"
        },
        {
          "source": "half-n-half-georgette-saree-in-pink-and-orange-skk22497",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "half-n-half-georgette-saree-in-pink-and-orange-skk22497",
          "strength": 0.00046061722708429296,
          "target": "contrast-bordered-georgette-saree-in-black-sjra62"
        },
        {
          "source": "digital-printed-dupion-silk-dhoti-kurta-set-in-light-beige-mgv344",
          "strength": 0.00046061722708429296,
          "target": "plain-crepe-top-in-old-rose-thu263"
        },
        {
          "source": "digital-printed-dupion-silk-dhoti-kurta-set-in-light-beige-mgv344",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-jodhpuri-suit-in-cream-mhg857"
        },
        {
          "source": "embroidered-art-silk-jacket-style-kurta-in-light-pink-tsx164",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-gown-in-light-olive-green-tch3"
        },
        {
          "source": "embroidered-net-gown-in-light-olive-green-tch3",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-sherwani-in-maroon-mse456"
        },
        {
          "source": "embroidered-velvet-a-line-lehenga-in-dark-green-lqm174",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-a-line-lehenga-in-dark-maroon-lqm169"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-off-white-mgv335",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-set-in-maroon-mpw240"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-off-white-mgv335",
          "strength": 0.00046061722708429296,
          "target": "plain-jute-silk-sherwani-in-off-white-mgv199"
        },
        {
          "source": "plain-velvet-sherwani-in-black-mgv333",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-sherwani-in-dark-blue-mpw187"
        },
        {
          "source": "embroidered-velvet-sherwani-in-dark-blue-mpw187",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-coral-sew2932"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-teal-green-ubg60",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-salwar-set-in-teal-green-uvt10"
        },
        {
          "source": "embroidered-taffeta-silk-pakistani-suit-in-maroon-kch316",
          "strength": 0.00046061722708429296,
          "target": "mysore-crepe-saree-in-off-white-snga156"
        },
        {
          "source": "embroidered-taffeta-silk-pakistani-suit-in-maroon-kch316",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-maroon-kch1125"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-maroon-kch1125",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-maroon-snba790"
        },
        {
          "source": "jamdani-woven-cotton-silk-saree-in-black-srga863",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-silk-jamdani-saree-in-turquoise-stla109"
        },
        {
          "source": "handloom-cotton-silk-jamdani-saree-in-turquoise-stla109",
          "strength": 0.00046061722708429296,
          "target": "jamdani-woven-cotton-silk-saree-in-black-srga863"
        },
        {
          "source": "woven-pure-chanderi-silk-saree-in-orange-skba279",
          "strength": 0.00046061722708429296,
          "target": "handloom-chanderi-silk-saree-in-yellow-skba180"
        },
        {
          "source": "handloom-chanderi-silk-saree-in-yellow-skba180",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-linen-kurta-set-in-white-mpw145"
        },
        {
          "source": "handloom-chanderi-silk-saree-in-yellow-skba180",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-orange-swz88"
        },
        {
          "source": "woven-kanchipuram-silk-saree-in-orange-seh1424",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-saree-in-red-and-sky-blue-sgka1996"
        },
        {
          "source": "pure-kanchipuram-silk-saree-in-red-and-sky-blue-sgka1996",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-chiffon-jacquard-saree-in-pink-and-fuchsia-spfa237"
        },
        {
          "source": "lehenga-style-chiffon-jacquard-saree-in-pink-and-fuchsia-spfa237",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-maroon-snea723"
        },
        {
          "source": "woven-art-silk-saree-in-maroon-snea723",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-red-sbta95"
        },
        {
          "source": "banarasi-silk-saree-in-red-sbta95",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-orange-seh864"
        },
        {
          "source": "embroidered-chiffon-saree-in-orange-seh864",
          "strength": 0.00046061722708429296,
          "target": "banarasi-pure-silk-handloom-saree-in-maroon-snea401"
        },
        {
          "source": "banarasi-pure-silk-handloom-saree-in-maroon-snea401",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-sbh1234"
        },
        {
          "source": "embroidered-georgette-saree-in-red-sbh1234",
          "strength": 0.00046061722708429296,
          "target": "woven-georgette-jacquard-saree-in-pink-sjn6566"
        },
        {
          "source": "woven-georgette-jacquard-saree-in-pink-sjn6566",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-lehenga-net-saree-in-teal-green-sar821"
        },
        {
          "source": "hand-embroidered-lehenga-net-saree-in-teal-green-sar821",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-pure-kanchipuram-saree-in-coral-red-sbra460"
        },
        {
          "source": "hand-embroidered-pure-kanchipuram-saree-in-coral-red-sbra460",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-nehru-jacket-in-grey-mhg369"
        },
        {
          "source": "woven-brocade-nehru-jacket-in-grey-mhg369",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-saree-in-pink-sjn5646"
        },
        {
          "source": "embroidered-net-saree-in-pink-sjn5646",
          "strength": 0.00046061722708429296,
          "target": "banarasi-handloom-pure-silk-saree-in-orange-stea326"
        },
        {
          "source": "banarasi-handloom-pure-silk-saree-in-orange-stea326",
          "strength": 0.00046061722708429296,
          "target": "banarasi-handloom-pure-silk-saree-in-violet-stea332"
        },
        {
          "source": "banarasi-handloom-pure-silk-saree-in-violet-stea332",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-saree-in-beige-and-violet-sqfa129"
        },
        {
          "source": "half-n-half-net-saree-in-beige-and-violet-sqfa129",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-handloom-silk-saree-in-multi-color-sgta85"
        },
        {
          "source": "pure-kanchipuram-handloom-silk-saree-in-multi-color-sgta85",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-royal-blue-seh1408"
        },
        {
          "source": "woven-art-silk-saree-in-royal-blue-seh1408",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-jacquard-saree-in-pink-and-light-orange-stya52"
        },
        {
          "source": "half-n-half-art-silk-jacquard-saree-in-pink-and-light-orange-stya52",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-dhoti-sherwani-in-brown-mhg505"
        },
        {
          "source": "plain-khadi-dhoti-sherwani-in-brown-mhg505",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-and-crepe-saree-in-dark-brown-and-beige-skk21870"
        },
        {
          "source": "half-n-half-georgette-and-crepe-saree-in-dark-brown-and-beige-skk21870",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-orange-and-beige-stya62"
        },
        {
          "source": "half-n-half-art-silk-saree-in-orange-and-beige-stya62",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-ombre-orange-and-fuchsia-stl636"
        },
        {
          "source": "embroidered-chiffon-saree-in-ombre-orange-and-fuchsia-stl636",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-kanchipuram-saree-in-sky-blue-and-maroon-shp620"
        },
        {
          "source": "half-n-half-kanchipuram-saree-in-sky-blue-and-maroon-shp620",
          "strength": 0.00046061722708429296,
          "target": "woven-tussar-silk-saree-in-green-seh1726"
        },
        {
          "source": "woven-tussar-silk-saree-in-green-seh1726",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-satin-silk-banarasi-saree-in-teal-blue-snea909"
        },
        {
          "source": "woven-pure-satin-silk-banarasi-saree-in-teal-blue-snea909",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-fuchsia-sbta298"
        },
        {
          "source": "banarasi-silk-saree-in-fuchsia-sbta298",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-fuchsia-sbta248"
        },
        {
          "source": "banarasi-silk-saree-in-fuchsia-sbta248",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-fuchsia-and-baby-pink-sew3336"
        },
        {
          "source": "half-n-half-georgette-saree-in-fuchsia-and-baby-pink-sew3336",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-kanchipuram-silk-saree-in-coral-red-sgka2478"
        },
        {
          "source": "woven-pure-kanchipuram-silk-saree-in-coral-red-sgka2478",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-cream-sbra913"
        },
        {
          "source": "kanchipuram-saree-in-cream-sbra913",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-and-net-saree-in-peach-and-pink-sws5166"
        },
        {
          "source": "half-n-half-georgette-and-net-saree-in-peach-and-pink-sws5166",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-mustard-sbta272"
        },
        {
          "source": "banarasi-silk-saree-in-mustard-sbta272",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-pink-and-yellow-sud1044"
        },
        {
          "source": "half-n-half-art-silk-saree-in-pink-and-yellow-sud1044",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-green-sga7783"
        },
        {
          "source": "hand-embroidered-net-saree-in-green-sga7783",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-shaded-off-white-and-green-skk22151"
        },
        {
          "source": "embroidered-georgette-saree-in-shaded-off-white-and-green-skk22151",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-net-saree-in-teal-green-and-purple-skk13605"
        },
        {
          "source": "lehenga-style-net-saree-in-teal-green-and-purple-skk13605",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-saree-in-light-olive-green-sws5226"
        },
        {
          "source": "embroidered-net-saree-in-light-olive-green-sws5226",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-shimmer-saree-in-rose-gold-and-pink-skga129"
        },
        {
          "source": "half-n-half-lycra-shimmer-saree-in-rose-gold-and-pink-skga129",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-light-green-sau1708"
        },
        {
          "source": "embroidered-art-silk-saree-in-light-green-sau1708",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bangle-set-in-royal-blue-and-golden-jda905"
        },
        {
          "source": "stone-studded-bangle-set-in-royal-blue-and-golden-jda905",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-coral-red-seh1476"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-coral-red-seh1476",
          "strength": 0.00046061722708429296,
          "target": "banarasi-handloom-silk-saree-in-light-brown-stea282"
        },
        {
          "source": "banarasi-handloom-silk-saree-in-light-brown-stea282",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-old-rose-sbta261"
        },
        {
          "source": "banarasi-silk-saree-in-old-rose-sbta261",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-beige-sbta308"
        },
        {
          "source": "banarasi-silk-saree-in-beige-sbta308",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-sbra887"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-sbra887",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-saree-in-antique-and-cream-sfs629"
        },
        {
          "source": "half-n-half-lycra-saree-in-antique-and-cream-sfs629",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-jacquard-saree-in-wine-and-blue-stya47"
        },
        {
          "source": "half-n-half-art-silk-jacquard-saree-in-wine-and-blue-stya47",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-net-saree-in-beige-and-magenta-sew2988"
        },
        {
          "source": "lehenga-style-net-saree-in-beige-and-magenta-sew2988",
          "strength": 0.00046061722708429296,
          "target": "lehenga-style-embroidered-art-silk-saree-in-coral-and-green-sud1180"
        },
        {
          "source": "lehenga-style-embroidered-art-silk-saree-in-coral-and-green-sud1180",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-ombre-orange-and-fuchsia-stl636"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-royal-blue-sar865",
          "strength": 0.0013818516812528789,
          "target": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-royal-blue-sar888",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-red-lyc188",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-circular-lehenga-in-shaded-beige-and-pink-lcc142"
        },
        {
          "source": "hand-embroidered-brocade-jodhpuri-suit-in-cream-mhg857",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "hand-embroidered-brocade-jodhpuri-suit-in-cream-mhg857",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-jute-silk-jodhpuri-suit-in-light-beige-mhg854"
        },
        {
          "source": "bengal-handloom-cotton-silk-tant-saree-in-beige-and-maroon-stla91",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-beige-and-red-syc7357"
        },
        {
          "source": "woven-cotton-silk-saree-in-beige-and-red-syc7357",
          "strength": 0.00046061722708429296,
          "target": "bengal-handloom-cotton-silk-tant-saree-in-beige-and-maroon-stla91"
        },
        {
          "source": "banarasi-saree-in-maroon-stea515",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-green-sew5280"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-teal-blue-mmq10",
          "strength": 0.0009212344541685859,
          "target": "plain-art-silk-sherwani-in-blue-mgv147"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-teal-blue-mmq10",
          "strength": 0.0009212344541685859,
          "target": "plain-art-silk-sherwani-in-beige-mmq9"
        },
        {
          "source": "plain-art-silk-sherwani-in-beige-mmq9",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-sherwani-in-maroon-mrg324"
        },
        {
          "source": "plain-art-silk-sherwani-in-beige-mmq9",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-blue-mgv147"
        },
        {
          "source": "plain-art-silk-sherwani-in-beige-mmq9",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-teal-blue-mmq10"
        },
        {
          "source": "plain-art-silk-sherwani-in-beige-mmq9",
          "strength": 0.0009212344541685859,
          "target": "woven-brocade-silk-sherwani-in-beige-mpw312"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mpw312",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-sherwani-in-blue-mgv147"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mpw312",
          "strength": 0.0009212344541685859,
          "target": "plain-art-silk-sherwani-in-beige-mmq9"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mpw312",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-beige-mpw310"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mpw310",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-beige-mpw312"
        },
        {
          "source": "hand-embroidered-brocade-dhoti-sherwani-in-white-mhg458",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-sherwani-in-beige-mkc253"
        },
        {
          "source": "hand-embroidered-brocade-dhoti-sherwani-in-white-mhg458",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-golden-mxh59"
        },
        {
          "source": "hand-embroidered-brocade-dhoti-sherwani-in-white-mhg458",
          "strength": 0.00046061722708429296,
          "target": "embellished-chiffon-saree-in-teal-blue-sjra135"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-light-green-kepd38",
          "strength": 0.00046061722708429296,
          "target": "embroidered-brocade-sherwani-in-maroon-and-gold-mpe85"
        },
        {
          "source": "woven-art-silk-saree-in-green-syc6999",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-dark-green-snea919"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-peach-kch1016",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-pink-mrg278"
        },
        {
          "source": "hand-embroidered-brocade-silk-sherwani-in-golden-mxh59",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-navy-blue-kry990"
        },
        {
          "source": "plain-cotton-asymmetric-kurta-set-in-dark-purple-mee326",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-sherwani-in-maroon-and-golden-mpw161"
        },
        {
          "source": "plain-cotton-asymmetric-kurta-set-in-dark-purple-mee326",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-linen-kurta-set-in-yellow-mpw158"
        },
        {
          "source": "embroidered-cotton-linen-kurta-set-in-yellow-mpw158",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-churidar-in-wine-mcd1669"
        },
        {
          "source": "cotton-kurta-pyjama-in-white-mvj18",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-crepe-saree-in-fuchsia-and-off-white-sjra389"
        },
        {
          "source": "cotton-kurta-pyjama-in-white-mvj18",
          "strength": 0.00046061722708429296,
          "target": "woven-dupion-silk-kurta-churidar-in-off-white-mat28"
        },
        {
          "source": "embroidered-placket-cotton-pathani-suit-in-black-meu14",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-viscose-georgette-flared-gown-in-sea-green-tsp178"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-sea-green-kmsg305",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-light-green-kepd38"
        },
        {
          "source": "plain-rayon-cotton-combo-of-kurta-in-yellow-and-magenta-tdr792",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-beige-mhg729"
        },
        {
          "source": "abstract-printed-cotton-straight-suit-in-maroon-khbz152",
          "strength": 0.00046061722708429296,
          "target": "shibhori-printed-cotton-straight-suit-in-mustard-khbz151"
        },
        {
          "source": "woven-mysore-chiffon-saree-in-green-sbra784",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-crepe-saree-in-dark-green-sew5420"
        },
        {
          "source": "woven-mysore-chiffon-saree-in-green-sbra784",
          "strength": 0.0009212344541685859,
          "target": "woven-mysore-chiffon-saree-in-teal-green-sbra780"
        },
        {
          "source": "woven-mysore-chiffon-saree-in-teal-green-sbra780",
          "strength": 0.0009212344541685859,
          "target": "woven-mysore-chiffon-saree-in-green-sbra784"
        },
        {
          "source": "embroidered-patch-border-georgette-saree-in-fuchsia-sqh1477",
          "strength": 0.00046061722708429296,
          "target": "printed-patch-border-georgette-saree-in-fuchsia-sbz2751"
        },
        {
          "source": "plain-dupion-silk-kurta-set-with-nehru-jacket-in-light-beige-mtr70",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-peach-kch1016"
        },
        {
          "source": "embroidered-satin-lehenga-in-cream-lyc255",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-shaded-red-and-maroon-lyc220"
        },
        {
          "source": "embroidered-velvet-lehenga-in-red-lyc259",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-maroon-ljn1137"
        },
        {
          "source": "embroidered-velvet-lehenga-in-red-lyc259",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-red-lqm188"
        },
        {
          "source": "embroidered-net-lehenga-in-maroon-ljn1137",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-red-lyc259"
        },
        {
          "source": "embroidered-net-lehenga-in-maroon-ljn1137",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-a-line-lehenga-in-red-ljn1168"
        },
        {
          "source": "embroidered-net-a-line-lehenga-in-red-ljn1168",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-red-lyc236"
        },
        {
          "source": "embroidered-velvet-lehenga-in-red-lyc236",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-maroon-ljn1137"
        },
        {
          "source": "woven-dupion-silk-kurta-churidar-in-off-white-mat28",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg699"
        },
        {
          "source": "woven-dupion-silk-kurta-churidar-in-off-white-mat28",
          "strength": 0.00046061722708429296,
          "target": "cotton-kurta-pyjama-in-white-mvj18"
        },
        {
          "source": "embroidered-art-silk-gown-in-dark-green-uku727",
          "strength": 0.00046061722708429296,
          "target": "embroidered-shimmer-net-lehenga-set-in-green-unj279"
        },
        {
          "source": "embellished-chiffon-saree-in-teal-blue-sjra135",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "source": "embellished-chiffon-saree-in-teal-blue-sjra135",
          "strength": 0.00046061722708429296,
          "target": "printed-art-silk-circular-lehenga-in-beige-lcc62"
        },
        {
          "source": "embellished-chiffon-saree-in-teal-blue-sjra135",
          "strength": 0.00046061722708429296,
          "target": "embellished-lycra-saree-in-royal-blue-spfa1522"
        },
        {
          "source": "embellished-lycra-saree-in-royal-blue-spfa1522",
          "strength": 0.00046061722708429296,
          "target": "embellished-chiffon-saree-in-teal-blue-sjra135"
        },
        {
          "source": "embellished-lycra-saree-in-royal-blue-spfa1522",
          "strength": 0.00046061722708429296,
          "target": "ombre-georgette-saree-in-fuchsia-and-magenta-ssf4494"
        },
        {
          "source": "ombre-georgette-saree-in-fuchsia-and-magenta-ssf4494",
          "strength": 0.00046061722708429296,
          "target": "embellished-lycra-saree-in-royal-blue-spfa1522"
        },
        {
          "source": "bell-sleeve-georgette-flared-kurti-in-cream-thu1786",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-turquoise-swz237"
        },
        {
          "source": "banarasi-saree-in-pink-syla52",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-pink-syla41"
        },
        {
          "source": "banarasi-saree-in-pink-syla52",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-light-teal-green-syla48"
        },
        {
          "source": "banarasi-saree-in-pink-syla41",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-stbn110"
        },
        {
          "source": "banarasi-saree-in-pink-syla41",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-fuchsia-sfka1714"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-and-green-seh1790",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-silk-saree-in-coral-skra1133"
        },
        {
          "source": "kanchipuram-silk-saree-in-coral-skra1133",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-pink-seh1659"
        },
        {
          "source": "kanchipuram-silk-saree-in-coral-skra1133",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-snba788"
        },
        {
          "source": "kanchipuram-silk-saree-in-coral-skra1133",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-stbn114"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-snba788",
          "strength": 0.00046061722708429296,
          "target": "velvet-blazer-in-fuchsia-mhg122"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-snba788",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-pink-syla41"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-snba788",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-silk-saree-in-coral-skra1133"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-snba788",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-black-snba787"
        },
        {
          "source": "banarasi-saree-in-fuchsia-sfka1714",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-stbn110"
        },
        {
          "source": "kanchipuram-saree-in-black-snba787",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-snba788"
        },
        {
          "source": "banarasi-saree-in-light-teal-green-syla48",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-red-snba856"
        },
        {
          "source": "woven-art-silk-saree-in-red-snba856",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-stbn114",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-pink-sfva140"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-pink-sfva140",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-black-syla47"
        },
        {
          "source": "woven-art-silk-saree-in-black-syla47",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-maroon-sew5055"
        },
        {
          "source": "embroidered-art-silk-sherwani-in-white-mkc255",
          "strength": 0.0009212344541685859,
          "target": "embroidered-brocade-sherwani-in-white-mse370"
        },
        {
          "source": "beaded-maang-tikka-jjr13857",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bridal-set-in-maroon-and-golden-jnc2298"
        },
        {
          "source": "printed-art-silk-saree-in-orange-sbm6575",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-jodhpuri-suit-in-cream-mhg857"
        },
        {
          "source": "pure-mysore-crepe-woven-saree-in-old-rose-shu680",
          "strength": 0.00046061722708429296,
          "target": "pure-mysore-crepe-woven-saree-in-olive-green-shu674"
        },
        {
          "source": "block-printed-cotton-maxi-dress-in-indigo-blue-tjw503",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chambray-dress-in-blue-thu1529"
        },
        {
          "source": "embroidered-chambray-dress-in-blue-thu1529",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-flared-tunic-in-red-tyq90"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-brown-kch1013",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-straight-suit-in-beige-kfx2968"
        },
        {
          "source": "woven-mysore-silk-saree-in-blue-stbn127",
          "strength": 0.00046061722708429296,
          "target": "mysore-silk-saree-in-light-green-sbra388"
        },
        {
          "source": "mysore-silk-saree-in-light-green-sbra388",
          "strength": 0.00046061722708429296,
          "target": "pure-mysore-silk-saree-in-yellow-sbra222"
        },
        {
          "source": "pure-mysore-silk-saree-in-yellow-sbra222",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-mysore-silk-saree-in-light-beige-sbra201"
        },
        {
          "source": "woven-pure-mysore-silk-saree-in-light-beige-sbra201",
          "strength": 0.00046061722708429296,
          "target": "woven-mysore-silk-saree-in-pink-stbn126"
        },
        {
          "source": "kanchipuram-saree-in-orange-snba658",
          "strength": 0.00046061722708429296,
          "target": "pure-gadwal-silk-saree-in-royal-blue-stga112"
        },
        {
          "source": "pure-gadwal-silk-saree-in-royal-blue-stga112",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-navy-blue-sqpa177"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-black-sar816",
          "strength": 0.00046061722708429296,
          "target": "woven-border-jute-cotton-saree-in-sea-green-sjn7083"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-black-sar816",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-chiffon-saree-in-light-beige-sar966"
        },
        {
          "source": "woven-border-jute-cotton-saree-in-sea-green-sjn7083",
          "strength": 0.00046061722708429296,
          "target": "patch-border-lycra-shimmer-saree-in-pink-smu3164"
        },
        {
          "source": "patch-border-lycra-shimmer-saree-in-pink-smu3164",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-maroon-snba786"
        },
        {
          "source": "patch-border-lycra-shimmer-saree-in-pink-smu3164",
          "strength": 0.00046061722708429296,
          "target": "pure-silk-embroidered-saree-in-fuchsia-snea1057"
        },
        {
          "source": "pure-silk-embroidered-saree-in-fuchsia-snea1057",
          "strength": 0.00046061722708429296,
          "target": "patch-border-lycra-shimmer-saree-in-pink-smu3164"
        },
        {
          "source": "pure-silk-embroidered-saree-in-fuchsia-snea1057",
          "strength": 0.00046061722708429296,
          "target": "pure-silk-embroidered-saree-in-coral-pink-snea1060"
        },
        {
          "source": "pure-silk-embroidered-saree-in-coral-pink-snea1060",
          "strength": 0.00046061722708429296,
          "target": "embroidered-shimmer-chiffon-prestitched-saree-in-pink-suf6834"
        },
        {
          "source": "embroidered-shimmer-chiffon-prestitched-saree-in-pink-suf6834",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-black-sar816"
        },
        {
          "source": "embroidered-shimmer-chiffon-prestitched-saree-in-pink-suf6834",
          "strength": 0.00046061722708429296,
          "target": "pure-silk-embroidered-saree-in-coral-pink-snea1060"
        },
        {
          "source": "hand-embroidered-chiffon-saree-in-light-beige-sar966",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-saree-in-mustard-sjra538"
        },
        {
          "source": "plain-chiffon-saree-in-mustard-sjra538",
          "strength": 0.00046061722708429296,
          "target": "woven-border-jute-cotton-saree-in-black-sjn7084"
        },
        {
          "source": "woven-border-jute-cotton-saree-in-black-sjn7084",
          "strength": 0.00046061722708429296,
          "target": "embroidered-shimmer-chiffon-prestitched-saree-in-pink-suf6834"
        },
        {
          "source": "handloom-cotton-tant-saree-in-light-green-and-white-ssna98",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-brocade-silk-sherwani-in-golden-mxh59"
        },
        {
          "source": "handloom-cotton-tant-saree-in-light-green-and-white-ssna98",
          "strength": 0.00046061722708429296,
          "target": "prestitched-maharashtrian-nauvari-saree-in-fuchsia-ssf3010"
        },
        {
          "source": "bangalore-silk-saree-in-maroon-sbra1068",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-mustard-ssf4392"
        },
        {
          "source": "bangalore-silk-saree-in-maroon-sbra1068",
          "strength": 0.00046061722708429296,
          "target": "block-printed-dupion-silk-kurta-set-in-off-white-und618"
        },
        {
          "source": "embroidered-cotton-pathani-suit-in-white-muf654",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-grey-sew4349"
        },
        {
          "source": "embroidered-cotton-pathani-suit-in-white-muf654",
          "strength": 0.00046061722708429296,
          "target": "printed-art-silk-pakistani-suit-in-pink-kmsg367"
        },
        {
          "source": "dupion-silk-dhoti-in-beige-mpc568",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-churidar-in-maroon-mcd2388"
        },
        {
          "source": "plain-cotton-straight-suit-in-beige-kfx2968",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-jacket-style-lehenga-in-peach-lcc67"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-fuchsia-sxc1779",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-layered-gown-in-fuchsia-uku571"
        },
        {
          "source": "dupion-silk-dhoti-in-maroon-mpc578",
          "strength": 0.00046061722708429296,
          "target": "printed-straight-cut-suit-in-sky-blue-kcv1182"
        },
        {
          "source": "printed-straight-cut-suit-in-sky-blue-kcv1182",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-fuchsia-kch623"
        },
        {
          "source": "dupion-silk-dhoti-in-maroon-mpc645",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-saree-in-pastel-green-snba1395"
        },
        {
          "source": "digital-printed-georgette-saree-in-pastel-green-snba1395",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-long-skirt-in-navy-blue-thu412"
        },
        {
          "source": "plain-dupion-silk-long-skirt-in-navy-blue-thu412",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-abaya-style-suit-in-orange-kgzt136"
        },
        {
          "source": "embroidered-chanderi-silk-abaya-style-suit-in-orange-kgzt136",
          "strength": 0.00046061722708429296,
          "target": "tant-handloom-cotton-saree-in-off-white-spn1895"
        },
        {
          "source": "bandhej-crepe-saree-in-black-sjn7073",
          "strength": 0.00046061722708429296,
          "target": "bandhani-crepe-saree-in-onion-pink-sjn5755"
        },
        {
          "source": "bandhej-crepe-saree-in-black-sjn7073",
          "strength": 0.00046061722708429296,
          "target": "brocade-flared-dress-in-beige-thu279"
        },
        {
          "source": "bandhej-crepe-saree-in-black-sjn7073",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-saree-in-off-white-spn2715"
        },
        {
          "source": "bandhej-crepe-saree-in-black-sjn7073",
          "strength": 0.00046061722708429296,
          "target": "brocade-blouse-in-gold-dbu617"
        },
        {
          "source": "block-printed-dupion-silk-kurta-set-in-off-white-und618",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-cotton-silk-kurta-set-in-blue-mse787"
        },
        {
          "source": "embroidered-neckline-cotton-silk-kurta-set-in-blue-mse787",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-asymmetric-pakistani-suit-in-mustard-kjr149"
        },
        {
          "source": "embroidered-georgette-asymmetric-pakistani-suit-in-mustard-kjr149",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-tant-saree-in-light-green-and-white-ssna98"
        },
        {
          "source": "prestitched-maharashtrian-nauvari-saree-in-fuchsia-ssf3010",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-circular-lehenga-in-pink-ltl90"
        },
        {
          "source": "embroidered-net-circular-lehenga-in-pink-ltl90",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-mustard-kjn3156"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-mustard-kjn3156",
          "strength": 0.00046061722708429296,
          "target": "bandhani-georgette-saree-in-white-suv123"
        },
        {
          "source": "woven-cotton-tant-saree-in-off-white-and-sky-blue-shxa130",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-brasso-saree-in-pink-and-light-green-skra1308"
        },
        {
          "source": "woven-cotton-tant-saree-in-off-white-and-sky-blue-shxa130",
          "strength": 0.00046061722708429296,
          "target": "pure-banarasi-chiffon-saree-in-fuchsia-sau1491"
        },
        {
          "source": "woven-cotton-tant-saree-in-off-white-and-sky-blue-shxa130",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-purple-suf7412"
        },
        {
          "source": "half-n-half-net-brasso-saree-in-pink-and-light-green-skra1308",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-blouse-in-magenta-utt449"
        },
        {
          "source": "embroidered-art-silk-blouse-in-magenta-utt449",
          "strength": 0.00046061722708429296,
          "target": "beaded-antique-necklace-set-jdw522"
        },
        {
          "source": "beaded-antique-necklace-set-jdw522",
          "strength": 0.00046061722708429296,
          "target": "bhagalpuri-silk-saree-in-blue-sts3674"
        },
        {
          "source": "bhagalpuri-silk-saree-in-blue-sts3674",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-nehru-jacket-in-beige-mhg559"
        },
        {
          "source": "woven-cotton-silk-jacquard-nehru-jacket-in-beige-mhg559",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-purple-sswa612"
        },
        {
          "source": "woven-cotton-silk-saree-in-purple-sswa612",
          "strength": 0.00046061722708429296,
          "target": "printed-double-layered-straight-cut-cotton-chanderi-suit-in-blue-kjn2034"
        },
        {
          "source": "printed-double-layered-straight-cut-cotton-chanderi-suit-in-blue-kjn2034",
          "strength": 0.00046061722708429296,
          "target": "embroidered-viscose-georgette-straight-suit-in-red-ktj392"
        },
        {
          "source": "embroidered-viscose-georgette-straight-suit-in-red-ktj392",
          "strength": 0.00046061722708429296,
          "target": "banarasi-pure-silk-handloom-saree-in-maroon-snea436"
        },
        {
          "source": "banarasi-pure-silk-handloom-saree-in-maroon-snea436",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-black-shxa12"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-black-shxa12",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-mustard-sgpn377"
        },
        {
          "source": "woven-cotton-silk-saree-in-mustard-sgpn377",
          "strength": 0.00046061722708429296,
          "target": "woven-art-chanderi-silk-anarkali-suit-in-beige-and-blue-kpk46"
        },
        {
          "source": "woven-art-chanderi-silk-anarkali-suit-in-beige-and-blue-kpk46",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-blue-sfwa245"
        },
        {
          "source": "woven-chanderi-silk-saree-in-blue-sfwa245",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-magenta-sas1341"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-magenta-sas1341",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-abaya-style-suit-in-wine-kym419"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-magenta-sas1341",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-beige-snea1171"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-magenta-sas1341",
          "strength": 0.00046061722708429296,
          "target": "bhagalpuri-silk-saree-in-black-sts3638"
        },
        {
          "source": "embroidered-chanderi-silk-abaya-style-suit-in-wine-kym419",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-saree-in-pink-svqa81"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-pink-svqa81",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-light-orange-spfa1533"
        },
        {
          "source": "woven-cotton-silk-saree-in-light-orange-spfa1533",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-flared-tunic-in-purple-tdr756"
        },
        {
          "source": "plain-rayon-flared-tunic-in-purple-tdr756",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-mustard-and-red-sbta231"
        },
        {
          "source": "banarasi-silk-saree-in-mustard-and-red-sbta231",
          "strength": 0.00046061722708429296,
          "target": "embroidered-crepe-a-line-lehenga-in-fuchsia-luf571"
        },
        {
          "source": "embroidered-crepe-a-line-lehenga-in-fuchsia-luf571",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-black-svra379"
        },
        {
          "source": "printed-cotton-saree-in-black-svra379",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-handloom-saree-in-red-snea650"
        },
        {
          "source": "embroidered-art-silk-handloom-saree-in-red-snea650",
          "strength": 0.00046061722708429296,
          "target": "plain-shimmer-georgette-palazzo-in-rose-gold-thu935"
        },
        {
          "source": "plain-shimmer-georgette-palazzo-in-rose-gold-thu935",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-mustard-ssl36179"
        },
        {
          "source": "printed-georgette-saree-in-mustard-ssl36179",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-anarkali-suit-in-black-kvng63"
        },
        {
          "source": "printed-georgette-saree-in-mustard-ssl36179",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-set-in-teal-green-msf321"
        },
        {
          "source": "embroidered-cotton-anarkali-suit-in-black-kvng63",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-striaght-cut-suit-in-red-kux122"
        },
        {
          "source": "embroidered-georgette-striaght-cut-suit-in-red-kux122",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-saree-in-pink-and-green-sqfa159"
        },
        {
          "source": "half-n-half-net-saree-in-pink-and-green-sqfa159",
          "strength": 0.00046061722708429296,
          "target": "embroidered-leather-mojari-in-pink-duf22"
        },
        {
          "source": "embroidered-leather-mojari-in-pink-duf22",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-yellow-ssha826"
        },
        {
          "source": "embroidered-georgette-saree-in-yellow-ssha826",
          "strength": 0.00046061722708429296,
          "target": "american-diamond-necklace-set-jvr36"
        },
        {
          "source": "embroidered-georgette-saree-in-yellow-ssha826",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-brown-and-black-szma62"
        },
        {
          "source": "american-diamond-necklace-set-jvr36",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-gown-in-black-tsp176"
        },
        {
          "source": "hand-embroidered-georgette-gown-in-black-tsp176",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-mustard-sxc2240"
        },
        {
          "source": "embroidered-art-silk-saree-in-mustard-sxc2240",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-pink-and-beige-lql46"
        },
        {
          "source": "embroidered-art-silk-saree-in-mustard-sxc2240",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-tant-saree-in-grey-and-green-srga841"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-pink-and-beige-lql46",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-silk-saree-in-olive-green-snea639"
        },
        {
          "source": "embroidered-pure-silk-saree-in-olive-green-snea639",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-beige-kch944"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-beige-kch944",
          "strength": 0.00046061722708429296,
          "target": "block-printed-pure-cotton-saree-in-red-suv114"
        },
        {
          "source": "block-printed-pure-cotton-saree-in-red-suv114",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-fuchsia-ukx121"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-fuchsia-ukx121",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-mustard-sxc2240"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-fuchsia-ukx121",
          "strength": 0.00046061722708429296,
          "target": "woven-jamdani-cotton-silk-handloom-saree-in-white-srga709"
        },
        {
          "source": "woven-jamdani-cotton-silk-handloom-saree-in-white-srga709",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-dhoti-sherwani-in-beige-mhg506"
        },
        {
          "source": "plain-art-silk-dhoti-sherwani-in-beige-mhg506",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-tussar-silk-saree-in-purple-snea536"
        },
        {
          "source": "embroidered-pure-tussar-silk-saree-in-purple-snea536",
          "strength": 0.00046061722708429296,
          "target": "woven-mysore-chiffon-saree-in-maroon-sbra783"
        },
        {
          "source": "woven-mysore-chiffon-saree-in-maroon-sbra783",
          "strength": 0.00046061722708429296,
          "target": "contrast-trim-rayon-kurta-in-red-tdr1036"
        },
        {
          "source": "digital-printed-yoke-cotton-straight-suit-in-navy-blue-kye789",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-wine-suf6106"
        },
        {
          "source": "woven-art-silk-saree-in-wine-suf6106",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pair-of-bangles-jvk2998"
        },
        {
          "source": "stone-studded-pair-of-bangles-jvk2998",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-saree-in-dark-fawn-sgpn207"
        },
        {
          "source": "embroidered-lycra-saree-in-dark-fawn-sgpn207",
          "strength": 0.00046061722708429296,
          "target": "woven-organza-saree-in-pink-sfwa144"
        },
        {
          "source": "woven-organza-saree-in-pink-sfwa144",
          "strength": 0.00046061722708429296,
          "target": "embroidered-banarasi-silk-anarkali-suit-in-fuchsia-kjn2092"
        },
        {
          "source": "embroidered-banarasi-silk-anarkali-suit-in-fuchsia-kjn2092",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-saree-in-dark-green-sfl2711"
        },
        {
          "source": "woven-cotton-silk-jacquard-saree-in-dark-green-sfl2711",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-crop-top-set-in-rust-ttz75"
        },
        {
          "source": "woven-cotton-silk-jacquard-crop-top-set-in-rust-ttz75",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-anarkali-suit-in-dark-blue-kgb2630"
        },
        {
          "source": "embroidered-georgette-anarkali-suit-in-dark-blue-kgb2630",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-maroon-kch644"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-maroon-kch644",
          "strength": 0.00046061722708429296,
          "target": "polki-necklace-set-jjr16090"
        },
        {
          "source": "polki-necklace-set-jjr16090",
          "strength": 0.00046061722708429296,
          "target": "bandhani-crepe-saree-in-onion-pink-sjn5755"
        },
        {
          "source": "embroidered-art-silk-saree-in-grey-and-white-srp648",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-pakistani-suit-in-white-and-black-knf369"
        },
        {
          "source": "embroidered-art-silk-saree-in-grey-and-white-srp648",
          "strength": 0.00046061722708429296,
          "target": "printed-jute-hand-bag-in-green-and-multicolor-dlj38"
        },
        {
          "source": "printed-georgette-pakistani-suit-in-white-and-black-knf369",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "printed-georgette-pakistani-suit-in-white-and-black-knf369",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-mysore-silk-saree-in-teal-green-sbra956"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-pink-kuf10216",
          "strength": 0.00046061722708429296,
          "target": "abstract-printed-georgette-saree-in-red-sgja131"
        },
        {
          "source": "abstract-printed-georgette-saree-in-red-sgja131",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-silk-saree-in-purple-sswa612"
        },
        {
          "source": "handloom-cotton-silk-saree-in-purple-sswa612",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-malmal-top-in-white-trb291"
        },
        {
          "source": "block-printed-cotton-malmal-top-in-white-trb291",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-tant-saree-in-off-white-and-sky-blue-shxa130"
        },
        {
          "source": "pure-banarasi-chiffon-saree-in-fuchsia-sau1491",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-pakistani-suit-in-black-kjf74"
        },
        {
          "source": "embroidered-taffeta-silk-pakistani-suit-in-black-kjf74",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-pajama-in-royal-blue-mtr31"
        },
        {
          "source": "plain-dupion-silk-kurta-pajama-in-royal-blue-mtr31",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jjr16537"
        },
        {
          "source": "stone-studded-necklace-set-jjr16537",
          "strength": 0.00046061722708429296,
          "target": "beaded-oxidised-earrings-jnj1927"
        },
        {
          "source": "beaded-oxidised-earrings-jnj1927",
          "strength": 0.00046061722708429296,
          "target": "woven-south-cotton-saree-in-blue-sfl2613"
        },
        {
          "source": "beaded-oxidised-earrings-jnj1927",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-chanderi-silk-saree-in-black-skba181"
        },
        {
          "source": "woven-south-cotton-saree-in-blue-sfl2613",
          "strength": 0.00046061722708429296,
          "target": "border-satin-georgette-saree-in-shaded-blue-and-teal-green-syc6131"
        },
        {
          "source": "border-satin-georgette-saree-in-shaded-blue-and-teal-green-syc6131",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-lehenga-in-pink-lsh70"
        },
        {
          "source": "embroidered-georgette-lehenga-in-pink-lsh70",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-pink-and-orange-sfs802"
        },
        {
          "source": "embroidered-art-silk-saree-in-pink-and-orange-sfs802",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-olive-green-sbra927"
        },
        {
          "source": "woven-bangalore-silk-saree-in-olive-green-sbra927",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-dark-brown-snca361"
        },
        {
          "source": "woven-cotton-silk-saree-in-dark-brown-snca361",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-light-green-lrf19"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-light-green-lrf19",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-set-in-blue-mpw2"
        },
        {
          "source": "embroidered-dupion-silk-kurta-set-in-blue-mpw2",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-teal-blue-spfa1886"
        },
        {
          "source": "embroidered-art-silk-saree-in-teal-blue-spfa1886",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-layered-mathapatti-jrl909"
        },
        {
          "source": "embroidered-art-silk-saree-in-teal-blue-spfa1886",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-saree-in-grey-scda82"
        },
        {
          "source": "stone-studded-layered-mathapatti-jrl909",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-blue-kve129"
        },
        {
          "source": "printed-cotton-straight-suit-in-blue-kve129",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-tussar-silk-saree-in-maroon-snea971"
        },
        {
          "source": "embroidered-pure-tussar-silk-saree-in-maroon-snea971",
          "strength": 0.00046061722708429296,
          "target": "pacchikari-hoop-jhumka-style-earring-jjr14384"
        },
        {
          "source": "pacchikari-hoop-jhumka-style-earring-jjr14384",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-yellow-sew4571"
        },
        {
          "source": "printed-georgette-saree-in-yellow-sew4571",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-maroon-kfx1489"
        },
        {
          "source": "printed-cotton-straight-suit-in-maroon-kfx1489",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-cream-sgka3089"
        },
        {
          "source": "kanchipuram-saree-in-cream-sgka3089",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-beige-stla278"
        },
        {
          "source": "handloom-tant-cotton-saree-in-beige-stla278",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-fuchsia-kej941"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-fuchsia-kej941",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-abaya-style-suit-in-pink-kxzd38"
        },
        {
          "source": "hand-embroidered-georgette-abaya-style-suit-in-pink-kxzd38",
          "strength": 0.00046061722708429296,
          "target": "embroidered-viscose-georgette-abaya-style-suit-in-navy-blue-kuf9171"
        },
        {
          "source": "embroidered-viscose-georgette-abaya-style-suit-in-navy-blue-kuf9171",
          "strength": 0.00046061722708429296,
          "target": "pre-stitched-net-saree-in-purple-suf5371"
        },
        {
          "source": "pre-stitched-net-saree-in-purple-suf5371",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jjr16523"
        },
        {
          "source": "stone-studded-necklace-set-jjr16523",
          "strength": 0.00046061722708429296,
          "target": "patch-border-georgette-saree-in-cream-sjn5924"
        },
        {
          "source": "patch-border-georgette-saree-in-cream-sjn5924",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-pink-sga5415"
        },
        {
          "source": "patch-border-georgette-saree-in-cream-sjn5924",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chikankari-poly-cotton-kurta-set-in-white-mpe163"
        },
        {
          "source": "embroidered-georgette-saree-in-pink-sga5415",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-beige-skra739"
        },
        {
          "source": "embroidered-art-silk-saree-in-beige-skra739",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-dark-green-sgqa19"
        },
        {
          "source": "woven-art-silk-saree-in-dark-green-sgqa19",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-net-saree-in-baby-pink-stl737"
        },
        {
          "source": "embroidered-lycra-net-saree-in-baby-pink-stl737",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-front-slit-kurta-in-teal-blue-tqj260"
        },
        {
          "source": "printed-cotton-front-slit-kurta-in-teal-blue-tqj260",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pendant-jts488"
        },
        {
          "source": "stone-studded-pendant-jts488",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-pink-sbra808"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-pink-sbra808",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-adjustable-pair-of-anklet-jrl703"
        },
        {
          "source": "stone-studded-adjustable-pair-of-anklet-jrl703",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pendant-set-jvm2500"
        },
        {
          "source": "stone-studded-pendant-set-jvm2500",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-silk-saree-in-pink-snea591"
        },
        {
          "source": "embroidered-pure-silk-saree-in-pink-snea591",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-maroon-scfa450"
        },
        {
          "source": "printed-cotton-saree-in-maroon-scfa450",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-brown-and-off-white-kfx2290"
        },
        {
          "source": "printed-cotton-straight-suit-in-brown-and-off-white-kfx2290",
          "strength": 0.00046061722708429296,
          "target": "net-lehenga-with-embroidered-choli-in-coral-and-maroon-lxw241"
        },
        {
          "source": "net-lehenga-with-embroidered-choli-in-coral-and-maroon-lxw241",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-orange-sgka3100"
        },
        {
          "source": "kanchipuram-saree-in-orange-sgka3100",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-long-kurta-in-black-trv43"
        },
        {
          "source": "woven-chanderi-silk-long-kurta-in-black-trv43",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-straight-suit-in-beige-kqu916"
        },
        {
          "source": "embroidered-art-silk-straight-suit-in-beige-kqu916",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-light-olive-green-ssf3783"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-light-olive-green-ssf3783",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-shaded-grey-and-yellow-sgja637"
        },
        {
          "source": "embroidered-georgette-saree-in-shaded-grey-and-yellow-sgja637",
          "strength": 0.00046061722708429296,
          "target": "handloom-mangalgiri-cotton-saree-in-black-and-turquoise-sbpa48"
        },
        {
          "source": "handloom-mangalgiri-cotton-saree-in-black-and-turquoise-sbpa48",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-tant-saree-in-off-white-and-sky-blue-shxa130"
        },
        {
          "source": "handloom-mangalgiri-cotton-saree-in-black-and-turquoise-sbpa48",
          "strength": 0.00046061722708429296,
          "target": "tie-n-dye-georgette-long-kurta-in-red-and-black-tjw425"
        },
        {
          "source": "kanchipuram-saree-in-purple-suf7412",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-saree-in-coral-red-sud1467"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-coral-red-sud1467",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-red-kej892"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-red-kej892",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-black-srp611"
        },
        {
          "source": "embroidered-art-silk-saree-in-black-srp611",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-asymmetric-kurta-in-black-tnc874"
        },
        {
          "source": "embroidered-georgette-asymmetric-kurta-in-black-tnc874",
          "strength": 0.00046061722708429296,
          "target": "ombre-art-silk-saree-in-beige-and-green-svta91"
        },
        {
          "source": "ombre-art-silk-saree-in-beige-and-green-svta91",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-saree-in-turquoise-sud1472"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-turquoise-sud1472",
          "strength": 0.00046061722708429296,
          "target": "dori-bangle-set-jdp37"
        },
        {
          "source": "dori-bangle-set-jdp37",
          "strength": 0.00046061722708429296,
          "target": "plain-crepe-asymmetric-tunic-in-red-thu739"
        },
        {
          "source": "plain-crepe-asymmetric-tunic-in-red-thu739",
          "strength": 0.00046061722708429296,
          "target": "bandhani-crepe-saree-in-onion-pink-sjn5755"
        },
        {
          "source": "tie-dye-printed-cotton-straight-suit-in-blue-kmsg210",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-layered-kurta-in-yellow-tbf70"
        },
        {
          "source": "tie-dye-printed-cotton-straight-suit-in-blue-kmsg210",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jmy78"
        },
        {
          "source": "printed-cotton-layered-kurta-in-yellow-tbf70",
          "strength": 0.00046061722708429296,
          "target": "brocade-jacket-style-lehenga-in-yellow-luz2"
        },
        {
          "source": "brocade-jacket-style-lehenga-in-yellow-luz2",
          "strength": 0.00046061722708429296,
          "target": "printed-jute-hand-bag-in-green-and-multicolor-dlj38"
        },
        {
          "source": "printed-jute-hand-bag-in-green-and-multicolor-dlj38",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-beige-uyp19"
        },
        {
          "source": "printed-jute-hand-bag-in-green-and-multicolor-dlj38",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-saree-in-grey-scda82"
        },
        {
          "source": "printed-jute-hand-bag-in-green-and-multicolor-dlj38",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-red-lqu201"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-beige-uyp19",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-black-szma151"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-beige-uyp19",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-dark-beige-lqm185"
        },
        {
          "source": "woven-art-silk-saree-in-black-szma151",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-sky-blue-sbta300"
        },
        {
          "source": "banarasi-silk-saree-in-sky-blue-sbta300",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-teal-blue-spfa1886"
        },
        {
          "source": "banarasi-silk-saree-in-sky-blue-sbta300",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-yellow-stu626"
        },
        {
          "source": "woven-cotton-saree-in-grey-scda82",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-off-white-mhg703"
        },
        {
          "source": "stone-studded-armlet-jvk2731",
          "strength": 0.00046061722708429296,
          "target": "pacchikari-anklet-jjr14989"
        },
        {
          "source": "pacchikari-anklet-jjr14989",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-embroidered-saree-in-fuchsia-and-orange-sqfa234"
        },
        {
          "source": "half-n-half-art-silk-embroidered-saree-in-fuchsia-and-orange-sqfa234",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-maroon-scfa445"
        },
        {
          "source": "printed-cotton-saree-in-maroon-scfa445",
          "strength": 0.00046061722708429296,
          "target": "contrast-border-chiffon-saree-in-red-szy676"
        },
        {
          "source": "contrast-border-chiffon-saree-in-red-szy676",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-georgette-saree-in-beige-ssf4325"
        },
        {
          "source": "floral-printed-georgette-saree-in-beige-ssf4325",
          "strength": 0.00046061722708429296,
          "target": "plain-net-abaya-style-suit-in-turquoise-kxz119"
        },
        {
          "source": "plain-net-abaya-style-suit-in-turquoise-kxz119",
          "strength": 0.00046061722708429296,
          "target": "abstract-printed-cotton-flared-long-kurta-in-blue-tbk127"
        },
        {
          "source": "abstract-printed-cotton-flared-long-kurta-in-blue-tbk127",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-skpa706"
        },
        {
          "source": "embroidered-georgette-saree-in-black-skpa706",
          "strength": 0.00046061722708429296,
          "target": "gota-work-chiffon-lehenga-in-sky-blue-ljn1380"
        },
        {
          "source": "gota-work-chiffon-lehenga-in-sky-blue-ljn1380",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-teal-green-ssf4235"
        },
        {
          "source": "woven-cotton-handloom-saree-in-off-white-srga680",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jmy78"
        },
        {
          "source": "stone-studded-necklace-set-jmy78",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-shaded-purple-and-cream-sew4469"
        },
        {
          "source": "stone-studded-necklace-set-jmy78",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-saree-in-black-svqa78"
        },
        {
          "source": "stone-studded-necklace-set-jmy78",
          "strength": 0.00046061722708429296,
          "target": "plain-satin-crepe-saree-in-teal-blue-smda503"
        },
        {
          "source": "half-n-half-georgette-saree-in-shaded-purple-and-cream-sew4469",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-red-and-cream-sbz3393"
        },
        {
          "source": "half-n-half-georgette-saree-in-red-and-cream-sbz3393",
          "strength": 0.00046061722708429296,
          "target": "embroidered-patch-border-work-chiffon-saree-in-red-sga5354"
        },
        {
          "source": "embroidered-patch-border-work-chiffon-saree-in-red-sga5354",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-chiffon-saree-in-pink-and-dark-brown-skk22546"
        },
        {
          "source": "half-n-half-chiffon-saree-in-pink-and-dark-brown-skk22546",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-light-old-rose-ssx5447"
        },
        {
          "source": "printed-cotton-saree-in-light-old-rose-ssx5447",
          "strength": 0.00046061722708429296,
          "target": "teal-green-cotton-dupatta-brj131"
        },
        {
          "source": "teal-green-cotton-dupatta-brj131",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-golden-mxh55"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-golden-mxh55",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-teal-green-sgja631"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-golden-mxh55",
          "strength": 0.00046061722708429296,
          "target": "kundan-adjustable-hasli-choker-necklace-set-jvk1692"
        },
        {
          "source": "embroidered-georgette-saree-in-teal-green-sgja631",
          "strength": 0.00046061722708429296,
          "target": "plain-south-cotton-long-kurta-set-in-off-white-tja350"
        },
        {
          "source": "plain-south-cotton-long-kurta-set-in-off-white-tja350",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-choker-necklace-set-jmy183"
        },
        {
          "source": "stone-studded-choker-necklace-set-jmy183",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-yellow-skpa1054"
        },
        {
          "source": "embroidered-art-silk-saree-in-yellow-skpa1054",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-jacket-style-lehenga-in-olive-green-and-beige-lqm140"
        },
        {
          "source": "embroidered-net-jacket-style-lehenga-in-olive-green-and-beige-lqm140",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-inshaded-red-and-navy-blue-sqfa245"
        },
        {
          "source": "half-n-half-georgette-saree-inshaded-red-and-navy-blue-sqfa245",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-cotton-straight-suit-in-green-ombre-kfx2658"
        },
        {
          "source": "floral-printed-cotton-straight-suit-in-green-ombre-kfx2658",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-sky-blue-safa130"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-sky-blue-safa130",
          "strength": 0.00046061722708429296,
          "target": "jacquard-nehru-jacket-in-magenta-mse512"
        },
        {
          "source": "jacquard-nehru-jacket-in-magenta-mse512",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-magenta-kym423"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-magenta-kym423",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-rust-scfa306"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-rust-scfa306",
          "strength": 0.00046061722708429296,
          "target": "hand-printed-pure-tussar-silk-saree-in-light-beige-sffa25"
        },
        {
          "source": "hand-printed-pure-tussar-silk-saree-in-light-beige-sffa25",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-tant-saree-in-yellow-spn3522"
        },
        {
          "source": "hand-printed-pure-tussar-silk-saree-in-light-beige-sffa25",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-jacquard-saree-in-violet-and-cream-sjra174"
        },
        {
          "source": "handloom-cotton-tant-saree-in-yellow-spn3522",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-sherwani-in-beige-mhg734"
        },
        {
          "source": "woven-brocade-sherwani-in-beige-mhg734",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-yellow-sgpn464"
        },
        {
          "source": "woven-cotton-silk-saree-in-yellow-sgpn464",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-saree-in-peach-sfs621"
        },
        {
          "source": "woven-cotton-silk-saree-in-yellow-sgpn464",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-circular-lehenga-in-blue-lqu413"
        },
        {
          "source": "embroidered-net-saree-in-peach-sfs621",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pendant-set-jvk2097"
        },
        {
          "source": "stone-studded-pendant-set-jvk2097",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-short-kurta-in-aqua-blue-mee289"
        },
        {
          "source": "plain-cotton-short-kurta-in-aqua-blue-mee289",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-pink-and-mustard-stma148"
        },
        {
          "source": "banarasi-silk-saree-in-pink-and-mustard-stma148",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-anarkali-style-lehenga-in-red-luf540"
        },
        {
          "source": "banarasi-silk-saree-in-pink-and-mustard-stma148",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-raw-silk-kurta-set-in-light-yellow-mxh127"
        },
        {
          "source": "embroidered-net-anarkali-style-lehenga-in-red-luf540",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-grey-and-white-srp648"
        },
        {
          "source": "handloom-cotton-saree-in-grey-scda82",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-black-sbra318"
        },
        {
          "source": "kanchipuram-saree-in-black-sbra318",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-georgette-saree-in-black-sjra367"
        },
        {
          "source": "embroidered-border-georgette-saree-in-black-sjra367",
          "strength": 0.00046061722708429296,
          "target": "tye-n-die-georgette-saree-in-blue-sbh1742"
        },
        {
          "source": "tye-n-die-georgette-saree-in-blue-sbh1742",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-royal-blue-kch168"
        },
        {
          "source": "tye-n-die-georgette-saree-in-blue-sbh1742",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-red-svra388"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-royal-blue-kch168",
          "strength": 0.00046061722708429296,
          "target": "plain-art-dupion-silk-dhoti-kurta-in-off-white-mpc768"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-royal-blue-kch168",
          "strength": 0.00046061722708429296,
          "target": "handloom-art-tussar-silk-dupatta-in-light-grey-bbe61"
        },
        {
          "source": "plain-art-dupion-silk-dhoti-kurta-in-off-white-mpc768",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-pure-silk-saree-in-purple-stbn49"
        },
        {
          "source": "plain-art-dupion-silk-dhoti-kurta-in-off-white-mpc768",
          "strength": 0.00046061722708429296,
          "target": "pure-georgette-banarasi-saree-in-royal-blue-snea273"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-purple-stbn49",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-beige-scfa459"
        },
        {
          "source": "printed-cotton-saree-in-beige-scfa459",
          "strength": 0.00046061722708429296,
          "target": "solid-crepe-zouave-pant-in-black-thu851"
        },
        {
          "source": "solid-crepe-zouave-pant-in-black-thu851",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-wine-stla302"
        },
        {
          "source": "solid-crepe-zouave-pant-in-black-thu851",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-beige-kfx1890"
        },
        {
          "source": "solid-crepe-zouave-pant-in-black-thu851",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-mysore-silk-saree-in-dark-green-shu791"
        },
        {
          "source": "embroidered-art-silk-jacquard-saree-in-beige-snea803",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-yellow-sgpn464"
        },
        {
          "source": "embroidered-art-silk-jacquard-saree-in-beige-snea803",
          "strength": 0.00046061722708429296,
          "target": "printed-rayon-straight-kurta-in-pink-and-grey-tbk88"
        },
        {
          "source": "printed-rayon-straight-kurta-in-pink-and-grey-tbk88",
          "strength": 0.00046061722708429296,
          "target": "woven-rayon-cowl-style-kurta-set-in-beige-and-maroon-mhg725"
        },
        {
          "source": "woven-rayon-cowl-style-kurta-set-in-beige-and-maroon-mhg725",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-tant-saree-in-orange-spn3017"
        },
        {
          "source": "woven-cotton-tant-saree-in-orange-spn3017",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-crop-top-in-maroon-trb648"
        },
        {
          "source": "embroidered-velvet-crop-top-in-maroon-trb648",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-pathani-suit-in-white-muf654"
        },
        {
          "source": "embroidered-velvet-crop-top-in-maroon-trb648",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-net-saree-in-beige-and-fuchsia-svla323"
        },
        {
          "source": "printed-art-silk-pakistani-suit-in-pink-kmsg367",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-straight-suit-in-light-beige-and-red-kyz201"
        },
        {
          "source": "woven-chanderi-silk-straight-suit-in-light-beige-and-red-kyz201",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-in-light-purple-tnz15"
        },
        {
          "source": "embroidered-cotton-kurta-in-light-purple-tnz15",
          "strength": 0.00046061722708429296,
          "target": "contrast-trim-rayon-kurta-in-red-tdr1036"
        },
        {
          "source": "woven-art-silk-handloom-saree-in-yellow-snea667",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-light-beige-and-multicolor-spfa1975"
        },
        {
          "source": "printed-georgette-saree-in-light-beige-and-multicolor-spfa1975",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-chiffon-saree-in-sea-green-sck493"
        },
        {
          "source": "embroidered-border-chiffon-saree-in-sea-green-sck493",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-beige-sud1026"
        },
        {
          "source": "embroidered-art-silk-saree-in-beige-sud1026",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-green-sbra330"
        },
        {
          "source": "kanchipuram-saree-in-green-sbra330",
          "strength": 0.00046061722708429296,
          "target": "hand-printed-pure-tussar-silk-saree-in-light-beige-sffa25"
        },
        {
          "source": "half-n-half-georgette-jacquard-saree-in-violet-and-cream-sjra174",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-light-moss-green-scxa601"
        },
        {
          "source": "half-n-half-georgette-jacquard-saree-in-violet-and-cream-sjra174",
          "strength": 0.00046061722708429296,
          "target": "printed-straight-cut-suit-in-red-and-white-ket30"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-light-moss-green-scxa601",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-cotton-saree-in-sky-blue-sswa525"
        },
        {
          "source": "woven-pure-cotton-saree-in-sky-blue-sswa525",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-sbh714"
        },
        {
          "source": "embroidered-georgette-saree-in-black-sbh714",
          "strength": 0.00046061722708429296,
          "target": "golden-stones-studded-earrings-jsw124"
        },
        {
          "source": "golden-stones-studded-earrings-jsw124",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-jacquard-saree-in-green-and-teal-green-sgja601"
        },
        {
          "source": "half-n-half-art-silk-jacquard-saree-in-green-and-teal-green-sgja601",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-indigo-blue-stea399"
        },
        {
          "source": "half-n-half-art-silk-jacquard-saree-in-green-and-teal-green-sgja601",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-beige-mhg728"
        },
        {
          "source": "woven-art-silk-saree-in-indigo-blue-stea399",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-printed-cotton-saree-in-olive-green-and-blue-scfa427"
        },
        {
          "source": "woven-art-silk-saree-in-indigo-blue-stea399",
          "strength": 0.00046061722708429296,
          "target": "pearl-work-georgette-saree-in-red-skpa150"
        },
        {
          "source": "half-n-half-printed-cotton-saree-in-olive-green-and-blue-scfa427",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-jvk2002"
        },
        {
          "source": "stone-studded-necklace-jvk2002",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-lehenga-in-off-white-and-indigo-blue-luc19"
        },
        {
          "source": "hand-embroidered-net-lehenga-in-off-white-and-indigo-blue-luc19",
          "strength": 0.00046061722708429296,
          "target": "color-block-rayon-kurta-in-blue-and-white-trq266"
        },
        {
          "source": "color-block-rayon-kurta-in-blue-and-white-trq266",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-chiffon-saree-in-pink-and-light-blue-skra727"
        },
        {
          "source": "half-n-half-chiffon-saree-in-pink-and-light-blue-skra727",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-pajama-in-maroon-mtr50"
        },
        {
          "source": "plain-dupion-silk-kurta-pajama-in-maroon-mtr50",
          "strength": 0.00046061722708429296,
          "target": "woven-super-net-saree-in-turquoise-sfwa169"
        },
        {
          "source": "woven-super-net-saree-in-turquoise-sfwa169",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-hasli-choker-set-jvk1748"
        },
        {
          "source": "stone-studded-hasli-choker-set-jvk1748",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-anarkali-suit-in-red-kuf8351"
        },
        {
          "source": "embroidered-georgette-anarkali-suit-in-red-kuf8351",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-black-kbz57"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-black-kbz57",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-saree-in-red-and-blue-sqh1387"
        },
        {
          "source": "half-n-half-net-saree-in-red-and-blue-sqh1387",
          "strength": 0.00046061722708429296,
          "target": "handloom-banarasi-silk-saree-in-orange-snea1158"
        },
        {
          "source": "handloom-banarasi-silk-saree-in-orange-snea1158",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-chanderi-straight-cut-suit-in-light-yellow-kjn2807"
        },
        {
          "source": "handloom-banarasi-silk-saree-in-orange-snea1158",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-lycra-combo-leggings-in-blue-yellow-and-green-tsf366"
        },
        {
          "source": "plain-cotton-chanderi-straight-cut-suit-in-light-yellow-kjn2807",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-golden-mxh55"
        },
        {
          "source": "kundan-adjustable-hasli-choker-necklace-set-jvk1692",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-turquoise-and-navy-blue-sjra126"
        },
        {
          "source": "half-n-half-georgette-saree-in-turquoise-and-navy-blue-sjra126",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-asymmetric-suit-in-black-kuz270"
        },
        {
          "source": "half-n-half-georgette-saree-in-turquoise-and-navy-blue-sjra126",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-net-saree-in-beige-and-coral-red-svla324"
        },
        {
          "source": "embroidered-georgette-asymmetric-suit-in-black-kuz270",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-jacquard-kurti-in-peach-tdr647"
        },
        {
          "source": "woven-cotton-jacquard-kurti-in-peach-tdr647",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-green-ombre-kucd7"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-green-ombre-kucd7",
          "strength": 0.00046061722708429296,
          "target": "embroidered-viscose-pakistani-suit-in-beige-kau131"
        },
        {
          "source": "embroidered-viscose-pakistani-suit-in-beige-kau131",
          "strength": 0.00046061722708429296,
          "target": "handloom-chanderi-silk-saree-in-coral-red-skba171"
        },
        {
          "source": "handloom-chanderi-silk-saree-in-coral-red-skba171",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-light-orange-utt574"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-light-orange-utt574",
          "strength": 0.00046061722708429296,
          "target": "kundan-necklace-set-jmy337"
        },
        {
          "source": "kundan-necklace-set-jmy337",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-magenta-sfka778"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-magenta-sfka778",
          "strength": 0.00046061722708429296,
          "target": "solid-crepe-zouave-pant-in-black-thu851"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-magenta-sfka778",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-red-sar867"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-magenta-sfka778",
          "strength": 0.00046061722708429296,
          "target": "woven-kanchipuram-silk-saree-in-purple-seh1462"
        },
        {
          "source": "printed-cotton-straight-suit-in-beige-kfx1890",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-dupatta-in-peach-orange-bmb6"
        },
        {
          "source": "woven-chanderi-cotton-dupatta-in-peach-orange-bmb6",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-wine-stla302"
        },
        {
          "source": "stone-studded-earring-in-royal-blue-and-white-jvm2159",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-saree-in-beige-shp812"
        },
        {
          "source": "pure-kanchipuram-silk-saree-in-beige-shp812",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-satin-georgette-saree-in-red-and-beige-sew4200"
        },
        {
          "source": "half-n-half-satin-georgette-saree-in-red-and-beige-sew4200",
          "strength": 0.00046061722708429296,
          "target": "embroidered-straight-cut-suit-in-off-white-and-old-rose-kds696"
        },
        {
          "source": "embroidered-straight-cut-suit-in-off-white-and-old-rose-kds696",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-dhoti-in-maroon-mpc572"
        },
        {
          "source": "plain-dupion-silk-dhoti-in-maroon-mpc572",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695"
        },
        {
          "source": "handloom-cotton-tant-saree-in-orange-spn3017",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-pink-lzr175"
        },
        {
          "source": "embroidered-net-lehenga-in-pink-lzr175",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-rayon-tunic-in-orange-trq209"
        },
        {
          "source": "plain-cotton-rayon-tunic-in-orange-trq209",
          "strength": 0.00046061722708429296,
          "target": "tye-n-die-georgette-saree-in-blue-sbh1742"
        },
        {
          "source": "printed-cotton-saree-in-red-svra388",
          "strength": 0.00046061722708429296,
          "target": "pure-georgette-silk-banarasi-saree-in-royal-blue-snea273"
        },
        {
          "source": "pure-georgette-silk-banarasi-saree-in-royal-blue-snea273",
          "strength": 0.00046061722708429296,
          "target": "pure-satin-silk-banarasi-saree-in-wine-snea879"
        },
        {
          "source": "pure-satin-silk-banarasi-saree-in-wine-snea879",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-anarkali-suit-in-royal-blue-kuz110"
        },
        {
          "source": "embroidered-georgette-anarkali-suit-in-royal-blue-kuz110",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-cotton-saree-in-sky-blue-sswa525"
        },
        {
          "source": "handloom-pure-cotton-saree-in-sky-blue-sswa525",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-and-net-saree-in-orange-sas1210"
        },
        {
          "source": "half-n-half-georgette-and-net-saree-in-orange-sas1210",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-pink-kae778"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-pink-kae778",
          "strength": 0.00046061722708429296,
          "target": "studded-bangle-pair-in-magenta-jvk1211"
        },
        {
          "source": "studded-bangle-pair-in-magenta-jvk1211",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-jacquard-saree-in-green-and-teal-green-sgja601"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mhg728",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-maroon-scfa304"
        },
        {
          "source": "woven-brocade-silk-sherwani-in-beige-mhg728",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-south-cotton-straight-suit-in-pink-kfx2647"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-maroon-scfa304",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-top-in-green-tbf38"
        },
        {
          "source": "plain-rayon-top-in-green-tbf38",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-red-scxa599"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-red-scxa599",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-indigo-blue-stea399"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-red-scxa599",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-linen-kurta-jacket-set-in-white-and-coral-pink-mhg665"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-red-scxa599",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-navy-blue-ombre-sjra133"
        },
        {
          "source": "pearl-work-georgette-saree-in-red-skpa150",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-neckline-dupion-silk-kurta-set-in-olive-green-mxh20"
        },
        {
          "source": "hand-embroidered-neckline-dupion-silk-kurta-set-in-olive-green-mxh20",
          "strength": 0.00046061722708429296,
          "target": "printed-bhagalpuri-silk-circular-lehenga-in-beige-ljn1272"
        },
        {
          "source": "printed-bhagalpuri-silk-circular-lehenga-in-beige-ljn1272",
          "strength": 0.00046061722708429296,
          "target": "embroidered-poly-cotton-straight-suit-in-fuchsia-kmsg96"
        },
        {
          "source": "embroidered-poly-cotton-straight-suit-in-fuchsia-kmsg96",
          "strength": 0.00046061722708429296,
          "target": "jacquard-cotton-silk-nehru-jacket-in-light-beige-mjt8"
        },
        {
          "source": "jacquard-cotton-silk-nehru-jacket-in-light-beige-mjt8",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-light-beige-spn2820"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-light-beige-spn2820",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-red-scxa599"
        },
        {
          "source": "woven-cotton-linen-kurta-jacket-set-in-white-and-coral-pink-mhg665",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-dhoti-in-red-mpc657"
        },
        {
          "source": "dupion-silk-dhoti-in-red-mpc657",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-anarkali-suit-in-black-kqb37"
        },
        {
          "source": "dupion-silk-dhoti-in-red-mpc657",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-beige-kgzt236"
        },
        {
          "source": "embroidered-georgette-anarkali-suit-in-black-kqb37",
          "strength": 0.00046061722708429296,
          "target": "handloom-mangalgiri-cotton-saree-in-black-and-turquoise-sbpa48"
        },
        {
          "source": "tie-n-dye-georgette-long-kurta-in-red-and-black-tjw425",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-saree-in-off-white-ssf3747"
        },
        {
          "source": "woven-cotton-saree-in-off-white-ssf3747",
          "strength": 0.00046061722708429296,
          "target": "woven-dupion-silk-saree-in-beige-svqa189"
        },
        {
          "source": "woven-dupion-silk-saree-in-beige-svqa189",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-neckline-art-silk-gown-in-brown-ttv15"
        },
        {
          "source": "hand-embroidered-neckline-art-silk-gown-in-brown-ttv15",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-fuchsia-kxc762"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-fuchsia-kxc762",
          "strength": 0.00046061722708429296,
          "target": "gota-patti-art-silk-circular-lehenga-in-maroon-ombre-lzl19"
        },
        {
          "source": "gota-patti-art-silk-circular-lehenga-in-maroon-ombre-lzl19",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-fuchsia-sbra796"
        },
        {
          "source": "woven-bangalore-silk-saree-in-fuchsia-sbra796",
          "strength": 0.00046061722708429296,
          "target": "gota-patti-chinon-chiffon-saree-in-yellow-snya21"
        },
        {
          "source": "gota-patti-chinon-chiffon-saree-in-yellow-snya21",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-linen-kurta-jacket-set-in-white-and-golden-mhg666"
        },
        {
          "source": "woven-cotton-linen-kurta-jacket-set-in-white-and-golden-mhg666",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bhagalpuri-silk-saree-in-green-sjn2929"
        },
        {
          "source": "embroidered-bhagalpuri-silk-saree-in-green-sjn2929",
          "strength": 0.00046061722708429296,
          "target": "mangalgiri-cotton-saree-in-orange-sbpa61"
        },
        {
          "source": "mangalgiri-cotton-saree-in-orange-sbpa61",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-sherwani-in-off-white-mrg445"
        },
        {
          "source": "embroidered-art-silk-sherwani-in-off-white-mrg445",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-straight-cut-suit-in-off-white-kjn2513"
        },
        {
          "source": "plain-cotton-silk-straight-cut-suit-in-off-white-kjn2513",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-straight-suit-in-pink-kcu166"
        },
        {
          "source": "woven-cotton-silk-jacquard-straight-suit-in-pink-kcu166",
          "strength": 0.00046061722708429296,
          "target": "ombre-chiffon-saree-in-peach-and-orange-szy675"
        },
        {
          "source": "ombre-chiffon-saree-in-peach-and-orange-szy675",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-a-line-lehenga-in-red-lyn2"
        },
        {
          "source": "ombre-chiffon-saree-in-peach-and-orange-szy675",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-coral-red-sau2308"
        },
        {
          "source": "embroidered-net-a-line-lehenga-in-red-lyn2",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-dark-blue-ssja192"
        },
        {
          "source": "woven-art-silk-saree-in-dark-blue-ssja192",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bangle-set-jdp415"
        },
        {
          "source": "woven-art-silk-saree-in-dark-blue-ssja192",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-asymmetric-kurta-in-navy-blue-tgp72"
        },
        {
          "source": "stone-studded-bangle-set-jdp415",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bangle-pair-in-magenta-jvk1208"
        },
        {
          "source": "stone-studded-bangle-pair-in-magenta-jvk1208",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-maroon-sgka1583"
        },
        {
          "source": "woven-bangalore-silk-saree-in-maroon-sgka1583",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-satin-georgette-saree-in-teal-green-and-sea-green-sew3344"
        },
        {
          "source": "half-n-half-satin-georgette-saree-in-teal-green-and-sea-green-sew3344",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-blue-kds817"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-blue-kds817",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-gown-in-peach-tbl106"
        },
        {
          "source": "embroidered-art-silk-gown-in-peach-tbl106",
          "strength": 0.00046061722708429296,
          "target": "hand-block-printed-art-silk-indowestern-lehenga-in-white-lkc97"
        },
        {
          "source": "hand-block-printed-art-silk-indowestern-lehenga-in-white-lkc97",
          "strength": 0.00046061722708429296,
          "target": "metallic-bangles-in-fuchsia-jvk475"
        },
        {
          "source": "metallic-bangles-in-fuchsia-jvk475",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-punjabi-suit-in-light-beige-and-blue-kyz208"
        },
        {
          "source": "woven-chanderi-silk-punjabi-suit-in-light-beige-and-blue-kyz208",
          "strength": 0.00046061722708429296,
          "target": "contrast-border-chiffon-saree-in-purple-sur11"
        },
        {
          "source": "contrast-border-chiffon-saree-in-purple-sur11",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-sky-blue-scxa538"
        },
        {
          "source": "woven-art-silk-saree-in-sky-blue-scxa538",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-viscose-georgette-straight-suit-in-peach-krgv27"
        },
        {
          "source": "hand-embroidered-viscose-georgette-straight-suit-in-peach-krgv27",
          "strength": 0.00046061722708429296,
          "target": "tie-n-dye-georgette-saree-in-pink-sjn6382"
        },
        {
          "source": "tie-n-dye-georgette-saree-in-pink-sjn6382",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-turquoise-and-navy-blue-sjra126"
        },
        {
          "source": "half-n-half-lycra-net-saree-in-beige-and-coral-red-svla324",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-blue-kqu957"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-blue-kqu957",
          "strength": 0.00046061722708429296,
          "target": "embroidered-placket-cotton-pathani-suit-in-black-meu14"
        },
        {
          "source": "hand-embroidered-viscose-georgette-flared-gown-in-sea-green-tsp178",
          "strength": 0.00046061722708429296,
          "target": "beaded-necklace-set-jvm2456"
        },
        {
          "source": "beaded-necklace-set-jvm2456",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-red-kxc762"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-red-kxc762",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-red-shk1521"
        },
        {
          "source": "printed-cotton-saree-in-red-shk1521",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-and-chanderi-silk-jacquard-circular-lehenga-in-pink-ljn1267"
        },
        {
          "source": "plain-dupion-silk-and-chanderi-silk-jacquard-circular-lehenga-in-pink-ljn1267",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-pant-in-yellow-thu964"
        },
        {
          "source": "plain-cotton-silk-pant-in-yellow-thu964",
          "strength": 0.00046061722708429296,
          "target": "ombre-chiffon-saree-in-coral-red-sqfa341"
        },
        {
          "source": "ombre-chiffon-saree-in-coral-red-sqfa341",
          "strength": 0.00046061722708429296,
          "target": "block-printed-pure-cotton-saree-in-beige-suv113"
        },
        {
          "source": "block-printed-pure-cotton-saree-in-beige-suv113",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-anarkali-suit-in-black-kxh82"
        },
        {
          "source": "block-printed-cotton-anarkali-suit-in-black-kxh82",
          "strength": 0.00046061722708429296,
          "target": "pure-paithani-silk-saree-in-magenta-stbn102"
        },
        {
          "source": "pure-paithani-silk-saree-in-magenta-stbn102",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-dark-green-stla270"
        },
        {
          "source": "pure-paithani-silk-saree-in-magenta-stbn102",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-shaded-orange-sew5495"
        },
        {
          "source": "handloom-tant-cotton-saree-in-dark-green-stla270",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-silk-saree-in-pink-snea634"
        },
        {
          "source": "woven-pure-silk-saree-in-pink-snea634",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-handloom-silk-saree-in-olive-green-and-beige-sgta75"
        },
        {
          "source": "printed-rayon-kurta-in-red-tqz166",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-flared-long-kurta-in-navy-blue-and-off-white-tkx125"
        },
        {
          "source": "printed-crepe-flared-long-kurta-in-navy-blue-and-off-white-tkx125",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-top-n-skirt-in-navy-blue-ttz92"
        },
        {
          "source": "woven-chanderi-silk-top-n-skirt-in-navy-blue-ttz92",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-blue-sgya505"
        },
        {
          "source": "printed-georgette-saree-in-blue-sgya505",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-handloom-silk-saree-in-olive-green-and-beige-sgta75"
        },
        {
          "source": "printed-georgette-saree-in-blue-sgya505",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-green-sau1787"
        },
        {
          "source": "woven-art-silk-saree-in-green-sau1787",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-saree-in-fuchsia-sbea514"
        },
        {
          "source": "woven-cotton-silk-jacquard-saree-in-fuchsia-sbea514",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-sjra345"
        },
        {
          "source": "embroidered-georgette-saree-in-red-sjra345",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-light-blue-mrg417"
        },
        {
          "source": "embroidered-art-silk-kurta-set-in-light-blue-mrg417",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-circular-lehenga-in-royal-blue-lcc139"
        },
        {
          "source": "embroidered-velvet-circular-lehenga-in-royal-blue-lcc139",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-purple-sqpa251"
        },
        {
          "source": "woven-bangalore-silk-saree-in-purple-sqpa251",
          "strength": 0.00046061722708429296,
          "target": "ombre-chiffon-saree-in-peach-and-orange-szy675"
        },
        {
          "source": "woven-bangalore-silk-saree-in-purple-sqpa251",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-earrings-jjr15202"
        },
        {
          "source": "woven-art-silk-saree-in-coral-red-sau2308",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-layered-matha-patti-jpm2348"
        },
        {
          "source": "stone-studded-layered-matha-patti-jpm2348",
          "strength": 0.00046061722708429296,
          "target": "kundan-choker-necklace-set-jjr16421"
        },
        {
          "source": "kundan-choker-necklace-set-jjr16421",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-silk-tant-saree-in-old-rose-shxa396"
        },
        {
          "source": "handloom-cotton-silk-tant-saree-in-old-rose-shxa396",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-set-with-nehru-jacket-in-grey-mtr71"
        },
        {
          "source": "plain-dupion-silk-kurta-set-with-nehru-jacket-in-grey-mtr71",
          "strength": 0.00046061722708429296,
          "target": "pintucked-cotton-flex-kurta-in-beige-tja681"
        },
        {
          "source": "pintucked-cotton-flex-kurta-in-beige-tja681",
          "strength": 0.00046061722708429296,
          "target": "embroidered-abaya-style-art-silk-suit-in-blue-kch81"
        },
        {
          "source": "embroidered-abaya-style-art-silk-suit-in-blue-kch81",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-saree-in-yellow-sas1041"
        },
        {
          "source": "embroidered-jacquard-saree-in-yellow-sas1041",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bordered-georgette-saree-in-fuchsia-and-orange-sew3817"
        },
        {
          "source": "embroidered-jacquard-saree-in-yellow-sas1041",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-teal-green-sbra313"
        },
        {
          "source": "embroidered-bordered-georgette-saree-in-fuchsia-and-orange-sew3817",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-blue-sgya505"
        },
        {
          "source": "embroidered-dupion-kurta-set-in-maroon-mve281",
          "strength": 0.00046061722708429296,
          "target": "prestitched-maharashtrian-nauvari-saree-in-purple-ssf3007"
        },
        {
          "source": "embroidered-dupion-kurta-set-in-maroon-mve281",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-light-beige-stu761"
        },
        {
          "source": "prestitched-maharashtrian-nauvari-saree-in-purple-ssf3007",
          "strength": 0.00046061722708429296,
          "target": "prestitched-maharashtrian-nauvari-saree-in-mustard-ssf3004"
        },
        {
          "source": "prestitched-maharashtrian-nauvari-saree-in-mustard-ssf3004",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-yellow-sgja640"
        },
        {
          "source": "embroidered-georgette-saree-in-yellow-sgja640",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-chiffon-saree-in-pink-szta121"
        },
        {
          "source": "handloom-pure-chiffon-saree-in-pink-szta121",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bhagalpuri-silk-saree-in-teal-blue-skra933"
        },
        {
          "source": "embroidered-bhagalpuri-silk-saree-in-teal-blue-skra933",
          "strength": 0.00046061722708429296,
          "target": "kerala-kasavu-hand-painted-cotton-saree-in-white-sfya30"
        },
        {
          "source": "kerala-kasavu-hand-painted-cotton-saree-in-white-sfya30",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-maroon-sqpa67"
        },
        {
          "source": "woven-bangalore-silk-saree-in-maroon-sqpa67",
          "strength": 0.00046061722708429296,
          "target": "plain-georgette-a-line-suit-in-purple-kjn2539"
        },
        {
          "source": "plain-georgette-a-line-suit-in-purple-kjn2539",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-pink-ssf3611"
        },
        {
          "source": "plain-georgette-a-line-suit-in-purple-kjn2539",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-saree-in-yellow-shxa243"
        },
        {
          "source": "printed-georgette-saree-in-pink-ssf3611",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-pink-seh1808"
        },
        {
          "source": "kanchipuram-saree-in-pink-seh1808",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bordered-georgette-saree-in-shaded-red-and-pink-sfs680"
        },
        {
          "source": "kanchipuram-saree-in-pink-seh1808",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-maroon-kch300"
        },
        {
          "source": "embroidered-bordered-georgette-saree-in-shaded-red-and-pink-sfs680",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-black-sur162"
        },
        {
          "source": "embroidered-chiffon-saree-in-black-sur162",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-beige-and-dark-green-sgpn305"
        },
        {
          "source": "woven-cotton-silk-saree-in-beige-and-dark-green-sgpn305",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-purple-and-beige-suf7414"
        },
        {
          "source": "half-n-half-art-silk-saree-in-purple-and-beige-suf7414",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-mysore-silk-saree-in-old-rose-and-royal-blue-shu451"
        },
        {
          "source": "half-n-half-art-silk-saree-in-purple-and-beige-suf7414",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-rayon-kurta-in-turquoise-tyq58"
        },
        {
          "source": "woven-pure-mysore-silk-saree-in-old-rose-and-royal-blue-shu451",
          "strength": 0.00046061722708429296,
          "target": "foil-printed-chiffon-saree-in-blue-suv90"
        },
        {
          "source": "foil-printed-chiffon-saree-in-blue-suv90",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-red-suf7713"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-red-suf7713",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-navy-blue-skpa922"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-red-suf7713",
          "strength": 0.00046061722708429296,
          "target": "contrast-border-georgette-saree-in-navy-blue-sjra250"
        },
        {
          "source": "embroidered-georgette-saree-in-navy-blue-skpa922",
          "strength": 0.00046061722708429296,
          "target": "tant-cotton-saree-in-orange-shxa288"
        },
        {
          "source": "tant-cotton-saree-in-orange-shxa288",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-and-net-saree-in-black-and-pink-sqh1394"
        },
        {
          "source": "half-n-half-georgette-and-net-saree-in-black-and-pink-sqh1394",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-teal-green-spfa1897"
        },
        {
          "source": "embroidered-georgette-saree-in-teal-green-spfa1897",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-black-mhg696"
        },
        {
          "source": "embroidered-georgette-saree-in-teal-green-spfa1897",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-off-white-skra1323"
        },
        {
          "source": "printed-georgette-saree-in-red-sud1239",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-maroon-kch300"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-maroon-kch300",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-red-suf7713"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-maroon-kch300",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-dupion-silk-lehenga-in-orange-and-red-lvx95"
        },
        {
          "source": "embroidered-art-dupion-silk-lehenga-in-orange-and-red-lvx95",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-dark-green-sqpa207"
        },
        {
          "source": "woven-bangalore-silk-saree-in-dark-green-sqpa207",
          "strength": 0.00046061722708429296,
          "target": "plain-shimmer-georgette-circular-lehenga-in-off-white-lkc36"
        },
        {
          "source": "plain-shimmer-georgette-circular-lehenga-in-off-white-lkc36",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-peach-shka320"
        },
        {
          "source": "kanchipuram-silk-saree-in-off-white-skra1323",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-yellow-sfwa257"
        },
        {
          "source": "woven-chanderi-silk-saree-in-yellow-sfwa257",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-wine-snba726"
        },
        {
          "source": "printed-georgette-saree-in-wine-snba726",
          "strength": 0.00046061722708429296,
          "target": "ombre-chiffon-saree-in-blue-szy670"
        },
        {
          "source": "ombre-chiffon-saree-in-blue-szy670",
          "strength": 0.00046061722708429296,
          "target": "ghatchola-crepe-saree-in-indigo-suf7334"
        },
        {
          "source": "ghatchola-crepe-saree-in-indigo-suf7334",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-hasli-choker-set-jvk1765"
        },
        {
          "source": "stone-studded-hasli-choker-set-jvk1765",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-straight-kurta-in-brown-tja594"
        },
        {
          "source": "block-printed-cotton-straight-kurta-in-brown-tja594",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-off-white-mse658"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-off-white-mse658",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jjr16515"
        },
        {
          "source": "stone-studded-necklace-set-jjr16515",
          "strength": 0.00046061722708429296,
          "target": "american-diamonds-necklace-set-jjr16474"
        },
        {
          "source": "american-diamonds-necklace-set-jjr16474",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jmy116"
        },
        {
          "source": "stone-studded-necklace-set-jmy116",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-light-teal-green-sjra101"
        },
        {
          "source": "embroidered-georgette-saree-in-light-teal-green-sjra101",
          "strength": 0.00046061722708429296,
          "target": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2701"
        },
        {
          "source": "embroidered-georgette-saree-in-light-teal-green-sjra101",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-kurta-in-navy-blue-tmz106"
        },
        {
          "source": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2701",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-satin-saree-in-green-and-teal-green-spfa2146"
        },
        {
          "source": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2701",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-teal-green-and-navy-blue-spta252"
        },
        {
          "source": "half-n-half-satin-saree-in-green-and-teal-green-spfa2146",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-handloom-saree-in-orange-dual-tone-shp791"
        },
        {
          "source": "pure-kanchipuram-silk-handloom-saree-in-orange-dual-tone-shp791",
          "strength": 0.00046061722708429296,
          "target": "woven-kanchipuram-saree-in-orange-sbra18"
        },
        {
          "source": "woven-kanchipuram-saree-in-orange-sbra18",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-cotton-satin-pakistani-suit-in-light-green-kaz177"
        },
        {
          "source": "digital-printed-cotton-satin-pakistani-suit-in-light-green-kaz177",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-brasso-straight-suit-in-fuchsia-kry925"
        },
        {
          "source": "embroidered-georgette-brasso-straight-suit-in-fuchsia-kry925",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-seh1311"
        },
        {
          "source": "embroidered-georgette-saree-in-red-seh1311",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-cut-suit-in-coral-kfx1957"
        },
        {
          "source": "printed-cotton-straight-cut-suit-in-coral-kfx1957",
          "strength": 0.00046061722708429296,
          "target": "embroidered-lycra-saree-in-orange-sbja51"
        },
        {
          "source": "embroidered-lycra-saree-in-orange-sbja51",
          "strength": 0.00046061722708429296,
          "target": "pure-silk-paithani-saree-in-light-purple-stbn98"
        },
        {
          "source": "pure-silk-paithani-saree-in-light-purple-stbn98",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-cream-seh1906"
        },
        {
          "source": "kanchipuram-saree-in-cream-seh1906",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-cut-suit-in-red-kux122"
        },
        {
          "source": "kanchipuram-saree-in-cream-seh1906",
          "strength": 0.00046061722708429296,
          "target": "pure-mysore-silk-saree-in-green-shu638"
        },
        {
          "source": "embroidered-georgette-straight-cut-suit-in-red-kux122",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-megenta-kfx1967"
        },
        {
          "source": "printed-cotton-straight-suit-in-megenta-kfx1967",
          "strength": 0.00046061722708429296,
          "target": "woven-kerala-kasavu-cotton-saree-in-off-white-spn2701"
        },
        {
          "source": "half-n-half-art-silk-saree-in-teal-green-and-navy-blue-spta252",
          "strength": 0.00046061722708429296,
          "target": "art-silk-blouse-in-red-dbu443"
        },
        {
          "source": "half-n-half-art-silk-saree-in-teal-green-and-navy-blue-spta252",
          "strength": 0.00046061722708429296,
          "target": "plain-linen-cotton-kurta-set-in-white-mhg392"
        },
        {
          "source": "art-silk-blouse-in-red-dbu443",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-kurta-in-navy-blue-and-rust-tbk160"
        },
        {
          "source": "art-silk-blouse-in-red-dbu443",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-black-skba238"
        },
        {
          "source": "printed-cotton-kurta-in-navy-blue-and-rust-tbk160",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-anarkali-suit-in-pink-kvng62"
        },
        {
          "source": "embroidered-cotton-anarkali-suit-in-pink-kvng62",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-beige-smda704"
        },
        {
          "source": "printed-georgette-saree-in-beige-smda704",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-gown-in-red-tsp7"
        },
        {
          "source": "printed-georgette-saree-in-beige-smda704",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-viscose-saree-in-brown-sas1084"
        },
        {
          "source": "hand-embroidered-georgette-gown-in-red-tsp7",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-dark-olive-sfka337"
        },
        {
          "source": "woven-cotton-silk-saree-in-dark-olive-sfka337",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-temple-necklace-set-jnj1982"
        },
        {
          "source": "stone-studded-temple-necklace-set-jnj1982",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jmy78"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-black-svqa78",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-pink-and-mustard-stma148"
        },
        {
          "source": "hand-embroidered-raw-silk-kurta-set-in-light-yellow-mxh127",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-bhagalpurisilk-crop-top-with-skirt-in-teal-blue-trb619"
        },
        {
          "source": "hand-embroidered-bhagalpurisilk-crop-top-with-skirt-in-teal-blue-trb619",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-pathani-suit-in-black-mxh102"
        },
        {
          "source": "plain-cotton-pathani-suit-in-black-mxh102",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-cream-seh1906"
        },
        {
          "source": "pure-mysore-silk-saree-in-green-shu638",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-short-kurta-in-black-mee280"
        },
        {
          "source": "plain-cotton-short-kurta-in-black-mee280",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-saree-in-red-and-black-sgpn391"
        },
        {
          "source": "woven-cotton-saree-in-red-and-black-sgpn391",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-set-in-beige-mpc741"
        },
        {
          "source": "embroidered-dupion-silk-kurta-set-in-beige-mpc741",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-shaded-fuchsia-and-peach-stl742"
        },
        {
          "source": "embroidered-chiffon-saree-in-shaded-fuchsia-and-peach-stl742",
          "strength": 0.00046061722708429296,
          "target": "pacchikari-necklace-set-jjr16060"
        },
        {
          "source": "pacchikari-necklace-set-jjr16060",
          "strength": 0.00046061722708429296,
          "target": "printed-pure-georgette-saree-in-pink-sjn7062"
        },
        {
          "source": "printed-pure-georgette-saree-in-pink-sjn7062",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-orange-ssf4149"
        },
        {
          "source": "printed-pure-georgette-saree-in-pink-sjn7062",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pendent-set-jjr14239"
        },
        {
          "source": "printed-georgette-saree-in-orange-ssf4149",
          "strength": 0.00046061722708429296,
          "target": "handloom-banarasi-silk-saree-in-orange-snea1158"
        },
        {
          "source": "plain-cotton-lycra-combo-leggings-in-blue-yellow-and-green-tsf366",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-adjustable-haathphool-jjr16496"
        },
        {
          "source": "stone-studded-adjustable-haathphool-jjr16496",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-satin-pakistani-suit-in-cream-kpv137"
        },
        {
          "source": "embroidered-cotton-satin-pakistani-suit-in-cream-kpv137",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-sherwani-in-beige-mhg728"
        },
        {
          "source": "floral-printed-south-cotton-straight-suit-in-pink-kfx2647",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-saree-in-peach-svqa181"
        },
        {
          "source": "woven-chanderi-silk-saree-in-peach-svqa181",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-sky-blue-sbta300"
        },
        {
          "source": "printed-chiffon-saree-in-yellow-stu626",
          "strength": 0.00046061722708429296,
          "target": "ombre-satin-chiffon-saree-in-orange-and-maroon-sew5414"
        },
        {
          "source": "ombre-satin-chiffon-saree-in-orange-and-maroon-sew5414",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-beige-kch890"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-beige-kch890",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-blue-and-light-teal-green-sgja57"
        },
        {
          "source": "half-n-half-georgette-saree-in-blue-and-light-teal-green-sgja57",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-blue-sgka2808"
        },
        {
          "source": "woven-cotton-silk-saree-in-blue-sgka2808",
          "strength": 0.00046061722708429296,
          "target": "handloom-banarasi-silk-saree-in-green-snea1151"
        },
        {
          "source": "handloom-banarasi-silk-saree-in-green-snea1151",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-and-georgette-saree-in-pink-and-green-ssx5405"
        },
        {
          "source": "half-n-half-lycra-and-georgette-saree-in-pink-and-green-ssx5405",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-dusty-green-lcc150"
        },
        {
          "source": "embroidered-net-lehenga-in-dusty-green-lcc150",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-art-silk-saree-in-green-sqfa153"
        },
        {
          "source": "embroidered-border-art-silk-saree-in-green-sqfa153",
          "strength": 0.00046061722708429296,
          "target": "bandhani-crepe-saree-in-onion-pink-sjn5755"
        },
        {
          "source": "embroidered-net-indowestern-lehenga-in-beige-ljn1417",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chikankari-poly-cotton-kurta-in-white-mpe126"
        },
        {
          "source": "embroidered-chikankari-poly-cotton-kurta-in-white-mpe126",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-and-net-abaya-suit-in-navy-blue-kym387"
        },
        {
          "source": "embroidered-art-silk-and-net-abaya-suit-in-navy-blue-kym387",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-jacquard-kurta-set-in-navy-blue-mve305"
        },
        {
          "source": "embroidered-neckline-art-silk-jacquard-kurta-set-in-navy-blue-mve305",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-waist-chain-in-white-jjr13196"
        },
        {
          "source": "embroidered-neckline-art-silk-jacquard-kurta-set-in-navy-blue-mve305",
          "strength": 0.00046061722708429296,
          "target": "ombre-georgette-saree-in-green-sfva51"
        },
        {
          "source": "stone-studded-waist-chain-in-white-jjr13196",
          "strength": 0.00046061722708429296,
          "target": "plain-poly-cotton-straight-kurta-in-fuchsia-tbk60"
        },
        {
          "source": "plain-poly-cotton-straight-kurta-in-fuchsia-tbk60",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-cotton-silk-saree-in-red-and-black-srga388"
        },
        {
          "source": "half-n-half-cotton-silk-saree-in-red-and-black-srga388",
          "strength": 0.00046061722708429296,
          "target": "bhagalpuri-silk-saree-in-light-green-sts3673"
        },
        {
          "source": "bhagalpuri-silk-saree-in-light-green-sts3673",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-jacquard-saree-in-violet-and-cream-sjra174"
        },
        {
          "source": "printed-straight-cut-suit-in-red-and-white-ket30",
          "strength": 0.00046061722708429296,
          "target": "printed-art-silk-nehru-jacket-in-cream-mhg711"
        },
        {
          "source": "printed-art-silk-nehru-jacket-in-cream-mhg711",
          "strength": 0.00046061722708429296,
          "target": "jacquard-art-silk-kurta-set-in-maroon-and-beige-mcd2954"
        },
        {
          "source": "jacquard-art-silk-kurta-set-in-maroon-and-beige-mcd2954",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-handloom-saree-in-beige-shp812"
        },
        {
          "source": "pure-kanchipuram-silk-handloom-saree-in-beige-shp812",
          "strength": 0.00046061722708429296,
          "target": "printed-rayon-straight-cut-suit-in-green-and-blue-ktp1616"
        },
        {
          "source": "printed-rayon-straight-cut-suit-in-green-and-blue-ktp1616",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-jacquard-saree-in-beige-snea803"
        },
        {
          "source": "embroidered-net-circular-lehenga-in-blue-lqu413",
          "strength": 0.00046061722708429296,
          "target": "handloom-silk-saree-in-maroon-sts3420"
        },
        {
          "source": "printed-georgette-brasso-pakistani-suit-in-teal-green-kcv1200",
          "strength": 0.00046061722708429296,
          "target": "jute-silk-jodhpuri-jacket-in-black-mcd2959"
        },
        {
          "source": "jute-silk-jodhpuri-jacket-in-black-mcd2959",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-handloom-saree-in-light-beige-and-orange-srga675"
        },
        {
          "source": "woven-cotton-silk-handloom-saree-in-light-beige-and-orange-srga675",
          "strength": 0.00046061722708429296,
          "target": "zari-lace-work-art-silk-short-kurta-in-peach-mnl134"
        },
        {
          "source": "zari-lace-work-art-silk-short-kurta-in-peach-mnl134",
          "strength": 0.00046061722708429296,
          "target": "plain-art-dupion-silk-dhoti-kurta-in-off-white-mpc768"
        },
        {
          "source": "pure-georgette-banarasi-saree-in-royal-blue-snea273",
          "strength": 0.00046061722708429296,
          "target": "chanderi-anarkali-suit-in-orange-with-gota-patti-jacket-kjn2798"
        },
        {
          "source": "chanderi-anarkali-suit-in-orange-with-gota-patti-jacket-kjn2798",
          "strength": 0.00046061722708429296,
          "target": "printed-bhagalpuri-silk-saree-in-black-and-off-white-sbh1596"
        },
        {
          "source": "printed-bhagalpuri-silk-saree-in-black-and-off-white-sbh1596",
          "strength": 0.00046061722708429296,
          "target": "printed-pure-georgette-saree-in-pink-sjn7062"
        },
        {
          "source": "stone-studded-pendent-set-jjr14239",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-blouse-in-maroon-ufa51"
        },
        {
          "source": "embroidered-taffeta-silk-blouse-in-maroon-ufa51",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-set-in-navy-blue-mpw243"
        },
        {
          "source": "embroidered-dupion-silk-kurta-set-in-maroon-mpc694",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-in-red-and-white-jrl1952"
        },
        {
          "source": "stone-studded-necklace-set-in-red-and-white-jrl1952",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-top-in-pink-tja569"
        },
        {
          "source": "printed-cotton-top-in-pink-tja569",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-brasso-saree-in-red-sfka1022"
        },
        {
          "source": "woven-chanderi-silk-brasso-saree-in-red-sfka1022",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-straight-suit-in-beige-kfx2248"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-beige-kfx2248",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-mustard-sfka1443"
        },
        {
          "source": "woven-art-silk-saree-in-mustard-sfka1443",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-dhoti-in-red-mpc657"
        },
        {
          "source": "embroidered-chanderi-silk-straight-suit-in-beige-kgzt236",
          "strength": 0.00046061722708429296,
          "target": "kundan-armlet-jda1543"
        },
        {
          "source": "kundan-armlet-jda1543",
          "strength": 0.00046061722708429296,
          "target": "dupion-silk-top-in-fuchsia-thu770"
        },
        {
          "source": "dupion-silk-top-in-fuchsia-thu770",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-straight-suit-in-light-pink-kcu152"
        },
        {
          "source": "woven-cotton-silk-jacquard-straight-suit-in-light-pink-kcu152",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-navy-blue-sgja450"
        },
        {
          "source": "embroidered-art-silk-saree-in-navy-blue-sgja450",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-skirt-in-black-bts170"
        },
        {
          "source": "block-printed-cotton-skirt-in-black-bts170",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-armlet-jvk2695"
        },
        {
          "source": "stone-studded-armlet-jvk2695",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-dark-blue-ssja192"
        },
        {
          "source": "embroidered-chanderi-silk-asymmetric-kurta-in-navy-blue-tgp72",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-kurta-set-in-maroon-mve281"
        },
        {
          "source": "embroidered-georgette-saree-in-light-beige-stu761",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-maroon-spfa1904"
        },
        {
          "source": "embroidered-georgette-saree-in-maroon-spfa1904",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-black-sma5614"
        },
        {
          "source": "woven-art-silk-saree-in-black-sma5614",
          "strength": 0.00046061722708429296,
          "target": "pure-banarasi-silk-handloom-saree-in-maroon-snea1235"
        },
        {
          "source": "pure-banarasi-silk-handloom-saree-in-maroon-snea1235",
          "strength": 0.00046061722708429296,
          "target": "kalamkari-cotton-saree-in-light-pink-sswa728"
        },
        {
          "source": "kalamkari-cotton-saree-in-light-pink-sswa728",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-indigo-sfva75"
        },
        {
          "source": "kalamkari-cotton-saree-in-light-pink-sswa728",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-chiffon-and-net-saree-in-pink-and-yellow-seh1514"
        },
        {
          "source": "embroidered-chiffon-saree-in-indigo-sfva75",
          "strength": 0.00046061722708429296,
          "target": "gota-patti-embroidered-straight-cut-suit-in-shaded-mustard-kjn1311"
        },
        {
          "source": "gota-patti-embroidered-straight-cut-suit-in-shaded-mustard-kjn1311",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-black-kut209"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-black-kut209",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-art-silk-lehenga-in-peach-lyv11"
        },
        {
          "source": "hand-embroidered-art-silk-lehenga-in-peach-lyv11",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-abaya-style-suit-in-black-kqu886"
        },
        {
          "source": "printed-crepe-abaya-style-suit-in-black-kqu886",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-rust-seh1577"
        },
        {
          "source": "woven-art-silk-saree-in-rust-seh1577",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-off-white-sul434"
        },
        {
          "source": "banarasi-silk-saree-in-off-white-sul434",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-kurta-set-in-beige-mpd329"
        },
        {
          "source": "embroidered-neckline-art-silk-kurta-set-in-beige-mpd329",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-abaya-style-suit-in-grey-kjn2677"
        },
        {
          "source": "embroidered-neckline-art-silk-kurta-set-in-beige-mpd329",
          "strength": 0.00046061722708429296,
          "target": "woven-muga-silk-saree-in-blue-srp598"
        },
        {
          "source": "printed-cotton-abaya-style-suit-in-grey-kjn2677",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-beige-kfx2018"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-beige-kfx2018",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-red-lqm187"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-red-lqm187",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-saree-in-mustard-sew2534"
        },
        {
          "source": "embroidered-net-saree-in-mustard-sew2534",
          "strength": 0.00046061722708429296,
          "target": "woven-khadi-cotton-saree-in-black-sgpn467"
        },
        {
          "source": "half-n-half-art-silk-saree-in-wine-and-orange-spfa1788",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-navy-blue-mgv124"
        },
        {
          "source": "half-n-half-art-silk-saree-in-wine-and-orange-spfa1788",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-cotton-straight-suit-in-beige-kfx1818"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-navy-blue-mgv124",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-silk-saree-in-light-green-skra1105"
        },
        {
          "source": "kanchipuram-silk-saree-in-light-green-skra1105",
          "strength": 0.00046061722708429296,
          "target": "embroidered-front-slit-straight-cut-art-silk-suit-in-navy-blue-kxz76"
        },
        {
          "source": "embroidered-front-slit-straight-cut-art-silk-suit-in-navy-blue-kxz76",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-satin-georgette-saree-in-red-and-peach-sgja401"
        },
        {
          "source": "floral-printed-satin-georgette-saree-in-red-and-peach-sgja401",
          "strength": 0.00046061722708429296,
          "target": "bandhej-printed-art-silk-a-line-lehenga-in-shaded-royal-blue-luf1285"
        },
        {
          "source": "bandhej-printed-art-silk-a-line-lehenga-in-shaded-royal-blue-luf1285",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-circular-lehenga-in-red-lqm187"
        },
        {
          "source": "abstract-printed-georgette-saree-in-light-beige-sgja409",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-yellow-smda727"
        },
        {
          "source": "printed-georgette-saree-in-yellow-smda727",
          "strength": 0.00046061722708429296,
          "target": "pure-paithani-silk-saree-in-magenta-stbn102"
        },
        {
          "source": "printed-georgette-saree-in-shaded-orange-sew5495",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-anarkali-long-kurta-in-off-white-tnc562"
        },
        {
          "source": "embroidered-straight-cut-georgette-suit-in-off-white-and-black-kuz235",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-tussar-silk-saree-in-coral-and-peach-seh1557"
        },
        {
          "source": "half-n-half-tussar-silk-saree-in-coral-and-peach-seh1557",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-brown-ukx101"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-brown-ukx101",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-long-kurta-in-pink-tdr619"
        },
        {
          "source": "plain-rayon-long-kurta-in-pink-tdr619",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-tissue-saree-in-maroon-sjn6614"
        },
        {
          "source": "embroidered-pure-tissue-saree-in-maroon-sjn6614",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-choker-necklace-set-jjr15671"
        },
        {
          "source": "embroidered-pure-tissue-saree-in-maroon-sjn6614",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-maroon-and-olive-green-scfa326"
        },
        {
          "source": "stone-studded-choker-necklace-set-jjr15671",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-white-kve154"
        },
        {
          "source": "printed-cotton-straight-suit-in-white-kve154",
          "strength": 0.00046061722708429296,
          "target": "kerala-kasavu-woven-cotton-saree-in-off-white-spn2941"
        },
        {
          "source": "kerala-kasavu-woven-cotton-saree-in-off-white-spn2941",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-short-kurta-in-brown-mee312"
        },
        {
          "source": "kerala-kasavu-woven-cotton-saree-in-off-white-spn2941",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-violet-seh1782"
        },
        {
          "source": "plain-cotton-short-kurta-in-brown-mee312",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-net-saree-in-beige-and-orange-svla325"
        },
        {
          "source": "half-n-half-lycra-net-saree-in-beige-and-orange-svla325",
          "strength": 0.00046061722708429296,
          "target": "pearl-layered-kanthimala-mtj31"
        },
        {
          "source": "pearl-layered-kanthimala-mtj31",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bordered-georgette-saree-in-teal-green-sew3819"
        },
        {
          "source": "embroidered-bordered-georgette-saree-in-teal-green-sew3819",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-orange-uyp58"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-orange-uyp58",
          "strength": 0.00046061722708429296,
          "target": "meenakri-necklace-set-jjr14481"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-orange-uyp58",
          "strength": 0.00046061722708429296,
          "target": "kundan-bridal-necklace-set-jjr14449"
        },
        {
          "source": "meenakri-necklace-set-jjr14481",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-cotton-straight-suit-in-orange-scda93"
        },
        {
          "source": "hand-embroidered-cotton-straight-suit-in-orange-scda93",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-green-and-red-spfa2023"
        },
        {
          "source": "half-n-half-art-silk-saree-in-green-and-red-spfa2023",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-beige-and-pink-ssf3466"
        },
        {
          "source": "half-n-half-art-silk-saree-in-green-and-red-spfa2023",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-tussar-silk-saree-in-purple-snea486"
        },
        {
          "source": "printed-chiffon-saree-in-beige-and-pink-ssf3466",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140"
        },
        {
          "source": "printed-chiffon-saree-in-beige-and-pink-ssf3466",
          "strength": 0.00046061722708429296,
          "target": "kundan-earrings-jjr15232"
        },
        {
          "source": "kundan-earrings-jjr15232",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-beige-sga6849"
        },
        {
          "source": "kundan-earrings-jjr15232",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-circular-lehenga-in-beige-and-red-lxw187"
        },
        {
          "source": "printed-georgette-saree-in-beige-sga6849",
          "strength": 0.00046061722708429296,
          "target": "printed-dupion-silk-kurta-set-in-navy-blue-mfy246"
        },
        {
          "source": "printed-dupion-silk-kurta-set-in-navy-blue-mfy246",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bordered-satin-georgette-saree-in-pink-syc6342"
        },
        {
          "source": "embroidered-bordered-satin-georgette-saree-in-pink-syc6342",
          "strength": 0.00046061722708429296,
          "target": "lehariya-printed-georgette-saree-in-mustard-sjn6087"
        },
        {
          "source": "lehariya-printed-georgette-saree-in-mustard-sjn6087",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-matka-silk-saree-in-green-spn3377"
        },
        {
          "source": "handloom-pure-matka-silk-saree-in-green-spn3377",
          "strength": 0.00046061722708429296,
          "target": "embossed-velvet-jodhpuri-suit-in-dark-blue-mhg476"
        },
        {
          "source": "embossed-velvet-jodhpuri-suit-in-dark-blue-mhg476",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-and-georgette-long-kurta-in-black-and-white-thu1445"
        },
        {
          "source": "printed-crepe-and-georgette-long-kurta-in-black-and-white-thu1445",
          "strength": 0.00046061722708429296,
          "target": "pintucks-kurta-set-in-white-mxh75"
        },
        {
          "source": "pintucks-kurta-set-in-white-mxh75",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-top-in-maroon-tcz294"
        },
        {
          "source": "printed-cotton-top-in-maroon-tcz294",
          "strength": 0.00046061722708429296,
          "target": "lehariya-printed-georgette-saree-in-orange-sjn5982"
        },
        {
          "source": "lehariya-printed-georgette-saree-in-orange-sjn5982",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-front-slit-straight-cut-suit-in-fuchsia-kjn2099"
        },
        {
          "source": "lehariya-printed-georgette-saree-in-orange-sjn5982",
          "strength": 0.00046061722708429296,
          "target": "printed-dupion-silk-dhoti-kurta-in-red-mty21"
        },
        {
          "source": "banarasi-silk-front-slit-straight-cut-suit-in-fuchsia-kjn2099",
          "strength": 0.00046061722708429296,
          "target": "pleated-georgette-palazzo-in-teal-green-byt89"
        },
        {
          "source": "pleated-georgette-palazzo-in-teal-green-byt89",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-a-line-suit-in-off-white-and-brown-kyk74"
        },
        {
          "source": "embroidered-chanderi-silk-a-line-suit-in-off-white-and-brown-kyk74",
          "strength": 0.00046061722708429296,
          "target": "bandhej-crepe-saree-in-black-sjn7073"
        },
        {
          "source": "brocade-flared-dress-in-beige-thu279",
          "strength": 0.00046061722708429296,
          "target": "cotton-settu-mundu-in-off-white-spn3001"
        },
        {
          "source": "cotton-settu-mundu-in-off-white-spn3001",
          "strength": 0.00046061722708429296,
          "target": "woven-georgette-brasso-saree-in-grey-sgpn577"
        },
        {
          "source": "woven-georgette-brasso-saree-in-grey-sgpn577",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-dress-in-red-tpx13"
        },
        {
          "source": "embroidered-georgette-dress-in-red-tpx13",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-mustard-sgka3075"
        },
        {
          "source": "kanchipuram-saree-in-mustard-sgka3075",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-peach-sfka593"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-peach-sfka593",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-kurta-set-in-grey-mve192"
        },
        {
          "source": "plain-khadi-kurta-set-in-grey-mve192",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-bangle-set-jdp169"
        },
        {
          "source": "plain-khadi-kurta-set-in-grey-mve192",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-red-utt603"
        },
        {
          "source": "stone-studded-bangle-set-jdp169",
          "strength": 0.00046061722708429296,
          "target": "plain-satin-crepe-saree-in-teal-blue-smda503"
        },
        {
          "source": "plain-satin-crepe-saree-in-teal-blue-smda503",
          "strength": 0.00046061722708429296,
          "target": "beaded-oxidised-earrings-jnj1927"
        },
        {
          "source": "plain-satin-crepe-saree-in-teal-blue-smda503",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-dupatta-in-olive-green-bmb28"
        },
        {
          "source": "woven-chanderi-cotton-dupatta-in-olive-green-bmb28",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-front-slit-straight-cut-suit-in-beige-kls693"
        },
        {
          "source": "woven-chanderi-cotton-dupatta-in-olive-green-bmb28",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-tant-saree-in-yellow-spn3289"
        },
        {
          "source": "embroidered-art-silk-front-slit-straight-cut-suit-in-beige-kls693",
          "strength": 0.00046061722708429296,
          "target": "pure-crepe-mysore-saree-in-off-white-and-maroon-shu697"
        },
        {
          "source": "pure-crepe-mysore-saree-in-off-white-and-maroon-shu697",
          "strength": 0.00046061722708429296,
          "target": "pure-banarasi-silk-saree-in-maroon-snea1007"
        },
        {
          "source": "embroidered-placket-cotton-pathani-suit-in-light-yellow-meu18",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-yellow-ssha826"
        },
        {
          "source": "printed-crepe-saree-in-brown-and-black-szma62",
          "strength": 0.00046061722708429296,
          "target": "beaded-maang-tikka-jjr14043"
        },
        {
          "source": "beaded-maang-tikka-jjr14043",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jmy80"
        },
        {
          "source": "stone-studded-necklace-set-jmy80",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-jacquard-top-n-skirt-in-beige-and-black-ttz98"
        },
        {
          "source": "woven-chanderi-silk-jacquard-top-n-skirt-in-beige-and-black-ttz98",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-navy-blue-kej950"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-navy-blue-kej950",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-tussar-silk-saree-in-pastel-orange-and-beige-snua167"
        },
        {
          "source": "digital-printed-tussar-silk-saree-in-pastel-orange-and-beige-snua167",
          "strength": 0.00046061722708429296,
          "target": "combo-front-slit-art-silk-straight-suit-in-light-purple-kvng82"
        },
        {
          "source": "combo-front-slit-art-silk-straight-suit-in-light-purple-kvng82",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-shaded-light-teal-blue-and-green-sqfa2"
        },
        {
          "source": "embroidered-chiffon-saree-in-shaded-light-teal-blue-and-green-sqfa2",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-layered-abaya-style-suit-in-peach-kch575"
        },
        {
          "source": "embroidered-net-layered-abaya-style-suit-in-peach-kch575",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-fuchsia-ukx121"
        },
        {
          "source": "woven-cotton-tant-saree-in-grey-and-green-srga841",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-tissue-saree-in-maroon-sjn6614"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-maroon-and-olive-green-scfa326",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-pure-silk-saree-in-black-and-white-stbn87"
        },
        {
          "source": "abstract-printed-cotton-straight-suit-in-fuchsia-kfx2613",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-saree-in-fuchsia-sas1043"
        },
        {
          "source": "embroidered-jacquard-saree-in-fuchsia-sas1043",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-kurta-set-in-beige-mpw235"
        },
        {
          "source": "plain-dupion-silk-kurta-set-in-beige-mpw235",
          "strength": 0.00046061722708429296,
          "target": "leheriya-georgette-abaya-style-suit-in-green-kjn2548"
        },
        {
          "source": "leheriya-georgette-abaya-style-suit-in-green-kjn2548",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-pakistani-suit-in-peach-kthb167"
        },
        {
          "source": "printed-crepe-pakistani-suit-in-peach-kthb167",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-satin-punjabi-suit-in-coral-kfx2151"
        },
        {
          "source": "printed-cotton-satin-punjabi-suit-in-coral-kfx2151",
          "strength": 0.00046061722708429296,
          "target": "woven-super-net-jacquard-saree-in-white-sbea518"
        },
        {
          "source": "woven-super-net-jacquard-saree-in-white-sbea518",
          "strength": 0.00046061722708429296,
          "target": "printed-rayon-flared-kurta-in-navy-blue-tqg11"
        },
        {
          "source": "woven-super-net-jacquard-saree-in-white-sbea518",
          "strength": 0.00046061722708429296,
          "target": "embroidered-poly-cotton-straight-suit-in-teal-blue-kmsg95"
        },
        {
          "source": "printed-rayon-flared-kurta-in-navy-blue-tqg11",
          "strength": 0.00046061722708429296,
          "target": "printed-dupion-silk-saree-in-fuchsia-sgya143"
        },
        {
          "source": "printed-dupion-silk-saree-in-fuchsia-sgya143",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-silk-saree-in-blue-sswa339"
        },
        {
          "source": "handloom-tant-cotton-silk-saree-in-blue-sswa339",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-orange-sbta100"
        },
        {
          "source": "woven-banarasi-silk-saree-in-orange-sbta100",
          "strength": 0.00046061722708429296,
          "target": "lehariya-printed-georgette-saree-in-orange-sjn5982"
        },
        {
          "source": "printed-dupion-silk-dhoti-kurta-in-red-mty21",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-light-teal-green-sjra101"
        },
        {
          "source": "printed-cotton-kurta-in-navy-blue-tmz106",
          "strength": 0.00046061722708429296,
          "target": "kantha-hand-embroidered-cotton-straight-suit-in-teal-green-kvng16"
        },
        {
          "source": "kantha-hand-embroidered-cotton-straight-suit-in-teal-green-kvng16",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-orange-sau2408"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-orange-sau2408",
          "strength": 0.00046061722708429296,
          "target": "handloom-khadi-cotton-saree-in-brown-sqvd64"
        },
        {
          "source": "handloom-khadi-cotton-saree-in-brown-sqvd64",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-jacquard-straight-suit-in-black-kmsg6"
        },
        {
          "source": "handloom-khadi-cotton-saree-in-brown-sqvd64",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-red-sbta498"
        },
        {
          "source": "embroidered-cotton-jacquard-straight-suit-in-black-kmsg6",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-teal-green-and-navy-blue-spta252"
        },
        {
          "source": "plain-linen-cotton-kurta-set-in-white-mhg392",
          "strength": 0.00046061722708429296,
          "target": "brass-based-necklace-set-jdw452"
        },
        {
          "source": "brass-based-necklace-set-jdw452",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-anarkali-suit-in-mustard-kuf8529"
        },
        {
          "source": "printed-cotton-anarkali-suit-in-mustard-kuf8529",
          "strength": 0.00046061722708429296,
          "target": "art-silk-blouse-in-red-dbu443"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-black-skba238",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-set-of-bangles-jxm24"
        },
        {
          "source": "pure-chanderi-silk-woven-saree-in-black-skba238",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-maroon-mpd362"
        },
        {
          "source": "stone-studded-set-of-bangles-jxm24",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-net-saree-in-beige-and-orange-stl691"
        },
        {
          "source": "half-n-half-lycra-net-saree-in-beige-and-orange-stl691",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-saree-in-turquoise-sfva20"
        },
        {
          "source": "half-n-half-lycra-net-saree-in-beige-and-orange-stl691",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-abaya-style-suit-in-light-yellow-kxzd41"
        },
        {
          "source": "plain-art-silk-saree-in-turquoise-sfva20",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-red-sud1127"
        },
        {
          "source": "plain-art-silk-saree-in-turquoise-sfva20",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-white-kfx2371"
        },
        {
          "source": "embroidered-art-silk-saree-in-red-sud1127",
          "strength": 0.00046061722708429296,
          "target": "patch-border-georgette-saree-in-cream-sjn5924"
        },
        {
          "source": "embroidered-chikankari-poly-cotton-kurta-set-in-white-mpe163",
          "strength": 0.00046061722708429296,
          "target": "printed-anarkali-suit-in-black-and-white-ktv82"
        },
        {
          "source": "printed-anarkali-suit-in-black-and-white-ktv82",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-lehenga-in-royal-blue-lxw106"
        },
        {
          "source": "embroidered-velvet-lehenga-in-royal-blue-lxw106",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-georgette-saree-in-beige-sgya382"
        },
        {
          "source": "floral-printed-georgette-saree-in-beige-sgya382",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-saree-in-green-svqa80"
        },
        {
          "source": "floral-printed-georgette-saree-in-beige-sgya382",
          "strength": 0.00046061722708429296,
          "target": "contrast-border-georgette-saree-in-navy-blue-sjra250"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-green-svqa80",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-teal-green-sew2129"
        },
        {
          "source": "embroidered-chiffon-saree-in-teal-green-sew2129",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-pure-silk-saree-in-turquoise-and-white-stbn37"
        },
        {
          "source": "handloom-pure-chanderi-silk-saree-in-black-skba181",
          "strength": 0.00046061722708429296,
          "target": "linen-nehru-jacket-in-grey-mhg224"
        },
        {
          "source": "handloom-pure-chanderi-silk-saree-in-black-skba181",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-kurti-in-beige-tmz241"
        },
        {
          "source": "linen-nehru-jacket-in-grey-mhg224",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-beige-uyp19"
        },
        {
          "source": "linen-nehru-jacket-in-grey-mhg224",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-chanderi-silk-saree-in-black-skba181"
        },
        {
          "source": "linen-nehru-jacket-in-grey-mhg224",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-violet-kbz228"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-dark-beige-lqm185",
          "strength": 0.00046061722708429296,
          "target": "printed-jute-hand-bag-in-green-and-multicolor-dlj38"
        },
        {
          "source": "embroidered-net-lehenga-in-red-lqu201",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-light-green-sfka1067"
        },
        {
          "source": "woven-art-silk-saree-in-light-green-sfka1067",
          "strength": 0.00046061722708429296,
          "target": "woven-velvet-and-brocade-sherwani-in-purple-and-multicolor-mpc805"
        },
        {
          "source": "woven-art-silk-saree-in-light-green-sfka1067",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-blue-sbra412"
        },
        {
          "source": "woven-velvet-and-brocade-sherwani-in-purple-and-multicolor-mpc805",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-navy-blue-ombre-sjra133"
        },
        {
          "source": "half-n-half-georgette-saree-in-navy-blue-ombre-sjra133",
          "strength": 0.00046061722708429296,
          "target": "contrast-trim-rayon-kurta-in-red-tdr1036"
        },
        {
          "source": "half-n-half-georgette-saree-in-navy-blue-ombre-sjra133",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-light-green-sfka1067"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-red-sar867",
          "strength": 0.00046061722708429296,
          "target": "butterfly-pallu-lycra-shimmer-georgette-saree-in-royal-blue-sjn6949"
        },
        {
          "source": "butterfly-pallu-lycra-shimmer-georgette-saree-in-royal-blue-sjn6949",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-royal-blue-kch168"
        },
        {
          "source": "handloom-art-tussar-silk-dupatta-in-light-grey-bbe61",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-saree-in-pastel-pink-sfva48"
        },
        {
          "source": "plain-art-silk-saree-in-pastel-pink-sfva48",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-blouse-in-maroon-dbu405"
        },
        {
          "source": "embroidered-velvet-blouse-in-maroon-dbu405",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-art-silk-circular-lehenga-in-shaded-blue-lfu473"
        },
        {
          "source": "digital-printed-art-silk-circular-lehenga-in-shaded-blue-lfu473",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-saree-in-light-blue-and-golden-suf7635"
        },
        {
          "source": "woven-cotton-silk-jacquard-saree-in-light-blue-and-golden-suf7635",
          "strength": 0.00046061722708429296,
          "target": "printed-bhagalpuri-silk-saree-in-grey-sga6390"
        },
        {
          "source": "printed-bhagalpuri-silk-saree-in-grey-sga6390",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-handloom-saree-in-fuchsia-sgka988"
        },
        {
          "source": "pure-kanchipuram-silk-handloom-saree-in-fuchsia-sgka988",
          "strength": 0.00046061722708429296,
          "target": "solid-crepe-zouave-pant-in-black-thu851"
        },
        {
          "source": "woven-pure-mysore-silk-saree-in-dark-green-shu791",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jjr15567"
        },
        {
          "source": "stone-studded-necklace-set-jjr15567",
          "strength": 0.00046061722708429296,
          "target": "printed-brocade-sharara-lehenga-in-navy-blue-lkc21"
        },
        {
          "source": "printed-brocade-sharara-lehenga-in-navy-blue-lkc21",
          "strength": 0.00046061722708429296,
          "target": "printed-art-silk-saree-in-green-spta25"
        },
        {
          "source": "printed-brocade-sharara-lehenga-in-navy-blue-lkc21",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-choker-necklace-set-jmy246"
        },
        {
          "source": "printed-art-silk-saree-in-green-spta25",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-crop-top-in-maroon-trb648"
        },
        {
          "source": "half-n-half-lycra-net-saree-in-beige-and-fuchsia-svla323",
          "strength": 0.00046061722708429296,
          "target": "printed-chanderi-kurti-in-beige-tuf504"
        },
        {
          "source": "printed-chanderi-kurti-in-beige-tuf504",
          "strength": 0.00046061722708429296,
          "target": "tie-dyed-georgette-saree-in-shaded-brown-and-rust-sjra242"
        },
        {
          "source": "tie-dyed-georgette-saree-in-shaded-brown-and-rust-sjra242",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-kurta-in-maroon-and-white-tcz217"
        },
        {
          "source": "tie-dyed-georgette-saree-in-shaded-brown-and-rust-sjra242",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-chiffon-saree-in-red-and-peach-spta280"
        },
        {
          "source": "printed-cotton-kurta-in-maroon-and-white-tcz217",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pair-of-openable-bangles-jvk2873"
        },
        {
          "source": "stone-studded-pair-of-openable-bangles-jvk2873",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-hasli-choker-set-jvk1754"
        },
        {
          "source": "stone-studded-hasli-choker-set-jvk1754",
          "strength": 0.00046061722708429296,
          "target": "printed-brocade-sharara-lehenga-in-navy-blue-lkc21"
        },
        {
          "source": "stone-studded-hasli-choker-set-jvk1754",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-shimmer-georgette-saree-in-olive-green-and-beige-spfa1960"
        },
        {
          "source": "half-n-half-shimmer-georgette-saree-in-olive-green-and-beige-spfa1960",
          "strength": 0.00046061722708429296,
          "target": "beaded-jhapta-in-green-jjr13821"
        },
        {
          "source": "beaded-jhapta-in-green-jjr13821",
          "strength": 0.00046061722708429296,
          "target": "woven-super-net-jacquard-saree-in-white-sbea518"
        },
        {
          "source": "embroidered-poly-cotton-straight-suit-in-teal-blue-kmsg95",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-fuchsia-seh952"
        },
        {
          "source": "woven-cotton-silk-saree-in-fuchsia-seh952",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-maroon-skk22112"
        },
        {
          "source": "embroidered-georgette-saree-in-maroon-skk22112",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-saree-in-fuchsia-sfka278"
        },
        {
          "source": "woven-chanderi-saree-in-fuchsia-sfka278",
          "strength": 0.00046061722708429296,
          "target": "pure-muga-silk-kurta-set-in-olive-green-mrg253"
        },
        {
          "source": "woven-chanderi-saree-in-fuchsia-sfka278",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-saree-in-off-white-spn2715"
        },
        {
          "source": "pure-muga-silk-kurta-set-in-olive-green-mrg253",
          "strength": 0.00046061722708429296,
          "target": "cotton-combo-sets-in-petticoat-uub58"
        },
        {
          "source": "pure-muga-silk-kurta-set-in-olive-green-mrg253",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-teal-green-sgja423"
        },
        {
          "source": "cotton-combo-sets-in-petticoat-uub58",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-red-and-beige-ssja246"
        },
        {
          "source": "woven-cotton-silk-saree-in-red-and-beige-ssja246",
          "strength": 0.00046061722708429296,
          "target": "georgette-readymade-long-kurta-in-light-teal-blue-tbl86"
        },
        {
          "source": "georgette-readymade-long-kurta-in-light-teal-blue-tbl86",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-satin-saree-in-ombre-beige-and-fuchsia-szg655"
        },
        {
          "source": "embroidered-border-satin-saree-in-ombre-beige-and-fuchsia-szg655",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-black-mgv91"
        },
        {
          "source": "embroidered-border-satin-saree-in-ombre-beige-and-fuchsia-szg655",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-cut-suit-in-yellow-and-white-kfx1972"
        },
        {
          "source": "plain-art-silk-kurta-set-in-black-mgv91",
          "strength": 0.00046061722708429296,
          "target": "plain-crepe-asymmetric-long-kurta-in-green-thu1453"
        },
        {
          "source": "plain-art-silk-kurta-set-in-black-mgv91",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-maroon-scfa350"
        },
        {
          "source": "plain-crepe-asymmetric-long-kurta-in-green-thu1453",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-pink-seh1815"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-pink-seh1815",
          "strength": 0.00046061722708429296,
          "target": "brocade-blouse-in-beige-uyc75"
        },
        {
          "source": "brocade-blouse-in-beige-uyc75",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-dark-pink-scxa794"
        },
        {
          "source": "woven-art-silk-saree-in-dark-pink-scxa794",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-wine-sjra74"
        },
        {
          "source": "embroidered-georgette-saree-in-wine-sjra74",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-shrug-in-blue-thu564"
        },
        {
          "source": "embroidered-georgette-saree-in-wine-sjra74",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-sherwani-in-dark-blue-mpw174"
        },
        {
          "source": "embroidered-net-shrug-in-blue-thu564",
          "strength": 0.00046061722708429296,
          "target": "embellished-satin-chiffon-saree-in-pastel-green-ssva257"
        },
        {
          "source": "embellished-satin-chiffon-saree-in-pastel-green-ssva257",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-chanderi-cotton-silk-saree-in-white-snea533"
        },
        {
          "source": "woven-pure-chanderi-cotton-silk-saree-in-white-snea533",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-saree-in-off-white-spn2715"
        },
        {
          "source": "woven-cotton-saree-in-off-white-spn2715",
          "strength": 0.00046061722708429296,
          "target": "embroidered-kota-silk-saree-in-red-safa115"
        },
        {
          "source": "woven-cotton-saree-in-off-white-spn2715",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-jacquard-tunic-in-pink-tmw98"
        },
        {
          "source": "woven-cotton-saree-in-off-white-spn2715",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-grey-lsh157"
        },
        {
          "source": "embroidered-kota-silk-saree-in-red-safa115",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-olive-green-sau1461"
        },
        {
          "source": "woven-art-silk-saree-in-olive-green-sau1461",
          "strength": 0.00046061722708429296,
          "target": "pure-chanderi-silk-woven-saree-in-black-skba238"
        },
        {
          "source": "plain-art-silk-kurta-set-in-maroon-mpd362",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-matka-silk-saree-in-baby-pink-stla352"
        },
        {
          "source": "plain-art-silk-kurta-set-in-maroon-mpd362",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-sherwani-in-maroon-mrg306"
        },
        {
          "source": "handloom-pure-matka-silk-saree-in-baby-pink-stla352",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-brooch-in-white-and-maroon-jjr12719"
        },
        {
          "source": "stone-studded-brooch-in-white-and-maroon-jjr12719",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-net-saree-in-beige-and-orange-stl691"
        },
        {
          "source": "hand-embroidered-georgette-abaya-style-suit-in-light-yellow-kxzd41",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-sherwani-in-grey-ombre-mpc803"
        },
        {
          "source": "woven-art-silk-jacquard-sherwani-in-grey-ombre-mpc803",
          "strength": 0.00046061722708429296,
          "target": "woven-south-cotton-saree-in-cream-stba72"
        },
        {
          "source": "woven-south-cotton-saree-in-cream-stba72",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-jodhpuri-suit-in-fuchsia-mhg609"
        },
        {
          "source": "plain-art-silk-jodhpuri-suit-in-fuchsia-mhg609",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-light-olive-green-shk1496"
        },
        {
          "source": "printed-cotton-saree-in-light-olive-green-shk1496",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-cotton-saree-in-off-white-sswa581"
        },
        {
          "source": "printed-cotton-saree-in-light-olive-green-shk1496",
          "strength": 0.00046061722708429296,
          "target": "handloom-kanchipuram-art-silk-saree-in-yellow-skra657"
        },
        {
          "source": "woven-pure-cotton-saree-in-off-white-sswa581",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-linen-saree-in-dark-blue-spn3374"
        },
        {
          "source": "handloom-cotton-linen-saree-in-dark-blue-spn3374",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-silk-jacquard-saree-in-pink-sbea564"
        },
        {
          "source": "woven-chanderi-silk-jacquard-saree-in-pink-sbea564",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-nehru-jacket-in-beige-and-mustard-mhg606"
        },
        {
          "source": "woven-art-silk-jacquard-nehru-jacket-in-beige-and-mustard-mhg606",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-georgette-saree-in-beige-sgya382"
        },
        {
          "source": "contrast-border-georgette-saree-in-navy-blue-sjra250",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-jacquard-kurta-set-in-navy-blue-mve305"
        },
        {
          "source": "contrast-border-georgette-saree-in-navy-blue-sjra250",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-teal-blue-and-pastel-green-sew3335"
        },
        {
          "source": "contrast-border-georgette-saree-in-navy-blue-sjra250",
          "strength": 0.00046061722708429296,
          "target": "jute-silk-nehru-jacket-in-white-mpe21"
        },
        {
          "source": "ombre-georgette-saree-in-green-sfva51",
          "strength": 0.00046061722708429296,
          "target": "linen-nehru-jacket-in-grey-mhg224"
        },
        {
          "source": "printed-cotton-kurti-in-beige-tmz241",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-red-scxa599"
        },
        {
          "source": "kanchipuram-saree-in-blue-sbra412",
          "strength": 0.00046061722708429296,
          "target": "floral-printed-cotton-straight-suit-in-cream-kfx2608"
        },
        {
          "source": "floral-printed-cotton-straight-suit-in-cream-kfx2608",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-spfa2222"
        },
        {
          "source": "embroidered-georgette-saree-in-red-spfa2222",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-navy-blue-kvs1709"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-navy-blue-kvs1709",
          "strength": 0.00046061722708429296,
          "target": "pure-mysore-silk-saree-in-orange-shu620"
        },
        {
          "source": "pure-mysore-silk-saree-in-orange-shu620",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-georgette-anarkali-suit-in-navy-blue-kuz253"
        },
        {
          "source": "pure-mysore-silk-saree-in-orange-shu620",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-gown-in-red-taz1"
        },
        {
          "source": "embroidered-neckline-georgette-anarkali-suit-in-navy-blue-kuz253",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-beige-kyx1134"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-beige-kyx1134",
          "strength": 0.00046061722708429296,
          "target": "woven-border-georgette-saree-in-red-skpa756"
        },
        {
          "source": "woven-border-georgette-saree-in-red-skpa756",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-floral-printed-georgette-saree-in-maroon-and-beige-sew4700"
        },
        {
          "source": "half-n-half-floral-printed-georgette-saree-in-maroon-and-beige-sew4700",
          "strength": 0.00046061722708429296,
          "target": "woven-brocade-silk-punjabi-suit-in-navy-blue-kxz140"
        },
        {
          "source": "half-n-half-floral-printed-georgette-saree-in-maroon-and-beige-sew4700",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-light-beige-kch822"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-light-beige-kch822",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-straight-suit-in-white-kthb142"
        },
        {
          "source": "printed-georgette-straight-suit-in-white-kthb142",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-yellow-ssf3607"
        },
        {
          "source": "printed-georgette-straight-suit-in-white-kthb142",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-cotton-straight-suit-in-fuchsia-kexm93"
        },
        {
          "source": "contrast-patch-border-rayon-a-line-kurta-in-fuchsia-trq242",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bordered-georgette-saree-in-shaded-turquoise-sfs692"
        },
        {
          "source": "contrast-patch-border-rayon-a-line-kurta-in-fuchsia-trq242",
          "strength": 0.00046061722708429296,
          "target": "handloom-linen-jamdani-saree-in-black-skza70"
        },
        {
          "source": "embroidered-bordered-georgette-saree-in-shaded-turquoise-sfs692",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-sherwani-in-maroon-mse456"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-olive-green-kye830",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-jacquard-nehru-jacket-in-grey-mhg587"
        },
        {
          "source": "embroidered-georgette-straight-suit-in-olive-green-kye830",
          "strength": 0.00046061722708429296,
          "target": "pacchikari-necklace-set-jjr16238"
        },
        {
          "source": "woven-cotton-jacquard-nehru-jacket-in-grey-mhg587",
          "strength": 0.00046061722708429296,
          "target": "plain-cowl-style-rayon-kurta-pajama-in-black-mhg696"
        },
        {
          "source": "half-n-half-georgette-saree-in-red-and-pastel-green-sqfa126",
          "strength": 0.00046061722708429296,
          "target": "pachhikari-jhumki-style-earring-jjr13408"
        },
        {
          "source": "pachhikari-jhumki-style-earring-jjr13408",
          "strength": 0.00046061722708429296,
          "target": "maroon-and-white-artificial-pearl-bangles-jvk1481"
        },
        {
          "source": "maroon-and-white-artificial-pearl-bangles-jvk1481",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-saree-in-peach-and-beige-sbja252"
        },
        {
          "source": "half-n-half-lycra-saree-in-peach-and-beige-sbja252",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-prestitched-saree-in-teal-blue-and-off-white-suf7700"
        },
        {
          "source": "half-n-half-lycra-saree-in-peach-and-beige-sbja252",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-sherwani-in-maroon-mrg306"
        },
        {
          "source": "half-n-half-georgette-prestitched-saree-in-teal-blue-and-off-white-suf7700",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-brasso-saree-in-cream-and-wine-suf7454"
        },
        {
          "source": "half-n-half-brasso-saree-in-cream-and-wine-suf7454",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-floral-printed-georgette-saree-in-maroon-and-beige-sew4700"
        },
        {
          "source": "half-n-half-brasso-saree-in-cream-and-wine-suf7454",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-and-art-silk-brocade-anarkali-suit-in-white-kthb6"
        },
        {
          "source": "half-n-half-brasso-saree-in-cream-and-wine-suf7454",
          "strength": 0.00046061722708429296,
          "target": "plain-net-saree-in-off-white-svx797"
        },
        {
          "source": "hand-embroidered-net-and-art-silk-brocade-anarkali-suit-in-white-kthb6",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-black-and-purple-scfa257"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-black-and-purple-scfa257",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-light-blue-srp689"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-light-blue-srp689",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-yellow-sgka2962"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-light-blue-srp689",
          "strength": 0.00046061722708429296,
          "target": "woven-tussar-silk-saree-in-white-seh1415"
        },
        {
          "source": "woven-art-silk-saree-in-yellow-sgka2962",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-silk-punjabi-suit-in-violet-kch975"
        },
        {
          "source": "embroidered-taffeta-silk-punjabi-suit-in-violet-kch975",
          "strength": 0.00046061722708429296,
          "target": "plain-ghicha-silk-sherwani-in-teal-blue-mse632"
        },
        {
          "source": "plain-ghicha-silk-sherwani-in-teal-blue-mse632",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-pure-silk-saree-in-green-stbn63"
        },
        {
          "source": "kanchipuram-pure-silk-saree-in-green-stbn63",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-shaded-pink-sew5322"
        },
        {
          "source": "printed-georgette-saree-in-shaded-pink-sew5322",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-red-sfka1247"
        },
        {
          "source": "woven-chanderi-cotton-saree-in-red-sfka1247",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-punjabi-suit-in-blue-kve121"
        },
        {
          "source": "printed-cotton-punjabi-suit-in-blue-kve121",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-rust-scfa331"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-rust-scfa331",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-pink-sas1237"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-pink-sas1237",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-pathani-suit-in-white-mrg299"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-pink-sas1237",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-and-net-abaya-style-suit-in-baby-pink-kch228"
        },
        {
          "source": "embroidered-chanderi-silk-saree-in-pink-sas1237",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-kurta-set-in-beige-mpd329"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-maroon-scfa350",
          "strength": 0.00046061722708429296,
          "target": "printed-half-n-half-crepe-jacquard-and-georgette-saree-in-brown-sga6044"
        },
        {
          "source": "printed-half-n-half-crepe-jacquard-and-georgette-saree-in-brown-sga6044",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-lehenga-in-coral-luf1353"
        },
        {
          "source": "embroidered-georgette-lehenga-in-coral-luf1353",
          "strength": 0.00046061722708429296,
          "target": "beaded-necklace-set-jjr16176"
        },
        {
          "source": "beaded-necklace-set-jjr16176",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jvk2151"
        },
        {
          "source": "stone-studded-necklace-set-jvk2151",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-kota-silk-saree-in-green-sjn5637"
        },
        {
          "source": "embroidered-pure-kota-silk-saree-in-green-sjn5637",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-crepe-saree-in-orange-ombre-and-fuchsia-sws5257"
        },
        {
          "source": "embroidered-border-crepe-saree-in-orange-ombre-and-fuchsia-sws5257",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-wine-sjra74"
        },
        {
          "source": "embroidered-net-sherwani-in-dark-blue-mpw174",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-saree-in-off-white-spn2715"
        },
        {
          "source": "embroidered-art-silk-jacquard-tunic-in-pink-tmw98",
          "strength": 0.00046061722708429296,
          "target": "handloom-silk-saree-in-maroon-sts3420"
        },
        {
          "source": "handloom-kanchipuram-art-silk-saree-in-yellow-skra657",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-straight-suit-in-coral-pink-kcu162"
        },
        {
          "source": "handloom-kanchipuram-art-silk-saree-in-yellow-skra657",
          "strength": 0.00046061722708429296,
          "target": "pachikari-jhumka-style-long-dangle-earring-jjr13978"
        },
        {
          "source": "woven-cotton-silk-jacquard-straight-suit-in-coral-pink-kcu162",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-dupatta-in-pastel-blue-bmb14"
        },
        {
          "source": "woven-chanderi-cotton-dupatta-in-pastel-blue-bmb14",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-saree-in-off-white-spn2715"
        },
        {
          "source": "handloom-cotton-saree-in-off-white-spn2715",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-saree-in-blue-shxa361"
        },
        {
          "source": "handloom-cotton-saree-in-off-white-spn2715",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-set-in-maroon-mpc715"
        },
        {
          "source": "jamdani-cotton-saree-in-blue-shxa361",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-jacquard-straight-suit-in-beige-kxz150"
        },
        {
          "source": "embroidered-cotton-jacquard-straight-suit-in-beige-kxz150",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-net-brasso-saree-in-teal-blue-and-red-skra1291"
        },
        {
          "source": "half-n-half-net-brasso-saree-in-teal-blue-and-red-skra1291",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-kurta-set-in-maroon-mpd362"
        },
        {
          "source": "embroidered-velvet-sherwani-in-maroon-mrg306",
          "strength": 0.00046061722708429296,
          "target": "coimbatore-cotton-silk-saree-in-purple-saha225"
        },
        {
          "source": "embroidered-velvet-sherwani-in-maroon-mrg306",
          "strength": 0.00046061722708429296,
          "target": "embroidered-khadi-cotton-kurta-pajama-in-beige-mms722"
        },
        {
          "source": "coimbatore-cotton-silk-saree-in-purple-saha225",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-pair-of-bangles-jvk3102"
        },
        {
          "source": "stone-studded-pair-of-bangles-jvk3102",
          "strength": 0.00046061722708429296,
          "target": "plain-linen-kurta-set-in-white-mhg553"
        },
        {
          "source": "plain-linen-kurta-set-in-white-mhg553",
          "strength": 0.00046061722708429296,
          "target": "embroidered-anarkali-suit-in-yellow-and-blue-ktj405"
        },
        {
          "source": "embroidered-anarkali-suit-in-yellow-and-blue-ktj405",
          "strength": 0.00046061722708429296,
          "target": "woven-border-georgette-saree-with-embroidered-blouse-in-orange-sew3700"
        },
        {
          "source": "woven-border-georgette-saree-with-embroidered-blouse-in-orange-sew3700",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-silk-kurti-in-light-purple-tgs60"
        },
        {
          "source": "embroidered-cotton-silk-kurti-in-light-purple-tgs60",
          "strength": 0.00046061722708429296,
          "target": "embroidered-half-n-half-art-silk-and-net-saree-in-fuchsia-and-blue-szg452"
        },
        {
          "source": "embroidered-half-n-half-art-silk-and-net-saree-in-fuchsia-and-blue-szg452",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-light-green-sgka2961"
        },
        {
          "source": "woven-art-silk-saree-in-light-green-sgka2961",
          "strength": 0.00046061722708429296,
          "target": "handloom-kanchipuram-art-silk-saree-in-yellow-skra657"
        },
        {
          "source": "pachikari-jhumka-style-long-dangle-earring-jjr13978",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-saree-in-purple-ssk4756a"
        },
        {
          "source": "embroidered-net-saree-in-purple-ssk4756a",
          "strength": 0.00046061722708429296,
          "target": "woven-rayon-a-line-suit-in-maroon-kjn3152"
        },
        {
          "source": "woven-rayon-a-line-suit-in-maroon-kjn3152",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-lehenga-in-shaded-pastel-green-and-teal-blue-lcc133"
        },
        {
          "source": "plain-rayon-assymetric-kurta-in-mustard-tjw693",
          "strength": 0.00046061722708429296,
          "target": "leheriya-art-silk-lehenga-in-fuchsia-and-pink-ljn1386"
        },
        {
          "source": "leheriya-art-silk-lehenga-in-fuchsia-and-pink-ljn1386",
          "strength": 0.00046061722708429296,
          "target": "banarasi-pure-silk-saree-in-fuchsia-snea1309"
        },
        {
          "source": "banarasi-pure-silk-saree-in-fuchsia-snea1309",
          "strength": 0.00046061722708429296,
          "target": "bandhej-crepe-saree-in-black-sjn7073"
        },
        {
          "source": "banarasi-pure-silk-saree-in-fuchsia-snea1309",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-beige-and-pink-ssf3466"
        },
        {
          "source": "banarasi-pure-silk-saree-in-fuchsia-snea1309",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-red-suf7624"
        },
        {
          "source": "woven-art-silk-saree-in-red-suf7624",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-satin-and-chiffon-jacquard-saree-in-fuchsia-and-purple-stl253"
        },
        {
          "source": "half-n-half-satin-and-chiffon-jacquard-saree-in-fuchsia-and-purple-stl253",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-dark-green-and-beige-stl724"
        },
        {
          "source": "half-n-half-art-silk-saree-in-dark-green-and-beige-stl724",
          "strength": 0.00046061722708429296,
          "target": "contrast-patch-border-rayon-a-line-kurta-in-fuchsia-trq242"
        },
        {
          "source": "handloom-linen-jamdani-saree-in-black-skza70",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-lycra-saree-in-peach-and-beige-sbja252"
        },
        {
          "source": "embroidered-khadi-cotton-kurta-pajama-in-beige-mms722",
          "strength": 0.00046061722708429296,
          "target": "prestitched-embroidered-shimmer-net-saree-in-black-suf7169"
        },
        {
          "source": "prestitched-embroidered-shimmer-net-saree-in-black-suf7169",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-fuchsia-snea920"
        },
        {
          "source": "printed-georgette-saree-in-light-grey-ssf3609",
          "strength": 0.00046061722708429296,
          "target": "digital-printed-georgette-pakistani-suit-in-dusty-olive-green-kch1004"
        },
        {
          "source": "digital-printed-georgette-pakistani-suit-in-dusty-olive-green-kch1004",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-brasso-saree-in-cream-and-wine-suf7454"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-light-peach-shxa105",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-earrings-jjr14373"
        },
        {
          "source": "stone-studded-earrings-jjr14373",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-and-net-lehenga-in-red-and-beige-luf1012"
        },
        {
          "source": "embroidered-art-silk-and-net-lehenga-in-red-and-beige-luf1012",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-saree-in-green-syc7070"
        },
        {
          "source": "embroidered-art-silk-saree-in-green-syc7070",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-green-sar861"
        },
        {
          "source": "hand-embroidered-georgette-saree-in-green-sar861",
          "strength": 0.00046061722708429296,
          "target": "solid-color-polyester-palazzo-in-brown-bdz349"
        },
        {
          "source": "solid-color-polyester-palazzo-in-brown-bdz349",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-cotton-silk-straight-kurta-in-grey-tgp44"
        },
        {
          "source": "hand-embroidered-cotton-silk-straight-kurta-in-grey-tgp44",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-chiffon-saree-in-ombre-yellow-and-purple-svea10"
        },
        {
          "source": "hand-embroidered-chiffon-saree-in-ombre-yellow-and-purple-svea10",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-silk-saree-in-off-white-and-sky-blue-stla206"
        },
        {
          "source": "handloom-tant-cotton-silk-saree-in-off-white-and-sky-blue-stla206",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-light-grey-stu493"
        },
        {
          "source": "printed-georgette-saree-in-light-grey-stu493",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-light-blue-srp689"
        },
        {
          "source": "woven-tussar-silk-saree-in-white-seh1415",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-straight-suit-in-olive-green-kye830"
        },
        {
          "source": "pacchikari-necklace-set-jjr16238",
          "strength": 0.00046061722708429296,
          "target": "woven-art-linen-saree-in-off-white-srga788"
        },
        {
          "source": "woven-art-linen-saree-in-off-white-srga788",
          "strength": 0.00046061722708429296,
          "target": "plain-linen-cotton-kurta-set-in-light-blue-mpc898"
        },
        {
          "source": "plain-linen-cotton-kurta-set-in-light-blue-mpc898",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-pink-sas1237"
        },
        {
          "source": "half-n-half-embroidered-lycra-shimmer-saree-in-pink-and-yellow-spfa1292",
          "strength": 0.00046061722708429296,
          "target": "embroidered-khadi-silk-punjabi-suit-in-off-white-kae643"
        },
        {
          "source": "embroidered-khadi-silk-punjabi-suit-in-off-white-kae643",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-abaya-style-suit-in-fuchsia-kch882"
        },
        {
          "source": "embroidered-art-silk-abaya-style-suit-in-fuchsia-kch882",
          "strength": 0.00046061722708429296,
          "target": "kerala-kasavu-woven-cotton-saree-in-off-white-spn2941"
        },
        {
          "source": "woven-banarasi-silk-saree-in-violet-seh1782",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-grey-and-beige-spfa1971"
        },
        {
          "source": "stone-studded-choker-necklace-set-jmy165",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-viscose-georgette-punjabi-suit-in-yellow-krgv35"
        },
        {
          "source": "hand-embroidered-viscose-georgette-punjabi-suit-in-yellow-krgv35",
          "strength": 0.00046061722708429296,
          "target": "banarasi-pure-silk-saree-in-fuchsia-snea1309"
        },
        {
          "source": "embroidered-net-lehenga-in-fuchsia-lga292",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-orange-ssf4215"
        },
        {
          "source": "printed-georgette-saree-in-orange-ssf4215",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-top-in-black-tja585"
        },
        {
          "source": "printed-cotton-top-in-black-tja585",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-orange-stbn178"
        },
        {
          "source": "woven-bangalore-silk-saree-in-orange-stbn178",
          "strength": 0.00046061722708429296,
          "target": "embroidered-velvet-and-brocade-sherwani-in-beige-mpc797"
        },
        {
          "source": "embroidered-velvet-and-brocade-sherwani-in-beige-mpc797",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-satin-straight-suit-in-white-kfx1904"
        },
        {
          "source": "printed-cotton-satin-straight-suit-in-white-kfx1904",
          "strength": 0.00046061722708429296,
          "target": "contrast-border-georgette-saree-in-navy-blue-sjra250"
        },
        {
          "source": "printed-cotton-satin-straight-suit-in-white-kfx1904",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-neon-green-skra1060"
        },
        {
          "source": "kanchipuram-saree-in-neon-green-skra1060",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-abaya-style-suit-in-blue-and-beige-kqu885"
        },
        {
          "source": "kanchipuram-saree-in-neon-green-skra1060",
          "strength": 0.00046061722708429296,
          "target": "embroidered-straight-cut-georgette-suit-in-fuchsia-kjn1597"
        },
        {
          "source": "kanchipuram-saree-in-neon-green-skra1060",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-maroon-sfka343"
        },
        {
          "source": "printed-crepe-abaya-style-suit-in-blue-and-beige-kqu885",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-matka-silk-saree-in-green-spn3377"
        },
        {
          "source": "woven-pure-matka-silk-saree-in-green-spn3377",
          "strength": 0.00046061722708429296,
          "target": "ornamental-printed-art-silk-saree-in-pastel-green-sew5140"
        },
        {
          "source": "ornamental-printed-art-silk-saree-in-pastel-green-sew5140",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-shaded-teal-green-kch997"
        },
        {
          "source": "embroidered-georgette-pakistani-suit-in-shaded-teal-green-kch997",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-blue-sfka637"
        },
        {
          "source": "woven-cotton-silk-saree-in-blue-sfka637",
          "strength": 0.00046061722708429296,
          "target": "stripe-printed-cotton-front-slit-kurta-in-off-white-tja829"
        },
        {
          "source": "stripe-printed-cotton-front-slit-kurta-in-off-white-tja829",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-green-spfa2210"
        },
        {
          "source": "woven-art-silk-saree-in-green-spfa2210",
          "strength": 0.00046061722708429296,
          "target": "ombre-georgette-saree-in-green-sew5498"
        },
        {
          "source": "ombre-georgette-saree-in-green-sew5498",
          "strength": 0.00046061722708429296,
          "target": "handloom-linen-saree-in-off-white-srga788"
        },
        {
          "source": "handloom-linen-saree-in-off-white-srga788",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-cut-suit-in-beige-khbz1"
        },
        {
          "source": "printed-cotton-straight-cut-suit-in-beige-khbz1",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-mysore-silk-saree-in-charcoal-black-shu515"
        },
        {
          "source": "woven-pure-mysore-silk-saree-in-charcoal-black-shu515",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-art-silk-kurta-set-in-off-white-mrg263"
        },
        {
          "source": "embroidered-neckline-art-silk-kurta-set-in-off-white-mrg263",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-adjustable-haathphool-jjr16501"
        },
        {
          "source": "stone-studded-adjustable-haathphool-jjr16501",
          "strength": 0.00046061722708429296,
          "target": "ombre-satin-chiffon-saree-in-sky-blue-sqfa340"
        },
        {
          "source": "ombre-satin-chiffon-saree-in-sky-blue-sqfa340",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-long-kurta-in-light-yellow-tkh57"
        },
        {
          "source": "printed-georgette-long-kurta-in-light-yellow-tkh57",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-pink-seh1659"
        },
        {
          "source": "plain-rayon-kurta-set-in-teal-blue-trb511",
          "strength": 0.00046061722708429296,
          "target": "embroidered-tussar-silk-saree-in-rust-svqa60"
        },
        {
          "source": "embroidered-tussar-silk-saree-in-rust-svqa60",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-gown-in-blue-ufb19"
        },
        {
          "source": "embroidered-net-gown-in-blue-ufb19",
          "strength": 0.00046061722708429296,
          "target": "banarasi-silk-saree-in-magenta-sbta172"
        },
        {
          "source": "banarasi-silk-saree-in-magenta-sbta172",
          "strength": 0.00046061722708429296,
          "target": "pure-banarasi-silk-saree-in-maroon-snea1007"
        },
        {
          "source": "printed-cotton-saree-in-off-white-and-grey-svra439",
          "strength": 0.00046061722708429296,
          "target": "plain-khadi-kurta-set-in-grey-mve192"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-red-utt603",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-layered-mathapatti-jrl939"
        },
        {
          "source": "stone-studded-layered-mathapatti-jrl939",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-crop-top-set-in-rose-gold-tzg28"
        },
        {
          "source": "hand-embroidered-georgette-crop-top-set-in-rose-gold-tzg28",
          "strength": 0.00046061722708429296,
          "target": "bengal-handloom-pure-cotton-saree-in-off-white-sswa198"
        },
        {
          "source": "hand-embroidered-georgette-crop-top-set-in-rose-gold-tzg28",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-jamdani-saree-in-beige-spn3562"
        },
        {
          "source": "bengal-handloom-pure-cotton-saree-in-off-white-sswa198",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-set-in-beige-mqz16"
        },
        {
          "source": "embroidered-cotton-kurta-set-in-beige-mqz16",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-dupatta-in-olive-green-bmb28"
        },
        {
          "source": "embroidered-cotton-kurta-set-in-beige-mqz16",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-kurta-set-in-white-and-blue-mtr107"
        },
        {
          "source": "woven-cotton-tant-saree-in-yellow-spn3289",
          "strength": 0.00046061722708429296,
          "target": "leheriya-printed-cotton-palazzo-in-sky-blue-bnj307"
        },
        {
          "source": "leheriya-printed-cotton-palazzo-in-sky-blue-bnj307",
          "strength": 0.00046061722708429296,
          "target": "woven-jacquard-kurta-set-in-beige-mse553"
        },
        {
          "source": "woven-jacquard-kurta-set-in-beige-mse553",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-coral-red-utt570"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-coral-red-utt570",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-satin-chiffon-saree-in-fuchsia-sew3843"
        },
        {
          "source": "embroidered-border-satin-chiffon-saree-in-fuchsia-sew3843",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-cotton-saree-in-off-white-sswa581"
        },
        {
          "source": "handloom-pure-cotton-saree-in-off-white-sswa581",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-turquoise-sgka1615"
        },
        {
          "source": "kanchipuram-saree-in-turquoise-sgka1615",
          "strength": 0.00046061722708429296,
          "target": "linen-nehru-jacket-in-grey-mhg224"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-violet-kbz228",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-long-kurta-in-beige-tzq300"
        },
        {
          "source": "embroidered-cotton-long-kurta-in-beige-tzq300",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-top-set-in-coral-tjw77"
        },
        {
          "source": "embroidered-satin-top-set-in-coral-tjw77",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-teal-green-srga321"
        },
        {
          "source": "handloom-tant-cotton-saree-in-teal-green-srga321",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-navy-blue-mrg403"
        },
        {
          "source": "embroidered-art-silk-kurta-set-in-navy-blue-mrg403",
          "strength": 0.00046061722708429296,
          "target": "plain-chanderi-silk-layered-kurta-in-copper-thu1473"
        },
        {
          "source": "embroidered-art-silk-kurta-set-in-navy-blue-mrg403",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-georgette-saree-in-teal-green-and-beige-sbja362"
        },
        {
          "source": "plain-chanderi-silk-layered-kurta-in-copper-thu1473",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jjr16393"
        },
        {
          "source": "stone-studded-necklace-set-jjr16393",
          "strength": 0.00046061722708429296,
          "target": "solid-color-rayon-palazzo-in-green-tja965"
        },
        {
          "source": "solid-color-rayon-palazzo-in-green-tja965",
          "strength": 0.00046061722708429296,
          "target": "handloom-art-silk-dupatta-in-indigo-blue-bbe26"
        },
        {
          "source": "handloom-art-silk-dupatta-in-indigo-blue-bbe26",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-midi-dress-in-teal-blue-tyg12"
        },
        {
          "source": "plain-cotton-midi-dress-in-teal-blue-tyg12",
          "strength": 0.00046061722708429296,
          "target": "golden-and-silver-color-bangle-set-jvm1693"
        },
        {
          "source": "golden-and-silver-color-bangle-set-jvm1693",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-grey-sgka1170"
        },
        {
          "source": "kanchipuram-saree-in-grey-sgka1170",
          "strength": 0.00046061722708429296,
          "target": "abstract-printed-rayon-kurta-in-green-tdr1064"
        },
        {
          "source": "abstract-printed-rayon-kurta-in-green-tdr1064",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-chiffon-saree-in-royal-blue-seh860"
        },
        {
          "source": "hand-embroidered-chiffon-saree-in-royal-blue-seh860",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-satin-straight-suit-in-white-kfx1904"
        },
        {
          "source": "hand-embroidered-chiffon-saree-in-royal-blue-seh860",
          "strength": 0.00046061722708429296,
          "target": "plain-chanderi-cotton-abaya-style-suit-in-light-yellow-kjn2512"
        },
        {
          "source": "half-n-half-art-silk-saree-in-teal-blue-and-pastel-green-sew3335",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-neon-green-skra1060"
        },
        {
          "source": "embroidered-straight-cut-georgette-suit-in-fuchsia-kjn1597",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-kurti-in-mustard-tuf1109"
        },
        {
          "source": "embroidered-georgette-kurti-in-mustard-tuf1109",
          "strength": 0.00046061722708429296,
          "target": "cotton-pathani-suit-in-sky-blue-mse318"
        },
        {
          "source": "digital-printed-georgette-saree-in-blue-ssf4225",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-tant-saree-in-off-white-spn3039"
        },
        {
          "source": "woven-cotton-tant-saree-in-off-white-spn3039",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-saree-in-beige-svqa87"
        },
        {
          "source": "embroidered-dupion-silk-saree-in-beige-svqa87",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-mustard-mve193"
        },
        {
          "source": "plain-cotton-kurta-set-in-mustard-mve193",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-brasso-saree-in-cream-and-wine-suf7454"
        },
        {
          "source": "plain-cotton-kurta-set-in-mustard-mve193",
          "strength": 0.00046061722708429296,
          "target": "kantha-embroidered-cotton-straight-suit-in-teal-green-kjg46"
        },
        {
          "source": "plain-net-saree-in-off-white-svx797",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-red-scfa332"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-red-scfa332",
          "strength": 0.00046061722708429296,
          "target": "woven-satin-georgette-saree-in-sky-blue-and-fuchisa-sfva56"
        },
        {
          "source": "woven-satin-georgette-saree-in-sky-blue-and-fuchisa-sfva56",
          "strength": 0.00046061722708429296,
          "target": "plain-shimmer-georgette-circular-lehenga-in-off-white-lkc36"
        },
        {
          "source": "woven-satin-georgette-saree-in-sky-blue-and-fuchisa-sfva56",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-straight-suit-in-dark-brown-kye817"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-dark-brown-kye817",
          "strength": 0.00046061722708429296,
          "target": "woven-georgette-brasso-saree-in-pink-ombre-sud1406"
        },
        {
          "source": "woven-georgette-brasso-saree-in-pink-ombre-sud1406",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-punjabi-suit-in-navy-blue-kbz230"
        },
        {
          "source": "embroidered-georgette-punjabi-suit-in-navy-blue-kbz230",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-in-light-brown-tnc997"
        },
        {
          "source": "embroidered-cotton-kurta-in-light-brown-tnc997",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-magenta-sas1341"
        },
        {
          "source": "woven-cotton-silk-saree-in-beige-snea1171",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-mustard-mve193"
        },
        {
          "source": "woven-cotton-silk-saree-in-beige-snea1171",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-jacquard-straight-suit-in-pink-kcu122"
        },
        {
          "source": "woven-cotton-silk-jacquard-straight-suit-in-pink-kcu122",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-brown-sgka1186"
        },
        {
          "source": "kanchipuram-saree-in-brown-sgka1186",
          "strength": 0.00046061722708429296,
          "target": "plain-satin-blouse-in-red-dbu498"
        },
        {
          "source": "plain-satin-blouse-in-red-dbu498",
          "strength": 0.00046061722708429296,
          "target": "woven-bangalore-silk-saree-in-purple-sqpa251"
        },
        {
          "source": "stone-studded-earrings-jjr15202",
          "strength": 0.00046061722708429296,
          "target": "pure-tussar-silk-saree-in-light-beige-snea1166"
        },
        {
          "source": "pure-tussar-silk-saree-in-light-beige-snea1166",
          "strength": 0.00046061722708429296,
          "target": "embroidered-jacquard-saree-in-yellow-sas1041"
        },
        {
          "source": "kanchipuram-saree-in-teal-green-sbra313",
          "strength": 0.00046061722708429296,
          "target": "embellished-art-silk-saree-in-peach-ssva275"
        },
        {
          "source": "embellished-art-silk-saree-in-peach-ssva275",
          "strength": 0.00046061722708429296,
          "target": "plain-crushed-cotton-skirt-in-off-white-and-red-bnj193"
        },
        {
          "source": "embellished-art-silk-saree-in-peach-ssva275",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-in-golden-jdw292"
        },
        {
          "source": "plain-crushed-cotton-skirt-in-off-white-and-red-bnj193",
          "strength": 0.00046061722708429296,
          "target": "ombre-georgette-saree-in-green-and-navy-blue-spfa1626"
        },
        {
          "source": "ombre-georgette-saree-in-green-and-navy-blue-spfa1626",
          "strength": 0.00046061722708429296,
          "target": "kalamkari-cotton-saree-in-light-pink-sswa728"
        },
        {
          "source": "hand-embroidered-chiffon-and-net-saree-in-pink-and-yellow-seh1514",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-pink-seh1659"
        },
        {
          "source": "woven-chanderi-silk-saree-in-light-green-sfwa247",
          "strength": 0.00046061722708429296,
          "target": "bandhej-printed-cotton-straight-suit-in-turquoise-kfx2583"
        },
        {
          "source": "bandhej-printed-cotton-straight-suit-in-turquoise-kfx2583",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-pink-sas1237"
        },
        {
          "source": "woven-muga-silk-saree-in-blue-srp598",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-cotton-saree-in-light-beige-sswa544"
        },
        {
          "source": "woven-pure-cotton-saree-in-light-beige-sswa544",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-kurta-jacket-set-in-white-and-fawn-mhg625"
        },
        {
          "source": "plain-cotton-linen-kurta-jacket-set-in-white-and-fawn-mhg625",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-front-slit-kurti-in-red-tnz58"
        },
        {
          "source": "embroidered-cotton-front-slit-kurti-in-red-tnz58",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-red-sbh1346"
        },
        {
          "source": "embroidered-georgette-saree-in-red-sbh1346",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-orange-sbz3262"
        },
        {
          "source": "embroidered-georgette-saree-in-orange-sbz3262",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-turquoise-sew4345"
        },
        {
          "source": "printed-crepe-saree-in-turquoise-sew4345",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-necklace-set-jvk1869"
        },
        {
          "source": "stone-studded-necklace-set-jvk1869",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-coral-pink-stea494"
        },
        {
          "source": "woven-banarasi-silk-saree-in-coral-pink-stea494",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-crop-top-set-in-rose-gold-tzg28"
        },
        {
          "source": "woven-banarasi-silk-saree-in-coral-pink-stea494",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-front-slit-kurta-in-violet-tbe269"
        },
        {
          "source": "handloom-cotton-jamdani-saree-in-beige-spn3562",
          "strength": 0.00046061722708429296,
          "target": "embroidered-kota-silk-saree-in-teal-green-safa112"
        },
        {
          "source": "embroidered-kota-silk-saree-in-teal-green-safa112",
          "strength": 0.00046061722708429296,
          "target": "embroidered-bordered-crepe-saree-in-sea-green-sws5445"
        },
        {
          "source": "embroidered-bordered-crepe-saree-in-sea-green-sws5445",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-rust-sfka1152"
        },
        {
          "source": "woven-cotton-silk-saree-in-rust-sfka1152",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-teal-green-spfa1897"
        },
        {
          "source": "woven-art-silk-saree-in-off-white-skra1323",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-kurta-set-in-navy-blue-mrg403"
        },
        {
          "source": "half-n-half-georgette-saree-in-teal-green-and-beige-sbja362",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-saree-in-red-and-beige-ssf3481"
        },
        {
          "source": "embroidered-chiffon-saree-in-red-and-beige-ssf3481",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-straight-cut-viscose-suit-in-fuchsia-kau148"
        },
        {
          "source": "hand-embroidered-straight-cut-viscose-suit-in-fuchsia-kau148",
          "strength": 0.00046061722708429296,
          "target": "embroidered-neckline-georgette-jacket-style-kurta-in-beige-thu1722"
        },
        {
          "source": "embroidered-neckline-georgette-jacket-style-kurta-in-beige-thu1722",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-linen-jodhpuri-suit-in-blue-mpc348"
        },
        {
          "source": "plain-cotton-linen-jodhpuri-suit-in-blue-mpc348",
          "strength": 0.00046061722708429296,
          "target": "woven-satin-georgette-saree-in-sky-blue-and-fuchisa-sfva56"
        },
        {
          "source": "woven-art-silk-saree-in-peach-shka320",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "source": "brocade-jacket-in-beige-and-maroon-thu268",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-chiffon-saree-in-royal-blue-seh860"
        },
        {
          "source": "plain-chanderi-cotton-abaya-style-suit-in-light-yellow-kjn2512",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-mustard-ssl36179"
        },
        {
          "source": "plain-dupion-silk-kurta-set-in-teal-green-msf321",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-orange-uyp58"
        },
        {
          "source": "kundan-bridal-necklace-set-jjr14449",
          "strength": 0.00046061722708429296,
          "target": "pure-crepe-mysore-saree-in-off-white-shu700"
        },
        {
          "source": "pure-crepe-mysore-saree-in-off-white-shu700",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chanderi-silk-saree-in-magenta-sas1341"
        },
        {
          "source": "bhagalpuri-silk-saree-in-black-sts3638",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-saree-in-maroon-svra165"
        },
        {
          "source": "printed-cotton-saree-in-maroon-svra165",
          "strength": 0.00046061722708429296,
          "target": "embroidered-pure-raw-silk-circular-lehenga-in-purple-ljn1178"
        },
        {
          "source": "embroidered-pure-raw-silk-circular-lehenga-in-purple-ljn1178",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-green-and-red-spfa2023"
        },
        {
          "source": "woven-pure-tussar-silk-saree-in-purple-snea486",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-saree-in-beige-snea1171"
        },
        {
          "source": "kantha-embroidered-cotton-straight-suit-in-teal-green-kjg46",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-straight-kurta-in-dusty-green-tjw572"
        },
        {
          "source": "plain-rayon-straight-kurta-in-dusty-green-tjw572",
          "strength": 0.00046061722708429296,
          "target": "handloom-pure-cotton-saree-in-light-beige-sswa544"
        },
        {
          "source": "handloom-pure-cotton-saree-in-light-beige-sswa544",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-abaya-style-suit-in-navy-blue-kuf10228"
        },
        {
          "source": "embroidered-georgette-abaya-style-suit-in-navy-blue-kuf10228",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-saree-in-orange-dual-tone-shp791"
        },
        {
          "source": "pure-kanchipuram-silk-saree-in-orange-dual-tone-shp791",
          "strength": 0.00046061722708429296,
          "target": "woven-cotton-silk-kurta-in-fuchsia-trv179"
        },
        {
          "source": "woven-cotton-silk-kurta-in-fuchsia-trv179",
          "strength": 0.00046061722708429296,
          "target": "kundan-earrings-jjr15232"
        },
        {
          "source": "embroidered-art-silk-circular-lehenga-in-beige-and-red-lxw187",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-baby-pink-ssf3424"
        },
        {
          "source": "art-silk-nehru-jacket-in-beige-mpe8",
          "strength": 0.00046061722708429296,
          "target": "printed-straight-cut-suit-in-blue-kfx1140"
        },
        {
          "source": "printed-straight-cut-suit-in-blue-kfx1140",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-straight-suit-in-red-kpz74"
        },
        {
          "source": "plain-cotton-straight-suit-in-red-kpz74",
          "strength": 0.00046061722708429296,
          "target": "satin-petticoat-in-fuchsia-uub76"
        },
        {
          "source": "satin-petticoat-in-fuchsia-uub76",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-cotton-combo-of-kurta-in-fuchsia-and-beige-tdr763"
        },
        {
          "source": "plain-rayon-cotton-combo-of-kurta-in-fuchsia-and-beige-tdr763",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-black-szma153"
        },
        {
          "source": "woven-art-silk-saree-in-black-szma153",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-punjabi-suit-in-indigo-blue-ktn324"
        },
        {
          "source": "block-printed-cotton-punjabi-suit-in-indigo-blue-ktn324",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-long-kurta-in-orange-tqz71"
        },
        {
          "source": "embroidered-georgette-long-kurta-in-orange-tqz71",
          "strength": 0.00046061722708429296,
          "target": "embroidered-abaya-style-art-silk-suit-in-grey-and-pink-kgf5093"
        },
        {
          "source": "embroidered-abaya-style-art-silk-suit-in-grey-and-pink-kgf5093",
          "strength": 0.00046061722708429296,
          "target": "woven-dupion-silk-blouse-in-black-utt616"
        },
        {
          "source": "woven-dupion-silk-blouse-in-black-utt616",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-silk-saree-in-black-shxa25"
        },
        {
          "source": "jamdani-cotton-silk-saree-in-black-shxa25",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-olive-green-swz235"
        },
        {
          "source": "banarasi-saree-in-olive-green-swz235",
          "strength": 0.00046061722708429296,
          "target": "embroidered-chiffon-tunic-in-blue-tnc88"
        },
        {
          "source": "embroidered-chiffon-tunic-in-blue-tnc88",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-kurta-set-in-maroon-mpc715"
        },
        {
          "source": "embroidered-dupion-silk-kurta-set-in-maroon-mpc715",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-wine-and-orange-spfa1788"
        },
        {
          "source": "embroidered-dupion-silk-kurta-set-in-maroon-mpc715",
          "strength": 0.00046061722708429296,
          "target": "woven-banarasi-silk-saree-in-coral-pink-stea494"
        },
        {
          "source": "embroidered-georgette-front-slit-kurta-in-violet-tbe269",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-adjustable-pair-of-anklet-jrl697"
        },
        {
          "source": "stone-studded-adjustable-pair-of-anklet-jrl697",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-kurta-in-black-tcd16"
        },
        {
          "source": "embroidered-georgette-kurta-in-black-tcd16",
          "strength": 0.00046061722708429296,
          "target": "printed-crepe-saree-in-beige-ssf4366"
        },
        {
          "source": "printed-crepe-saree-in-beige-ssf4366",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-saree-in-magenta-sfka778"
        },
        {
          "source": "woven-kanchipuram-silk-saree-in-purple-seh1462",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-lehenga-in-sky-blue-lxw348"
        },
        {
          "source": "embroidered-art-silk-lehenga-in-sky-blue-lxw348",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-light-green-sqfa30"
        },
        {
          "source": "printed-georgette-saree-in-light-green-sqfa30",
          "strength": 0.00046061722708429296,
          "target": "embellished-art-silk-saree-in-peach-ssva275"
        },
        {
          "source": "stone-studded-necklace-set-in-golden-jdw292",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-kurta-set-in-yellow-meu6"
        },
        {
          "source": "plain-cotton-kurta-set-in-yellow-meu6",
          "strength": 0.00046061722708429296,
          "target": "woven-terry-rayon-jacquard-jodhpuri-suit-in-blue-mhg756"
        },
        {
          "source": "embroidered-cotton-linen-kurta-set-in-lilac-tzy73",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-linen-kurti-set-in-lilac-tzy73"
        },
        {
          "source": "pearl-earring-jjr15106",
          "strength": 0.00046061722708429296,
          "target": "velvet-blouse-in-dark-green-dbu400"
        },
        {
          "source": "velvet-blouse-in-dark-green-dbu400",
          "strength": 0.00046061722708429296,
          "target": "plain-cotton-silk-churidar-in-light-green-thu1584"
        },
        {
          "source": "plain-cotton-silk-churidar-in-light-green-thu1584",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-saree-in-coral-red-ssf3782"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-coral-red-ssf3782",
          "strength": 0.00046061722708429296,
          "target": "handloom-khadi-cotton-saree-in-brown-sqvd64"
        },
        {
          "source": "woven-banarasi-silk-saree-in-red-sbta498",
          "strength": 0.00046061722708429296,
          "target": "pintucked-denim-kurta-in-dark-blue-tcz301"
        },
        {
          "source": "pintucked-denim-kurta-in-dark-blue-tcz301",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-fuchsia-sgka3095"
        },
        {
          "source": "kanchipuram-saree-in-fuchsia-sgka3095",
          "strength": 0.00046061722708429296,
          "target": "embroidered-poly-georgette-tunic-in-black-tap23"
        },
        {
          "source": "embroidered-poly-georgette-tunic-in-black-tap23",
          "strength": 0.00046061722708429296,
          "target": "plain-art-silk-saree-in-turquoise-sfva20"
        },
        {
          "source": "printed-cotton-straight-suit-in-white-kfx2371",
          "strength": 0.00046061722708429296,
          "target": "woven-pure-chanderi-silk-saree-in-neon-green-skba274"
        },
        {
          "source": "woven-pure-chanderi-silk-saree-in-neon-green-skba274",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-in-off-white-tnc926"
        },
        {
          "source": "embroidered-cotton-kurta-in-off-white-tnc926",
          "strength": 0.00046061722708429296,
          "target": "plain-chiffon-jacquard-saree-in-red-sgya102"
        },
        {
          "source": "plain-chiffon-jacquard-saree-in-red-sgya102",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-pakistani-suit-in-white-and-black-knf369"
        },
        {
          "source": "woven-pure-mysore-silk-saree-in-teal-green-sbra956",
          "strength": 0.00046061722708429296,
          "target": "tie-dyed-georgette-saree-in-shaded-brown-and-rust-sjra242"
        },
        {
          "source": "half-n-half-chiffon-saree-in-red-and-peach-spta280",
          "strength": 0.00046061722708429296,
          "target": "two-part-pure-crepe-saree-in-mustard-and-maroon-sjn7063"
        },
        {
          "source": "two-part-pure-crepe-saree-in-mustard-and-maroon-sjn7063",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-cotton-dupatta-in-peach-orange-bmb33"
        },
        {
          "source": "woven-chanderi-cotton-dupatta-in-peach-orange-bmb33",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-beige-and-red-szma114"
        },
        {
          "source": "half-n-half-art-silk-saree-in-beige-and-red-szma114",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-saree-in-beige-smda704"
        },
        {
          "source": "embroidered-jacquard-viscose-saree-in-brown-sas1084",
          "strength": 0.00046061722708429296,
          "target": "embroidered-border-satin-saree-in-ombre-beige-and-fuchsia-szg655"
        },
        {
          "source": "printed-cotton-straight-cut-suit-in-yellow-and-white-kfx1972",
          "strength": 0.00046061722708429296,
          "target": "stone-studded-hasli-choker-set-jvk1754"
        },
        {
          "source": "stone-studded-choker-necklace-set-jmy246",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-linen-saree-in-white-sgpn331"
        },
        {
          "source": "jamdani-cotton-linen-saree-in-white-sgpn331",
          "strength": 0.00046061722708429296,
          "target": "hand-printed-half-n-half-pure-silk-saree-in-black-and-red-sffa45"
        },
        {
          "source": "hand-printed-half-n-half-pure-silk-saree-in-black-and-red-sffa45",
          "strength": 0.00046061722708429296,
          "target": "pure-muga-silk-kurta-set-in-olive-green-mrg253"
        },
        {
          "source": "woven-art-silk-jacquard-saree-in-teal-green-sgja423",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-abaya-style-net-suit-in-pink-kvl63"
        },
        {
          "source": "hand-embroidered-abaya-style-net-suit-in-pink-kvl63",
          "strength": 0.00046061722708429296,
          "target": "woven-chanderi-saree-in-fuchsia-sfka278"
        },
        {
          "source": "embroidered-net-lehenga-in-grey-lsh157",
          "strength": 0.00046061722708429296,
          "target": "embroidered-yoke-taffeta-abaya-style-suit-in-blue-kes301"
        },
        {
          "source": "embroidered-yoke-taffeta-abaya-style-suit-in-blue-kes301",
          "strength": 0.00046061722708429296,
          "target": "handloom-art-silk-dupatta-in-indigo-blue-bbe32"
        },
        {
          "source": "handloom-art-silk-dupatta-in-indigo-blue-bbe32",
          "strength": 0.00046061722708429296,
          "target": "embroidered-dupion-silk-blouse-in-green-uyp57"
        },
        {
          "source": "embroidered-dupion-silk-blouse-in-green-uyp57",
          "strength": 0.00046061722708429296,
          "target": "printed-georgette-straight-suit-in-white-kthb142"
        },
        {
          "source": "digital-printed-cotton-straight-suit-in-fuchsia-kexm93",
          "strength": 0.00046061722708429296,
          "target": "banarasi-saree-in-fuchsia-snea920"
        },
        {
          "source": "embroidered-art-dupion-silk-kurta-set-in-off-white-mpw101",
          "strength": 0.00046061722708429296,
          "target": "pure-satin-silk-banarasi-saree-in-maroon-snea892"
        },
        {
          "source": "pure-satin-silk-banarasi-saree-in-maroon-snea892",
          "strength": 0.00046061722708429296,
          "target": "pure-mysore-silk-saree-in-orange-shu620"
        },
        {
          "source": "pure-satin-silk-banarasi-saree-in-maroon-snea892",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-handloom-saree-in-green-snea687"
        },
        {
          "source": "embroidered-georgette-gown-in-red-taz1",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-mustard-sew4834"
        },
        {
          "source": "woven-art-silk-saree-in-mustard-sew4834",
          "strength": 0.00046061722708429296,
          "target": "pure-satin-silk-banarasi-saree-in-maroon-snea892"
        },
        {
          "source": "embroidered-art-silk-handloom-saree-in-green-snea687",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-black-snba1149"
        },
        {
          "source": "woven-art-silk-saree-in-black-snba1149",
          "strength": 0.00046061722708429296,
          "target": "embroidered-cotton-kurta-set-in-beige-mqz16"
        },
        {
          "source": "printed-cotton-kurta-set-in-white-and-blue-mtr107",
          "strength": 0.00046061722708429296,
          "target": "printed-rayon-top-in-white-tvt101"
        },
        {
          "source": "printed-rayon-top-in-white-tvt101",
          "strength": 0.00046061722708429296,
          "target": "leheriya-printed-kota-silk-saree-in-teal-green-sqta168"
        },
        {
          "source": "embroidered-satin-saree-in-peach-sppa32",
          "strength": 0.00046061722708429296,
          "target": "kerala-kasavu-hand-painted-cotton-saree-in-white-sfya36"
        },
        {
          "source": "kerala-kasavu-hand-painted-cotton-saree-in-white-sfya36",
          "strength": 0.00046061722708429296,
          "target": "woven-viscose-georgette-jacquard-lehenga-in-off-white-luf1155"
        },
        {
          "source": "woven-viscose-georgette-jacquard-lehenga-in-off-white-luf1155",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-straight-suit-in-maroon-kexm83"
        },
        {
          "source": "printed-cotton-straight-suit-in-maroon-kexm83",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-muslin-lehenga-in-coral-red-ljz83"
        },
        {
          "source": "hand-embroidered-muslin-lehenga-in-coral-red-ljz83",
          "strength": 0.00046061722708429296,
          "target": "pure-tussar-silk-banarasi-saree-in-fuchsia-snea229"
        },
        {
          "source": "plain-cotton-silk-nehru-jacket-in-blue-mhg563",
          "strength": 0.00046061722708429296,
          "target": "plain-rayon-kurti-in-tdr657"
        },
        {
          "source": "plain-rayon-kurti-in-tdr657",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-saree-in-white-sfya9"
        },
        {
          "source": "block-printed-cotton-saree-in-white-sfya9",
          "strength": 0.00046061722708429296,
          "target": "jamdani-cotton-saree-in-yellow-shxa243"
        },
        {
          "source": "jamdani-cotton-saree-in-yellow-shxa243",
          "strength": 0.00046061722708429296,
          "target": "banarasi-pure-silk-saree-in-fuchsia-snea1309"
        },
        {
          "source": "jamdani-cotton-saree-in-yellow-shxa243",
          "strength": 0.00046061722708429296,
          "target": "ikkat-printed-cotton-saree-in-black-scfa315"
        },
        {
          "source": "embroidered-chanderi-cotton-straight-suit-in-beige-kfx1818",
          "strength": 0.00046061722708429296,
          "target": "plain-georgette-a-line-suit-in-purple-kjn2539"
        },
        {
          "source": "ikkat-printed-cotton-saree-in-black-scfa315",
          "strength": 0.00046061722708429296,
          "target": "bandhej-crepe-saree-in-black-sjn7073"
        },
        {
          "source": "brocade-blouse-in-gold-dbu617",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-lehenga-in-beige-ljn1080"
        },
        {
          "source": "embroidered-net-lehenga-in-beige-ljn1080",
          "strength": 0.00046061722708429296,
          "target": "woven-dupion-silk-blouse-in-fuchsia-utt619"
        },
        {
          "source": "woven-dupion-silk-blouse-in-fuchsia-utt619",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-pink-seh1808"
        },
        {
          "source": "jute-silk-nehru-jacket-in-white-mpe21",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-cotton-silk-pakistani-suit-in-light-blue-kyk58"
        },
        {
          "source": "hand-embroidered-cotton-silk-pakistani-suit-in-light-blue-kyk58",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-purple-and-beige-suf7414"
        },
        {
          "source": "embroidered-cotton-rayon-kurta-in-turquoise-tyq58",
          "strength": 0.00046061722708429296,
          "target": "kanchipuram-saree-in-neon-green-skra1060"
        },
        {
          "source": "embroidered-half-n-half-art-silk-saree-in-maroon-and-blue-sew3188",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-jacquard-lehenga-in-red-lqu508"
        },
        {
          "source": "woven-art-silk-jacquard-lehenga-in-red-lqu508",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-satin-saree-in-shaded-peach-and-black-sau2018"
        },
        {
          "source": "half-n-half-satin-saree-in-shaded-peach-and-black-sau2018",
          "strength": 0.00046061722708429296,
          "target": "plain-dupion-silk-blouse-in-red-utt705"
        },
        {
          "source": "plain-dupion-silk-blouse-in-red-utt705",
          "strength": 0.00046061722708429296,
          "target": "handloom-cotton-silk-jamdani-saree-in-beige-ssna62"
        },
        {
          "source": "plain-rayon-cotton-a-line-kurta-in-fuchsia-tdr685",
          "strength": 0.00046061722708429296,
          "target": "embroidered-satin-georgette-abaya-style-suit-in-red-kch1172"
        },
        {
          "source": "woven-velvet-sherwani-in-navy-blue-mhg507",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-saree-in-black-skpa889"
        },
        {
          "source": "embroidered-georgette-saree-in-black-skpa889",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-and-art-silk-anarkali-suit-in-peach-kjy899"
        },
        {
          "source": "embroidered-net-and-art-silk-anarkali-suit-in-peach-kjy899",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-lehenga-style-gown-in-orange-and-green-tsp101"
        },
        {
          "source": "embroidered-cotton-straight-suit-in-orange-kye811",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-abaya-style-suit-in-beige-kch543"
        },
        {
          "source": "embroidered-net-abaya-style-suit-in-beige-kch543",
          "strength": 0.00046061722708429296,
          "target": "embroidered-georgette-pakistani-suit-in-red-kvu17"
        },
        {
          "source": "georgette-saree-with-embroidered-blouse-in-green-scga35",
          "strength": 0.00046061722708429296,
          "target": "pure-kanchipuram-silk-saree-in-dark-green-sgka2987"
        },
        {
          "source": "block-printed-georgette-kurta-in-off-white-thu1037",
          "strength": 0.00046061722708429296,
          "target": "printed-chiffon-saree-in-red-ssf4758"
        },
        {
          "source": "mysore-chiffon-saree-in-dark-blue-sbra1091",
          "strength": 0.00046061722708429296,
          "target": "woven-art-silk-saree-in-light-green-swz399"
        },
        {
          "source": "woven-art-silk-saree-in-light-green-swz399",
          "strength": 0.00046061722708429296,
          "target": "kerala-kasavu-pure-cotton-saree-in-off-white-spn2518"
        },
        {
          "source": "kerala-kasavu-pure-cotton-saree-in-off-white-spn2518",
          "strength": 0.00046061722708429296,
          "target": "kerala-kasavu-pure-cotton-saree-in-off-white-spn2183"
        },
        {
          "source": "kerala-kasavu-pure-cotton-saree-in-off-white-spn2183",
          "strength": 0.00046061722708429296,
          "target": "block-print-pure-georgette-saree-in-mustard-sjn7119"
        },
        {
          "source": "handloom-cotton-silk-saree-in-maroon-srga786",
          "strength": 0.00046061722708429296,
          "target": "bandhej-crepe-saree-in-maroon-sjn7093"
        },
        {
          "source": "bandhej-crepe-saree-in-maroon-sjn7093",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-net-saree-in-royal-blue-sar825"
        },
        {
          "source": "hand-embroidered-net-saree-in-royal-blue-sar825",
          "strength": 0.00046061722708429296,
          "target": "embroidered-beige-art-silk-kurta-set-mcd2852"
        },
        {
          "source": "embroidered-chiffon-saree-in-dusty-blue-spta32",
          "strength": 0.00046061722708429296,
          "target": "block-printed-cotton-viscose-layered-kurta-in-beige-trb553"
        },
        {
          "source": "embroidered-chiffon-saree-in-off-white-saqv5",
          "strength": 0.00046061722708429296,
          "target": "embroidered-art-silk-anarkali-suit-in-brown-kym373"
        },
        {
          "source": "banarasi-saree-in-blue-and-golden-swz193",
          "strength": 0.00046061722708429296,
          "target": "printed-cotton-punjabi-suit-in-royal-blue-khbz205"
        },
        {
          "source": "printed-cotton-punjabi-suit-in-royal-blue-khbz205",
          "strength": 0.00046061722708429296,
          "target": "half-n-half-art-silk-saree-in-purple-and-pink-spfa2026"
        },
        {
          "source": "half-n-half-art-silk-saree-in-purple-and-pink-spfa2026",
          "strength": 0.00046061722708429296,
          "target": "digital-print-crepe-saree-in-orange-ssf4275"
        },
        {
          "source": "digital-print-crepe-saree-in-orange-ssf4275",
          "strength": 0.00046061722708429296,
          "target": "hand-embroidered-georgette-saree-in-coral-red-sar939"
        },
        {
          "source": "stone-studded-earrings-jjr15193",
          "strength": 0.00046061722708429296,
          "target": "embroidered-taffeta-abaya-style-suit-in-black-kch281"
        },
        {
          "source": "embroidered-rayon-cotton-kurta-in-black-tmw173",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-beige-sswa272"
        },
        {
          "source": "embroidered-rayon-kurta-in-green-tdd1542",
          "strength": 0.00046061722708429296,
          "target": "handloom-tant-cotton-saree-in-red-stla295"
        },
        {
          "source": "hand-embroidered-satin-saree-in-navy-blue-ssva332",
          "strength": 0.00046061722708429296,
          "target": "embroidered-net-a-line-lehenga-in-red-lyn4"
        }
      ]
    }
}

export default POP_CONFIG;    