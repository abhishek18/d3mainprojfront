import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopingDirectedGraphComponent } from './poping-directed-graph.component';

describe('PopingDirectedGraphComponent', () => {
  let component: PopingDirectedGraphComponent;
  let fixture: ComponentFixture<PopingDirectedGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopingDirectedGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopingDirectedGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
