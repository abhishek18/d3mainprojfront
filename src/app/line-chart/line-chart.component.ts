import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as d3 from 'd3';
import BAR_CONFIG from './bar.config';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  @ViewChild('sidebarChartContainer') private chartContainer: ElementRef;

  axisMargin:number=20;
  valueMargin:number=4;
  width:any;
  height:any;
  hostElement:any;
  barHeight:any;
  barPadding:any;
  div:any;
  svg:any;
  bar:any;
  scale:any=0;
  margin: { top: number; right: number; bottom: number; left: number; };
  max: number;
  labelWidth:any=0;
  x: d3.ScaleLinear<number, number>;
  y: d3.ScaleBand<string>;
  charts: any;
  border: any=50;
  yScale: d3.ScaleLinear<number, number>;
  xScale: d3.ScaleBand<string>;
  chartList: string[];
  yAxis: d3.ScaleBand<string>;
  barGraph: any;
  xAxis: d3.ScaleLinear<number, number>;
  tooltip: any;
  vertical:any;
  mousex:any;
  textElements: any;

  constructor(
    private elementRef: ElementRef
  ) { 
        this.chartList = ["SideBar","Second","Third"]
  }

  ngOnInit() {
    //this.createChart();
    this.hostElement = this.chartContainer.nativeElement;
      console.log("this.hostElement >> ",this.hostElement);

      this.margin = {top: 20, right: 20, bottom: 30, left: 40},
      this.width = 1100 - this.margin.left - this.margin.right,
      this.height = 500 - this.margin.top - this.margin.bottom;
     //this.createAnimatedChart();
  }
  filterCharts(value){
    console.log("filterCharts >> ",value);
    switch (value) {
      case 'SideBar':
          console.log("Selected Case Number is SideBar");
          this.createChart();
          break;
      case 'Second':
          console.log("Selected Case Number is Second");
          this.createAnimatedChart();
          break;
      case 'Third':
          console.log("Selected Case Number is Third");
          this.createHorizontalChart();
          break;
      default:
          console.log("Selected Case Number is default");
          break;

  }
  }
  createChart(){
    
    /*this.div = d3.select("body").append("div").attr("class", "toolTip");

    this.border = { top: 1, right: 40, bottom: 1, left: 1};
    //this.margin=40;

      
      console.log("this.this.height >> ",this.height);
      console.log("this.margin >> ",this.margin); 
      this.barHeight = (this.height-this.axisMargin-this.margin*2)* 0.4/BAR_CONFIG.barData.length
      this.barPadding = (this.height-this.axisMargin-this.margin*2)*0.6/BAR_CONFIG.barData.length
      //bar, svg, scale, xAxis, labelWidth = 0;
      console.log("this.barHeight >> ",this.barHeight); 
      
      this.max = d3.max(BAR_CONFIG.barData, (d) => { return d.value; });

      //----------SVG creation----------------------//
        this.svg = d3.select(this.hostElement)
                    .append('svg')
                    .attr("class", "svg-graph")
                    .attr('width', this.width)
                    .attr('height', this.height)
                    .attr("style", "background: rgb(211,211,211)")
    //----------Bar creation add value----------------------//
        this.bar = this.svg.selectAll("g")
                    .data(BAR_CONFIG.barData)
                    .enter()
                    .append("g");
        console.log("this.bar >> ",this.bar);            
    //-----------Rectangle Creation-------------------------//
    this.bar.attr("class", "bar")
            .attr("cx",0)
            .attr("transform", (d, i) => {
              console.log("this.margin >> ",this.margin,",     ",i); 
              return "translate(" + this.margin + "," + (i * (this.barHeight + this.barPadding) + this.barPadding) + ")";
            });
    //-------------Add Text---------------------//
      this.bar.append("text")
            .attr("class", "label")
            .attr("y", this.barHeight / 2)
            .attr("dy", ".35em") //vertical align middle
            .text((d) => {
                return d.label;
            }).each(function() {
                this.labelWidth = Math.ceil(Math.max(this.labelWidth, this.getBBox().width));
            });      

      this.scale = d3.scaleLinear()
                    .domain([0, this.max])
                    .range([0, this.width - this.margin*2 - this.labelWidth])
     
      this.xAxis = d3.svg.axis()
            .scale(this.scale)
            .tickSize(-this.height + 2*this.margin + this.axisMargin)
            .orient("bottom");*/
      

      /*this.width = parseInt(d3.select(this.hostElement).style('width'),10)
      this.height = parseInt(d3.select('body').style('height'), 10)
      this.margin = { top: 1, right: 40, bottom: 1, left: 1};*/
      d3.select("svg").remove();
        // set the ranges
          this.y = d3.scaleBand()
                      .range([this.height, 0])
                      .padding(0.1);

          this.x = d3.scaleLinear()
                      .range([0, this.width]);
          // append the svg object to the body of the page
          // append a 'group' element to 'svg'
          // moves the 'group' element to the top left margin
          this.svg = d3.select(this.hostElement)
                    .append("svg")
                    .attr("width", this.width )
                    .attr("height", this.height)
                    .attr("style", "background: rgb(211,211,211)")
                    .append("g")
                    //.attr("transform","translate(" + this.margin.left + "," + this.margin.top + ")")
                    .attr("transform","translate(" +250+"," +5 + ")");
           // format the data
           BAR_CONFIG.barData.reverse().forEach((d) => {
              d.value = +d.value;
            }); 
            
             // Scale the range of the data in the domains
             this.x.domain([0, d3.max(BAR_CONFIG.barData, (d)=>{ return d.value; })])
             this.y.domain(BAR_CONFIG.barData.map((d) => { return d.label; }));
            //y.domain([0, d3.max(data, function(d) { return d.sales; })]);

            // append the rectangles for the bar chart
            this.svg.append("g")
            .attr("fill", "steelblue").selectAll("rect")
                .data(BAR_CONFIG.barData)
              .enter().append("rect")
                //.attr("class", "bar")
                //.attr("x", function(d) { return x(d.sales); })
               /*.attr("width", function(d) {return x(d.value); } )
                .attr("y", function(d) { return (d.label); })
                .attr("height", this.y.bandwidth());*/
                .attr("x", this.x(0))
                .attr("y", d => this.y(d.label)+13)
                .attr("width", d => this.x(d.value) - this.x(10))
                .attr("height", 15);

            // add the x Axis
            this.svg.append("g")
               .attr("transform", "translate("+ this.width +"," + this.height + ")")
                .call(d3.axisBottom(this.x));

            // add the y Axis
            this.svg.append("g")
                .call(d3.axisLeft(this.y));
        
    }  
     createAnimatedChart(){
      d3.select("svg").remove();

        console.log("this.border >> ",this.border); 
        this.svg = d3.select(this.hostElement)
                    .append("svg")
                    .attr("width", this.width )
                    .attr("height", this.height)
                    .attr("style", "background: rgb(255,215,0)")
        this.charts = this.svg.append('g')
                        .attr('transform', "translate("+60+", "+40+")");

        this.yScale = d3.scaleLinear()
                        .range([this.height-70, 0])
                        .domain([0, d3.max(BAR_CONFIG.barData, (d)=>{ return d.value*1.2; })]);
        this.charts.append('g')
                    .call(d3.axisLeft(this.yScale));  
        this.xScale = d3.scaleBand()
                    .range([0, this.width-200])
                    .domain(BAR_CONFIG.barData.map((s) => s.label))
                    .padding(0.2)
                
        this.charts.append('g')
                    .attr('transform', "translate(0, "+(this.height-70)+")")
                    .call(d3.axisBottom(this.xScale));
        this.charts.selectAll()
                  .data(BAR_CONFIG.barData)
                  .enter()
                  .append('rect')
                  .attr('x', (s) => this.xScale(s.label))
                  //.attr('x', (actual, index, array) => this.xScale(actual.value)) 
                  .attr('y', (s) => this.yScale(s.value))
                  .attr('height', (s) => (this.height -70) - this.yScale(s.value))
                  .attr('width', this.xScale.bandwidth())
                               

     }  
     createHorizontalChart(){
      d3.select("svg").remove();
      this.svg = d3.select(this.hostElement)
                    .append("svg")
                    .attr("width", this.width )
                    .attr("height", this.height)
                    .attr("style", "background: rgb(161,170,239)")//rgb(220,220,220)

      //----------Lebel of the Axis------------------//  
      this.svg.append('text')
                    .attr('x', -(this.height / 2) - this.border)
                    .attr('y', this.border / 2.4)
                    .attr('transform', 'rotate(-90)')
                    .attr('text-anchor', 'middle')
                    .text('<---------Item Name( Top 10)---------->')

      this.svg.append('text')
                    .attr('x', this.width / 2 + this.border)
                    .attr('y', 30)
                    .attr('text-anchor', 'middle')
                    .text('<----Number of Click/View------->')
        //----------Space allocation for Graph and axis------//            
        this.barGraph = this.svg.append('g')
                        .attr('transform', "translate("+400+", "+40+")");
        let data = BAR_CONFIG.barData.reverse();                

        //------------set the ranges of yaxis-----------// 
        this.yAxis = d3.scaleBand()
                      .range([this.height-60,10])
                      .domain(data.map((s) => s.label))
                      .padding(0.2)
        //------------draw the yaxis-----------//               
        this.barGraph.append('g')
                      .attr('transform', "translate(0, 0)")
                      .style("font-size",15)
                      .style("word-wrap","break-word")
                      .call(d3.axisLeft(this.yAxis)); 
        //------------set the ranges of x-axis-----------// 
        this.xAxis = d3.scaleLinear()
                 .range([ 0, this.width-440])
                 .domain([0, d3.max(data, (d)=>{ return d.value*1.25; })]);  
        //----------------draw the x-axis------------------// 
        this.barGraph.append('g')
                    //.attr('transform', "translate(0, "+(this.height-60)+")")
                    .call(d3.axisBottom(this.xAxis));

      //----------draw the bar according to value---------------//
        this.barGraph.selectAll()
                  .data(data)
                  .enter()
                  .append('rect')
                  .attr('x', (s) => this.xAxis(0))
                  //.attr('x', (actual, index, array) => this.xScale(actual.value)) 
                  .attr('y', (s) => this.yAxis(s.label))
                  .attr('height', this.yAxis.bandwidth())
                  .attr('width', (s) => (this.xAxis(s.value)))
                  .style("fill", "rgb(6,42,239)")//#373737
                  
                  .on('mouseenter', function(d) {
                    console.log("nodeObj >>",JSON.stringify(d),"     ",d3.event); 
                    //this.barGraph.attr('width', (s) => (this.xAxis(s.value)+10))
                    d3.select(this).attr("opacity", 0.4)
                    this.tooltip = d3.select("body")
                                  .append("div")
                                  .attr("class", "tooltip")
                                  .style("opacity", 0);
                    this.tooltip.transition()
                                .duration(300)
                                .style("opacity", 0.9);
                      this.tooltip.html(" <div style='width: 170px;margin-right: 10px;padding: 3px;display: inline-block;height: 80px;position: absolute;background: rgb(250,250,210);border-radius: 12px;border: 2px;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 13px;'> "+
                                            " <div class='left'> <b >Name:</b>" + d.label +"<br><b>Value:</b>" + d.value+"<br><span style='font-size:10px;padding: 10px;'>("+d3.event.pageX+","+d3.event.pageY+")</span>" +
                                            " </div> " +
                                        " </div>")
                
                      .style("left", (d3.event.pageX ) + "px")
                      .style("top", (d3.event.pageY) + "px")
                      .style("position","absolute")
                    //console.log("this.tooltip >>",this.tooltip);   
                        
                  })
                  .on('mouseover', function(d){
                    
                    this.vertical = d3.select("body")
                                    .append("line")
                                    .attr("class", "remove")
                                    .style("position", "absolute")
                                    .style("z-index", "19")
                                    .style("width", "2px")
                                    .style("height", "410px")
                                    //.style("bottom", "30px")
                                    .style("background", "rgb(250,250,210)"); //rgb(255,0,0)
                       let mousex1 = d3.mouse(this);
                       this.mousex = mousex1[0] + 5;
                       console.log("d3.event.pageX >> ",d3.event.pageX,"   ",this.height);
                       this.vertical.style("left", d3.event.pageX + "px") 
                       .style("top",   "210px")
                       //.style("position","absolute") 
                })
                 .on('mouseleave', function (actual, i){
                  console.log("mouseleave >>"); 
                  
                  this.vertical.style("visibility", "hidden").style("opacity", "0"); 
                 })
                  .on('mouseout', function (actual, i) {
                    console.log("mouseout >>");                   
                    this.tooltip.style("visibility", "hidden"); 
                    this.vertical.style("visibility", "hidden").style("opacity", "0"); 
                          d3.select(this).attr("opacity", 1)})
                          
      //---------Making a grid view------------//
        this.barGraph.append('g')
                  .attr('class', 'grid')
                  .attr('transform', "translate(0, "+(this.height-60)+")")
                  .call(d3.axisBottom(this.xAxis)
                      .tickSize(-this.height+75))
                      //.tickFormat(''))
        
        //---------Write value on bar------------//
        this.barGraph.selectAll()
                    .data(data)
                    .enter()
                    .append("text")
                    .attr("class", "below")
                    .attr("x", (s) => (this.xAxis(s.value)-this.margin.bottom))
                    .attr("y", (s) => this.yAxis(s.label))
                    .attr("dy", "1.2em")
                    .attr("text-anchor", "right")
                    .text((d) =>{ return d.value; })
                    .style("fill", "white");
                    
                 
     }   
}
