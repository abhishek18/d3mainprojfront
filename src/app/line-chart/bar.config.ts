const BAR_CONFIG = {
    barData : [
        {
          "value": 35,
          "label": "woven-brocade-silk-sherwani-in-beige-mgv131"
        },
        {
          "value": 33,
          "label": "cotton-settu-mundu-in-light-beige-spn2987"
        },
        {
          "value": 33,
          "label": "plain-crepe-top-in-black-thu763"
        },
        {
          "value": 31,
          "label": "embroidered-art-silk-lehenga-in-fuchsia-lyc184"
        },
        {
          "value": 24,
          "label": "embroidered-georgette-lehenga-in-old-rose-lcc121"
        },
        {
          "value": 17,
          "label": "embroidered-georgette-saree-in-red-szra409"
        },
        {
          "value": 15,
          "label": "hand-embroidered-net-saree-in-cream-sar885"
        },
        {
          "value": 15,
          "label": "digital-printed-satin-saree-in-light-pink-snba1203"
        },
        {
          "value": 14,
          "label": "embroidered-placket-cotton-pathani-suit-in-black-meu14"
        },
        {
          "value": 13,
          "label": "embroidered-satin-lehenga-in-red-lyc158"
        }
      ]
}

export default BAR_CONFIG;    