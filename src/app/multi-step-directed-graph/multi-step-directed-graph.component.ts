import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as d3 from 'd3';
import MULTI_CONFIG from './multi-step.config';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-multi-step-directed-graph',
  templateUrl: './multi-step-directed-graph.component.html',
  styleUrls: ['./multi-step-directed-graph.component.css']
})
export class MultiStepDirectedGraphComponent implements OnInit {

  @ViewChild('multiStepGraphContainer') private chartContainer: ElementRef;

  hostElement: any;
  margin: {
    top: number;right: number;bottom: number;left: number;
  };
  svg: any;
  svgWidth = 1150;
  svgHeight = 800;
  g: any;
  minZoom: any = 1 / 4;
  maxZoom: any = 8;
  simulation: any;
  nodeElements: any;
  linkForce: any;
  linkElements: any;
  textElements: any;
  linkedByIndex = {};
  tooltip: any;
  tooltipPredict: d3.Selection<d3.BaseType, {}, HTMLElement, any>;
  treeGraphData = [];


  constructor(
    private elementRef: ElementRef,
    public toastr: ToastrManager,
    private httpClient: HttpClient,
    private router: Router,
  ) {}

  ngOnInit() {

    this.hostElement = this.chartContainer.nativeElement;
    console.log("this.hostElement >> ", this.hostElement);
    this.margin = {
      top: 50,
      right: 20,
      bottom: 70,
      left: 40
    };

    this.createMultiStepGraph();
    /*let d = { "id":  "embroidered-art-silk-lehenga-in-fuchsia-lyc184"}
    this.cretatePath(d);*/
  }

  createMultiStepGraph() {

    d3.select("svg").remove(); 
    let innerWidth = this.svgWidth - this.margin.left - this.margin.right,
      innerHeight = this.svgHeight - this.margin.top - this.margin.bottom,
      neighbourNode = [{ "id":"" }];
     // linksArray = [{}];


    //----------SVG creation----------------------//
    this.svg = d3.select(this.hostElement)
      .append('svg')
      .attr("class", "svg-graph")
      .attr('width', innerWidth)
      .attr('height', innerHeight)
      .attr("style", "background: rgb(74,69,69)")

    //----------Zoom SVG and Element-------------------//
    this.g = this.svg.append("g")
    /*.append('svg:path')
    .attr('d', 'M0,0 L0,10 L10,5 z'); //for arrow */
    this.svg.call(d3.zoom()
              .on("zoom", () => {this.g.attr("transform", d3.event.transform);})
              .scaleExtent([this.minZoom, this.maxZoom]))
    //---------------------------------------------//                  

    this.linkForce = d3.forceLink(MULTI_CONFIG.graph.links)
                        .id((link) => {return link['id']})
                        .strength(function (link) { return (link.strength * 200) })

    this.simulation = d3.forceSimulation()
      .nodes(MULTI_CONFIG.graph.nodes)
      .force('link', this.linkForce)
      .force('charge', d3.forceManyBody().strength(-8))
      .force('center', d3.forceCenter(innerWidth / 2, innerHeight / 2))

    //-----------Links Creation--------------------//
    this.linkElements = this.g.append("g")
      .attr("class", "links")
      .selectAll("line")
      .data(MULTI_CONFIG.graph.links)
      .enter().append("line")
      .attr("stroke-width", (l) => {
        return l.strength * 6000
      })
      .attr("stroke", '#DFE4E7') //"rgba(51,0,0)"
      .attr("stroke-opacity", 0.8)
     /* .style("opacity", function(o) {
        return o.source === d || o.target === d ? 1 : "green";}) 
      .forEach(function(d) {
          this.linkedByIndex[d.source.index + "," + d.target.index] = 1;
        }); */

    //----------NODE Creation------------------//
    this.nodeElements = this.g.append('g')
      .selectAll('circle')
      .data(MULTI_CONFIG.graph.nodes)
      .enter().append('circle')
      .attr('r', (d) => {
        let intens = d.viewIntensity;
        if (intens >= 1 && intens < 4) return 8;
        else if (intens >= 4 && intens < 9) return 11;
        else if (intens >= 9 && intens < 12) return 15;
        else if (intens >= 12 && intens <= 15) return 20;
        else if (intens > 16 ) return 27;
      })
      .attr('fill', this.getNodeColor)
      .style('stroke', function (d) {  return d3.color("#000080"); })
      .style('stroke-width',(d)=>{ return d.viewIntensity >20 ? '2px':'0.5px'})  

    //----------Add properties in NODE------------------//    
    this.nodeElements
      //-------------Get back to previous state by clicking outside--------------//                    
      .on('blur', () => {
        console.log("on blur >> ", this.nodeElements);
        this.nodeElements.attr('fill', this.getNodeColor).style('opacity', 1)
        this.linkElements.style("stroke", '#DFE4E7').style("stroke-opacity", 0.8)
      })
      //-----------Tooltip on mouse over Node---------------//                    
      .on('mouseover', function (d) {
        //console.log("nodeObj >>",JSON.stringify(d)); 
        this.tooltip = d3.select("body")
          .append("div")
          .attr("class", "tooltip")
          .style("opacity", 0)
          .style("-moz-animation", "cssAnimation 0s ease-in 2s forwards")
          .style("-o-animation", "cssAnimation 0s ease-in 2s forwards")
          .style("-webkit-animation", "cssAnimation 0s ease-in 2s forwards");
        this.tooltip.transition()
          .duration(300)
          .style("opacity", 0.8);
        this.tooltip.html(" <div style='width: 200px;margin-right: 10px;padding: 0px;display: inline-block;height: 300px;position: absolute;background: #FFFFFF;'> " +
            " <div class='left'> <b >Name:</b>" + d.id + "<br><b>Level:</b>" + d.label +
            " </div> " +
            " <div style='display:table-cell; vertical-align:middle; width:150px; margin:0;height: 250px padding:0;  '>" +
            "   <a target='_blank' href=" + "./saara-original.jpeg" + ">" +
            "     <img src='./saara-original.jpeg' alt='5Terre' width='200' height='250'>" +
            "   </a>" +
            " </div>" +
            " </div>")

          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY) + "px")
          .style("position", "absolute")
        //console.log("this.tooltip >>",this.tooltip);           
      })
      .on('mouseout', function (d) {
        //console.log("mouseout.tooltip >>",this.tooltip); 
        this.tooltip.style("visibility", "hidden");
      })
      //-----------Highlighting the clicked node-----------//
      .on('click', (d) => {
        
        console.log("clicked item >> ", JSON.stringify(d));
        
        let neighbors = [], neighbourNodes = [d.id], flag=false;
        MULTI_CONFIG.treeMap.forEach((element)=>{
          if(d.id === element.id) {flag=false;this.createPrimarytree(d)}
          else  flag=true;
          }) 
          if(flag) this.Error("Please choose the right node!!","Sorry !!");
          /*
          MULTI_CONFIG.multiStepGraph.forEach((element)=>{
          if(d.id === element.id) {flag=false;this.cretatePath(d)}
          else  flag=true;
          }) 
          if(flag) this.Error("Please choose the right node!!","Sorry !!");
          */ 

      })
      //----------Drag & Drop-----------------//
      .call(d3.drag()
        .on("start", (d) => {
          if (!d3.event.active) this.simulation.alphaTarget(0.08).restart();
          d['fx'] = d['x'];
          d['fy'] = d['y'];
        })
        .on("drag", (d) => {
          d['fx'] = d3.event.x;
          d['fy'] = d3.event.y;
        })
        .on("end", (d) => {
          if (!d3.event.active) this.simulation.alphaTarget(0);
          d['fx'] = null;
          d['fy'] = null;
        }))
    /*.style("opacity", function(o) {
                  return this.neighboring(d, o) ? 1 : "green";})   */
    //----------------------------------------------------//          
    this.textElements = this.g.append('g')
      .selectAll('text')
      .data(MULTI_CONFIG.graph.nodes)
      .enter().append('text')
      .text(node => node.label)
      .attr('font-size', 10)
      .attr('dx', -1)
      .attr('dy', 2)

     this.simulation.nodes(MULTI_CONFIG.graph.nodes).on('tick', () => {
      this.nodeElements
        .attr('cx', node => node.x)
        .attr('cy', node => node.y)
     this.textElements
        .attr('x', node => node.x)
        .attr('y', node => node.y)
      /*this.linkElements
        .attr('x1', link => link.source.x)
        .attr('y1', link => link.source.y)
        .attr('x2', link => link.target.x)
        .attr('y2', link => link.target.y)*/
      this.tooltip

    })
    MULTI_CONFIG.graph.links.forEach((d) => {
      this.linkedByIndex[d.source['index'] + "," + d.target['index']] = 1;
    });
    this.simulation.force("link").links(MULTI_CONFIG.graph.links)

  }
  getNodeColor(node, neighbors, flag) {

    let intens = node.viewIntensity;
    if (intens >= 1 && intens < 4) return '#FFE5E7';
    else if (intens >= 4 && intens < 9) return '#FF9A9E';
    else if (intens >= 9 && intens < 12) return '#FF4F56';
    else if (intens >= 12 && intens <= 15) return '#FF030D';
    else if (intens > 16 ) return '#cc0000';

    //return node.level = "rgb(255, 26, 26)";
  }
  getNeighbors(selectedNode) {
    return MULTI_CONFIG.graph.links.reduce((neighbors, link) => {
      //method calls the callbackfn function one time for each element in the array
      if (link.target['id'] === selectedNode.id) {
        neighbors.push(link.source['id'])
      } else if (link.source['id'] === selectedNode.id) {
        neighbors.push(link.target['id'])
      }
      //console.log(" neighbors >> ",JSON.stringify(neighbors));
      return neighbors;
    }, [selectedNode.id]) //it is used as the initial value to start the accumulation
  }

  cretatePath(d){
    d3.select("svg").remove(); 
    
    console.log("from calling  >>",JSON.stringify(d)); 
    this.Success('This is predicted path.', 'Success!') 
    let innerWidth = this.svgWidth - this.margin.left - this.margin.right,
      innerHeight = this.svgHeight - this.margin.top - this.margin.bottom;
     
    //----------SVG creation----------------------//
    this.svg = d3.select(this.hostElement)
      .append('svg')
      .attr("class", "svg-graph")
      .attr('width', innerWidth)
      .attr('height', innerHeight)
      .attr("style", "background: rgb(74,69,69)")
    //--------------------//
    let neighbors = [], neighbourNodes = [{"id": d.id}];
    console.log("upcoming cretatePath >> ", JSON.stringify(d));

        MULTI_CONFIG.multiStepGraph.forEach((element)=>{
          if(d.id === element.id){
            console.log("element.id inside neighbors loop >> ",neighbors);
            element.chart.forEach((chartObj)=>{
              neighbors.push({
                                "source":chartObj.source,
                                "strength":chartObj.strength,
                                "target":chartObj.target })
                                  })
                          } 
          }) 
        console.log("neighbors cretatePath >> ", JSON.stringify(neighbors), "  >> ", neighbors.indexOf(d.id));
        neighbors.forEach((element) => {
          console.log("element.target inside neighbourNodes loop >> ",neighbors);
          neighbourNodes.push({"id": element.target })
        })
        console.log("neighbourNodes cretatePath >> ", JSON.stringify(neighbourNodes), "  >> ", neighbourNodes.indexOf(d.id));
            let graphTemp = {
              "neighbourNodesTemp" : neighbourNodes,
              "neighborLink" : neighbors
            }
        console.log("graphTemp cretatePath >> ", JSON.stringify(graphTemp));
    //-----------------Zoom SVG and Element-------------------//
        this.g = this.svg.append("g")
        /*.append('svg:path')
        .attr('d', 'M0,0 L0,10 L10,5 z'); //for arrow */
        this.svg.call(d3.zoom()
                  .on("zoom", () => {this.g.attr("transform", d3.event.transform);})
                  .scaleExtent([this.minZoom, this.maxZoom]))
    //---------------------------------------------//                  

        let linkForce = d3.forceLink(graphTemp.neighborLink)
                            .id((link) => { return link['id'] })
                            .strength(function (link) { return (link.strength ) })

        this.simulation = d3.forceSimulation()
          .nodes(graphTemp.neighbourNodesTemp)
          .force('link', linkForce)
          .force('charge', d3.forceManyBody().strength(-180))
          .force('center', d3.forceCenter(innerWidth / 2, innerHeight / 2))
    //----------NODE Creation------------------//
      this.nodeElements = this.g.append('g')
                      .selectAll('circle')
                      .data(graphTemp.neighbourNodesTemp)
                      .enter().append('circle')
                      .attr('r', 20)
                      .attr('fill', 'white')
                      .style('stroke', function (d) {
                          return d3.color("#000080");
                        })
    //-----------Links Creation--------------------//
      this.linkElements = this.g.append("g")
                              .attr("class", "links")
                              .selectAll("line")
                              .data(graphTemp.neighborLink)
                              .enter().append("line")
                              .attr("stroke-width", (l) => { return l.strength*10  })
                              .attr("stroke", 'rgb(0,255,255)') //"rgba(51,0,0)"
                              .attr("stroke-opacity", 0.8)
    

    //----------Add properties in NODE------------------//    
      this.nodeElements
    //-----------Tooltip on mouse over Node---------------//                    
        .on('mouseover', function (d) {
          //console.log("nodeObj >>",JSON.stringify(d)); 
          this.tooltip = d3.select("body")
            .append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);
          this.tooltip.transition()
            .duration(100)
            .style("opacity", 0.8)
          this.tooltip.html(" <div style='width: 200px;margin-right: 10px;padding: 0px;display: inline-block;height: 300px;position: absolute;background: #FFFFFF;'> " +
              " <div class='left'> <b >Name:</b>" + d.id + 
              " </div> " +
              " <div style='display:table-cell; vertical-align:middle; width:150px; margin:0;height: 250px padding:0;  '>" +
              "   <a target='_blank' href=" + "./saara-original.jpeg" + ">" +
              "     <img src='./saara-original.jpeg' alt='5Terre' width='200' height='250'>" +
              "   </a>" +
              " </div>" +
              " </div>")

            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY) + "px")
            .style("position", "absolute")
          //console.log("this.tooltip >>",this.tooltip);           
        })
          .on('mouseout', function (d) {
          //console.log("mouseout.tooltip >>",this.tooltip); 
          this.tooltip.style("visibility", "hidden");
        })

      //-----------------tick---------------------//
        this.simulation.nodes(graphTemp.neighbourNodesTemp).on('tick', () => {
              this.nodeElements
                .attr('cx', node => node.x)
                .attr('cy', node => node.y)
             this.linkElements
                .attr('x1', link => link.source.x)
                .attr('y1', link => link.source.y)
                .attr('x2', link => link.target.x)
                .attr('y2', link => link.target.y)
              //this.tooltip
        
            })
        graphTemp.neighborLink.forEach((d) => {
          this.linkedByIndex[d.source['index'] + "," + d.target['index']] = 1;
        });
        this.simulation.force("link").links(graphTemp.neighborLink)

  }
  
  Success(title:string,message?:string){
    this.toastr.successToastr(title,message);
  }
  Warning(title:string,message?:string){
    this.toastr.warningToastr(message,title);
  }
  Error(message:string,title?:string){
    this.toastr.errorToastr(message,title);
  }
  Info(message:string){
    this.toastr.infoToastr(message);
  }

  createPrimarytree(selectNode){

    //window.location.reload(); 
    d3.select("svg").remove();
    this.Success('This is predicted path.', 'Success!') 
    let innerWidth = 950 - this.margin.left - this.margin.right,
      innerHeight = 500 - this.margin.top - this.margin.bottom,
      margin = { top: 20, right: 90, bottom: 30, left: 90 };
      

    //----------SVG creation----------------------//
    this.svg = d3.select(this.hostElement)
              .append('svg')
              .attr('width', innerWidth + margin.left + margin.right)
              .attr('height', innerHeight+ margin.top + margin.bottom)
              .attr("style", "background: rgb(238,238,238)")
             // .attr("style", "text-shadow: 0 0 20px black")

    console.log("upcoming createPrimarytree >> ", JSON.stringify(selectNode));

    MULTI_CONFIG.treeMap.forEach((element)=>{
      if(selectNode.id===element.id){ this.treeGraphData = element.chart }
    })
    console.log("treeGraphData List >> ", JSON.stringify(this.treeGraphData));   

    // convert the flat data into a hierarchy 
     let treeData  = d3.stratify()
                    .id((d)=> { return d['name']; })
                    .parentId((d) =>{ return d['parent']; })
                    (this.treeGraphData);

    // assign the name to each node
    treeData .each((d) =>{ d['name'] = d.id; }); 

    // declares a tree layout and assigns the size
      let treemap = d3.tree()
                      .size([innerHeight, innerWidth]),

      //  assigns the data to a hierarchy using parent-child relationships
      nodesData = d3.hierarchy(treeData, (d)=>{  return d.children; }),

      // maps the node data to the tree layout
      nodes = treemap(nodesData);

    //----------Zoom SVG and Element-------------------//
      this.g = this.svg.append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
      this.svg.call(d3.zoom()              
                    .on("zoom", ()=>{this.g.attr("transform", d3.event.transform);})
                    .scaleExtent([this.minZoom, this.maxZoom])) 

    //------------adds the links between the nodes------------//
      let link = this.g.selectAll(".link")
                  .data( nodes.descendants().slice(1.5))
                  .enter().append("path")
                  .attr("d", function(d) {
                    return "M" + d.y + "," + d.x
                      + "C" + (d.y + d.parent.y) / 2 + "," + d.x
                      + " " + (d.y + d.parent.y) / 2 + "," + d.parent.x
                      + " " + d.parent.y + "," + d.parent.x;  })                       
                      .attr('fill', "none")
                      .attr('stroke','#ccc')
                      .attr('stroke-width',(d)=>{  return d.data.data.strength*20; })

    // adds each node as a group
      let node = this.g.selectAll(".node")
                  .data(nodes.descendants())
                  .enter().append("g")
                  .attr("class", function(d) { 
                    //console.log("node obj >> ",d);
                    return "node" + 
                      (d.children ? " node--internal" : " node--leaf"); })
                  .attr("transform", function(d) { 
                    return "translate(" + d.y + "," + d.x + ")"; });

      // adds the circle to the node
      node.append("circle")
                .attr("r", this.getNodeRadius)
                .attr('fill', this.getTreeNodeColor)
                .attr('stroke', (d) =>{ return d3.color("#000080"); })
                .attr('stroke-width', "3px")
                //----------Drag & Drop-----------------//
                .call(d3.drag()
                .on("start", (d)=>{
                  if (!d3.event.active) this.g.alphaTarget(0.08).restart();
                          d['fx'] = d['x'];d['fy'] = d['y'];})
                .on("drag", (d)=>{
                  d['fx'] = d3.event.x;
                  d['fy'] = d3.event.y;})
                .on("end", (d)=>{
                            if (!d3.event.active) this.g.alphaTarget(0);
                            d['fx'] = null;
                            d['fy'] = null;}))
                .on('mouseover',(d)=>{
                  this.tooltipPredict = d3.select("body")
                                  .append("div")
                                  .attr("class", "tooltipPredict")
                                  .style("opacity", 0)
                                  .style("-moz-animation", "cssAnimation 0s ease-in 2s forwards")
                                  .style("-o-animation", "cssAnimation 0s ease-in 2s forwards")
                                  .style("-webkit-animation", "cssAnimation 0s ease-in 2s forwards");
                  this.tooltipPredict.transition()
                                  .duration(300)
                                  .style("opacity", 0.8);
                  this.tooltipPredict.html(" <div style='width: 200px;margin-right: 10px;padding: 0px;display: inline-block;height: 300px;position: absolute;background: #FFFFFF;'> " +
                      " <div class='left'> <b >Name:</b>" + d.data.data.name /*+ "<br><b>Level:</b>" + d.label */+
                      " </div> " +
                      " <div style='display:table-cell; vertical-align:middle; width:150px; margin:0;height: 250px padding:0;  '>" +
                      "   <a target='_blank' href=" + "./saara-original.jpeg" + ">" +
                      "     <img src='./saara-original.jpeg' alt='5Terre' width='200' height='250'>" +
                      "   </a>" +
                      " </div>" +
                      " </div>")
                  .style("left", (d3.event.pageX) + "px")
                  .style("top", (d3.event.pageY) + "px")
                  .style("position", "absolute") })

                .on("mouseout",(d)=>{
                  this.tooltipPredict.style("visibility", "hidden");
                })

      // adds the text to the node
      /*node.append("text")
                .attr("dy", ".35em")
                .attr("x", function(d) { return d.children ? -13 : 13; })
                .style("text-anchor", function(d) { 
                return d.children ? "end" : "start"; })
                .text(function(d) { return d.data.name; })
                .style('font', "12px sans-serif");*/

      
  }
  getTreeNodeColor(node) {
    
    let intens = node.data.data.strength;
    //console.log("intens >> ",intens);
      if(intens>=0.3 && intens<0.55) return '#FFE5E7';
      else if(intens>=0.55 && intens<0.7) return '#FF9A9E';
      else if(intens>=0.7 && intens<0.9) return '#FF4F56';
      else if(intens>=0.9 && intens<=1) return '#FF030D';
  }
  getNodeRadius(node){
    let intens = node.data.data.strength;
    //console.log("intens >> ",intens);
      if(intens>=0.3 && intens<0.55) return 11;
      else if(intens>=0.55 && intens<0.7) return 15;
      else if(intens>=0.7 && intens<0.9) return 20;
      else if(intens>=0.9 && intens<1) return 27;
      else if(intens==1) return 33;
  }
  getExcelData(data){
    console.log("clicked getExcelData >>",JSON.stringify(data));

  }
  
}
  