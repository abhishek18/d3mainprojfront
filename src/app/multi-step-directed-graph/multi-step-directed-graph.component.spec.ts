import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiStepDirectedGraphComponent } from './multi-step-directed-graph.component';

describe('MultiStepDirectedGraphComponent', () => {
  let component: MultiStepDirectedGraphComponent;
  let fixture: ComponentFixture<MultiStepDirectedGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiStepDirectedGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiStepDirectedGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
