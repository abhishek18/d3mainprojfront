import {Component,OnInit,ViewChild,ElementRef,ChangeDetectorRef} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Theme,  Item,  ResponseData} from '../interface';
import {  JavaService} from 'src/service_providers/service.status';
import {  ThemeItemProvider} from 'src/service_providers/themeItemService';
//import {AutoCompleteModule} from 'primeng/autocomplete';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css']
})
export class ThemeComponent implements OnInit {

  @ViewChild("tref", {read: ElementRef}) domEditor: ElementRef;
 
  item: Item = {};
  theme: Theme = {};
  themeList: Array<Theme> = [{}];
  viewModel: any={};
  viewModelList: Array<any> = [];
  results: string[];
  inputs = [{value: "first"}];

  constructor(
    private httpClient: HttpClient,
    private themeService: ThemeItemProvider,
    private service: JavaService,
    private cdr: ChangeDetectorRef,
  ) {
    this.getThemeItem();
    
  }

  ngOnInit() {
    
  }

  saveItemTheme() {
    console.log(JSON.stringify(this.theme), 'SUCCESS!! :-)\n\n', JSON.stringify(this.item));

    return new Promise((resolve, reject) => {
      let themeObj: Theme = {};
      themeObj = {
        "themeId": 0,
        "themeName": this.theme.themeName,
        "themeDesc": this.theme.themeDesc,
        "itemList": []
      }
      let toArray = [];
      toArray = this.item.itemNameList.split("\n");
      console.log("after separate >> ", JSON.stringify(toArray));

      toArray.forEach((toarrObj) => {
        if (toarrObj != "" && null != toarrObj) {
          console.log("toarrObj element >> ", JSON.stringify(toarrObj));
          themeObj.itemList.push({
                                  itemId: 0,
                                  itemName: toarrObj,
                                });
        }
      })
      console.log("this.theme.itemList >> ", JSON.stringify(themeObj.itemList));
      this.themeService.saveThemeItem(themeObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if (err) {
            console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {
            alert(data.responseMsg);
            this.theme.themeDesc = "";
            this.item.itemNameList = "";
            this.getThemeItem();
          } else {
            console.log("Oops!! Data save failed");
          }
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }

  getThemeItem(){
    return new Promise((resolve, reject) => {
      this.themeService.getAllThemeItem().subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in getText >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {     
            this.themeList = data.obj;  
            this.viewModelList = [];
            console.log("this.themeList >> ",JSON.stringify(this.themeList)); 
            this.themeList.forEach((element)=>{
              element.itemList.forEach((itemObj)=>{
                this.viewModel = {};
                this.viewModel.themeId = element.themeId
                this.viewModel.themeName = element.themeName
                this.viewModel.themeDesc = element.themeDesc
                this.viewModel.itemId = itemObj.itemId
                this.viewModel.itemName = itemObj.itemName
                this.viewModelList.push(this.viewModel);
              })
            });
            console.log("this.viewModelList >> ",JSON.stringify(this.viewModelList)); 
            this.cdr.detectChanges();
            //data.obj.forEach((element)=>{});
          } else {
            console.log("Oops!! Data fetch failed");
          } 
        })
      }, error => {
        console.log("error in Big fetch >> ");
      });
    });
  }
  search(event) {
   
    this.themeList.forEach((element)=>{
      this.results.push(element.themeDesc);
    });
  }
  
  addInput()  {
    if(this.themeList){

    }
    this.themeList.push({themeDesc: ''});
  }
  deleteInput(){

  }
}
