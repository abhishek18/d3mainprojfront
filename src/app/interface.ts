export interface Theme {
    themeId ? : number,
    themeName ? : string,
    themeDesc ? : string,
    itemList ? : Array < Item > ,
}
export interface Item {
    itemId ? : number,
    itemName ? : string,
    itemNameList?:string,
    itemDesc?:string,
    themeList ? : Array < Theme > ,
}
export interface ResponseData{
    responseStatus?:string,
    responseCode?:string,
    responseObj?:string,
    responseUidUserType?:string,
    responseMsg?:string,
    obj?: any
}