import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import { HttpClient} from '@angular/common/http';
import {  JavaService} from 'src/service_providers/service.status';
import {  ThemeItemProvider} from 'src/service_providers/themeItemService';
import { Theme, ResponseData } from '../interface';

@Component({
  selector: 'app-theme-create',
  templateUrl: './theme-create.component.html',
  styleUrls: ['./theme-create.component.css']
})
export class ThemeCreateComponent implements OnInit {

  themeObj: Theme={};
  paramTemp: Theme={};
  themeList: Array<Theme> = [{}];
  model: any={};
  constructor(
    private router: Router,
    private activRoute: ActivatedRoute,
    private httpClient: HttpClient,
    private themeService: ThemeItemProvider,
    private service: JavaService,
    private cdr: ChangeDetectorRef,
  ) { 
    this.getThemeItem();
  }

  ngOnInit() {
    this.activRoute.queryParams.subscribe((param)=>{ 
      console.log("before parse ThemeObj >> ",JSON.stringify(param.themeObj));   
      if(param.themeObj ){
        this.paramTemp = JSON.parse(param.themeObj);

        this.themeObj.themeId = this.paramTemp.themeId
        this.themeObj.themeName = this.paramTemp.themeName
        this.themeObj.themeDesc = this.paramTemp.themeDesc
        this.themeObj.itemList = this.paramTemp.itemList
        console.log("After parse ThemeObj >> ",(this.themeObj));
      }      
    });
  }

  gotoEdit(){
    console.log('You clicked to go themeCreate>> ');
    this.router.navigate(['themeEdit']);
  }
  saveNewTheme(params){
    console.log("save New Theme >> ",JSON.stringify(params));
    if(params.themeId && params.themeId > 0){  this.updateItemTheme(params); } 
    else {   this.saveItemTheme(params) }
  }
  getThemeItem(){
    return new Promise((resolve, reject) => {
      this.themeService.getAllThemeItem().subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in getText >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {     
            this.themeList = data.obj;  
            console.log("this.themeList >> ",JSON.stringify(this.themeList));
            this.cdr.detectChanges();
          } else {
            console.log("Oops!! Data fetch failed");
          } 
        })
      }, error => {
        console.log("error in Big fetch >> ");
      });
    });
  }
  saveItemTheme(params) {
    return new Promise((resolve, reject) => {
      let themeObj: Theme = {};
      themeObj = {
        "themeId": 0,
        "themeDesc": params.themeDesc.trim(),
        "themeName": params.themeName.trim(),
        "itemList": params.itemList
      }
      
      console.log("Theme Object >> ", JSON.stringify(themeObj));
      this.themeService.saveThemeItem(themeObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if (err) {
            console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {
            alert(data.responseMsg);
            this.themeObj.themeDesc = "";
            this.themeObj.themeName = "";
            this.getThemeItem();
          } else {
            console.log("Oops!! Data save failed");
          }
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }
  updateItemTheme(params){
    console.log("Update this.theme Before >> ", JSON.stringify(params));
    return new Promise((resolve, reject) => {
      let themeObj: Theme = {};
      themeObj = {
        "themeId": params.themeId,
        "themeDesc": params.themeDesc.trim(),
        "themeName": params.themeName.trim(),
        "itemList": []
      }
      let toArray = [];
      toArray = params.itemList;
      console.log("after separate >> ", JSON.stringify(toArray));
      if(toArray != undefined){
        themeObj.itemList = toArray;
      }else {
        delete themeObj.itemList;
      }
      console.log("Update this.theme.itemList >> ", JSON.stringify(themeObj.itemList));
      console.log("Update this.theme >> ", JSON.stringify(themeObj));
      this.themeService.updateThemeItem(themeObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if (err) {
            console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {
            alert(data.responseMsg);
            this.themeObj.themeDesc = "";
            this.themeObj.themeName = "";
            this.getThemeItem();
          } else {
            console.log("Oops!! Data save failed");
          }
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }
}
