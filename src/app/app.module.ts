import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ForceDirectGraphComponent } from './force-direct-graph/force-direct-graph.component';
import { HttpClientModule } from '@angular/common/http';
import { PopingDirectedGraphComponent } from './poping-directed-graph/poping-directed-graph.component';
import { DragDirectedGraphComponent } from './drag-directed-graph/drag-directed-graph.component';
import { TooltipCustomclassComponent } from './tooltip-customclass/tooltip-customclass.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { WeekMonthLineChartComponent } from './week-month-line-chart/week-month-line-chart.component';
import { MultiStepDirectedGraphComponent } from './multi-step-directed-graph/multi-step-directed-graph.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ng6-toastr-notifications';
import { TreePathComponent } from './tree-path/tree-path.component';
import { Routes,RouterModule } from '@angular/router';
import { ThemeComponent } from './theme/theme.component'; 
import { FormsModule } from '@angular/forms';

import { JavaService } from 'src/service_providers/service.status';
import { CommonServiceProvider } from 'src/service_providers/common-service';
import { ServiceList } from 'src/service_providers/service-list';
import { ThemeItemProvider } from 'src/service_providers/themeItemService';
import { ThemeEditComponent } from './theme-edit/theme-edit.component';
import { ThemeCreateComponent } from './theme-create/theme-create.component';
import { ItemThemeAllocateComponent } from './item-theme-allocate/item-theme-allocate.component';
import { NgxTypeaheadModule } from 'ngx-typeahead';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';


@NgModule({
  declarations: [
    AppComponent,
    ForceDirectGraphComponent,
    PopingDirectedGraphComponent,
    DragDirectedGraphComponent,
    TooltipCustomclassComponent,
    LineChartComponent,
    WeekMonthLineChartComponent,
    MultiStepDirectedGraphComponent,
    TreePathComponent,
    ThemeComponent,
    ThemeEditComponent,
    ThemeCreateComponent,
    ItemThemeAllocateComponent,
    
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    RouterModule.forRoot([]),
    HttpClientModule,
    FormsModule,
    NgxTypeaheadModule,
    TypeaheadModule.forRoot(),
  ],
  providers: [
    JavaService,
    ThemeItemProvider,
    CommonServiceProvider,
    ServiceList,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
