import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForceDirectGraphComponent } from './force-direct-graph/force-direct-graph.component';
import { PopingDirectedGraphComponent } from './poping-directed-graph/poping-directed-graph.component';
import { DragDirectedGraphComponent } from './drag-directed-graph/drag-directed-graph.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { WeekMonthLineChartComponent } from './week-month-line-chart/week-month-line-chart.component';
import { MultiStepDirectedGraphComponent } from './multi-step-directed-graph/multi-step-directed-graph.component';
import { TreePathComponent } from './tree-path/tree-path.component';
import { ThemeComponent } from './theme/theme.component';
import { ThemeEditComponent } from './theme-edit/theme-edit.component';
import { ThemeCreateComponent } from './theme-create/theme-create.component';
import { ItemThemeAllocateComponent } from './item-theme-allocate/item-theme-allocate.component';

const routes: Routes = [
  { path: 'directGraph', component: ForceDirectGraphComponent},
  { path: 'popGraph', component: PopingDirectedGraphComponent},
  { path: 'dragGraph', component: DragDirectedGraphComponent},
  { path: 'barChart', component: LineChartComponent},
  { path: 'lineChart', component: WeekMonthLineChartComponent},
  { path: 'multiStep', component: MultiStepDirectedGraphComponent},
  { path: 'treeMap', component: TreePathComponent},
  { path: 'theme', component: ThemeComponent},
  { path: 'themeEdit', component: ThemeEditComponent},
  { path: 'themeCreate', component: ThemeCreateComponent},
  { path: 'itemThemeAllocation', component: ItemThemeAllocateComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ForceDirectGraphComponent,PopingDirectedGraphComponent,
                                  DragDirectedGraphComponent,LineChartComponent,
                                  WeekMonthLineChartComponent,MultiStepDirectedGraphComponent,
                                  TreePathComponent,ThemeComponent,ThemeEditComponent,ThemeCreateComponent,
                                  ItemThemeAllocateComponent]
