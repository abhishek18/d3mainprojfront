import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as d3 from 'd3';
import DRAG_CONFIG from './drag.config';
import { Link } from 'd3';

@Component({
  selector: 'app-drag-directed-graph',
  templateUrl: './drag-directed-graph.component.html',
  styleUrls: ['./drag-directed-graph.component.css']
})
export class DragDirectedGraphComponent implements OnInit {
  @ViewChild('DragDirectedChartContainer') private chartContainer: ElementRef;
  
  simulation:any;
  link_force:any;
  charge_force:any;
  center_force:any;
  width:any=800;
  height:any=400;
  svg:any;
  collide_force:any;
  radius:number =10;
  link:any;
  node:any;
  drag_handler:any;
  hostElement: any;
  constructor(private elementRef: ElementRef) { }

  ngOnInit() {

    //--------------------------------//
    this.hostElement = this.chartContainer.nativeElement;
    this.svg = d3.select(this.hostElement)
      .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .attr("style", "background: grey");

      this.link_force =  d3.forceLink(DRAG_CONFIG.graph.links_data)
                          .id( (link) =>{ return link['id'] })
                          .strength(function (link) { return 0.8 }) 

    //set up the simulation 
      this.simulation = d3.forceSimulation()
      .nodes(DRAG_CONFIG.graph.nodes_data)
      .force('link', this.link_force)
      .force('charge', this.charge_force) 
      .force('center', this.center_force)                               
          
        this.charge_force = d3.forceManyBody()
                              .strength(-40);

        this.center_force = d3.forceCenter(this.width / 2, this.height / 2);  

        //High strength value causes unstable effects  
        this.collide_force = d3.forceCollide(this.radius)
                              .strength(10);   
                      
        this.simulation = d3.forceSimulation()
        .nodes(DRAG_CONFIG.graph.nodes_data)
        .force("charge_force", this.charge_force)
        .force("center_force", this.center_force)
        .force("links", this.link_force)
        .force("collide", this.collide_force);
        
         //draw lines for the links 
         this.link = this.svg.append("g")
         .attr("class", "links")
         .selectAll("line")
         .data(DRAG_CONFIG.graph.links_data)
         .enter().append("line")
         .attr("stroke-width", 2)
         .style("stroke", this.linkColour);        
 
         //draw circles for the nodes 
         this.node = this.svg.append("g")
         .attr("class", "nodes") 
         .selectAll("circle")
         .data(DRAG_CONFIG.graph.nodes_data)
         .enter()
         .append("circle")
         .attr("r", this.radius)
         .attr("fill", this.circleColour);

        //add tick instructions: 
        this.simulation.nodes(DRAG_CONFIG.graph.nodes_data).on("tick", ()=>{
          this.node
          .attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
  
          //update link positions 
          this.link
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
        } );

         
              

        this.drag_handler = d3.drag()
        .on("start", this.drag_start)
        .on("drag", this.drag_drag)
        .on("end", this.drag_end);	

        this.drag_handler(this.node);

        //drag handler
        //d is the node 
        
        
    //-------------------------------------//
  }
  /** Functions **/

        //Function to choose what color circle we have
        //Let's return blue for males and red for females
  circleColour(d){
    if(d.sex =="M"){
        return "blue";
    } else {
        return "pink";
    }
  }
  //Function to choose the line colour and thickness 
        //If the link type is "A" return green 
        //If the link type is "E" return red 
      linkColour(d){
          if(d.type == "A"){
            return "green";
          } else {
            return "red";
          }
        }
     tickActions() {
          //update circle positions each tick of the simulation 
          this.node
          .attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
  
          //update link positions 
          this.link
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
          } 
          
      drag_start(d) {
          if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
          d.fx = d.x;
          d.fy = d.y;
        }

        drag_drag(d) {
          d.fx = d3.event.x;
          d.fy = d3.event.y;
        }

        drag_end(d) {
            if (!d3.event.active) this.simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }
    

}
