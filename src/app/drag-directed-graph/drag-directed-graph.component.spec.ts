import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDirectedGraphComponent } from './drag-directed-graph.component';

describe('DragDirectedGraphComponent', () => {
  let component: DragDirectedGraphComponent;
  let fixture: ComponentFixture<DragDirectedGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragDirectedGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDirectedGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
