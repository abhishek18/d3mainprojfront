import { Component, OnInit, OnChanges, AfterViewChecked } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'd3-Own-Proj';
  showAppComponent: boolean=true;
  
  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ){
    
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "220px";
    document.getElementById("main").style.marginLeft = "250px";
}

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}
  getGraphData(event:any){
    console.log('You clicked to go directGraph>> ');
    this.router.navigate(['directGraph']);
  }
  getPopGraph(event:any){
    console.log('You clicked to go Pop Graph>> ');
    this.router.navigate(['popGraph']);
  }
  getDragGraph(event:any){
    console.log('You clicked to go Pop Graph>> ');
    this.router.navigate(['dragGraph']);
  }
  getBarChart(event:any){
    console.log('You clicked to go Bar Chart>> ');
    this.router.navigate(['barChart']);
  }
  getLineChart(event:any){
    console.log('You clicked to go Line Chart>> ');
    this.router.navigate(['lineChart']);
  }
  getMultistepGraph(event:any){
      console.log('You clicked to go getMultistepGraph>> ');
      this.router.navigate(['multiStep']);
    }
    getTreePath(event:any){
      console.log('You clicked to go getTreePath>> ');
      this.router.navigate(['treeMap']);
    }
    getTheme(event:any){
      console.log('You clicked to go getTheme>> ');
      this.router.navigate(['theme']);
    }
    getThemeCreation(event:any){
      console.log('You clicked to go getThemeCreation>> ');
      this.router.navigate(['themeEdit']);
    }
    getItemThemeAllocation(event:any){
      console.log('You clicked to go getItemThemeAllocation>> ');
      this.router.navigate(['itemThemeAllocation']);
    }

}
