import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as d3 from 'd3';
import TREE_CONFIG from './tree.config';

@Component({
  selector: 'app-tree-path',
  templateUrl: './tree-path.component.html',
  styleUrls: ['./tree-path.component.css']
})
export class TreePathComponent implements OnInit {

  @ViewChild('treeGraphContainer') private treeContainer: ElementRef;

  hostElement: any;
  margin: {top: number;right: number;bottom: number;left: number;};
  svg: any;
  svgWidth = 980;
  svgHeight = 500;
  g:any;
  minZoom:any=1/4;
  maxZoom:any=8;

  constructor(
    private elementRef: ElementRef,
    public toastr: ToastrManager,
    ) { }

  ngOnInit() {

    this.hostElement = this.treeContainer.nativeElement;
    console.log("this.hostElement tree >> ", this.hostElement);
    this.margin = {
      top: 20, right: 90, bottom: 30, left: 90
    };
    this.createPrimarytree();
  }

  createPrimarytree(){
    //d3.select("svg").remove(); 
    let innerWidth = this.svgWidth - this.margin.left - this.margin.right,
      innerHeight = this.svgHeight - this.margin.top - this.margin.bottom;

    //----------SVG creation----------------------//
    this.svg = d3.select(this.hostElement)
              .append('svg')
              .attr('width', innerWidth + this.margin.left + this.margin.right)
              .attr('height', innerHeight+ this.margin.top + this.margin.bottom)
              .attr("style", "background: rgb(238,238,238)")
             // .attr("style", "text-shadow: 0 0 20px black")

    // convert the flat data into a hierarchy 
      let treeData  = d3.stratify()
                    .id(function(d) { return d['name']; })
                    .parentId(function(d) { return d['parent']; })
                    (TREE_CONFIG.graph.chart);

    // assign the name to each node
    treeData .each(function(d) { d['name'] = d.id; }); 

    // declares a tree layout and assigns the size
      let treemap = d3.tree()
                      .size([innerHeight, innerWidth]),

      //  assigns the data to a hierarchy using parent-child relationships
      nodesData = d3.hierarchy(treeData, function(d) {  return d.children; }),

      // maps the node data to the tree layout
      nodes = treemap(nodesData);

    //----------Zoom SVG and Element-------------------//
      this.g = this.svg.append("g")
                    .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
      this.svg.call(d3.zoom()              
                    .on("zoom", ()=>{this.g.attr("transform", d3.event.transform);})
                    .scaleExtent([this.minZoom, this.maxZoom])) 

    //------------adds the links between the nodes------------//
      let link = this.g.selectAll(".link")
                  .data( nodes.descendants().slice(1.5))
                  .enter().append("path")
                  .attr("d", function(d) {
                    return "M" + d.y + "," + d.x
                      + "C" + (d.y + d.parent.y) / 2 + "," + d.x
                      + " " + (d.y + d.parent.y) / 2 + "," + d.parent.x
                      + " " + d.parent.y + "," + d.parent.x;  })                       
                      .attr('fill', "none")
                      .attr('stroke','#ccc')
                      .attr('stroke-width',(d)=>{ console.log("stroke-width >> ",d); return d.data.data.strength*10; })

    // adds each node as a group
      let node = this.g.selectAll(".node")
                  .data(nodes.descendants())
                  .enter().append("g")
                  .attr("class", function(d) { 
                    console.log("node obj >> ",d);
                    return "node" + 
                      (d.children ? " node--internal" : " node--leaf"); })
                  .attr("transform", function(d) { 
                    return "translate(" + d.y + "," + d.x + ")"; });

      // adds the circle to the node
      node.append("circle")
                .attr("r", this.getNodeRadius)
                .attr('fill', this.getNodeColor)
                .attr('stroke', (d) =>{ return d3.color("#000080"); })
                .attr('stroke-width', "3px")
                //----------Drag & Drop-----------------//
                .call(d3.drag()
                .on("start", (d)=>{
                  if (!d3.event.active) this.g.alphaTarget(0.08).restart();
                          d['fx'] = d['x'];d['fy'] = d['y'];})
                .on("drag", (d)=>{
                  d['fx'] = d3.event.x;
                  d['fy'] = d3.event.y;})
                .on("end", (d)=>{
                            if (!d3.event.active) this.g.alphaTarget(0);
                            d['fx'] = null;
                            d['fy'] = null;}));

      // adds the text to the node
      node.append("text")
                .attr("dy", ".35em")
                .attr("x", function(d) { return d.children ? -13 : 13; })
                .style("text-anchor", function(d) { 
                return d.children ? "end" : "start"; })
                .text(function(d) { return d.data.name; })
                .style('font', "12px sans-serif");

      
  }
    getNodeColor(node) {
    
      let intens = node.data.data.strength;
      //console.log("intens >> ",intens);
        if(intens>=0.3 && intens<0.55) return '#FFE5E7';
        else if(intens>=0.55 && intens<0.7) return '#FF9A9E';
        else if(intens>=0.7 && intens<0.9) return '#FF4F56';
        else if(intens>=0.9 && intens<=1) return '#FF030D';
    }
    getNodeRadius(node){
      let intens = node.data.data.strength;
      //console.log("intens >> ",intens);
        if(intens>=0.3 && intens<0.55) return 11;
        else if(intens>=0.55 && intens<0.7) return 15;
        else if(intens>=0.7 && intens<0.9) return 20;
        else if(intens>=0.9 && intens<1) return 27;
        else if(intens==1) return 33;
    }
}
