const TREE_CONFIG = {

    graph : 
        {
          "id": "embroidered-georgette-saree-in-red-szra409",
          "chart": [
            {
              "strength": 1.0,
              "name": "embroidered-georgette-saree-in-red-szra409",
              "parent": null
            },
            {
              "strength": 0.9090909090909091,
              "name": "art-silk-saree-in-off-white-syc6556",
              "parent": "embroidered-georgette-saree-in-red-szra409"
            },
            {
              "strength": 0.8333333333333333,
              "name": "banarasi-silk-saree-in-maroon-sew5055",
              "parent": "art-silk-saree-in-off-white-syc6556"
            },
            {
              "strength": 0.769230769230769,
              "name": "plain-georgette-saree-in-maroon-sfva37",
              "parent": "banarasi-silk-saree-in-maroon-sew5055"
            },
            {
              "strength": 0.7142857142857141,
              "name": "kanchipuram-saree-in-maroon-snba786",
              "parent": "plain-georgette-saree-in-maroon-sfva37"
            },
            {
              "strength": 0.6666666666666665,
              "name": "embroidered-georgette-saree-in-black-sew4784",
              "parent": "kanchipuram-saree-in-maroon-snba786"
            },
            {
              "strength": 0.6249999999999998,
              "name": "jamdani-cotton-silk-saree-in-red-scda48",
              "parent": "plain-georgette-saree-in-maroon-sfva37"
            },
            {
              "strength": 0.5882352941176469,
              "name": "half-n-half-embroidered-chiffon-saree-in-beige-and-black-skk22069",
              "parent": "jamdani-cotton-silk-saree-in-red-scda48"
            },
            {
              "strength": 0.5555555555555554,
              "name": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695",
              "parent": "jamdani-cotton-silk-saree-in-red-scda48"
            },
            {
              "strength": 0.526315789473684,
              "name": "embroidered-georgette-saree-in-red-sfs423",
              "parent": "half-n-half-chiffon-and-georgette-saree-in-coral-and-beige-stl695"
            },
            {
              "strength": 0.4999999999999998,
              "name": "bandhani-printed-crepe-saree-in-red-sjn3039",
              "parent": "embroidered-georgette-saree-in-red-sfs423"
            }
          ]
        }
       /* graph:
        {
          "id": "embroidered-georgette-saree-in-red-szra409",
          "chart": [
                    {"strength": 1.0,"name": "Top Level", "parent": null}, 
                    {"strength": 0.8333333333333333,"name": "Level 2: A", "parent": "Top Level" },
                    {"strength": 0.7142857142857141,"name": "Level 2: B", "parent": "Top Level" },
                    {"strength": 0.6666666666666665,"name": "Son of A", "parent": "Level 2: A" },
                    {"strength": 0.5555555555555554,"name": "Daughter of A", "parent": "Level 2: A" }
          ]
        }*/
}
export default TREE_CONFIG;