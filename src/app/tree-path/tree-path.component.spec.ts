import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreePathComponent } from './tree-path.component';

describe('TreePathComponent', () => {
  let component: TreePathComponent;
  let fixture: ComponentFixture<TreePathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreePathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreePathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
