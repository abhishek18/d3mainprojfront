import { Component, OnInit, ViewChild,ElementRef,ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router'; 
import { HttpClient} from '@angular/common/http';
import {  JavaService} from 'src/service_providers/service.status';
import {  ThemeItemProvider} from 'src/service_providers/themeItemService';
import { ResponseData, Item, Theme } from '../interface';

@Component({
  selector: 'app-theme-edit',
  templateUrl: './theme-edit.component.html',
  styleUrls: ['./theme-edit.component.css']
})
export class ThemeEditComponent implements OnInit {

  item: Item = {};
  theme: Theme = {};
  themeList: Array<Theme> = [{}];
  showDivRow: boolean = true;
  
  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private themeService: ThemeItemProvider,
    private service: JavaService,
    private cdr: ChangeDetectorRef,) {

      this.getThemeItem();

     }

  ngOnInit() {
  }
  getThemeItem(){
    return new Promise((resolve, reject) => {
      this.themeService.getAllThemeItem().subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in getText >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {     
            this.themeList = data.obj;  
            console.log("this.themeList >> ",JSON.stringify(this.themeList));
            this.cdr.detectChanges();
          } else {
            console.log("Oops!! Data fetch failed");
          } 
        })
      }, error => {
        console.log("error in Big fetch >> ");
      });
    });
  }
  gotoCreate(themeObj){
    console.log('You clicked to go themeCreate>> ',(themeObj));
      this.router.navigate(['themeCreate'],{
        queryParams: {themeObj:JSON.stringify(themeObj)}
      });    
  }
  gotoCreateNew(){
    console.log('You clicked to go gotoCreateNew>> ');
      this.router.navigate(['themeCreate']);    
  }

}

