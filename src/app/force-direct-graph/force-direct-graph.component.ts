import { Component,OnInit, OnChanges, AfterViewChecked, ViewChild, ElementRef} from '@angular/core';
import * as d3 from 'd3';
import APP_CONFIG from './force.config';
//import { Link } from './link';

@Component({
  selector: 'app-force-direct-graph',
  templateUrl: './force-direct-graph.component.html',
  styleUrls: ['./force-direct-graph.component.css']
})
export class ForceDirectGraphComponent implements OnInit, OnChanges, AfterViewChecked {
  @ViewChild('forceDirectedChartContainer') private chartContainer: ElementRef;

  showAppComponent: boolean=false;
  hostElement: any;
  margin: any;
  width: any;
  height: any;
  svg: any;
  simulation:any;
  nodeElements:any;
  linkForce:any;
  linkElements:any;
  textElements:any;
  dragDrop:any;
  drag_handler:any;
  tooltip:any;
  g:any;
  zoom_handler: d3.ZoomBehavior<Element, {}>;

  constructor(
    private elementRef: ElementRef
  ) {}

  ngOnInit() {

    this.hostElement = this.chartContainer.nativeElement;
    console.log("this.hostElement >> ",this.hostElement);
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      
  //----------SVG creation----------------------//
  this.svg = d3.select(this.hostElement)
      .append('svg')
      .attr('width', 800)
      .attr('height', 400)
      .attr("style", "background: yellow");
  this.tooltip = this.svg
                  .append("div")
                  .attr("class", "tooltip")
                  .style("opacity", 0); 
    //add encompassing group for the zoom 
    this.g = this.svg.append("g")
        .attr("class", "everything");
                     

      this.linkForce = d3.forceLink(APP_CONFIG.graph.links)
              .id(function (link) { return link['id'] })
              .strength(function (link) { return link.strength })

      this.simulation = d3.forceSimulation()
      .nodes(APP_CONFIG.graph.nodes)
      .force('link', this.linkForce)
      .force('charge', d3.forceManyBody().strength(-120)) 
      .force('center', d3.forceCenter(400, 200)) 
           
    //-----------Links Creation--------------------//
        this.linkElements = this.svg.append("g")
        .attr("class", "links")
        .selectAll("line")
        .data(APP_CONFIG.graph.links)
        .enter().append("line")
          .attr("stroke-width", 1)
          .attr("stroke", "rgba(51,0,0)")

    //----------NODE Creation------------------//
      this.nodeElements = this.svg.append('g')
        .selectAll('circle')
        .data(APP_CONFIG.graph.nodes)
        .enter().append('circle')
        .attr('r', (node)=>{ return node.level === 1 ? 20 : 15})
        .attr('fill', this.getNodeColor)
        .on('click', this.selectNode)
        .style('stroke', function(d) { 
          return d3.color("#000080"); })  

        //----------Drag & Drop-----------------//
        .call(d3.drag()
              .on("start", (d)=>{
                if (!d3.event.active) this.simulation.alphaTarget(0.08).restart();
                        d['fx'] = d['x'];d['fy'] = d['y'];})
              .on("drag", (d)=>{
                d['fx'] = d3.event.x;
                d['fy'] = d3.event.y;})
              .on("end", (d)=>{
                          if (!d3.event.active) this.simulation.alphaTarget(0);
                          d['fx'] = null;
                          d['fy'] = null;}))
        
    
    this.textElements = this.svg.append('g')
      .selectAll('text')
      .data(APP_CONFIG.graph.nodes)
      .enter().append('text')
        .text(node => node.label)
        .attr('font-size', 15)
        .attr('dx', 15)
        .attr('dy', 4)

        this.simulation.nodes(APP_CONFIG.graph.nodes).on('tick', () => {
          this.nodeElements
            .attr('cx', node => node.x)
            .attr('cy', node => node.y)
          this.textElements
            .attr('x', node => node.x)
            .attr('y', node => node.y)
          this.linkElements  
            .attr('x1', link => link.source.x)
            .attr('y1', link => link.source.y)
            .attr('x2', link => link.target.x)
            .attr('y2', link => link.target.y)  
        })
   
    
    /*  */
    this.tooltip =  this.elementRef.nativeElement.querySelector('.tooltip');

         //add zoom capabilities 
    this.zoom_handler = d3.zoom()
    .on("zoom", ()=>{
        this.g.attr("transform", d3.event.transform)
    });

this.zoom_handler(this.svg);                               

    //this.simulation.force("link").links(APP_CONFIG.graph.links)                      
  }
  ngAfterViewChecked(){

    this.tooltip =  this.elementRef.nativeElement.querySelector('.tooltip');

  }
  ngOnChanges(){
    this.tooltip =  this.elementRef.nativeElement.querySelector('.tooltip');

  }
    repulsion(){
      
    }
    getNeighbors(node) {
      console.log("getNeighbors >> ",JSON.stringify(node));
      return APP_CONFIG.graph.links.reduce((neighbors, link) =>{
          if (link.target['id'] == node.id) {
            neighbors.push(link.source['id'])
          } else if (link.source['id'] == node.id) {
            neighbors.push(link.target['id'])
          }
          return neighbors
        },
        [node.id]
      )
    }
    isNeighborLink(node, link) {
      return link.target.id === node.id || link.source.id === node.id;
    }
    getNodeColor(node, neighbors) {
      if (Array.isArray(neighbors) && neighbors.indexOf(node.id) > -1) {
        return node.level === 1 ? 'blue' : 'green'
      }
      return node.level === 1 ? 'red' : 'gray'
    }
    getLinkColor(node, link) {
      return this.isNeighborLink(node, link) ? 'green' : '#E5E5E5'
    }
    getTextColor(node, neighbors) {
      return Array.isArray(neighbors) && neighbors.indexOf(node.id) > -1 ? 'green' : 'black'
    }
    selectNode(selectedNode) {
      console.log("selectedNode >> ",JSON.stringify(selectedNode));
      APP_CONFIG.graph.links.forEach(element => {
                                      console.log(element.source['id']);
                                      console.log("selectedNode.id >> ",selectedNode.id);
                                      if(element.source['id']== selectedNode.id){
                                        console.log("Inside if >> ",element.target['id']);
                                      }
                                    });

      /*let neighbors = this.getNeighbors(selectedNode)
      // we modify the styles to highlight selected nodes
      this.nodeElements.attr('fill', function (node) { return this.getNodeColor(node, neighbors) })
      this.textElements.attr('fill', function (node) { return this.getTextColor(node, neighbors) })
      this.linkElements.attr('stroke', function (link) { return this.getLinkColor(selectedNode, link) })*/
    }    

}
