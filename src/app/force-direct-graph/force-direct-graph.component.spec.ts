import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForceDirectGraphComponent } from './force-direct-graph.component';

describe('ForceDirectGraphComponent', () => {
  let component: ForceDirectGraphComponent;
  let fixture: ComponentFixture<ForceDirectGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForceDirectGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForceDirectGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
