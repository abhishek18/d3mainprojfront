import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemThemeAllocateComponent } from './item-theme-allocate.component';

describe('ItemThemeAllocateComponent', () => {
  let component: ItemThemeAllocateComponent;
  let fixture: ComponentFixture<ItemThemeAllocateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemThemeAllocateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemThemeAllocateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
