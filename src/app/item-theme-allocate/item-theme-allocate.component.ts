import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import { HttpClient} from '@angular/common/http';
import {  JavaService} from 'src/service_providers/service.status';
import {  ThemeItemProvider} from 'src/service_providers/themeItemService';
import { Theme, ResponseData, Item } from '../interface';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-item-theme-allocate',
  templateUrl: './item-theme-allocate.component.html',
  styleUrls: ['./item-theme-allocate.component.css']
})
export class ItemThemeAllocateComponent implements OnInit {

  model = { options: '' };
  showmulti: boolean=false;
  item: Item={};
  themeObj: Theme={};
  themeList: Array<Theme>=[];
  itemList: Array<Item>=[];
  public typeList: Array<any>=[];
  public query = '';
  selectedValue: string;
  selectedOption: any;
  viewModel: any={};
  viewModelList: Array<any> = [];
  themeId2: number=0;
  themeId: number=0;

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private themeService: ThemeItemProvider,
    private service: JavaService,
    private cdr: ChangeDetectorRef,

  ) {
      this.model.options = 'single';
      this.onSelectionChange();
   }

  ngOnInit() {
    this.themeObj.themeId = 0;
    this.getThemeItem();
    this.getAllItem();
  }
  onSelectionChange(){
    console.log("on Selection Change >> ", JSON.stringify(this.model));
    switch (this.model.options) {
      case 'single':  this.showmulti = false;
                      break;
      case 'multi':   this.showmulti = true;
                      break;
           default:   this.showmulti = false;
                      break;
    }
  }
  getThemeItem(){
    return new Promise((resolve, reject) => {
      this.themeService.getAllThemeItem().subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in getText >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {     
            this.themeList = data.obj;  
            this.viewModelList = [];
            this.themeList.forEach((element)=>{
              element.itemList.forEach((itemObj)=>{
                this.viewModel = {};
                this.viewModel.themeId = element.themeId
                this.viewModel.themeName = element.themeName
                this.viewModel.themeDesc = element.themeDesc
                this.viewModel.itemId = itemObj.itemId
                this.viewModel.itemName = itemObj.itemName
                this.viewModelList.push(this.viewModel);
              })
            });
            console.log("this.viewModelList >> ",JSON.stringify(this.viewModelList)); 
            this.cdr.detectChanges();
            console.log("this.themeList >> ",JSON.stringify(this.themeList));
            this.cdr.detectChanges();
          } else {
            console.log("Oops!! Data fetch failed");
          } 
        })
      }, error => {
        console.log("error in Big fetch >> ");
      });
    });
  }
  saveItemThemeAllocate(params){
    console.log("saveItemThemeAllocate >> ",JSON.stringify(params),"   ",JSON.stringify(this.selectedOption));
    return new Promise((resolve, reject) => {
      let themeObj: Theme = {};
      themeObj = {
        "themeId": params.themeId,
        "themeDesc": params.themeDesc.trim(),
        "themeName": params.themeName.trim(),
        "itemList": []
      }
      themeObj.itemList.push({
        itemId: this.selectedOption.itemId,
        itemName: this.selectedOption.itemName,
      });
      console.log("Update this.theme.itemList >> ", JSON.stringify(themeObj.itemList));
      console.log("Update this.theme >> ", JSON.stringify(themeObj));
      this.themeService.updateThemeItem(themeObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if (err) {
            console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {
            alert(data.responseMsg);
            this.themeObj.themeId = 0;
            this.item.itemName = "";
            this.getThemeItem();
          } else {
            console.log("Oops!! Data save failed");
          }
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    }); 
  }
  clearData(){
    let clr = confirm("Discard will clear all the input");
    if(clr){
      this.themeObj.themeId=0;
      this.item.itemName = ""
      this.item.itemDesc = ""
      this.item.itemNameList = ""
      this.themeId2 = 0
      this.themeId = 0
    }  
  }

  public handleStaticResultSelected (result) {
    console.log("result >> ",result);
    console.log("this.item.itemName >> ",this.item.itemName);
    this.query = result;

  }
  clearType(){
    this.item.itemName = "";
  }
  onSelectItem(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    console.log("this.selectedOption >> ",this.selectedOption);
  }
  getType(event){
    console.log("getType >> ",event.target.value);
  }
  getAllItem(){
    return new Promise((resolve, reject) => {
      this.themeService.getAllItem().subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if(err) {
           console.log("error in getAllItem >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {      
            data.obj.forEach((element)=>{
              this.itemList.push({
                "itemId": element.itemId,
                "itemName": element.itemName
              })
            })
            console.log("this.itemList >> ",JSON.stringify(this.itemList));
            this.cdr.detectChanges();
          } else {
            console.log("Oops!! Data fetch failed");
          } 
        })
      }, error => {
        console.log("error in Big fetch >> ");
      });
    });
  }
  saveMultiItemThemeAllocate(params){
    console.log("saveMultiItemThemeAllocate >> ",JSON.stringify(params),"   ",JSON.stringify(this.item));
    return new Promise((resolve, reject) => {
      let themeObj: Theme = {};
      themeObj = {
        "themeId": params.themeId,
        "themeName": params.themeName,
        "themeDesc": params.themeDesc,
        "itemList": params.itemList,
      }
      let toArray = [];
      toArray = this.item.itemNameList.split("\n");
      console.log("after separate >> ", JSON.stringify(toArray));

      toArray.forEach((toarrObj) => {
        if (toarrObj != "" && null != toarrObj) {
          console.log("toarrObj element >> ", JSON.stringify(toarrObj));
          themeObj.itemList.push({
                                  itemId: 0,
                                  itemName: toarrObj,
                                });
        }
      })
      console.log("this.theme.itemList >> ", JSON.stringify(themeObj.itemList));
      this.themeService.updateThemeItem(themeObj).subscribe((success: any) => {
        this.service.status(success).catch(err => {
          if (err) {
            console.log("error in save >> ");
            //this.alert.presentAlert(err.messageSeverity, err.message, 'OK')
          }
        }).then((data: ResponseData) => {
          if (data.responseStatus == "success") {
            alert(data.responseMsg);
            this.themeObj.themeId = 0;
            this.item.itemNameList = "";
            this.getThemeItem();
          } else {
            console.log("Oops!! Data save failed");
          }
        })
      }, error => {
        console.log("error in Big save >> ");
      });
    });
  }
  
}
