import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import LINE_CHART_CONFIG from './weak-month-chart.config';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-week-month-line-chart',
  templateUrl: './week-month-line-chart.component.html',
  styleUrls: ['./week-month-line-chart.component.css']
})
export class WeekMonthLineChartComponent implements OnInit {

  @ViewChild('lineChartContainer') private chartContainer: ElementRef;

  hostElement: any;
  svg: d3.Selection<d3.BaseType, {}, null, undefined>;
  svgWidth = 850; 
  svgHeight = 500;
  margin: { top: number; right: number; bottom: number; left: number; };
  width: number;
  height: number;
  parseTime: (date: Date) => string;
  xAxis: d3.ScaleTime<number, number>;
  yAxis: d3.ScaleLinear<number, number>;
  valueline: any;
  itemId: string;
  lineGraph: d3.Selection<d3.BaseType, {}, null, undefined>;
  tooltip: any;
  vertical:any;
  

  constructor(
                private elementRef: ElementRef
  ) { }

  ngOnInit() {

    this.hostElement = this.chartContainer.nativeElement;
      console.log("this.hostElement >> ",this.hostElement);
    
    //this.simpleLineChart();
    //this.complexLineChart();
    this.lineChartWithOption();
  }
  simpleLineChart(){
    this.margin = { top: 20, right: 20, bottom: 30, left: 50 };
    this.width = this.svgWidth - this.margin.left - this.margin.right;
    this.height = this.svgHeight - this.margin.top - this.margin.bottom;
    this.svg = d3.select(this.hostElement)
                    .append("svg")
                    .attr("width", this.width + this.margin.left + this.margin.right )
                    .attr("height", this.height + this.margin.top + this.margin.bottom)
                    .attr("style", "background: rgb(220,220,220)")//rgb(161,170,239)  rgb(220,220,220)

                    //.attr("transform","translate(" + this.margin.left + "," + 10.5 + ")")

                    // parse the date / time
    this.parseTime = d3.timeFormat("%d-%b-%y");
    let lineData = [];
    this.itemId = LINE_CHART_CONFIG.lineData.id;
    LINE_CHART_CONFIG.lineData.chart.forEach((d)=> {
                                                let objNew = {
                                                                "date": new Date('dd-MM-yyyy'), 
                                                                "value": 0
                                                              };
                                                //let a = new DatePipe('en-US').transform(d.date, 'dd-MM-yyyy');
                                                //let b = new Date();b=d3.timeParse(a)
                                                  //console.log("d3.timeParse(a)>> ",b,"type of>>",typeof b)          
                                                objNew.date = new Date(d.date);
                                                objNew.value = +d.value;
                                                lineData.push(objNew);
                                              })
    console.log("After operation data >> ",JSON.stringify(lineData)," size   >> ",lineData.length);

        // set the ranges
      let xAxis = d3.scaleTime().range([this.margin.left, this.width]);
      let yAxis = d3.scaleLinear()
                        .range([this.height, 10]);

        // define the line
      this.valueline =d3.line()//area()
                      .x((d) =>{ console.log("xAxis(d['date']) >> ",xAxis(d['date']));return xAxis(d['date'])})
                      //.y0(this.height)
                      .y(function(d) { console.log("yAxis(d['value']) >> ",yAxis(d['value']));return yAxis(d['value'])})

      xAxis.domain(d3.extent(lineData, function(d) { return new Date(d.date) }));
      //xAxis.domain([0, d3.max(lineData, (d)=>{ return new Date(d.date); })]);
      yAxis.domain([0, d3.max(lineData, (d) => (d.value))]).nice();
                      

      // append the svg obgect to the body of the page
      // appends a 'group' element to 'svg'
      // moves the 'group' element to the top left margin
      this.svg.append("g")
              .attr("transform",
                        "translate(" + 0 + "," + (this.height) + ")")
              .call(d3.axisBottom(xAxis).tickSize(-this.height))
              //.select(".domain")
              //.remove();

      this.svg.append("g")
                .attr("transform","translate(" + this.margin.left + "," +0 + ")")
                .call(d3.axisLeft(yAxis).tickSize(-this.width))
                .append("text")
                .attr("fill", "#000")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("x", -this.margin.left-this.margin.bottom)
                .attr("dy", "-3.71em")
                .attr("text-anchor", "right")
                .text("Item Viewed (number of click)");

      this.svg
      /*.selectAll()
                .data([lineData])
                .enter()*/
                .append("path")
                .datum(lineData)
                .attr("fill", "none")
                .attr("style", "opacity: 0.4")
                .attr("stroke", "rgb(26,10,253)")
                .attr("stroke-linejoin", "round")
                .attr("stroke-linecap", "round")
                .attr("stroke-width", 3)
                .attr("d", this.valueline)
                .on('mouseover', function(d) {
                  console.log("nodeObj >>",JSON.stringify(d),"     ",d3.event);
                  console.log("d3.select >>",d3.select(this));
                 })  
                
      /*this.svg.append('g')
                .attr("transform","translate(" + 45 + "," +0 + ")")
                .attr('class', 'grid')
                .call(d3.axisLeft(yAxis)
                    //.scale(yAxis)
                    .tickSize(-this.width))

                    //.tickFormat(''))*/
      
  }

  complexLineChart(){  
    d3.select("svg").remove();  
    this.svg = d3.select(this.hostElement)
                .append("svg")
                .attr('width',this.svgWidth)
                .attr('height',this.svgHeight)
                .attr("style", "background: rgb(211,211,211)")
    //let width = +this.svg.attr('width'), height = +this.svg.attr('height')
    //console.log("width >> ",width," height >> ",height);

    let margin = { top: 40, right: 40, bottom: 60, left: 60 };
    let innerWidth = this.svgWidth - margin.left - margin.right;
    let innerHeight = this.svgHeight - margin.top - margin.bottom;
    console.log("innerWidth >> ",innerWidth,"  innerHeight >>> ",innerHeight);
     
    this.lineGraph = this.svg.append('g')
                        .attr("transform","translate(" + margin.left + "," + margin.top + ")")

    let lineData = [];
    this.itemId = LINE_CHART_CONFIG.lineData.id;
    LINE_CHART_CONFIG.lineData.chart.forEach((d)=> {
                                                let objNew = {
                                                                "date": new Date('dd-MM-yyyy'), 
                                                                "value": 0
                                                              };
                                                //let a = new DatePipe('en-US').transform(d.date, 'dd-MM-yyyy');
                                                //let b = new Date();b=d3.timeParse(a)
                                                  //console.log("d3.timeParse(a)>> ",b,"type of>>",typeof b)          
                                                objNew.date = new Date(d.date);
                                                objNew.value = +d.value;
                                                lineData.push(objNew);
                                              })
    console.log("After operation data >> ",JSON.stringify(lineData)," size   >> ",lineData.length);

    let xValue = d => d.date;
    let xAxisLabel = '<---------Time--------->';
    let yValue = d => d.value;
    let circleRadius = 6;
    let yAxisLabel = '<------Number of View---------->';
    let title = 'A month of viewed item'
    let xScale = d3.scaleTime()
                  .domain(d3.extent(lineData, xValue))
                  .range([0, innerWidth])
                  .nice();
    let yScale = d3.scaleLinear()
                .domain(d3.extent(lineData, yValue)) //[0, d3.max(lineData, (d) => (d.value*1.2))]
                .range([innerHeight, 0])
                .nice();
    let xAxis = d3.axisBottom(xScale)
                .tickSize(-innerHeight)
                //.tickPadding(15);
    let yAxis = d3.axisLeft(yScale)
                .tickSize(-innerWidth)
                //.tickPadding(10);
    let yAxisG = this.lineGraph.append('g')
                      .call(yAxis)
    yAxisG.selectAll('.domain').remove();

    yAxisG.append('text')
              .attr('style', 'font-size: 2em')
              //.attr('style', 'fill: #8E8883')
              .attr('y', -30)
              .attr('x', -innerHeight / 2)
              .attr('fill', 'black')
              .attr('transform', `rotate(-90)`)
              .attr('text-anchor', 'middle')
              .text(yAxisLabel);

    let xAxisG = this.lineGraph.append('g').call(xAxis)
                      .attr("transform","translate(" + 5 + "," +innerHeight+ ")")
    xAxisG.select('.domain').remove();
    xAxisG.append('text')
                    .attr('style', 'font-size: 2em')
                      .attr('y', 50)
                      .attr('x', innerWidth / 2)
                      .attr('fill', 'black')
                      .text(xAxisLabel);

    let lineGenerator = d3.line()
                          .x(d => xScale(xValue(d)))
                          .y(d => yScale(yValue(d)))
                          .curve(d3.curveBasis);
    this.lineGraph.append("path")
    
                  .attr( "fill", "rgb(26,10,253)")
                  .attr( "stroke", "rgb(26,10,253)")
                  .attr( "stroke-width","3")
                  .attr( "stroke-linejoin","round")
                  .attr('d', lineGenerator(lineData))

    this.lineGraph.append('text')
                  .attr('font-size', '2.5em')
                  .attr('fill', '#635F5D')
                  .attr('y', -10)
                  .text(title);              
  }

  lineChartWithOption(){
    d3.select("svg").remove();
    //---------------Label--------------//
    let xAxisLabel = '<---------Time--------->',
    yAxisLabel = '<------Number of View---------->',
    title = 'A month of viewed item'
    //----------create svg-----------------//  
    this.svg = d3.select(this.hostElement)
                .append("svg")
                .attr('width',this.svgWidth)
                .attr('height',this.svgHeight)
                .attr("style", "background: rgb(211,211,211)")

    //------------Create Border and margin-------------//
    let margin = {top: 50, right: 30, bottom: 70, left: 60},
    innerWidth = this.svgWidth - margin.left - margin.right,
    innerHeight = this.svgHeight - margin.top - margin.bottom;

    let parseTime = d3.timeParse("%d-%b-%y") ,
    bisectDate = d3.bisector(function(d) { return d['date']; }).left;

    let xValue = d3.scaleTime().range([0, innerWidth]),
    yValue = d3.scaleLinear().range([innerHeight, 0]);

    let div = d3.select("body").append("div")   
                .attr("style", "position: absolute")                    
                .attr("style", "pointer-events: none")         
                .style("opacity", 0);
    let vertical = d3.select("body")
                      .append("line");

    let lineData = [];
    this.itemId = LINE_CHART_CONFIG.lineData.id;
    LINE_CHART_CONFIG.lineData.chart.forEach((d)=> {
                                                let objNew = {
                                                                "date": new Date('dd-MM-yyyy'), 
                                                                "value": 0
                                                              };         
                                                objNew.date = new Date(d.date);
                                                objNew.value = +d.value;
                                                lineData.push(objNew);
                                              })
    console.log("After operation data >> ",JSON.stringify(lineData)," size   >> ",lineData.length);

    let lineGenerate = d3.line()
              .x(function(d) { return xValue(d['date']); })
              .y(function(d) { return yValue(d['value']); });

    this.lineGraph = this.svg.append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    xValue.domain(d3.extent(lineData, function(d) { return new Date(d.date); })).nice();
    yValue.domain([d3.min(lineData, function(d) { return d.value; }), 
                      d3.max(lineData, function(d) { return d.value; })]).nice();

   //-------------Draw x-axis------------------// 
    this.lineGraph.append("g")
                  .attr("style","font: 10px sans-serif")
                  .attr("style","fill: none")
                  .attr("stroke-width"," 0.3px")
                  .attr("style","shape-rendering:crispEdges")
                  .attr("opacity"," 0.7")                                   
                  .attr("style","stroke: black")
                  .attr("transform", "translate(0," + innerHeight + ")")
                  .call(d3.axisBottom(xValue).tickSize(-innerHeight).tickPadding(15))
                  console.log("innerWidth >> ",innerWidth,"  innerHeight >> ",innerHeight);

    this.lineGraph.append('text')
                .attr('style', 'font-size: 1em')
                  .attr('y', (innerHeight +margin.left))
                  .attr('x', innerHeight-margin.left-margin.bottom)
                  .attr('fill', 'black')
                  .text(xAxisLabel);

    //------------Draw y-axis-------------------------//
    this.lineGraph.append("g")
              .attr("style","font: 10px sans-serif;")
              .attr("style","fill: none;")
              .attr("stroke-width","0.3px;")
              .attr("shape-rendering","crispEdges;")
              .attr("opacity"," 0.7")                                   
              .attr("style","stroke: black")
              .call(d3.axisLeft(yValue).tickSize(-innerWidth).ticks(10).tickPadding(5)) 

    this.lineGraph.append('text')
              .attr('style', 'font-size: 1em')
              //.attr('style', 'fill: #8E8883')
              .attr('y', -30)
              .attr('x', -innerHeight / 2)
              .attr('fill', 'black')
              .attr('transform', `rotate(-90)`)
              .attr('text-anchor', 'middle')
              .text(yAxisLabel) 
    
    //---------------Draw the line----------------//
    this.lineGraph.append("path")
                .datum(lineData)
                .attr("fill","none" )  
                .attr("stroke","#6F257F")  
                .attr("stroke-width"," 5px") 
                .attr("stroke-linejoin", "round")
                .attr("stroke-linecap", "round") 
                .attr("d", lineGenerate)
                
    this.lineGraph.selectAll("dot")    
                .data(lineData)         
                .enter().append("circle")                               
                .attr("r", 5)       
                .attr("cx", function(d) { return xValue(d['date']); })       
                .attr("cy", function(d) { return yValue(d['value']); }) 
                .attr("fill","#6F257F" )    
                .on("mouseover", function(d) {  
                    let userDt = new DatePipe("en-US").transform(d.date, 'dd-MMM-yyyy'),
                    lineHgt = d.value+ "px"
                               
                    div.transition()        
                        .duration(200)
                        .attr("r",7 )       
                        .style("opacity", .9);      
                    div.html((userDt) + "<br/>"  + d.value)  
                        .style("left", (d3.event.pageX+5) + "px").style("top", (d3.event.pageY - margin.right) + "px")                           
                        .style("position", "absolute").style("width","70px").style("height","28px")                                                       
                        .style("padding"," 4px").style("font"," 12px sans-serif").style("background","black")                                
                        .style("color","white").style("border", "0px").style("border-radius", " 8px")                      
                        .style("pointer-events", " none");  
                                                
                    vertical= d3.select("body")
                            .append("line").style("position", "absolute")
                            .style("z-index", "19")
                            .style("width", "4px")
                            .style("height", "404px")
                            //.attr("stroke","#6F257F")  
                            //.style("bottom", "30px")
                            .style("background", "#6F257F"); //rgb(255,0,0)
                      
                    console.log("d3.event.pageX >> ",d3.event.pageX,"   ",lineHgt);
                    vertical.style("left", d3.event.pageX + "px") 
                            .style("top",  d3.event.pageY + "px")
                            //.style("position", "absolute")
                    })                  
                .on("mouseout", function(d) {       
                    div.transition()        
                        .duration(500)      
                        .style("opacity", 0);
                    vertical.style("visibility", "hidden").style("opacity", 0);       
                });

    this.lineGraph.append('text')
                  .attr('font-size', '2.5em')
                  .attr('fill', '#635F5D')
                  .attr('y', -10)
                  .text(title); 

    //this.lineGraph

    //-----------MouseOver-----------//
      let focus = this.lineGraph.append("g")
                        .attr("fill","white")
                        .attr("style", "stroke: #6F257F;")
                        .attr("style", "stroke-width: 5px;")
                        .style("display", "none");
                        
      focus.append("line")
            .attr("style", "stroke: #6F257F;")
            .attr("style", "stroke-width: 2px;")
            .attr("style", "stroke-dasharray: 3,3;")
            
            
            .attr("y1", 0)
            .attr("y2", innerHeight);

    focus.append("line")
            .attr("style", "stroke: #6F257F;")
            .attr("style", "stroke-width: 2px;")
            .attr("style", "stroke-dasharray: 3,3;")
            .attr("x1", innerWidth)
            .attr("x2", innerWidth);

    focus.append("circle")
            .attr("r", 7.5);

    focus.append("text")
          .attr("x", 15)
          .attr("dy", ".31em");

    /*this.svg.append("rect")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .attr("fill","none")
        .attr("style", "pointer-events: all;")
        
        .attr("width", innerWidth)
        .attr("height", innerHeight)
        .on("mouseover", function() { focus.style("display", null); })
        .on("mouseout", function() { focus.style("display", "none"); })
        .on("mousemove", ()=> {
          console.log("d3.mouse(this)[0] >> ",d3.mouse(this)[0])
        });*/

  }
}
