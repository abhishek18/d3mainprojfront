import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekMonthLineChartComponent } from './week-month-line-chart.component';

describe('WeekMonthLineChartComponent', () => {
  let component: WeekMonthLineChartComponent;
  let fixture: ComponentFixture<WeekMonthLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeekMonthLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekMonthLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
