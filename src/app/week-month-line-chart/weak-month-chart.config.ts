const LINE_CHART_CONFIG = {
    lineData : {
        "id": "bandhej-printed-crepe-saree-in-maroon-sjn4921",
        "chart": [
          {
            "date": "2018-06-07",
            "value": 4
          },
          {
            "date": "2018-06-08",
            "value": 1
          },
          {
            "date": "2018-06-09",
            "value": 6
          },
          {
            "date": "2018-06-10",
            "value": 8
          },
          {
            "date": "2018-06-11",
            "value": 9
          },
          {
            "date": "2018-06-12",
            "value": 7
          },
          {
            "date": "2018-06-13",
            "value": 1
          },
          {
            "date": "2018-06-14",
            "value": 4
          },
          {
            "date": "2018-06-15",
            "value": 3
          },
          {
            "date": "2018-06-16",
            "value": 4
          },
          {
            "date": "2018-06-17",
            "value": 2
          },
          {
            "date": "2018-06-18",
            "value": 2
          },
          {
            "date": "2018-06-20",
            "value": 6
          },
          {
            "date": "2018-06-21",
            "value": 3
          },
          {
            "date": "2018-06-22",
            "value": 7
          },
          {
            "date": "2018-06-23",
            "value": 5
          },
          {
            "date": "2018-06-24",
            "value": 4
          },
          {
            "date": "2018-06-25",
            "value": 7
          },
          {
            "date": "2018-06-26",
            "value": 4
          },
          {
            "date": "2018-06-28",
            "value": 4
          },
          {
            "date": "2018-06-29",
            "value": 5
          },
          {
            "date": "2018-06-30",
            "value": 7
          }
        ]
      }
}

export default LINE_CHART_CONFIG;    